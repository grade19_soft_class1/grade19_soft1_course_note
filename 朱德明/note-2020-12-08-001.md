### 又双叒叕学到了一个新词👉：白莲花
# 续上5日所讲内容的后续
## 局部作用域(由于JavaScript的变量作用域实际上是函数内部，我们在for循环等语句块中是无法定义具有局部作用域的变量的)
```
例如：
'use strict'


function foo(){
    for(var i =0;i<10;i++){

    }
    console.log(i);
}
foo();
//你们认为输出的结果是多少呢？0？9？或者10？其实结果为10.

//为了解决块级作用域，ES6引入了新的关键字let，用let替代var可以申明一个块级作用域的变量：
'use strict';

function foo() {
    var sum = 0;
    for (let i=0; i<100; i++) {
        sum += i;
    }
    console.log(sum);
}
foo();
```

## 常量
```
由于var和let申明的是变量，如果要申明一个常量，在ES6之前是不行的，我们通常用全部大写的变量来表示“这是一个常量，不要修改它的值
例如：
我们申明一个变量：
var PAI =3.1415926;

ES6标准引入了新的关键字const来定义常量，const与let都具有块级作用域
例如：
'use strict';

const PAI = 3.1415926;
PAI = 3; // 无法改变他的值，打印出来还是3.1415926
console.log(PAI)

```

## 解构赋值(可以同时对一组变量进行赋值)
```
我们首先来看一下什么是解构赋值
就是把一个数组的元素分别赋值给几个变量

例如传统方法：
var arr=['我','是','马','保','国'];
var a=arr[0];
var b=arr[1];
var c=arr[2];
var d=arr[3];
var e=arr[4];
console.log(a);
console.log(b);
console.log(c);
console.log(d);
console.log(e);
//输出的就是我
//js.js:12 是
//js.js:13 马
//js.js:14 保
//js.js:15 国
例如：

'use strict';
var [a,b,c,d,e,f,g]=['我','是','马','保','国','✌','( •̀ ω •́ )y']
console.log(a,b,c,d,e,f,g);
//输出的便是      我 是 马 保 国 ✌ ( •̀ ω •́ )y
//注意，对数组元素进行解构赋值时，多个变量要用[...]括起来。
```
### 解构赋值还可以忽略某些元素！！！
```
例如：
'use strict';

var [,,,,,,g]=['我','是','马','保','国','✌','( •̀ ω •́ )y']

console.log(g);
//忽略前6个元素，只对g赋值第7个元素
```

### 如果需要从一个对象中取出若干属性，也可以使用解构赋值，便于快速获取对象的指定属性
```
例如：
'use strict';

var people={
    name:'马保国',
    age:69,
    school:'混元形意太极拳',
    job:'掌门人'
}

//name age school 分别为对应属性
var {name,age,school,} =people;

console.log('name:'+name+',age:'+age+',school:'+school);
//输出结果为 name:马保国,age:69,school:混元形意太极拳
```
### 对一个对象进行解构赋值时，同样可以直接对嵌套的对象属性进行赋值，只要保证对应的层次是一致的
```
例如：
'use strict';

var people={
    name:'马保国',
    age:69,
    school:'混元形意太极拳',
    job:'掌门人',
    address:{
        city : 'beijing',
        kill :'闪电五连鞭'
    }
};

var {name,school,address:{kill}}=people;

console.log({name ,school ,address:{kill}});
```

### 使用解构赋值对对象属性进行赋值时，如果对应的属性不存在，变量将被赋值为undefined，这和引用一个不存在的属性获得undefined是一致的