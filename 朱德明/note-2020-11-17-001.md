# 今天所讲内容

## 一、如何避免提交git中合并过程中出现冲突？
  1.原本仓库的东西不能去修改它，否则就会出现合并时有冲突的情况。


## 二、如何做好课堂笔记
  1.# 为一级标题 ## 为二级标题


## 三、Lambda表达式
```
  Func<int, int, int> a1 = (a, b) => a + b;

            Func<int, int> a2 = x => x * 5;

            Func<int, int, int> f1 = Add;

Lambda表达式可以有多个参数、一个参数，或者没有参数。其参数类型可以隐式或者显式。示例代码如下：
    (x, y) => x * y         //多参数，隐式类型=> 表达式  
     x => x * 5              //单参数， 隐式类型=>表达式  
     x => { return x * 5; }      //单参数，隐式类型=>语句块  
    (int x) => x * 5            //单参数，显式类型=>表达式  
    (int x) => { return x * 5; }      //单参数，显式类型=>语句块  
    () => Console.WriteLine()   //无参数 

    //示例  
     static void Main(string[] args)
        {
            Func<int, int, int> a1 = (a, b) => a + b;

            Func<int, int> a2 = x => x * 5;

            Func<int, int, int> f1 = Add;

            //  Action<int, int> a2 = Add;
            var d = a2(10);
            var t = a1(1, 2);

            var r = f1(3, 4);
            Console.WriteLine(d);
            Console.WriteLine(t);
            Console.WriteLine(r);
            Console.ReadLine();

            Program program = new Program(); program.test(); Console.Read();
        }
        public delegate void SaySomething(string name);
        public void SayHello(string name) { Console.WriteLine("Hello," + name + "!"); }

        public void SayNiceToMeetYou(string name) { Console.WriteLine("Nice to meet you," + name + "!"); }

        public event SaySomething come;
        public void test() { come += SayHello; come += SayNiceToMeetYou; come("张三"); }
        
        static int Add(int a ,int b)
        {
            return a * b+b;
        }
        static void BBc(int a,int b)
        {

        }
```


    