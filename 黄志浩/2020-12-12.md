# 一，reduce
再看reduce的用法。Array的reduce()把一个函数作用在这个Array的[x1, x2, x3...]上，这个函数必须接收两个参数，reduce()把结果继续和序列的下一个元素做累积计算，其效果就是：
```
[x1, x2, x3, x4].reduce(f) = f(f(f(x1, x2), x3), x4)
```
比方说对一个Array求和，就可以用reduce实现：
```
'use strict'

var arr=[1,3,5,7,9];

function fn(x,y){
    return x+y
}
console.log(arr.reduce(fn));  // 25
``` 
## 练习：利用reduce()求积：
### 练习：利用reduce()求积：
```
'use strict';

function product(arr) {
    return 0;

}

// 测试:
if (product([1, 2, 3, 4]) === 24 && product([0, 1, 2]) === 0 && product([99, 88, 77, 66]) === 44274384) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}
 ```
![图片错误](./img/ppp1.jpg)

要把[1, 3, 5, 7, 9]变换成整数13579，reduce()也能派上用场：
```
var arr = [1, 3, 5, 7, 9];
function fn ( x , y ){
    return x * 10 + y ;
}
let r=arr.reduce(fn);
console.log(r);   // 13579
```
如果我们继续改进这个例子，想办法把一个字符串13579先变成Array——[1, 3, 5, 7, 9]，再利用reduce()就可以写出一个把字符串转换为Number的函数。






# 好难！！！！！
### 练习：不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：
```
'use strict';

function string2int(s) {
    return 0;

}

// 测试:
if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        console.log('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        console.log('请勿使用Number()!');
    } else {
        console.log('测试通过!');
    }
}
else {
    console.log('测试失败!');
}
```
![图片错误](./img/ppp4.jpg)

### 练习请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。

练习
请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。
```
'use strict';

function normalize(arr) {
    return [];

}

// 测试:
if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}
``` 
![图片错误](./img/ppp3.jpg)
### 小明希望利用map()把字符串变成整数，他写的代码很简洁：
```
'use strict';

var arr = ['1', '2', '3'];
var r;
r = arr.map(Number);

console.log(r);
```
![图片错误](./img/ppp2.jpg)
结果竟然是1, NaN, NaN，小明百思不得其解，请帮他找到原因并修正代码。

提示：参考Array.prototype.map()的文档。



# 二，filter
filter也是一个常用的操作，它用于把Array的某些元素过滤掉，然后返回剩下的元素。

和map()类似，Array的filter()也接收一个函数。和map()不同的是，filter()把传入的函数依次作用于每个元素，然后根据返回值是true还是false决定保留还是丢弃该元素。

例如，在一个Array中，删掉偶数，只保留奇数，可以这么写：
```
var arr = [1, 2, 4, 5, 6, 9, 10, 15];
var r = arr.filter(function (x) {
    return x % 2 !== 0;
});
r; // [1, 5, 9, 15]
```
把一个Array中的空字符串删掉，可以这么写：
```
var arr = ['A', '', 'B', null, undefined, 'C', '  '];
var r = arr.filter(function (s) {
    return s && s.trim(); // 注意：IE9以下的版本没有trim()方法
});
r; // ['A', 'B', 'C']
```
可见用filter()这个高阶函数，关键在于正确实现一个“筛选”函数。
定义和用法
trim() 方法用于删除字符串的头尾空白符，空白符包括：空格、制表符 tab、换行符等其他空白符等。

trim() 方法不会改变原始字符串。

trim() 方法不适用于 null, undefined, Number 类型。


# 回调函数
filter()接收的回调函数，其实可以有多个参数。通常我们仅使用第一个参数，表示Array的某个元素。回调函数还可以接收另外两个参数，表示元素的位置和数组本身：
```
var arr=[1,2,6,9,5,8,10];
var r=arr.filter(function(x){
    return x%2!==0;
});
console.log(r);   // (3) [1, 9, 5]
```
利用filter，可以巧妙地去除Array的重复元素：
```
'use strict';

var
    r,
    arr = ['apple', 'strawberry', 'banana', 'pear', 'apple', 'orange', 'orange', 'strawberry'];
r = arr.filter(function (element, index, self) {
    return self.indexOf(element) === index;
});

console.log(r.toString());
``` 
去除重复元素依靠的是indexOf总是返回第一个元素的位置，后续的重复元素位置与indexOf返回的位置不相等，因此被filter滤掉了。

练习
请尝试用filter()筛选出素数：
```
'use strict';

function get_primes(arr) {
    return [];

}

// 测试:
var
    x,
    r,
    arr = [];
for (x = 1; x < 100; x++) {
    arr.push(x);
}
r = get_primes(arr);
if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + r.toString());
}
``` 
![图片错误](./img/ppp5.jpg)


# sort
## 排序算法



