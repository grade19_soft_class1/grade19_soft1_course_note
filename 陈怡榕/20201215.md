# 2020年12月15日  周三  阴☁

今天没有什么新鲜事，平平无奇的一天


# 一. every
## 1. 概念
+ every() 方法用于检测数组所有元素是否都符合指定条件（通过函数提供）。

+ every() 方法使用指定函数检测数组中的所有元素：

    如果数组中检测到有一个元素不满足，则整个表达式返回 false ，且剩余的元素不会再进行检测。

    如果所有元素都满足条件，则返回 true。

+ every() 不会对空数组进行检测。

    every() 不会改变原始数组。

## 2. 例子 
+ 所有元素是否都大于等于 18
```
    var ages = [32, 33, 16, 40];

    console.log(ages.every(function (age) {
        return age >= 18;
    }));   // false
```

+ 每个元素是否都为小写
```
    var arr = ['Apple', 'pear', 'orange'];

    console.log(arr.every(function (s) {
        return s.toLowerCase() === s;
}));   // false
```

+ 每个元素是否都大于3
```
    var arr = [ 4, 5, 6 ]; 
    
    console.log( arr.every( function( item ){ 
        return item > 3; 
    }));   // true
```


# 二. find
## 1. 概念

+ find() 方法返回通过测试（函数内判断）的数组的第一个元素的值。

+ find() 方法为数组中的每个元素都调用一次函数执行：

    当数组中的元素在测试条件时返回 true 时, find() 返回符合条件的元素，之后的值不会再调用执行函数。

    如果没有符合条件的元素返回 undefined
+ find() 对于空数组，函数是不会执行的。

    find() 并没有改变数组的原始值。

## 2.例子

+ 第一个大于3的元素
```
    var arr = [1 ,2 , 3 , 4, 5, 6 ]; 
    
    console.log( arr.find( function( item ){ 
        return item > 3; 
    }));    // 4
```

+ 第一个长度大于3的元素
```
    var arr = [122 ,233 , 32, 4 ]; 
    
    console.log( arr.find( function( item ){ 
        return item.length > 3; 
    }));    // undefined （没有长度大于3的）
```


# 三. findIndex
## 1. 概念
+ findIndex() 方法返回传入一个测试条件（函数）符合条件的数组第一个元素位置。

+ findIndex() 方法为数组中的每个元素都调用一次函数执行：

    当数组中的元素在测试条件时返回 true 时, findIndex() 返回符合条件的元素的索引位置，之后的值不会再调用执行函数。

    如果没有符合条件的元素返回 -1

+ findIndex() 对于空数组，函数是不会执行的。

    findIndex() 并没有改变数组的原始值。



## 2. 例子

+ 第一个大于40的元素的索引
```
    var arr = [122 ,233 , 32, 4 ]; 
    
    console.log( arr.findIndex( function( item ){ 
        return item> 40; 
    })); 
```

+ 第一个长度大于3的元素
```
    var arr = [122, 233, 32, 4];

    console.log(arr.findIndex(function (item) {
    return item.length > 3;
    }));    // -1
```

# 四. forEach
## 1. 概念
+ forEach() 方法用于调用数组的每个元素，并将元素传递给回调函数。

+ forEach() 对于空数组是不会执行回调函数的。

+ forEach()常用于遍历数组，因此，传入的函数不需要返回值


## 2. 例子

+ 依次打印每个元素
```
    var array = ['a', 'b', 'c'];

    array.forEach(function(n) {
    console.log(n);
    });
```

