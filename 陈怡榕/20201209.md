# 2020年12月09日  周三  晴🌞

## 一. 方法

+ 在一个对象中绑定函数，称为这个对象的方法。


```
    var person = {
        firstName: "王",
        lastName: "燕",
        id: 5566,
        fullName: function () {
            return this.firstName + this.lastName;
        }
    };
```

## 二. this关键词
+ 在方法中，this 表示该方法所属的对象。

    在一个方法内部，this是一个特殊变量，它始终指向当前对象，也就是person这个变量。所以，this.firstName可以拿到person的birth属性

```
    function getname() {
        return this.firstName + this.lastName;
    }

    var person = {
        firstName: "王",
        lastName: "燕",
        id: 5566,
        fullName: getname
    };

    console.log(person.fullName());
```

## 三. 要保证this指向正确，必须用obj.xxx()的形式调用！
1. 非严格模式，this 表示全局对象

    如果单独调用函数，比如getname()，此时，该函数的this指向全局对象，也就是window。
```
    console.log(getname());
    // NaN
```


2. 在函数中，在严格模式下，this 是未定义的(undefined)  

    此时，在strict模式下让函数的this指向undefined
```
    'use strict';

    var a=person.fullName;
    console.log(a()); 
    // Cannot read property 'firstName' of undefined
```

3. 嵌套函数

+ 此时this指针只在fullName方法的函数内指向person，在函数内部定义的函数，this又指向undefined了！

    （在非strict模式下，它重新指向全局对象window！）
```
    var person = {
    firstName: "王",
    lastName: "燕",
    id: 5566,
    fullName: function () {
        function getname() {
        return this.firstName + this.lastName;
        }
        return getname();
    }
    };

    console.log(person.fullName());
    // Cannot read property 'firstName' of undefined
```

+ 解决

    __在方法内部一开始就捕获this  ▶   var that = this;__
```
var person = {
  firstName: "王",
  lastName: "燕",
  id: 5566,
  fullName: function () {
    var that = this;  // 在方法内部一开始就捕获this
    function getname() {
      return that.firstName + that.lastName;  // 用that而不是this
    }
    return getname();
  }
};

console.log(person.fullName());
```

## 四. 函数 apply() 与 call

1. 可以控制this的指向

+ apply方法接受数组形式的参数。

    它接收两个参数，第一个参数就是需要绑定的this变量，第二个参数是Array，表示函数本身的参数
```
    getname.apply(person, []);
```


+ call() 方法分别接受参数。

    它可以用来调用所有者对象作为参数的方法。通过 call()，您能够使用属于另一个对象的方法。
```
    var person = {
    fullName: function() {
        return this.firstName + this.lastName;
    }
    }
    var person1 = {
    firstName: "王",
    lastName: "燕",
    }

    console.log(person.fullName.call(person1)); 
```


2. 区别
+ apply()把参数打包成Array再传入；
```
    Math.max.apply(null, [3, 5, 4]); // 5
```

+ call()把参数按顺序传入。
```
    Math.max.call(null, 3, 5, 4); // 5
```
+ 第一个参数（null）无关紧要
```
    console.log(Math.max.apply(Math, [1,2,3])) ; // 3

    console.log(Math.max.call('', 3, 5, 4)) ; // 5
```

## 五. 引用
+ https://www.w3school.com.cn/js/js_this.asp