    

# 一·什么是LINQ


1. LINQ是一组语言特性和API，使得你可以使用统一的方式编写各种查询。用于保存和检索来自不同数据源的数据，从而消除了编程语言和数据库之间的不匹配，以及为不同类型的数据源提供单个查询接口。
 LINQ总是使用对象，因此你可以使用相同的查询语法来查询和转换XML、对象集合、SQL数据库、ADO.NET数据集以及任何其他可用的LINQ提供程序格式的数据。


2. 把复杂的查询进行拆分

3. LINQ主要包含以下三部分：

* LINQ to Objects      主要负责对象的查询。

* LINQ to XML           主要负责XML的查询。

* LINQ to ADO.NET   主要负责数据库的查询。

    LINQ to SQL     

    LINQ to DataSet  

    LINQ to Entities

# 二·LINQ例子
## 1.使用for循环 => 麻烦,而且不可维护和可读

```
    //输出偶数
    int[] ints = { 5, 2, 0, 66, 4, 32, 7, 1 };
             
    List<int> list = new List<int>();
             
    foreach (int i in ints)
        {
               
            if (i % 2 == 0)
            {
                list.Add(i);
            }
        }          

        Console.WriteLine(string.Join(",", list));
         Console.ReadKey();
    }

```

## 2·委托 => 不必使用for循环来查询不同条件的数组。

```

    // 定义委托
    delegate bool FindEven(int item);

    class IntExtension
    {
        public static int[] where(int[] array, FindEven dele)
        {
            int[] result = new int[3];
            int i = 0;
            foreach (int item in array)
            {
                if (dele(item))
                {
                    result[i] = item;
                    i++;
                }
            }

            return result;
        }
    }

   
    class Program
    {
        static void Main(string[] args)
        {
            // 查询偶数
            int[] ints = { 5, 2, 0, 66, 4, 32, 7, 1 };
 
            //delegate(int item){return item % 2 == 0;}表示委托的实现
            List<int> list = IntExtension.where(ints, delegate (int item)
            {
                return item % 2 == 0;
            }).ToList();
           
            Console.WriteLine(string.Join(",", list));
            Console.ReadKey();
        }
    }

```

## 3·使用LINQ和Lambda表达式

```

    class Test
        {

            static void Main(string[] args)
            {
            
                int[] ints = { 5, 2, 0, 66, 4, 32, 7, 1 };

                // 使用LINQ和Lambda表达式查询数组中的偶数
                int[] intEvens = ints.Where(p => p % 2 == 0).ToArray();

                Console.WriteLine("偶数：" + string.Join(",", intEvens));
                Console.ReadKey();
            }

        }

```

# 三·更多


## https://baike.baidu.com/item/LINQ/4462670?fr=aladdin#4&qq-pf-to=pcqq.c2c
## https://docs.microsoft.com/zh-cn/dotnet/csharp/programming-guide/concepts/linq/introduction-to-linq-queries
## https://www.jb51.net/article/155419.htm
## https://www.cnblogs.com/liqingwen/p/5832322.html
## https://blog.csdn.net/zmh458/article/details/78935181



# 四·this 拓展方法

    扩展方法使您能够向现有类型“添加”方法，而无需创建新的派生类型、重新编译或以其他方式修改原始类型。扩展方法是一种特殊的静态方法，但可以像扩展类型上的实例方法一样进行调用。

    C#扩展方法第一个参数指定该方法作用于哪个类型，并且该参数以 this 修饰符为前缀。



```
    static void Main(string[] args)
        {
            string str = "测试";

            //注意调用扩展方法,必须用对象来调用
            string Newstr = str.Add().Bcc();

            Console.WriteLine(Newstr);
            Console.ReadKey();
        }


    //声明扩展方法
    //扩展方法必须是静态的
    //this 必须有，string表示我要扩展的类型，stringName表示对象名
    //this和扩展的类型必不可少，对象名可以自己随意取,如果需要传递参数再增加一个变量即可
    public static string Add(this string stringName)
    {
        return stringName + "a";
    }

    public static string Bcc(this string stringName)
    {
        return stringName + "b";
    }

```




    




