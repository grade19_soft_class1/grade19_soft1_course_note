# 今年倒计时还有一天呐

## jQuery

1. jQuery

jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers. With a combination of versatility and extensibility, jQuery has changed the way that millions of people write JavaScript.

jQuery 是一个快速、轻量级、可扩展的 js 库，它提供了易于使用的跨浏览器的API，使得

访问dom，时间处理、动画效果、ajax请求变得简单。

简化了JS对DOM的操作

2. 选择器

选择器是jQuery的核心。一个选择器写出来类似$('#dom-id')。


3. 按ID查找

什么是jQuery对象？jQuery对象类似数组，它的每个元素都是一个引用了DOM节点的对象。



```

    <table class="green">
        <tbody>
            <tr>五月天</tr>
            <tr>跨年演唱会</tr>
        </tbody>
        <tbody>
            <tr>May</tr>
            <tr>Day</tr>
        </tbody>
    </table>


<script src="./jq/jquery-3.5.1.js"> </script>


<script>


//jQuery与DOM之间的转换
//jQuery对象
var p =$('.green');
//获取第一个DOM元素
var pp = p.get(0);
//重新把DOM包装为jQuery对象
var another = $('pp');


console.log(p);
console.log(pp);
console.log(another);

</script>


```

4. 按class查找 当很多节点有多个class 可以同时查找包含....的节点


5. 按属性查找



```

<div name ='icon-emailwith'>
     邮箱
</div>

<div name='icon-password'>
     密码
</div>


//找出以with结尾的DOM
var p = $('[name$=with]');
//找出以icon-开头的DOM
var pp =$('[name^=icon-]');
console.log(p);
console.log(pp);



```

6. 组合查找

```
var emailInput = $('input[name=email]'); // 不会找出<div name="email">

同样的，根据tag和class来组合查找也很常见：

var tr = $('tr.red'); // 找出<tr class="red ...">...</tr>

```



7. 多项选择器

```

$('p,div'); // 把<p>和<div>都选出来
$('p.red,p.green'); // 把<p class="red">和<p class="green">都选出来

```


练习

```
        // 使用jQuery选择器分别选出指定元素：

        // + 仅选择JavaScript
            var p = $('#para-1');
            console.log(p)

        //     + 仅选择Erlang
            var p1 = $('#para-2');
            console.log(p1);

        //     + 选择JavaScript和Erlang
            var p2 = $('.color-red');
            console.log(p2);
            
        //     + 选择所有编程语言
            var p3 = $('[class^=color-]');
            console.log(p3);

        //     + 选择名字input
            var p4 = $("input[name= name]");
            console.log(p4);

        //     + 选择邮件和名字input
            var p5 = $('input[name =name],input[name = email]');
            console.log(p5);

```





### 层级选择器

1. 层级选择器

层级选择器比单个选择器好处在 它缩小了选择范围，首先要定位到父节点，才能选择相应的子节点,这样避免页面其他不相关元素

```
    <div class="testing">
        <form class='upload'>
            <p><label>名字: <input name="name"></label></p>
            <p><label>密码: <input name="name"></label></p>

        </form>
    </div>


    <script src="./jq/jquery-3.5.1.js"> </script>


    <script>

        var p = $('.lang-javascript');
        console.log(p);

        var p1 = $('ul.lang li.lang-javascript');
        console.log(p1);

        var p2 = $('ul.lang li');
        console.log(p2);

        // var p3 = $('form[name=upload] input');
        // console.log(p3);

        // var p4 = $('input[name=name]');
        // console.log(p4);

        var p5 = $('form.upload p input');
        console.log(p5)


    </script>


```
2. 子选择器

子选择器`$('parent>child')`类似层级选择器，但是限定了层级关系必须是父子关系，就是`<child>`节点必须是`<parent>`节点的直属子节点。还是以上面的例子：
```
$('ul.lang>li.lang-javascript'); // 可以选出[<li class="lang-javascript">JavaScript</li>]
$('div.testing>li.lang-javascript'); // [], 无法选出，因为<div>和<li>不构成父子关系
```


3. 过滤器

过滤器一般不单独使用，它通常附加在选择器上，帮助我们更精确地定位元素。观察过滤器的效果：


4. 表单相关

```
:input：可以选择<input>，<textarea>，<select>和<button>；

:file：可以选择<input type="file">，和input[type=file]一样；

:checkbox：可以选择复选框，和input[type=checkbox]一样；

:radio：可以选择单选框，和input[type=radio]一样；

:focus：可以选择当前输入焦点的元素，例如把光标放到一个<input>上，用$('input:focus')就可以选出；

:checked：选择当前勾上的单选框和复选框，用这个选择器可以立刻获得用户选择的项目，如$('input[type=radio]:checked')；

:enabled：可以选择可以正常输入的<input>、<select> 等，也就是没有灰掉的输入；

:disabled：和:enabled正好相反，选择那些不能输入的。


```

练习

```

     // 分别选择所有语言，所有动态语言，所有静态语言，JavaScript，Lua，C等:

        //所有语言
        var p = $('.test-lang')
        console.log(p);
        var p1 =$('[class^=lang-]');
        console.log(p1);
    
        //所有动态语言
        var p2= $('.test-selector ul.test-lang ');
        console.log(p2);

        //所有静态语言
        var p3= $('.test-selector ol.test-lang');
        console.log(p3);

        //JavaScript
        var p4 = $('ul.test-lang li:first-child');
        console.log(p4);

        //Lua 多层选择方法
        var p5 = $('ul.test-lang li.lang-lua');
        console.log(p5);


```






#### 查找和过滤

1. 查找

```
    <ul class="lang">
        <li class="js dy">JavaScript</li>
        <li class="dy">Python</li>
        <li id="swift">Swift</li>
        <li class="dy">Scheme</li>
        <li name="haskell">Haskell</li>
    </ul>

    <script src="./jq/jquery-3.5.1.js"> </script>


    <script>

     //查找用find()

     //先查找到ul
     var p = $('.lang')
     //再去利用find()
     var p1 = p.find('.dy');
     console.log(p1);
    

     //如果从当前节点向上查找使用parent()方法

     //获得swift
     var ul = $('#swift');
     //获得节点ul
     var parent = ul.parent();
     //传入过滤器条件 当不符合时返回空jQuery
     var a = ul.parent('.lang')
     console.log(a);

    // 对于位于同一层级的节点，可以通过next()和prev()方法，例如：
  

```

2. 过滤

或者传入一个函数，要特别注意函数内部的this被绑定为DOM对象，不是jQuery对象：

map()方法把一个jQuery对象包含的若干DOM节点转化为其他对象：

此外，一个jQuery对象如果包含了不止一个DOM节点，first()、last()和slice()方法可以返回一个新的jQuery对象，把不需要的DOM节点去掉：









