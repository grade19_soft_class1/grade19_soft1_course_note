# 晚上预习一下JavaScript课件内容

1. 函数内部的this指向


```


      //解析器在调用函数每次都会向函数传递一个隐含的参数 这个参数就是this
      //this指向的是一个对象，这个对象称为函数执行的上下文对象
      //根据函数的调用方式不同,this会指向不同的对象
      //     1.以函数形式调用this永远都是window
      //     2.以方法的方式调用 this就是调用方法的那个对象


     var name = 'Luhan' ;  
     
    function f(){
        console.log(this.name);
    }
 
    //以函数形式调用,this就是window
    f(); 

    var o = {
        name : '鹿哥',
        sayName : f
    }

    var o1 = {
        name : '麋鹿',
        sayName : f
    }


    o1.sayName();




```

2. 改变函数内部指向call与apply方法

```



    //call()方法改变this的指向
    //1.可以调用函数
    //2.改变函数内this的指向
    //3.call的主要作用可以实现继承

    

    function f(a,b) {
        console.log(this);
        console.log(a+b);
    }

    var o ={
        name : 'luhna'

    }
    
    //指向o构造函数
    f.call(o,2,3);

    function father(uname,age){
        this.uname= uname;
        this.age =age ;

    }

    function son(uname,age){
        //调用父构造函数 将父构造函数中的this替换为子函数中的this 那么子函数就可以使用父构造函数的属性了
        father.call(this,uname,age);

    }

    var son = new son('luhan',19);

    console.log(son);



```

3. apply方法

```

     
     //1.apply改变函数this的指向
     //2.但是它的参数必须是数组(伪数组)
     //3.apply主要应用
     //Math.max()

     
      function f(arr){
          console.log(this);
          console.log(arr)   //pig
      }

      var o ={
          name :'luhan'

      }
     
      //将f()里的this指向对象o
      f.apply(o,['pig']);


      var arr =[1,53,645,3,90] ;
      //调用数学中Math.max方法 不需要改变对象可以写空null
      var max  = Math.max.apply(Math,arr);

      console.log(max); // 645
     


```


