# 2020-12-10 js笔记整理

## 装饰器

### apply()

> 动态改变函数的行为

实例：
```
'use strict';
var count = 0;
//原函数
function add(x){
    return x*x;
}
//将原函数保存
var newAdd=add;
//重新编写函数
add=function (){
    count +=1;
    return newAdd.apply(null,arguments);
}
//查看效果
console.log(count);// 0
console.log(add(4));
//这边要注意一个点就是我们在重新写函数的时候返回要使用apply（）否则我们输出的将是NaN
console.log(count);// 1
console.log(add(5));
console.log(count);// 2
```

## 高阶函数

* JS的函数其实都指向某个变量。既然变量可以指向函数，函数的参数能接收变量，那么一个函数就可以接收另一个函数作为参数，这种函数就称之为高阶函数。

    > 一个简单的高阶函数,这边需要注意的是我们的f将传入一个函数因为我们代码内部是将f作为函数来使用的
    ```
        function add(x, y, f) {
            return f(x) + f(y);
        }
    ```

实例：
```
'use strict';

function add(x,y,f){
    return f(x)+f(y);
}
function sum(x){
    return x+x;
}
let a=add(10,20,sum);
console.log(a);//60
//将sum函数传入add中调用也就是f（x）实际就是调用了我们的sum函数。
```

* 编写高阶函数，就是让函数的参数能够接收别的函数

## map()

> map(函数)，作为高阶函数，事实上它把运算规则抽象了，因此，我们不但可以计算简单的f(x)=x+x，还可以计算任意复杂的函数

* 举例说明，比如我们有一个函数f(x)=x+x，要把这个函数作用在一个数组[1, 2, 3, 4, 5,6]上
```
'use strict';
let arr=[1,2,3,4,5,6]
console.log(arr);
function sum(x){
    return x+x;
}
let newArr=arr.map(sum);

console.log(newArr);
```

* 把Array的所有数字转为字符串或者把字符串转数字
```
'use strict';

let arr1=['1','2','3','4'];
console.log(arr1);
let newArr1=arr1.map(Number);//将字符串转换数字
console.log(newArr1);


let arr=[1,2,3,4,5,6]
console.log(arr);
let newarr2=arr.map(String);//将数字转换字符串
console.log(newarr2);
```

* 这样就不用使用循环的方式来实现

### 对这边说一下，输出的时候单个字符串的数字是黑色的，数字是蓝色的，而数组的字符串是带引号的红色。