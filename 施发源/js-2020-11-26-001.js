'use strict';
//1.查找item,3在arr的位置
console.log("第1题");
var arr=[1,2,3,4];
console.log(arr);//[1,2,3,4]
var item=3;
console.log(arr.indexOf(item));//2
//2.数组 arr 中所有元素的总和 
console.log("第2题");
var arr=[1,2,3,4];
console.log(arr);//[1,2,3,4]
var sum=0;
for(var i=0;i<arr.length;i++){
    sum=sum+arr[i]
}
console.log(sum);//10
//3.移除数组 arr 中的所有值与 item,2 相等的元素。不要直接修改数组 arr，结果返回新的数组
console.log("第3题");
var arr=[1,2,3,4,2];
console.log(arr);//[1, 2, 3, 4, 2]
var item=2;
var b=[];
for(var i=0;i<arr.length;i++){
    if(item != arr[i]){
        b.push(arr[i]);
    }
}
console.log(b);//[1, 3, 4]
//4.移除数组 arr 中的所有值与 item,2 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
console.log("第4题");
var item=2;
var arr=[1, 2, 2, 3, 4, 2, 2];
console.log(arr);//[1, 2, 2, 3, 4, 2, 2]
for(var i=0;i<arr.length;i++){
    arr.splice(arr.indexOf(item),1)
}
console.log(arr);//[1, 3, 4]
//5.在数组 arr 末尾添加元素 item,10。不要直接修改数组 arr，结果返回新的数组
console.log("第5题");
var item=10;
var arr=[1,2,3,4];
console.log(arr);//[1, 2, 3, 4]
var b=arr.slice();
b.push(item);
console.log(arr);//[1, 2, 3, 4]
console.log(b);//[1, 2, 3, 4, 10]
//6.删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
console.log("第6题");
var arr=[1,2,3,4];
console.log(arr);//[1, 2, 3, 4]
var b=arr.slice();
b.pop();
console.log(arr);//[1, 2, 3, 4]
console.log(b);//[1, 2, 3]
//7.在数组 arr 开头添加元素 item,10。不要直接修改数组 arr，结果返回新的数组
console.log("第7题");
var arr=[1,2,3,4];
console.log(arr);//[1, 2, 3, 4]
var b=arr.slice();
var item=10;
b.unshift(item);
console.log(arr);//[1, 2, 3, 4]
console.log(b);//[10, 1, 2, 3, 4]
//8. 删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
console.log("第8题");
var arr=[1,2,3,4];
console.log(arr);//[1, 2, 3, 4]
var b=arr.slice();
b.shift();
console.log(arr);//[1, 2, 3, 4]
console.log(b);//[2, 3, 4]
//9.合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
console.log("第9题");
var arr1=[1, 2, 3, 4];
var arr2=['a', 'b', 'c', 1];
var b=arr1.concat(arr2);
console.log(arr1);//[1, 2, 3, 4]
console.log(arr2);//['a', 'b', 'c', 1]
console.log(b);//[1, 2, 3, 4, 'a', 'b', 'c', 1]
//10.在数组 arr 的 index,2 处添加元素 item,'z'。不要直接修改数组 arr，结果返回新的数组
console.log("第10题");
var index=2;
var item='z';
var arr=[1,2,3,4];
console.log(arr);//[1, 2, 3, 4]
var b=arr.slice();
b.splice(index,0,item);
console.log(arr);//[1, 2, 3, 4]
console.log(b);//[1, 2, 'z', 3, 4]
//11.统计数组 arr 中值等于 item,4 的元素出现的次数
console.log("第11题");
var item=4;
var arr=[1, 2, 4, 4, 3, 4, 3];
console.log(arr);//[1, 2, 4, 4, 3, 4, 3]
var sum=0;
for(var i=0;i<arr.length;i++){
    if(arr[i]===item){
        sum++;
    }
}
console.log(sum);//3
//12.找出数组 arr 中重复出现过的元素
console.log("第12题");
var arr=[1, 2, 4, 4, 3, 3, 1, 5, 3];
console.log(arr);//[1, 2, 4, 4, 3, 3, 1, 5, 3]
var b=[];
for(var i=0;i<arr.length;i++){
    for(var j=i;j<arr.length;j++){
        if(i!=j){
            if(arr[i]===arr[j]){
                if(b.indexOf(arr[i])===-1){
                    b.push(arr[i]);
                }
                break;
            }
        }
    }
}
console.log(b.sort());//[1, 3, 4]
//13. 为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
console.log("第13题");
var arr=[1, 2, 3, 4];
console.log(arr);//[1, 2, 3, 4]
var b=[];
for(var i=0;i<arr.length;i++){
    b.push(arr[i]*arr[i])
}
console.log(b);//[1, 4, 9, 16]
//14.在数组 arr 中，查找值与 item,'a' 相等的元素出现的所有位置
console.log("第14题");
var item='a';
var arrs='abcdefabc';
console.log(arrs);//'abcdefabc'
var arr=[];
for(var i=0;i<arrs.length;i++){
    arr.push(arrs[i]);
}
console.log(arr);//["a", "b", "c", "d", "e", "f", "a", "b", "c"]
var b=[];
for(var i=0;i<arr.length;i++){
    if(arr[i]===item){
        b.push(i);
    }
}
console.log(b);//[0, 6]