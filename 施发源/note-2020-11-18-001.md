# 2020-11-18 c#笔记整理

## 一、拓展方法

1. *扩展方法能使你能够向现有类型添加“添加”方法，而无需创建新的派生类型,重新编译或以其他方式修改原始类型。*

2. *扩展方法是一种特殊的静态方法，但可以像扩展类型上的实例方法一样调用。*

* 必须是静态类，扩展方法也为静态方法

* 此方法的第一个参数指定方法所操作的类型；此参数前面必须加上 this 修饰符

* 在调用代码中，如何不再同一个命名空间，需要添加 using 指令，导入需要调用的扩展方法所在的命名空间

* 需要注意的是，第一个this标记的参数并非是实参，而是标识该扩展所指定的类型，调用的时候只需要提供this后的形参即可

* 常见的扩展方法有Linq扩展、有IEnumerable扩展等

实例：
```
    //新建静态类
    public static class Class1
    {
        public static int Getlen(this string a)
        {
            return a.Length;
        }

        public static string Getlink(this string a,string b)
        {
            return a + b;
        }
    }
    //main函数调用扩展方法
    class Program
    {
        static void Main(string[] args)
        {
            string a = "Hello";
            string b = " world";
            Console.WriteLine(a.Getlen());//返回5
            Console.WriteLine(a.Getlink(b));//返回Hello world
            Console.WriteLine(a.Getlink(b).Getlen());//返回11
            Console.ReadLine();
        }
    }
```

总结：
```
    1. 扩展方法的第一个参数指定该方法作用于那个类型，并且此参数用this为前缀修饰。仅当你使用using指令将命名空间显示导入到源码之中后，扩展方法才位于范围中。

    2. 在代码中，可以使用实例方法语法调用改扩展方法，但是，编译器生成的中间语言（IL）会将代码转换为对静态方法的调用。因此，并未真正 违反封装原则，实际上，扩展方法无法访问到他们所扩展类型中的私有变量。

    3. 不能重写扩展方法

    4. 与接口或者类具有相同名称和签名的扩展方法永远不会被调用（编译时优先级：实例方法 > 扩展方法）

                （方法签名：c#中指返回值和参数）
```