# 2020-12-05 js笔记整理

## 变量作用域与解构赋值

### 变量的作用域

> 在JavaScript中，用var申明的变量实际上是有作用域的,一个变量在函数体内部申明，则该变量的作用域为整个函数体，两个不同的函数各自申明了同一个变量，那么该变量只在各自的函数体内起作用，js的函数可以嵌套，这时候内部函数可以访问外部定义的，反过来不行。如果内部函数和外部函数的变量名重名js的函数在查找变量时从自身函数定义开始，从“内”向“外”查找。如果内部函数定义了与外部函数重名的变量，则内部函数的变量将“屏蔽”外部函数的变量

实例：
```
        //js
    function a(){
        var b=1;
    }
    console.log(b);//无法在函数体外引用变量b

    function a(){
        var b=1;
    }

    function c(){
        var b='A'
    }

    //两个b变量是不同的
```

### 变量提升

> js的函数定义有个特点，它会先扫描整个函数体的语句，把所有申明的变量“提升”到函数顶部

实例：
```
    var a=123;
    var b=a+c;
    var c=777;
    console.log(b);//nan
```

* 因为在js引擎看来，var c是undefined，因为这一特性请严格遵守“在函数内部首先申明所有变量”这一规则

### 全局作用域

> 不在任何函数内定义的变量就具有全局作用域。实际上，JavaScript默认有一个全局对象window，全局作用域的变量实际上被绑定到window的一个属性

实例：
```
function a(){
    console.log('123456');
}

a();
window.a();

var a=123;
console.log(a);
console.log(window.a)

function a(){
    console.log('我是初始值1234');
}
window.a();
var b=window.a;
window.a=function (){
    console.log('我被修改了7890');
}
window.a();
window.a=b;
window.a();

// 我们每次直接调用的a()函数其实也是window的一个变量
```

总结：这说明JavaScript实际上只有一个全局作用域。任何变量（函数也视为变量），如果没有在当前函数作用域中找到，就会继续往上查找，最后如果在全局作用域中也没有找到，则报ReferenceError错误

### 命名空间

> 全局变量会绑定到window上，不同的JavaScript文件如果使用了相同的全局变量，或者定义了相同名字的顶层函数，都会造成命名冲突，并且很难被发现。减少冲突的一个方法是把自己的所有变量和函数全部绑定到一个全局变量中

实例：
```
'use strict'

var MYAPP={};
MYAPP.a=123;
MYAPP.b=777;

MYAPP.sum=function (){
    return MYAPP.a+MYAPP.b
}

conso
```

* 把自己的代码全部放入唯一的名字空间MYAPP中，会大大减少全局变量冲突的可能





