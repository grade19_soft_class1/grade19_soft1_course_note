# 2020年12月22日课堂笔记
昨天中午睡觉了2小时，晚上从1点睡到今天早上11点，一天超过一半的时间在睡觉，可真是牛逼

## 今日所学知识
今天学了正则表达式和JSON还有JavaScript里的算是对象的继承吧，挺多的，也没办法，在赶进度。

## 正则表达式

### 用正则表达式判断邮箱合法不合法
```
1.创建一个匹配Email的正则表达式；

2.用该正则表达式去匹配用户的输入来判断是否合法。

3.因为正则表达式也是用字符串表示的，所以，我们要首先了解如何用字符来描述字符。
```
### 基本的正则表达式
用\d可以匹配一个数字，\w可以匹配一个字母或数字。所以：
```
var ifOk=/00\d/;
console.log(ifOk.test('001'));
var ifOk1=/\w\w\w/
console.log(ifOk1.test('sdg'));
```
要匹配变长的字符，在正则表达式中，用*表示任意个字符（包括0个），用+表示至少一个字符，用?表示0个或1个字符，用{n}表示n个字符，用{n,m}表示n-m个字符：
```
var ifOk=/\w{5}\s+\d{5,7}/;
console.log(ifOk.test('dfdfs    5645754'));
```
首先\w{5}表示有5个字母或者数字。\s可以匹配一个空格，\s+表示至少有1个空格，可以有多个空格。\d{5,7}表示至少要5个数字，最多7个数字
### 进阶
要做更精确地匹配，可以用[]表示范围，比如：
```
[0-9a-zA-Z\_]可以匹配一个数字、字母或者下划线；

[0-9a-zA-Z\_]+可以匹配至少由一个数字、字母或者下划线组成的字符串，比如'a100'，'0_Z'，'js2015'等等；

[a-zA-Z\_\$][0-9a-zA-Z\_\$]*可以匹配由字母或下划线、$开头，后接任意个由一个数字、字母或者下划线、$组成的字符串，也就是JavaScript允许的变量名；

[a-zA-Z\_\$][0-9a-zA-Z\_\$]{0, 19}更精确地限制了变量的长度是1-20个字符（前面1个字符+后面最多19个字符）
```
有了准备知识，我们就可以在JavaScript中使用正则表达式了。

JavaScript有两种方式创建一个正则表达式：

第一种方式是直接通过/正则表达式/写出来，第二种方式是通过new RegExp('正则表达式')创建一个RegExp对象。

两种写法是一样的：
```
var re1 = /ABC\-001/;
var re2 = new RegExp('ABC\\-001');

re1; // /ABC\-001/
re2; // /ABC\-001/
```
注意，如果使用第二种写法，因为字符串的转义问题，字符串的两个\\实际上是一个\。

先看看如何判断正则表达式是否匹配：
```
var re = /^\d{3}\-\d{3,8}$/;
re.test('010-12345'); // true
re.test('010-1234x'); // false
re.test('010 12345'); // false
```
RegExp对象的test()方法用于测试给定的字符串是否符合条件。
### 切分字符串
用正则表达式切分字符串比用固定的字符更灵活，请看正常的切分代码：
```
var str1= '234 34 234 325 4 6  4     5435    342    4   3 4';
var arr=str1.split(' ');
console.log(arr);   //["234", "34", "234", "325", "4", "6", "", "4", "", "", "", "", "5435", "", "", "", "342", "", "", "", "4", "", "", "3", "4"]
var arr2 = str1.split(/\s+/)
console.log(arr2);  //["234", "34", "234", "325", "4", "6", "4", "5435", "342", "4", "3", "4"]
```

### 分组
除了简单地判断是否匹配之外，正则表达式还有提取子串的强大功能。用()表示的就是要提取的分组（Group）。比如：
```
var re1 = /^(\w{3,6}-(\d{2,4}))/;
console.log(re1.exec('wer23-333'));     //"wer23-333", "wer23-333", "333"
console.log(re1.exec('dfsg-233'));      //"dfsg-233", "dfsg-233", "233"
```

### 贪婪匹配
需要特别指出的是，正则匹配默认是贪婪匹配，也就是匹配尽可能多的字符。举例如下，匹配出数字后面的0：
```
var re1 =/^(\d+)(0*)$/
var a =re1.exec('1230000000000')       
console.log(a);     //"1230000000000", "1230000000000", "",
```
由于\d+采用贪婪匹配，直接把后面的0全部匹配了，结果0*只能匹配空字符串了。

必须让\d+采用非贪婪匹配（也就是尽可能少匹配），才能把后面的0匹配出来，加个?就可以让\d+采用非贪婪匹配：
```
var re1 =/^(\d+?)(0*)$/
var a =re1.exec('1230000000000')        //"1230000000000", "123", "0000000000"
console.log(a);
```
### 练习
请尝试写一个验证Email地址的正则表达式。版本一应该可以验证出类似的Email：
```
'use strict';
var re = /^[0-9a-zA-Z\.]{1,12}\@[0-9a-zA-Z]+\.[a-zA-Z]+$/;

// 测试:
var
    i,
    success = true,
    should_pass = ['someone@gmail.com', 'bill.gates@microsoft.com', 'tom@voyager.org', 'bob2015@163.com'],
    should_fail = ['test#gmail.com', 'bill@microsoft', 'bill%gates@ms.com', '@voyager.org'];
for (i = 0; i < should_pass.length; i++) {
    if (!re.test(should_pass[i])) {
        console.log('测试失败: ' + should_pass[i]);
        success = false;
        break;
    }
}
for (i = 0; i < should_fail.length; i++) {
    if (re.test(should_fail[i])) {
        console.log('测试失败: ' + should_fail[i]);
        success = false;
        break;
    }
}
if (success) {
    console.log('测试通过!');
}
```
版本二可以验证并提取出带名字的Email地址：
```
'use strict';
var re = /^$/;

// 测试:
var r = re.exec('<Tom Paris> tom@voyager.org');
if (r === null || r.toString() !== ['<Tom Paris> tom@voyager.org', 'Tom Paris', 'tom@voyager.org'].toString()) {
    console.log('测试失败!');
}
else {
    console.log('测试成功!');
}
```
## JSON

JSON是JavaScript Object Notation的缩写，它是一种数据交换格式。

在JSON出现之前，大家一直用XML来传递数据。因为XML是一种纯文本格式，所以它适合在网络上交换数据。XML本身不算复杂，但是，加上DTD、XSD、XPath、XSLT等一大堆复杂的规范以后，任何正常的软件开发人员碰到XML都会感觉头大了，最后大家发现，即使你努力钻研几个月，也未必搞得清楚XML的规范。

终于，在2002年的一天，道格拉斯·克罗克福特（Douglas Crockford）同学为了拯救深陷水深火热同时又被某几个巨型软件企业长期愚弄的软件工程师，发明了JSON这种超轻量级的数据交换格式。

道格拉斯同学长期担任雅虎的高级架构师，自然钟情于JavaScript。他设计的JSON实际上是JavaScript的一个子集。在JSON中，一共就这么几种数据类型：
```
number：和JavaScript的number完全一致；
boolean：就是JavaScript的true或false；
string：就是JavaScript的string；
null：就是JavaScript的null；
array：就是JavaScript的Array表示方式——[]；
object：就是JavaScript的{ ... }表示方式。
```
以及上面的任意组合。

并且，JSON还定死了字符集必须是UTF-8，表示多语言就没有问题了。为了统一解析，JSON的字符串规定必须用双引号""，Object的键也必须用双引号""。

由于JSON非常简单，很快就风靡Web世界，并且成为ECMA标准。几乎所有编程语言都有解析JSON的库，而在JavaScript中，我们可以直接使用JSON，因为JavaScript内置了JSON的解析。

把任何JavaScript对象变成JSON，就是把这个对象序列化成一个JSON格式的字符串，这样才能够通过网络传递给其他计算机。

如果我们收到一个JSON格式的字符串，只需要把它反序列化成一个JavaScript对象，就可以在JavaScript中直接使用这个对象了。
### 序列化
让我们先把小明这个对象序列化成JSON格式的字符串：
```
'use strict';

var xiaoming = {
    name: '小明',
    age: 14,
    gender: true,
    height: 1.65,
    grade: null,
    'middle-school': '\"W3C\" Middle School',
    skills: ['JavaScript', 'Java', 'Python', 'Lisp']
};
var s = JSON.stringify(xiaoming)
console.log(s);
```
要输出得好看一些，可以加上参数，按缩进输出：
```
JSON.stringify(xiaoming, null, '  ');
```
输出结果：
```
{
  "name": "小明",
  "age": 14,
  "gender": true,
  "height": 1.65,
  "grade": null,
  "middle-school": "\"W3C\" Middle School",
  "skills": [
    "JavaScript",
    "Java",
    "Python",
    "Lisp"
  ]
}
```
第二个参数用于控制如何筛选对象的键值，如果我们只想输出指定的属性，可以传入Array：
```
var s = JSON.stringify(xiaoming,['age','name','grade'],' ')
console.log(s);
```
要记住一点，使用的时候属性名腰带''或者“”。

还可以传入一个函数，这样对象的每个键值对都会被函数先处理：
```
var xiaoming = {
    name: '小明',
    age: 14,
    gender: true,
    height: 1.65,
    grade: null,
    'middle-school': '\"W3C\" Middle School',
    skills: ['JavaScript', 'Java', 'Python', 'Lisp']
};
function convert(key,value){
    if(value==='string'){
        return value.toUpperCase();
    }
    return value
}

var a =JSON.stringify(xiaoming, convert, '  ');
 console.log(a);
```
