# 12月16日的课堂笔记
胡哥今天没有讲新的内容，讲了一下之前留下的课堂笔记，说实话，自己敲可以明白，但是看着别人的就会好难，头疼
## 练习1
如果我们继续改进这个例子，想办法把一个字符串13579先变成Array——[1, 3, 5, 7, 9]，再利用reduce()就可以写出一个把字符串转换为Number的函数。

练习：不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：

以下是自己做的
```
function string2int(s) {
    let arr= []
//把字符串的值一个一个存进数组里
for(t of s){
    arr.push(t)
}
//字符串是数字的经过一次加减乘除中任何一项就可以变成数字
var a=arr.reduce((x,y)=>{
    x+=1;
    x-=1;
    y=y+1;
    y=y-1;
    return x*10+y;
})
//不知道为什么会多出来一个0，那就除10删了
a=a/10;
return a
}

if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        console.log('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        console.log('请勿使用Number()!');
    } else {
        console.log('测试通过!');
    }
}
else {
    console.log('测试失败!');
}
```
以下是别人做的
```
function string2int(s) {
  let a = s.split('');
  console.log(a);
  let b = a.map(x=>{
      return x*1
  }
  );
  console.log(b);
  return b.reduce((x,y)=>{
      return x*10+y;
  })
  
}

if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        console.log('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        console.log('请勿使用Number()!');
    } else {
        console.log('测试通过!');
    }
}
else {
    console.log('测试失败!');
}
```
总结，不明白js里的split方法的应用，只是知道他是一个去除元素的函数,特意上网看了下
split() 方法用于把一个字符串分割成字符串数组。
以下是语法和属性
```
stringObject.split(separator,howmany)
separator	必需。字符串或正则表达式，从该参数指定的地方分割 stringObject。
howmany	可选。该参数可指定返回的数组的最大长度。如果设置了该参数，返回的子串不会多于这个参数指定的数组。如果没有设置该参数，整个字符串都会被分割，不考虑它的长度。
```
还是练习的太少了，太多的自带函数玩不明白
## 练习2
请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。
以下是我敲的
```
function normalize(arr) {
    var arr2=[];
    for(let s of arr){
        s=s.toLowerCase();
       let x = s[0].toUpperCase();
       let arr1 = []
       for(let a of s){
           arr1.push(a);
           arr1.shift();
           arr1.unshift(x);    
       }
       var b = '';
       for(let i=0;i<arr1.length;i++){
           b +=arr1[i];
       }
       console.log(b);
       arr2.push(b);
   }
   return arr2
}
```
以下是其它实现方法
```
function normalize(arr) {
    function x(a){
        a.toLowerCase();
        var x1 = a.substring(0,1).toUpperCase()//提取第一个字符，让其大写。
        var x2 = a.substring(1).toLowerCase()//让后面的字符小写
        return x1+x2;
    }
    return arr.map(x)
}
if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}
```
真的，看看其他人的代码，在看到自己的，想死，写的太复杂了
## 练习3
练习3就算了，太简单了
## 练习4
请尝试用filter()筛选出素数：
以下是自己敲的
```
'use strict';

function get_primes(arr) {
    function a(x){
        let b = 0;
        for(let i =1; i<=x;i++){
            if(x%i===0){
                b+=1
            }
        }
        return b==2;
    }
    return arr.filter(a)
}
// 测试:
var
    x,
    r,
    arr = [];
for (x = 1; x < 100; x++) {
    arr.push(x);
}
r = get_primes(arr);
if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + r.toString());
}
```
以下是其他人做的
```
function get_primes(arr) {
    function a(x) {
        if(x==1) {
            return false;//1不是素数，返回false
        }
        //i是2到x开平方根之间的整数
        //如果x对i取余等于0，则不是素数，返回false
        for(let i=2;i<x;i++) {
            if(x%i===0) {
                return false;
            }
        }
        //如果上述条件都不满足，则是素数，返回true
        return true;
    }
    //用filter()返回新的只含有素数的数组
    return arr.filter(a);
}


// 测试:
var
    x,
    r,
    arr = [];
for (x = 1; x < 100; x++) {
    arr.push(x);
}
r = get_primes(arr);
if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + r.toString());
}
```
思路不一样，解决问题的办法也会不同