# 12月2日课堂笔记

今天天气是真的无敌，在教室穿短袖，在外面穿羽绒服都觉得冷，龙岩的风是真的刺进骨头里的那种，害怕。

今天老胡讲的内容还是挺多的，这个课堂笔记慢慢做了，估计两节自习课是做不完了。

## Map和Set
```
JavaScript的默认对象表示方式{}可以视为其他语言中的Map或Dictionary的数据结构，即一组键值对。

但是JavaScript的对象有个小问题，就是键必须是字符串。但实际上Number或者其他数据类型作为键也是非常合理的。
```

### Map
```
Map是一组键值对的结构，具有极快的查找速度。

举个例子，假设要根据同学的名字查找对应的成绩，如果用Array实现，需要两个Array：

var name = ['小陈','小黄','小紫','小绿','小白'];

var grade = [88,72,55,43,0]

给定一个名字，要查找对应的成绩，就先要在names中找到对应的位置，再从scores取出对应的成绩，Array越长，耗时越长。

如果用Map实现，只需要一个“名字”-“成绩”的对照表，直接根据名字查找成绩，无论这个表有多大，查找速度都不会变慢。用JavaScript写一个Map如下：

var m = Map([['小陈',88],['小黄',72],['小紫',55],['小绿',43],['小白',0]])

第一个元素表示的是名字，也可以说是key,第二个表示的是分数，也是value

要调用也很简单

console.log(m.get('小陈'));     //88

//添加一个新key-value
m.set('小张',66);

//判断是否存在key返回布尔类型
console.log(m.has('小黄'))      //true
console.log(m.has('老地方'))      //false

//删除
m.delete('小陈');

//删除之后再查询
console.log(m.get('小陈'))      //undefined

需要注意的是，由于一个key只能对应一个value，所以，多次对一个key放入value，后面的值会把前面的值冲掉。
```
### set
```
Set和Map类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在Set中，没有重复的key。

var s = new Set([1,2,3,4]);

Set有自动过滤掉已经存在的元素

var s = new Set([1,2,3,4,4,5,6,6,5]);
console.log(s);     //{1,2,3,4,5,6}

Set添加元素和Map不同，他也是用add的
s.add(7)

不过删除都一样，都是用delete
s.delete(7)
```
## ## iterable
```
遍历Array可以采用下标循环，遍历Map和Set就无法使用下标。为了统一集合类型，ES6标准引入了新的iterable类型，Array、Map和Set都属于iterable类型。

具有iterable类型的集合可以通过新的for ... of循环来遍历。

var m = new Map([['小陈',88],['小黄',72],['小紫',55],['小绿',43],['小白',0]]);
var s = new Set([1,2,3,4,4,5,6,6,5]);

for(var x of m){
    console.log(x[0]+'的分数是'+x[1])
}

for(var x of s){
    console.log(x)
}

for of 和for in 的区别

var a = ['A', 'B', 'C'];
a.name = 'Hello';
for (var x in a) {
    console.log(x); // '0', '1', '2', 'name'
}

for ... in循环将把name包括在内，但Array的length属性却不包括在内。

for ... of循环则完全修复了这些问题，它只循环集合本身的元素：

var a = ['A', 'B', 'C'];
a.name = 'Hello';
for (var x of a) {
    console.log(x); // 'A', 'B', 'C'
}

更好的方式是直接使用iterable内置的forEach方法，它接收一个函数，每次迭代就自动回调该函数。

var arr1 = [134,3,54,52,4,34,23,42343,4,23413,12,3122,33,54,34,6,2,45,76,745,54,4];
arr1.forEach(function(x,y,z){
    console.log('数组的值是'+x);
    console.log('数组的下标是'+y);
    //z表示的是数组本身
})

Set与Array类似，但Set没有索引，因此回调函数的前两个参数都是元素本身：

var s = new Set([1,2,3,4,4,5,6,6,5]);
s.forEach(function(x,y,z){
    console.log(x)
    console.log(y)
})

x和y 的值都是一样的

Map的回调函数参数依次为value、key和map本身。这个是重点，要背得

var m = new Map([['小陈',88],['小黄',72],['小紫',55],['小绿',43],['小白',0]]);
m.forEach(function(value,key,valuem){
    console.log(key);
    console.log(value);
})
```

## 函数
```
说实话，java学了一边这个，c#跟深入的了解了这个玩意，真的也没什么好说的。
```

## 抽象
```
要使用有两个办法
第一个是在下载插件
第二个是加一段<script src="https://cdn.bootcdn.net/ajax/libs/mathjax/3.1.2/es5/a11y/assistive-mml.min.js"></script>的js
```
