# 第一个星期六的课堂笔记

## 备案的介绍
```
还有备案号，如果没有写备案号，是要被罚钱罚死的，听老胡说一次是5000到10000，这个谁顶得住啊
```

## 关于JavaScript

### 基本介绍
```
JavaScript（简称“JS”） 是一种具有函数优先的轻量级，解释型或即时编译型的高级编程语言。虽然它是作为开发Web页面的脚本语言而出名的，但是它也被用到了很多非浏览器环境中，JavaScript 基于原型编程、多范式的动态脚本语言，并且支持面向对象、命令式和声明式（如函数式编程）风格。 [1] 
JavaScript在1995年由Netscape公司的Brendan Eich，在网景导航者浏览器上首次设计实现而成。因为Netscape与Sun合作，Netscape管理层希望它外观看起来像Java，因此取名为JavaScript。但实际上它的语法风格与Self及Scheme较为接近。 [2] 
JavaScript的标准是ECMAScript 。截至 2012 年，所有浏览器都完整的支持ECMAScript 5.1，旧版本的浏览器至少支持ECMAScript 3 标准。2015年6月17日，ECMA国际组织发布了ECMAScript的第六版，该版本正式名称为 ECMAScript 2015，但通常被称为ECMAScript 6 或者ES6。 [1]
```

### 基本练习
```
<script>
    var a=40;
    var b='40.0';
    console.log(a==b);
</script>

这个输出出来是true
```
```
var a = 40;
var b ='40';
var c= a===b;
console.log(c);

这个输出出来是false
```
```
总结：判断==是转换后比较，===是先比较数据类型在比较值，感觉3个等于有点那味了
```
