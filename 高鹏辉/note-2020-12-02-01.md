# 课堂笔记
```
2020年12月02号第十天
```
# 课堂小计
## Σ(っ °Д °;)っ惊！是什么演讲居然能让大家那么压抑呢！
![](./imgs/2020-12-02-0001.png)
```
所以请关心身边的每一个人！O(∩_∩)O
```
# 内容
## Map和Set
### Map
```
var m =new Map();
m.set('Adam',67);
m.set('Bot',55);
console.log(m.has('Adam'));
console.log(m.get('Adam'));
m.delete('Adam');
console.log(m.get('Adam'));

```
### 总结
```
代表这Map是可读可写的，也是可以删除的，而且可以有重复值。
```
### Set
```
var s = new Set([1,2,3,3,3,7,5,6,7]);
console.log(s);
```
```
结果
```
![](./imgs/2020-12-02-0002.png)
```这里表示Set是会消除重复的值的，但是要类型相同才行消除哦(⊙o⊙)```
## iterable
```
'use strict';
var arr= [1,2,3,4];
var m =new Map();
m.set('name','方法');
m.set('name1','方法1');
m.set('name2','方法2');
m.set('name3','方法3');
var s = new Set(['a','b','c','d']);
arr.name='南村群童欺我老无力,轮流play with me啊'; 
arr.forEach((el)=>{
    console.log(el);
    //console.log(index);
});
m.forEach((el,index)=>{
    console.log(el);
    console.log(index);
});
s.forEach((abc,bbc,arr)=>{
    console.log(abc);
    console.log(bbc);
});
console.log(arr.name);
```
```结果```![](./imgs/2020-12-02-0003.png)
### 补充 for···of 和 for···in 的用法
```
var a = ['A', 'B', 'C'];
a.name = 'Hello';
for (var x in a) {
    console.log(x); 
}
for(var x of a){
    console.log(x);
}
```
```结果```![](./imgs/2020-12-02-0004.png)
## 函数
### 函数定义和调用
```
function abs(x){
    if(x>=0){
        return x;
    }else{
        return -x;
    }
}
var abs = function (x){
    if (x>=0){
        return x;
    }else{
        return -x;
    }
}
console.log(abs(10));
console.log(abs(-10));
console.log(abs(0));
console.log(abs());
```
```结果```![](./imgs/2020-12-02-0005.png)
### 总结 
```
函数的两种定义方法，还有就是调用的时候不给值会反回NoN哦。
```