# 课堂笔记
```
2020年12月03号第十一天
```
# 课堂小计
## Σ(っ °Д °;)っ惊！茶文化是什么呢？
![](./imgs/2020-12-03-0001.png)
```
一 是茶文化以茶德为中心，重视人的群体价值，倡导无私奉献，反对见利忘义和唯利是图。主张义重于利，注重协调人与人之间的相互关系，提倡对人尊敬，重视修生养德，有利于人的心态平衡，解决现代人的精神困惑，提高人的文化素质；

二 是茶文化是应对人生挑战的益友。在激烈的社会竞争，市场竞争下，紧张的工作、应酬、复杂的人际关系，以及各类依附在人们身上的压力不轻。参与茶文化，可以使精神和身心放松一番，以应对人生的挑战，香港茶楼的这个作用十分显著；

三 是有利于社区文明建设 。经济上去了，但文化不能落后，社会风气不能污浊，道德不能沦丧 和丑恶。改革开放后茶文化的传播表明，茶文化是有改变社会不正当 消费活动、创建精神文明、促进社会进步的作用；

四 是对提高人们生活质量，丰富文化生活的作用明显。茶文化具有知识性、趣味性和康乐性，品尝名茶、茶具、茶点，观看茶俗茶艺 ，都给人一种美的享受；

五 是促进开放，推进国际文化交流。上海市闸北区连续四届举办 国际茶文化节，扩大了闸北区对内对外的知名度，闸北区四套班子一 致决定茶文化节要一直办下去，并投资在闸北公园兴建茶文化景点， 以期建成茶文化大观园。
```
# 内容
## arguments
### 我也举个栗子
```
'use strict'
function bbc(x){
    for(var i =0;i<arguments.length;i++){
        console.log(i);
        console.log(arguments[i]);
    }
}
bbc(20,50,60);
function aac(){
    for(var i =0;i<arguments.length;i++){
        console.log(arguments[i]);
    }
}
aac('我','(c)','爱','(n)','你','(m)');
function cct(a,b,c){
    console.log(a);
    console.log(b);
    console.log(c);
    if (arguments.length === 2){
        c=b;
        b=null;
    }
    console.log(a);
    console.log(b);
    console.log(c);
}
cct(10,50);
```
### 效果图嘿嘿嘿😜
![](./imgs/2020-12-03-0002.png)
## 总结 
```
arguments就是你传进去的所有参数，就算不定义参数也能用，如果有三个参数，你只传了两个那么最后一个就会为空。
```
## rest参数
```
'use strict'
function sxx(a,b,...rest){
    console.log(a);
    console.log(b);
    console.log(rest);
}
sxx(10,20,30,40,50,6,8);
sxx(10);

function ssd(...rest){
    var sum = 0;
    for(var i=0;i<rest.length;i++){
        sum+=rest[i];
    }
    return sum
}
console.log(ssd(5));
console.log(ssd(5,50));
console.log(ssd(5,50,50,50));
```
### 效果图！！
![](./imgs/2020-12-03-0003.png)
## 总结 
```
rest就是本来这个方法只有两个参数的，你非要定义三个参数，多出来的那个，rest就可以获取到，而且是以数组的方式保存。
```
## 小心你的return语句
```
function foo() {
    return; // 自动添加了分号，相当于return undefined;
        { name: 'foo' }; // 这行语句已经没法执行到了
}
function dxx(){
    return 
        name:'foo'
}
//这样也不行哦！
```
所以正确的多行写法是：
```
function foo() {
    return { // 这里不会自动加分号，因为{表示语句尚未结束
        name: 'foo'
    };
}
```
## 练习1
```
定义一个计算圆面积的函数area_of_circle()，它有两个参数：
r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14
```
```
'use strict';
function area_of_circle(r, pi) {
    if(arguments.length===0){
        return 0;
    }else if (arguments.length===1){
        pi=3.14;
    }
    return pi*r*r;
}
测试:
if (area_of_circle(2) === 12.56 && area_of_circle(2, 3.1416) === 12.5664) {
    console.log('测试通过');
} else {
    console.log('测试失败');
}
```
```
结果是：测试通过
```
## 练习2
```
小明是一个JavaScript新手，他写了一个max()函数，返回两个数中较大的那个：
```
```
'use strict';
错误示范：
/*function max(a, b) {
    if (a > b) {
        return
                a;
    } else {
        return
                b;
    }

}
console.log(max(15, 20));*/
正确示范：
function max(a, b) {
    if (a > b) {
        return a;
    } else {
        return{
               b};
    }

}
console.log(max(15, 20));
```

