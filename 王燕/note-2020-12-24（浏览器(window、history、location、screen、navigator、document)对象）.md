# 今日份笔记😕
# 浏览器
# 浏览器对象
## 1.window对象
1.1 window对象
+ window对象是BOM的核心、window对象指当前的浏览器窗口
+ 所有JavaScript全局对象 、函数以及变量均自动成为window对象的成员
+ 全局变量是window对象的属性
+ 全局函数是window对象的方法
+ 甚至HTML DOM的document也是window对象属性之一

1.2 window.innerHeight  浏览器窗口的内部高度

     window.innerWidth  浏览器窗口的内部宽度

举个栗子：

        let str=`
        内宽：${window.innerWidth},内高:${window.innerHeight}`;

        let str1=`
        内宽：${window.outerWidth},内高:${window.outerHeight}`;

        console.log(str);
        console.log(str1);
运行结果：

        内宽：1365,内高:979
        内宽：1920,内高:1050
## 2.History对象
2.1 History对象

     window.history对象包含浏览器的历史(url)的集合

2.2 History方法
+ history.back()  与在浏览器点击后退按钮相同
+ history.firward()  与在浏览器点击向前按钮相同
+ history.go()  进入历史中的某个页面

history对象保存浏览器的历史记录。

操纵DOM。

+ 更新：更新该DOM节点的内容，相当于更新了该DOM节点表示的HTML的内容；

+ 遍历：遍历该DOM节点下的子节点，以便进行进一步操作；

+ 添加：在该DOM节点下新增一个子节点，相当于动态增加了一个HTML节点；

+ 删除：将该节点从HTML中删除，相当于删掉了该DOM节点的内容以及它包含的所有子节点。
## 3.Location对象
3.1 Location对象

     window.location对象用于获得当前页面的地址(url)，并把浏览器重定向到新的页面。

3.2 Location对象的属性
+ location.hostname 返回web主机的域名
+ location.psthname  返回当前页面的路径和文件名
+ location.port  返回web主机的端口
+ location.protocol  返回所使用的web协议(http://或https://)
+ location.href  属性返回当前页面的URL
+ location.assign()  方法加载新的文档
要加载一个新页面，可以调用location.assign()。如果要重新加载当前页面，调用location.reload()方法非常方便。
```
'use strict';
if (confirm('重新加载当前页' + location.href + '?')) {
    location.reload();
} else {
    location.assign('/'); // 设置一个新的URL地址
}
```
## 4. screen对象
4.1 Screen对象

      window.screen对象包含有关用户屏幕的信息

4.2 属性
+ screen.availWidth  可用的屏幕宽度
+ screen.availHeight  可用的屏幕高度
+ screen.Height  屏幕高度
+ screen.Width  屏幕宽度
+ screen.colorDepth  屏幕色彩深度
+ screen.pixelDepth  屏幕像素深度
```
'use strict';
console.log('Screen size = ' + screen.width + ' x ' + screen.height);
```
## 5. navigator对象
        属性	描述
        appCodeName	返回浏览器的代码名。
        appMinorVersion	返回浏览器的次级版本。
        appName	返回浏览器的名称。
        appVersion	返回浏览器的平台和版本信息。
        browserLanguage	返回当前浏览器的语言。
        cookieEnabled	返回指明浏览器中是否启用 cookie 的布尔值。
        cpuClass	返回浏览器系统的 CPU 等级。
        onLine	返回指明系统是否处于脱机模式的布尔值。
        platform	返回运行浏览器的操作系统平台。
        systemLanguage	返回 OS 使用的默认语言。
        userAgent	返回由客户机发送服务器的 user-agent 头部的值。
        userLanguage	返回 OS 的自然语言设置。
常用的几个如下：
```   
'use strict'   
console.log('appName = ' + navigator.appName);
console.log('appVersion = ' + navigator.appVersion);
console.log('userLanguage = ' + navigator.userLanguage);
console.log('platform = ' + navigator.platform);
console.log('userAgent = ' + navigator.userAgent);
```
## 6. document对象
每个载入浏览器的 HTML 文档都会成为 Document 对象。

Document 对象使我们可以从脚本中对 HTML 页面中的所有元素进行访问。

提示：Document 对象是 Window 对象的一部分，可通过 window.document 属性对其进行访问。
```
document表示当前的页面，由于html在浏览器中以DOM的形式展现，document对象就是整个DOM树的根节点

查找dom对象可以用getElementById()和getElementsByTagName()可以按ID获得一个DOM节点和按Tag名称获得一组DOM节点：

cookie是由服务器发送的key-value标识符，因为http是没有状态的，但是服务器要区分是哪个 用户发送过来的。就可以用cookie进行区分。当一个用户成功登陆后，服务器发一个cookie给浏览器，例如user=ABC123XYZ(加密的字符串)...，此后，浏览器访问该网站时，会在请求头附上这个Cookie，服务器根据Cookie即可区分出用户。

document.cookie这一属性。因为js能读取cookie 所以第三方的js文件会进行入侵。读取用户信息，，服务器在设置Cookie时可以使用httpOnly，设定了httpOnly的Cookie将不能被JavaScript读取。这个行为由浏览器实现，主流浏览器均支持httpOnly选项，IE从IE6 SP1开始支持。
```
### Document 对象集合
![当图片无法显示，展示的文本内容](./img/imgs71.png)
### Document 对象属性
![当图片无法显示，展示的文本内容](./img/imgs72.png)
### Document 对象方法
![当图片无法显示，展示的文本内容](./img/imgs73.png)