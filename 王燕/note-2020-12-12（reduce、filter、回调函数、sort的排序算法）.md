## 放假倒计时28天
## 今日份笔记
### reduce
reduce() 方法接收一个函数作为累加器,reduce 为数组中的每一个元素依次执行回调函数，不包括数组中被删除或从未被赋值的元素，接受四个参数：初始值（上一次回调的返回值），当前元素值，当前索引，原数组 

利用reduce()求和：

        var arr = [1,5,8,6,15,78,65,25,48,55]
        var sum = arr.reduce(function(x,y){
        return x+y;
        });
        console.log(sum);//306

练习：利用reduce()求积： 

        function product(arr) {
            return arr.reduce((x,y)=>{
                return x*y;
            })
        }

        // 测试:
        if (product([1, 2, 3, 4]) === 24 && product([0, 1, 2]) === 0 && product([99, 88, 77, 66]) === 44274384) {
            console.log('测试通过!');
        }
        else {
            console.log('测试失败!');
        } // 测试通过！

练习：把一个字符串13579先变成Array——[1, 3, 5, 7, 9]，再利用reduce()就可以写出一个把字符串转换为Number的函数

不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：

        function string2int(s) {
            function str2num(str){
                var strArr = str.split('');               //把字符串分割成字符串数组
                function toInt(data){
                    return +data;                  //通过js的弱类型转换，实现字符类型到数字类型的转换
                }
                var numArr = strArr.map(toInt);           //通过map()把字符串数组转换成数字数组
                return numArr;
            }
            var num = str2num(s);
            var res = num.reduce(function (x,y) {        //通过reduce()把数字数组转换成数字量
                return x*10+y;
            });
            return res;
        }

        // 测试:
        if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
            if (string2int.toString().indexOf('parseInt') !== -1) {
                console.log('请勿使用parseInt()!');
            } else if (string2int.toString().indexOf('Number') !== -1) {
                console.log('请勿使用Number()!');
            } else {
                console.log('测试通过!');
            }
        }
        else {
            console.log('测试失败!');
        }
结果为：

![当图片无法显示，展示的文本内容](./img/imgs63.png)
### filter
filter()方法会创建一个新数组，原数组的每个元素传入回调函数中，回调函数中有return返回值，若返回值为true，这个元素保存到新数组中；若返回值为false，则该元素不保存到新数组中；原数组不发生改变。

和map()类似，Array的filter()也接收一个函数。和map()不同的是，filter()把传入的函数依次作用于每个元素，然后根据返回值是true还是false决定保留还是丢弃该元素。

在一个Array中，删掉偶数，只保留奇数：

        var arr = [1, 2, 4, 5, 6, 9, 10, 15];
        var r = arr.filter(function (x) {
        return x % 2 !== 0;
        });
        console.log(r);; // [1, 5, 9, 15]
删掉Array里的字符串：

        var arr = ['A', '', 'B', null, undefined, 'C', '  '];
        var r = arr.filter(function (s) {
            return s && s.trim();
        });
        console.log(r);; // ['A', 'B', 'C']
### 回调函数
filter()接收的回调函数，其实可以有多个参数。通常我们仅使用第一个参数，表示Array的某个元素。回调函数还可以接收另外两个参数，表示元素的位置和数组本身：

        var arr = ['A', 'B', 'C'];
        var r = arr.filter(function (x, y, self) {
            console.log(x);
            console.log(y); 
            console.log(self); 
            return true;
        });
运行结果：

![当图片无法显示，展示的文本内容](./img/imgs64.png)
去除重复元素可以用indexof：

        var
        r,
        arr = ['春', '夏', '秋', '冬', '江西', '春', '秋', '春'];

        r = arr.filter(function (x, y, self) {
        return self.indexOf(x) === y;
        });

        alert(r.toString());
运行结果：

        春,夏,秋,冬,江西
练习：请尝试用filter()筛选出素数：

        function get_primes(arr) {
            function isPrimeNumber(x) {
                if(x===1) {
                    return false;//1不是素数，返回false
                }
                //i是2到x开平方根之间的整数
                //如果x对i取余等于0，则不是素数，返回false
                for(let i=2;i<=Math.sqrt(x,2);i++) {
                    if(x%i===0) {
                        return false;
                    }
                }
                //如果上述条件都不满足，则是素数，返回true
                return true;
            }
            //用filter()返回新的只含有素数的数组
            return arr.filter(isPrimeNumber);
        }

        // 以下代码用于测试:
        var
            x,
            r,
            arr = [];
        for (x = 1; x < 100; x++) {
            arr.push(x);
        }
        r = get_primes(arr);
        if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
            console.log('测试通过!');
        } else {
            console.log('测试失败: ' + r.toString());
        }
运行结果：

        测试通过！
## sort
### 排序算法
按数字大小排：

        var arr = [34, 57, 12, 23];
        arr.sort(function (x, y) {
            if (x < y) {
                return -1;
            }
            if (x > y) {
                return 1;
            }
            return 0;
        });
        console.log(arr); // [12, 23, 34, 57]
倒序排序：

        var arr = [34, 57, 12, 23];
        arr.sort(function (x, y) {
            if (x < y) {
                return 1;
            }
            if (x > y) {
                return -1;
            }
            return 0;
        });
        console.log(arr); // [57, 34, 23, 12]