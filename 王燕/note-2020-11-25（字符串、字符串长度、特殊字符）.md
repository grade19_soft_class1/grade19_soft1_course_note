# 今日内容👌：
## 字符串
JavaScript 字符串用于存储和操作文本，是引号中的零个或多个字符。用''或""括起来的字符表示。

可以在字符串中使用引号，只要不匹配围绕字符串的引号即可：

实例：

      var answer = "It's good to see you again!";
      var answer = "He is called 'Bill'";
      var answer = 'He is called "Bill"';

### 字符串长度

内建属性 length 可返回字符串的长度：

      var txt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      var sln = txt.length;

### 特殊字符
反斜杠转义字符把特殊字符转换为字符串字符：

![当图片无法显示，展示的文本内容](./img/imgs15.png)

实例

序列 \" 在字符串中插入双引号：

     var x = "How are \"you\"?"

序列 \' 在字符串中插入单引号：

     var x = 'I\' am fine,Thank you!'

序列 \\ 在字符串中插入反斜杠：

     var x = "字符 \\ 被称为反斜杠。";

转义字符（\）也可用于在字符串中插入其他特殊字符。

其他六个 JavaScript 中有效的转义序列：

![当图片无法显示，展示的文本内容](./img/imgs16.png)

这六个转义字符最初设计用于控制打字机、电传打字机和传真机。它们在 HTML 中没有任何意义。

### 查找字符串中的字符串
indexOf() 方法返回字符串中指定文本首次出现的索引（位置）：
实例：

     var str = "This is a good idea.";
     var pos = str.indexOf("idea");

0 是字符串中的第一个位置，1 是第二个，2 是第三个 ...

lastIndexOf() 方法返回指定文本在字符串中最后一次出现的索引：

实例:

      var str = "This is a good idea.";
      var pos = str.lastIndexOf("idea");
如果未找到文本， indexOf() 和 lastIndexOf() 均返回 -1。

实例：

     var str = "This is a good idea.";
     var pos = str.indexOf("lalal");

两种方法都接受作为检索起始位置的第二个参数。

     var str = "This is a good idea.";
     var pos = str.indexOf("idea"，11);

lastIndexOf() 方法向后进行检索（从尾到头），这意味着：假如第二个参数是 50，则从位置 50 开始检索，直到字符串的起点。

      var str = "This is a good idea.";
      var pos = str.lastIndexOf("idea", 50);

### 检索字符串中的字符串
search() 方法搜索特定值的字符串，并返回匹配的位置：

       var str = "This is a good idea.";
       var pos = str.search("idea");


### 提取部分字符串
有三种提取部分字符串的方法：

     slice(start, end)
     substring(start, end)
     substr(start, length)

### slice() 方法
slice() 提取字符串的某个部分并在新字符串中返回被提取的部分。

该方法设置两个参数：起始索引（开始位置），终止索引（结束位置）。

这个例子裁剪字符串中位置 7 到位置 13 的片段：

实例

     var str = "Apple, Banana, Mango";
     var res = str.slice(7,13);
res 的结果是：

     Banana

如果某个参数为负，则从字符串的结尾开始计数。

这个例子裁剪字符串中位置 -12 到位置 -6 的片段：

实例

     var str = "Apple, Banana, Mango";
     var res = str.slice(-13,-7);
res 的结果是：

     Banana

### substring() 方法
substring() 类似于 slice()。

不同之处在于 substring() 无法接受负的索引。

### substr() 方法
substr() 类似于 slice()。

不同之处在于第二个参数规定被提取部分的长度。