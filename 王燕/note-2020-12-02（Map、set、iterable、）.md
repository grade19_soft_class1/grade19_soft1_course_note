## 今日份笔记
### 一、Map和Set
### Map
Map是一组键值对的结构，具有极快的查找速度。

给定一个名字，查对应的成绩

      var m = new Map([['媛媛', 95], ['雅雅', 75], ['容容', 85]]);
       m.get('媛媛');
       m.set('花木兰','97');
      console.log(m.get('花木兰'));
      console.log(m.get('媛媛'));
      console.log(m.get('雅雅'));
      console.log(m.get('容容'));

结果为：

![当图片无法显示，展示的文本内容](./img/imgs43.png)

### Set
Set和Map类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在Set中，没有重复的key。

     var s1 = new Set();
     var s2 =  new Set([1, 2, 3]);
     var s = new Set([1, 2, 3, 3, '3']);
     console.log(s);
![当图片无法显示，展示的文本内容](./img/imgs44.png)
### 二、iterable
遍历Array可以采用下标循环，遍历Map和Set就无法使用下标。为了统一集合类型，ES6标准引入了新的iterable类型，Array、Map和Set都属于iterable类型。

具有iterable类型的集合可以通过新的for ... of循环来遍历。

for ... of循环是ES6引入的新的语法

      var arr=([['媛媛', 95], ['雅雅', 75], ['容容', 85]]);
      var m=new Map();


      m.set('花木兰','97');
      m.set('媛媛',95);
      m.set('雅雅',75);
      m.set('容容',85);


     var s1 = new Set();
      var s2 =  new Set([1, 2, 3]);
     var s = new Set([1, 2, 3, 3, '3']);


     m.forEach((el,index) =>{
       console.log(el);
        console.log(index);
     })

      s.forEach((el,index) =>{
        console.log(el);
       console.log(index);
      })
![当图片无法显示，展示的文本内容](./img/imgs45.png)

