## 一、什么是Linq？
   LINQ表示语言集成查询，它包括用于从数据源检索信息的一组功能。数据检索是许多程序的重要组成部分。C#通过一组互相关联的功能支持LINQ。这些功能包括C#语言中新增的查询语法、表达式、匿名方法和扩展方法。
   LINQ的核心是查询，查询指定从数据源获得哪些数据。使用查询时，首先必须创建某种形式的查询，然后执行该查询。执行查询的一种方法是在foreach循环中使用查询。

## 二、内置委托
## func<>
  1.Func(TResult)委托封装封装一个不具有参数但却返回 TResult 参数指定的类型值的方法

  2.Func(T,TResult)委托 封装一个具有一个参数并返回 TResult 参数指定的类型值的方法

  3.Func(T1,T2,TResult)委托 封装一个具有两个参数并返回 TResult 参数指定的类型值的方法

   …… ……

   17.Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,TResult>委托 封装一个方法，该方法具有16个参数，并返回TResult参数所指定的类型的值

   下面以Func<T,TResult>委托为例，示范如何使用Func类的委托，该类委托仅仅是参数个数上有区别而已。


       static void Main(string[] args)
       {

          #region Func<T,TResult>委托示例
         //需求：查找整型集合list中大于3的所有元素组成的新集合，并打印出集合元素
          List<int> list = new List<int>() { 1, 2, 3, 4, 5 };
         //将匿名方法分配给 Func<T,TResult> 委托实例
          Func<int, bool> concat1 = delegate(int i) { return i > 3; };
          var newlist1 = list.Where(concat1).ToList();
         //将 Lambda 表达式分配给 Func<T,TResult> 委托实例
          Func<int, bool> concat2 = i => i > 3;
          var newlist2 = list.Where(concat2).ToList();
          newlist1.ForEach(i => Console.WriteLine(i.ToString()));
          newlist2.ForEach(i => Console.WriteLine(i.ToString()));
          Console.ReadKey();
          #endregion
      }
        
   Func类的委托最少可以传入输入泛型参数(in，逆变) 1个，最多可以传入输入泛型参数(in，逆变) 16个，传入的输出泛型参数(out，协变)有且只有一个，这个类型是此委托封装的方法的返回值类型。

## action<>
  1.Action委托 封装一个方法，该方法不具有参数并且不返回值

  2.Action<T>委托 封装一个方法，该方法只有一个参数并且不返回值

  3.Action<T1,T2>委托 封装一个方法，该方法具有两个参数并且不返回值

   …… ……

  17.Action<T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16>委托 封装一个方法，该方法具有16个参数并且不返回值

  下面以Action<T>委托为例，示范如何使用Action类的委托，该类委托仅仅是参数个数上有区别而已。

       static void Main(string[] args)
       {

         #region Action<T>委托示例
              //需求：打印出整型集合list的元素
             List<int> list = new List<int>() { 1, 2, 3, 4, 5 };
              //将匿名方法分配给 Action<T> 委托实例
             Action<int> concat1 = delegate(int i) { Console.WriteLine(i); };
             list.ForEach(concat1);
              //将 lambda 表达式分配给 Action<T> 委托实例
             Action<int> concat2 = (i => Console.WriteLine(i));
             list.ForEach(concat2);
             Console.ReadKey();
             　#endregion 
        }
  Action类的委托最少可以传入0个参数，最多可以传入16个参数，参数类型皆为逆变，并且不返回值。


  ### 示例代码：
     namespace ConsoleApplication2
     {
         static void Main(string[] args)
         {
             static void Main(string[] args)
             static void Main(string[] args)

             //找出arr中的偶数放入 一个evenList里
             var result = from v in arr where v % 2 == 0 select v;
              evenList = result.ToList();

             //显示evenList的结果
             foreach (var v in evenList)
              Console.Write("{0} ", v);
             Console.WriteLine();

             Console.ReadKey();
         }
         }
     }
  ### 运行结果如下：
![当图片无法显示，展示的文本内容](./img/imgs4.png)

## 三、List用法与介绍
性能：在决定使用IList<T>还是使用ArrayList类(两者具有类似的功能)时，记住IList<T>类在大多数情况下执行得更好并且是类型安全的。如果对IList<T>类的类型T 使用引用类型，则两个类的行为是完全相同的。但是，如果对类型T 使用值类型，则需要考虑实现和装箱问题。
## List的基础常用方法：
### 一、声明：
    1、List<T> mList = new List<T>();
    T为列表中元素类型，现在以string类型作为例子
    List<string> mList = new List<string>(); 
    2、List<T> testList =new List<T> (IEnumerable<T> collection);
    以一个集合作为参数创建List：
    string[] temArr = { "Ha", "Hunter", "Tom", "Lily", "Jay", "Jim", "Kuku", "Locu"};
    List<string> testList = new List<string>(temArr);
  　
  ### 二、添加元素：
     1、List. Add(T item)添加一个元素
     例：
     mList.Add("John"); 
     2、List. AddRange(IEnumerable<T> collection)添加一组元素
     例：
     string[] temArr = {"Ha","Hunter","Tom","Lily","Jay","Jim","Kuku","Locu"};mList.AddRange(temArr);
     3、Insert(intindex, T item);在index位置添加一个元素
     例：
     mList.Insert(1,"Hei");
### 三、遍历List中元素：
    foreach(TelementinmList)T的类型与mList声明时一样
    {
        Console.WriteLine(element);
    }
    例：
    foreach(stringsinmList)
    {
        Console.WriteLine(s);
    }

### 四、删除元素:
    1、List. Remove(T item)删除一个值
    例：
    mList.Remove("Hunter");
    2、List. RemoveAt(intindex);删除下标为index的元素
    例：
    mList.RemoveAt(0);
    3、List. RemoveRange(intindex,intcount);
    从下标index开始，删除count个元素
    例：
    mList.RemoveRange(3, 2);

### 五、判断某个元素是否在该List中：

　　List. Contains(T item)返回true或false，很实用

　　例：

　　if(mList.Contains("Hunter"))

　　{
　　Console.WriteLine("There is Hunter in the list");

　　}

　　else

　　{
　　mList.Add("Hunter");

　　Console.WriteLine("Add Hunter successfully.");

　　}
 

### 六、给List里面元素排序：

　　List. Sort ()默认是元素第一个字母按升序

　　例：

　　mList.Sort();
 
### 七、给List里面元素顺序反转：

　　List. Reverse ()可以不List. Sort ()配合使用，达到想要的效果

　　例：

　　mList.Sort();
 

### 八、List清空：

　　List. Clear ()

　　例：

　　mList.Clear();
 

### 九、获得List中元素数目：

　　List. Count ()返回int值

　　例：

　　in tcount = mList.Count();

　　Console.WriteLine("The num of elements in the list: "+count);

　