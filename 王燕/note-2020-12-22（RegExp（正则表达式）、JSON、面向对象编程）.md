## 今天天气真不错，但我没有晒被子，这么好的太阳就这样浪费了
# 一、 RegExp（正则表达式）
以下三种表达式都会创建相同的正则表达式：
+ /ab+c/i; //字面量形式
+ new RegExp('ab+c', 'i'); // 首个参数为字符串模式的构造函数
+ new RegExp(/ab+c/, 'i'); // 首个参数为常规字面量的构造函数

正则表达式(regular expression)描述了一种字符串匹配的模式（pattern），可以用来检查一个串是否含有某种子串、将匹配的子串替换或者从某个串中取出符合某个条件的子串等。

它的设计思想是用一种描述性的语言来给字符串定义一个规则，凡是符合规则的字符串，我们就认为它“匹配”了，否则，该字符串就是不合法的。

判断一个字符串是否是合法的Email的方法是：

创建一个匹配Email的正则表达式；

用该正则表达式去匹配用户的输入来判断是否合法。

因为正则表达式也是用字符串表示的，所以，我们要首先了解如何用字符来描述字符。

在正则表达式中，如果直接给出字符，就是精确匹配。用\d可以匹配一个数字，\w可以匹配一个字母或数字，所以：
```
'00\d'可以匹配'007'，但无法匹配'00A'；

'\d\d\d'可以匹配'010'；

'\w\w'可以匹配'js'；
```
.可以匹配任意字符，所以：
```
'js.'可以匹配'jsp'、'jss'、'js!'等等。
```
要匹配变长的字符，在正则表达式中，用*表示任意个字符（包括0个），用+表示至少一个字符，用?表示0个或1个字符，用{n}表示n个字符，用{n,m}表示n-m个字符：
1. \d{3}表示匹配3个数字，例如'010'；

2. \s可以匹配一个空格（也包括Tab等空白符），所以\s+表示至少有一个空格，例如匹配' '，'\t\t'等；

3. \d{3,8}表示3-8个数字，例如'1234567'。

举个栗子：

        let bat=/\d\d\w/;

        console.log(bat.test(456)); //true

## 元字符
1.元字符是在正则表达式中有特殊含义的非字母字符
        . * + ? $ ^ | \ () {} []
        \t,\v,\n,\r,\0,\f,\cX
## 特殊意义的元字符

        1.\转义字符 转义后面紧跟的字符的意义\

        2.点代表除了\n以外的任何单个字符

        3.d，D，w,W，s,S，b，B

        4.^以什么开头

        5.$以什么结尾

        6.|代表或的意思

## 带有量词意思的元字符

        1.？代表0到1ge

        2*代表0到多个

        3.+代表1到多个

        4.{}系列

## 【】系列元字符

        【xyz】代表x或y或z的任意一个

        【0-9】代表0到9之间的数字

        【a-z】代表a到z之间的字母

        【A-Z】代表A到Z之间的字母

        【A-z】代表A到z之间的字母

正则的细节

1.正则定义的区别

        Var reg=/hello/g

        Var reg=new RegExp(“hello”,”g”)

前者里面不能引用变量，后者可以。

2.reg=/^\d$/

        因为^和$符不占用位置，也就说明开头和结尾之间夹了一个数字，表达一个纯数字

3././和/\./的区别

        [.],[+]的意思
        .和+在正则里是特殊的元字符，.是任意字符+是量词代表多个。

        在[]里面的.和+就是这个字符本身，没有意义。

|代表或
        [2|3] 代表2或3

        [12|34] 代表1或2|3或4，或者12，34 往往这种情况需要分组()

        [(12|34)]

        [xyz]

-的问题

        /-/ 就是-本身。没有意义，如果写在[0-9]代表0到9之间的任意一个数字。

## 量词

        ? 零次或一次
        + 一次或多次
        * 零次或多次
        {n} 出现n次
        {n,m} 出现m到n次
        {n,} 至少出现n次

## 贪婪模式
        '12345678'.replace(/\d{3,6}/g,'X')
        //匹配最大次

## 非贪婪模式
        在量词后面加?
        '12345678'。replace(/\d{3,6}?/g,'X')
        //匹配最少次

## 分组
        ()可以达到分组的功能，使量词作用于分组
        'a1b2c3d4'.replace(/([a-z]\d){3}/g,'X')

反向引用

        '2020-12-22'.replace(/(\d{4})-(\d{2})-(\d{2})/g,'$1$2$3')
# 二、 JSON
JSON的全称是”JavaScript Object Notation”，意思是JavaScript对象表示法，它是一种基于文本，独立于语言的轻量级数据交换格式。XML也是一种数据交换格式，为什么没 有选择XML呢？因为XML虽然可以作为跨平台的数据交换格式，但是在JS(JavaScript的简写)中处理XML非常不方便，同时XML标记比数据 多，增加了交换产生的流量，而JSON没有附加的任何标记，在JS中可作为对象处理，所以我们更倾向于选择JSON来交换数据

在JSON中，一共就这么几种数据类型：

        number：和JavaScript的number完全一致；
        boolean：就是JavaScript的true或false；
        string：就是JavaScript的string；
        null：就是JavaScript的null；
        array：就是JavaScript的Array表示方式——[]；
        object：就是JavaScript的{ ... }表示方式。
#### JSON的字符串规定必须用双引号""，Object的键也必须用双引号""。
## 序列化
把小明这个对象序列化成JSON格式的字符串：

        var xiaoming = {
            name: '小明',
            age: 14,
            gender: true,
            height: 1.65,
            grade: null,
            'middle-school': '\"W3C\" Middle School',
            skills: ['JavaScript', 'Java', 'Python', 'Lisp']
        };
        var s = JSON.stringify(xiaoming,null,' ');
        console.log(s);
运行结果：

        {
        "name": "小明",
        "age": 14,
        "gender": true,
        "height": 1.65,
        "grade": null,
        "middle-school": "\"W3C\" Middle School",
        "skills": [
        "JavaScript",
        "Java",
        "Python",
        "Lisp"
        ]
        }   
还可以筛选对象的键值，输出指定的属性：

        var xiaoming = {
            name: '小明',
            age: 14,
            gender: true,
            height: 1.65,
            grade: null,
            'middle-school': '\"W3C\" Middle School',
            skills: ['JavaScript', 'Java', 'Python', 'Lisp']
        };
        var s = JSON.stringify(xiaoming,['name','height','grade'],' ');
        console.log(s);
运行结果：

        {
        "name": "小明",
        "height": 1.65,
        "grade": null
        }
```
function convert(key, value) {
    if (typeof value === 'string') {
        return value.toUpperCase();
    }
    return value;
}

JSON.stringify(xiaoming, convert, '  ');
```
上面的代码把所有属性值都变成大写

举个例子：

        var xiaoming = {
            name: '小明',
            age: 14,
            gender: true,
            height: 1.65,
            grade: null,
            'middle-school': '\"W3C\" Middle School',
            skills: ['JavaScript', 'Java', 'Python', 'Lisp']
        };

        function convert(key, value) {
            if (typeof value === 'string') {
                return value.toUpperCase();
            }
            return value;
        }

        var t=JSON.stringify(xiaoming, convert, '  ');
        console.log(t);

        var s = JSON.stringify(xiaoming,null,' ');
        console.log(s);
运行结果：

![当图片无法显示，展示的文本内容](./img/imgs68.png)
## 反序列化
        console.log(JSON.parse('[1,2,3,true]')); // [1, 2, 3, true]
        console.log(JSON.parse('{"name":"小明","age":14}')); // Object {name: '小明', age: 14}
        console.log(JSON.parse('true')); // true
        console.log(JSON.parse('123.45')); // 123.45

JSON.parse()还可以接收一个函数，用来转换解析出的属性：

        var obj = JSON.parse('{"name":"小明","age":14}', function (key, value) {
            if (key === 'name') {
                return value + '同学';
            }
            return value;
        });
        console.log(JSON.stringify(obj)); // {name: '小明同学', age: 14}
# 三、 面向对象编程
面向对象的两个基本概念：
+ 类：类是对象的类型模板，例如，定义Student类来表示学生，类本身是一种类型，Student表示学生类型，但不表示任何具体的某个学生；

+ 实例：实例是根据类创建的对象，例如，根据Student类可以创建出xiaoming、xiaohong、xiaojun等多个实例，每个实例表示一个具体的学生，他们全都属于Student类型。

所以，类和实例是大多数面向对象编程语言的基本概念。

JavaScript不区分类和实例的概念，而是通过原型（prototype）来实现面向对象编程

举个例子：

        var Student = {
            name: 'Robot',
            age:20,
            height: 1.72,
            abc: function () {
                console.log(this.name + ' is 憨批...');
            }
        };

        var rong={
            name:'陈怡榕',
            
        }

        rong.__proto__=Student;

        console.log(rong.abc());
        console.log(rong.__proto__);
运行结果：

![当图片无法显示，展示的文本内容](./img/imgs69.png)
#### 上述代码仅用于演示目的。在编写JavaScript代码时，不要直接用obj.__proto__去改变一个对象的原型