# 第一次课堂笔记🙃
## 小小的纪念一下( •̀ ω •́ )✧

# 今日份课堂笔记整理：
## 一、git提交合并时会产生合并冲突的情况
    1.两个人对同一项目的不同文件进行了修改☑️
    2.两个人对同一项目的同一文件的不同区域进行了修改☑️
    3.对同一项目的同一文件的同一区域进行了不同修改❌
比如，a用户把文件改名为a.c，b用户把同一个文件改名为b.c，那么b将这两个commit合并时，会产生冲突。
    $ git status
      added by us:    b.c
      both deleted:   origin-name.c
      added by them:  a.c
如果最终确定用b.c，那么解决办法如下：
 git rm a.c
 git rm origin-name.c
 git add b.c
 git commit
执行前面两个git rm时，会告警“file-name : needs merge”，可以不必理会。
 
树冲突也可以用git mergetool来解决，但整个解决过程是在交互式问答中完成的，用d 删除不要的文件，用c保留需要的文件。
最后执行git commit提交即可。

    上述三种情况的前两种都可以由Git进行自动合并，而第三种情况无法进行自动合并，需要**手动合并**。
## 为什么需要手动合并？
  当可以自动合并时，说明两个人的修改不会冲突，但是当两个人对同一文件的同一区域进行了修改那么这两个修改就会产生冲突，Git将无法整合这两个修改，因为Git不知道它该保留两个修改中的哪一个（或是要一并保存），这是就需要人工进行手动合并了。
## 二、解决冲突的方法：
  解决方式：rebase，解决冲突，执行add+rebase continue；merge，解决冲突，重新提交
## rebase的冲突解决过程
就是解决每个应用补丁冲突的过程。
解决完一个补丁应用的冲突后，执行下面命令标记冲突已解决（也就是把修改内容加入缓存）：
git add -u
注：-u 表示把所有已track的文件的新的修改加入缓存，但不加入新的文件。
 
然后执行下面命令继续rebase：
git rebase --continue
有冲突继续解决，重复这这些步骤，直到rebase完成。
 
如果中间遇到某个补丁不需要应用，可以用下面命令忽略：
git rebase --skip
 
如果想回到rebase执行之前的状态，可以执行：
git rebase --abort
 
注：rebase之后，不需要执行commit，也不存在新的修改需要提交，都是git自动完成。
## 三、Lambda表达式
```
Func<int,int,int> al = (a,b) => a+b;
```
## 四、委托
```
delegate void Say();

function void SayHello(){

}

Say s= SayHello;

s();
```
![当图片无法显示，展示的文本内容](./img/imgs.jpg)
![当图片无法显示，展示的文本内容](./img/imgs2.png)
![当图片无法显示，展示的文本内容](./img/imgs3.png)