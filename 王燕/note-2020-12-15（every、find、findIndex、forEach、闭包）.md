## 第十七次笔记
## 放假倒计时25天
今天一天肚子都不舒服，估计是昨天晚上吃火锅吃太好了，我的胃承受不住，没有享福的命😶
## 高阶函数
### 一、every
every()方法是js中的迭代方法，用于检测数组中的元素是否满足指定条件。

1、依次执行数组元素，如果一个元素不满足条件就返回false，不会继续执行后面的元素判断；所有数组元素都满足条件则返回true。

2、不会改变原数组。

        var arr = ['JiangXi', 'NanChang', 'JinXian'];
        console.log(arr.every(function (s) {
            return s.length > 0;
        })); // true, 因为每个元素都满足s.length>0

        console.log(arr.every(function (s) {
            return s.toLowerCase() === s;
        })); // false, 因为不是每个元素都全部是小写

### 二、find
用法

1、find() 方法返回通过测试（函数内判断）的数组的第一个元素的值。

2、如果没有符合条件的元素返回 undefined

3、find() 对于空数组，函数是不会执行的。

4、find() 并没有改变数组的原始值。

        var arr = ['JangXi', 'nanchang', 'JinXian'];
        console.log(arr.find(function (s) {
            return s.toLowerCase() === s;
        })); // 'nanchang', 因为nanchang全部是小写

        console.log(arr.find(function (s) {
            return s.toUpperCase() === s;
        })); // undefined, 
        
### 三、findIndex
##### 定义和用法
findIndex() 方法返回传入一个测试条件（函数）符合条件的数组第一个元素位置。

findIndex() 方法为数组中的每个元素都调用一次函数执行：

+ 当数组中的元素在测试条件时返回 true 时, findIndex() 返回符合条件的元素的索引位置，之后的值不会再调用执行函数。
+ 如果没有符合条件的元素返回 -1

注意: findIndex() 对于空数组，函数是不会执行的。

注意: findIndex() 并没有改变数组的原始值。

        var arr = ['JangXi', 'nanchang', 'JinXian'];
        console.log(arr.findIndex(function (s) {
            return s.toLowerCase() === s;
        })); // 1

        console.log(arr.findIndex(function (s) {
            return s.toUpperCase() === s;
        })); // -1

### 四、forEach
forEach()和map()类似  

        var array = [1,2,3,4,5,6,7];
        
        array.forEach(e=>{
        console.log(e);
        });
        
        array.forEach(function(e){
        console.log(e);
        });      
    
## 五、闭包
闭包的三个特性:

①函数嵌套函数

②函数内部可以引用函数外部的参数和变量

③参数和变量不会被垃圾回收机制回收

 1、函数作为返回值

 高阶函数除了可以接受函数作为参数外，还可以把函数作为结果值返回。

比如：

        var arr = [1,2,3,4,5,6,7];
        
        function sum(arr) {
            return arr.reduce(function (x, y) {
                return x + y;
            });
        }
        console.log(sum(arr)); //28
这是一个Array求和，如果不需要立刻求和，可以不返回求和的结果，而是返回求和的函数！👇👇

        let arr = [1,2,3,4,5,6,7];
        
        function lazy_sum(arr) {
        return function sum(arr){
            return arr.reduce(function(x,y){
                return x+y;
            });
        }
        }

        let fn=lazy_sum(arr);
        console.log(fn);
结果如下：

![当图片无法显示，展示的文本内容](./img/imgs65.png)

        let arr = [1,2,3,4,5,6,7];
        
        function lazy_sum(arr) {
        return function sum(){
            return arr.reduce(function(x,y){
                return x+y;
            });
        }
        }

        let fn=lazy_sum(arr);
        console.log(fn);
        console.log(fn()); //28,这才是求和

当调用lazy_sum()时，每次调用都会返回一个新的函数，即使传入相同的参数：

        let arr = [1,2,3,4,5,6,7];
        
        function lazy_sum(arr) {
        return function sum(){
            return arr.reduce(function(x,y){
                return x+y;
            });
        }
        }

        let fn=lazy_sum(arr);
        let fn1=lazy_sum(arr);

        console.log(fn===fn1); // flase

若返回的函数并没有立刻执行，而是直到调用了f()才执行

        function count() {
            var arr = [];
            for (var i=1; i<=3; i++) {
                arr.push(function () {
                    return i * i;
                });
            }
            return arr;
        }

        var results = count();
        var f1 = results[0];
        var f2 = results[1];
        var f3 = results[2];

        console.log(f1()); //16
        console.log(f2()); //16
        console.log(f3()); //16

以上本来是1，4，9，结果却是16，若要是1，4，9，则可以：

     function count() {
            var arr = [];
            for (let i=1; i<=3; i++) {
                arr.push(function () {
                    return i * i;
                });
            }
            return arr;
        }

        var results = count();
        var f1 = results[0];
        var f2 = results[1];
        var f3 = results[2];

        console.log(f1()); //1
        console.log(f2()); //4
        console.log(f3()); //9

