# 都说今天降温，还以为有多冷呢，结果也不过如此🤔
## jQuery
jQuery 极大地简化了 JavaScript 编程。

jQuery是一个轻量级的"写的少，做的多"的JavaScript库。

jQuery库包含以下功能：
+ HTML 元素选取
+ HTML 元素操作
+ CSS 操作
+ HTML 事件函数
+ JavaScript 特效和动画
+ HTML DOM 遍历和修改
+ AJAX
+ Utilities
#### 除此之外，Jquery还提供了大量的插件。
## 下载jQuery
下载网址：https://jquery.com/download/

有两个版本的 jQuery 可供下载：
+ Production version - 用于实际的网站中，已被精简和压缩。
+ Development version - 用于测试和开发（未压缩，是可读的代码）

以上两个版本都可以从 jquery.com 中下载。
## jQuery的使用
jQuery 语法是通过选取 HTML 元素，并对选取的元素执行某些操作。

基础语法： $.action()
+ 美元符号定义 jQuery
+ 选择符（selector）"查询"和"查找" HTML 元素
+ jQuery 的 action() 执行对元素的操作

实例:
```
$(this).hide() - 隐藏当前元素

$("p").hide() - 隐藏所有 <p> 元素

$("p.test").hide() - 隐藏所有 class="test" 的 <p> 元素

$("#test").hide() - 隐藏 id="test" 的元素
```
## 选择器
一、基本选择器

基本选择器是jQuery中最常用也是最简单的选择器，它通过元素的id、class和标签名等来查找DOM元素。
```
1、ID选择器 #id
描述：根据给定的id匹配一个元素， 返回单个元素（注：在网页中，id名称不能重复）
示例：$("#test") 选取 id 为 test 的元素
2、类选择器 .class
描述：根据给定的类名匹配元素，返回元素集合
示例：$(".test") 选取所有class为test的元素
3、元素选择器 element
描述：根据给定的元素名匹配元素，返回元素集合
示例：$("p") 选取所有的<p>元素
4、*
描述：匹配所有元素，返回元素集合
示例：$("*") 选取所有的元素
5、selector1，selector2,...,selectorN
描述：将每个选择器匹配到的元素合并后一起返回，返回合并后的元素集合
示例：$("p,span,p.myClass") 选取所有<p>,<span>和class为myClass的<p>标签的元素集合
```
### 按ID查找：
        // 查找<div id="abc">:
        var div = $('#abc');
### 按tag查找：
        var ps = $('p'); // 返回所有<p>节点
        ps.length; // 数一数页面有多少个<p>节点

### 按class查找：
        var a = $('.red'); // 所有节点包含`class="red"`都将返回
        // 例如:
        // <div class="red">...</div>
        // <p class="green red">...</p>
### 按属性查找
        var email = $('[name=email]'); // 找出<??? name="email">
        var passwordInput = $('[type=password]'); // 找出<??? type="password">
        var a = $('[items="A B"]'); // 找出<??? items="A B">
### 多项选择器
多项选择器就是把多个选择器用,组合起来一块选：
```
$('p,div'); // 把<p>和<div>都选出来
$('p.red,p.green'); // 把<p class="red">和<p class="green">都选出来
```
要注意的是，选出来的元素是按照它们在HTML中出现的顺序排列的，而且不会有重复元素。例如，`<p class="red green">`不会被上面的`$('p.red,p.green')`选择两次。
## 层级选择器
```
1、后代选择器
示例：$("p span") 选取<p>元素里的所有的<span>元素（注：后代选择器选择父元素所有指定选择的元素，不管是儿子级，还是孙子级）
2、子选择器 $("parent>child")
示例：$("p>span") 选择<p>元素下的所有<span>元素 （注：子选择器只选择直属于父元素的子元素）
3、同辈选择器 $("prev+next")
描述：选取紧接在prev元素后的next元素，返回元素集合
示例：$(".one+p") 选取class为one的下一个<p>同辈元素集合
4、同辈选择器 $("prev~siblings")
描述：选取prev元素后的所有siblings元素，返回元素集合
示例：$("#two~p")选取id为two的元素后所有<p>同辈元素集合
```
### 过滤器（Filter）
```
$('ul.lang li'); // 选出JavaScript、Python和Lua 3个节点

$('ul.lang li:first-child'); // 仅选出JavaScript
$('ul.lang li:last-child'); // 仅选出Lua
$('ul.lang li:nth-child(2)'); // 选出第N个元素，N从1开始
$('ul.lang li:nth-child(even)'); // 选出序号为偶数的元素
$('ul.lang li:nth-child(odd)'); // 选出序号为奇数的元素
```
### 表单选择器（返回元素集合，使用相似）
```
1、:text
描述：选择所有的单行文本框
示例：$(":text")选取所有的单行文本框
2、:password
描述：选择所有的密码框
3、:button
描述：选择所有的按钮
4、:checkbox
描述：选择所有的多选框
````