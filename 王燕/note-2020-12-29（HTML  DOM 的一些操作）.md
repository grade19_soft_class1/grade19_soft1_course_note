# 今天好热啊！差点中暑！😓
## 操作表单
HTML表单的输入控件主要有以下几种：

+ 文本框，对应的`<input type="text">`，用于输入文本；

+ 口令框，对应的`<input type="password">`，用于输入口令；

+ 单选框，对应的`<input type="radio">`，用于选择一项；

+ 复选框，对应的`<input type="checkbox">`，用于选择多项；

+ 下拉框，对应的`<select>`，用于选择一项；

+ 隐藏文本，对应的`<input type="hidden">`，用户不可见，但表单提交时会把隐藏文本发送到服务器。

栗子：

        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>就这？</title>
        
        </head>
        <body>
            <form name="myForm" action="/action_page_post.php" onsubmit="return validateForm()" method="post">
                姓名：<input type="text" name="fname">
                <input type="submit" value="提交">
                </form>
        </body>
        </html>
结果为：

![当图片无法显示，展示的文本内容](./img/imgs77.png)

练习一下：

        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>就这？</title>
        
        </head>
        <body>
            <p>请输入 1 与 10 之间的数：</p>

            <input id="numb">
            
            <button type="button" onclick="myFunction()">提交</button>
            
            <p id="demo"></p>
            
            <script>
            function myFunction() {
            var x, text;
            
            // 获取 id="numb" 的输入字段的值
            x = document.getElementById("numb").value;
            
            // 如果 x 不是数字或小于 1 或大于 10
            if (isNaN(x) || x < 1 || x > 10) {
                text = "输入无效";
            } else {
                text = "输入有效";
            }
            document.getElementById("demo").innerHTML = text;
            }
            </script>
        </body>
        </html>
如下：

![当图片无法显示，展示的文本内容](./img/imgs78.png)
## 操作文件
在HTML表单中，可以上传文件的唯一控件就是`<input type="file">`。

注意：当一个表单包含`<input type="file">`时，表单的enctype必须指定为`multipart/form-data`，method必须指定为post，浏览器才能正确编码并以multipart/form-data格式发送表单的数据。

出于安全考虑，浏览器只允许用户点击`<input type="file">`来选择本地文件，用JavaScript对`<input type="file">`的value赋值是没有任何效果的。当用户选择了上传某个文件后，JavaScript也无法获得该文件的真实路径：

<input type="file"
### 待上传文件
通常，上传的文件都由后台服务器处理，JavaScript可以在提交表单时对文件扩展名做检查，以便防止用户上传无效格式的文件：
```
var f = document.getElementById('test-file-upload');
var filename = f.value; // 'C:\fakepath\test.png'
if (!filename || !(filename.endsWith('.jpg') || filename.endsWith('.png') || filename.endsWith('.gif'))) {
    alert('Can only upload image file.');
    return false;
}
```
练习一下：

<div name="single" style="text-align: center; ">
        <A class=btn_addPic href="javascript:void(0);"><SPAN>选择文件</SPAN>
    	   <input  id="file"  class="filePrew" type="file" name="file" multiple="multiple" οnchange="javascript:onc();" />
        </A>
    </div>  
 
    <div name="dropbox" id="dropbox" style="font-size:30px;color:#333333;background-color:#888888;min-width:30px;min-height:10px;border:3px dashed silver;">
        <p style="line-height: 100px;">拖拽文件上传</p>
    </div>  
 
    <div style="text-align: center; ">
        <button class="button" type="submit" οnclick="javascript:Upload();">上传</button>
    </div>  
 
    <div id="fileliststring" style="background-color:#cccccc;  color:#333333; ">
    </div>
结果如下：

![当图片无法显示，展示的文本内容](./img/imgs79.png)

