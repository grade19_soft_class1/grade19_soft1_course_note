# 今日份笔记
# DOM
## 一、DOM 节点
在 HTML DOM 中，所有事物都是节点。DOM 是被视为节点树的 HTML。

根据 W3C 的 HTML DOM 标准，HTML 文档中的所有内容都是节点：
+ 整个文档是一个文档节点
+ 每个 HTML 元素是元素节点
+ HTML 元素内的文本是文本节点
+ 每个 HTML 属性是属性节点
+ 注释是注释节点
## 二、HTML DOM 节点树
![当图片无法显示，展示的文本内容](./img/imgs74.png)

通过 HTML DOM，树中的所有节点均可通过 JavaScript 进行访问。所有 HTML 元素（节点）均可被修改，也可以创建或删除节点。

栗子：
```
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <script>
        function changeImage()
        {
            element=document.getElementById('myimage')
            if (element.src.match("bulbon"))
            {
                element.src="./img.12.png";
            }
            else
            {
                element.src="./img.11.png";
            }
        }
        </script>
        <img id="myimage" onclick="changeImage()" border="0" src="./img.12.png" width="100" height="180">
        <p>点击灯泡 开/关 灯</p>
</body>
</html>
```
运行结果：

![当图片无法显示，展示的文本内容](./img/imgs75.png)
![当图片无法显示，展示的文本内容](./img/imgs76.png)
## 三、节点父、子和同胞
节点树中的节点彼此拥有层级关系。

父、子和同胞等术语用于描述这些关系。父节点拥有子节点。同级的子节点被称为同胞（兄弟或姐妹）。
+ 在节点树中，顶端节点被称为根（root）
+ 每个节点都有父节点、除了根（它没有父节点）
+ 一个节点可拥有任意数量的子
+ 同胞是拥有相同父节点的节点
## 四、关于DOM的一些方法
#### 返回ID为'test'的节点：
        var test = document.getElementById('test');
#### 先定位ID为'test-table'的节点，再返回其内部所有tr节点：
        var trs = document.getElementById('test-table').getElementsByTagName('tr');
#### 'test-div'的节点，再返回其内部所有class包含red的节点：
        var reds = document.getElementById('test-div').getElementsByClassName('red');
#### 获取节点test下的所有直属子节点:
        var cs = test.children;

#### 获取节点test下第一个、最后一个子节点：
        var first = test.firstElementChild;
        var last = test.lastElementChild;

第二种方法是使用querySelector()和querySelectorAll()，需要了解selector语法，然后使用条件来获取节点，更加方便：
#### 通过querySelector获取ID为q1的节点：
        var q1 = document.querySelector('#q1');
#### 通过querySelectorAll获取q1节点内的符合条件的所有节点：
        var ps = q1.querySelectorAll('div.highlighted > p');
# 浏览器DOM的操作
## 五、更新DOM
修改节点有两种方式：

1、修改innerHTML属性
```
// 获取<p id="p-id">...</p>
var p = document.getElementById('p-id');
// 设置文本为abc:
p.innerHTML = 'ABC'; // <p id="p-id">ABC</p>
// 设置HTML:
p.innerHTML = 'ABC <span style="color:red">RED</span> XYZ';
// <p>...</p>的内部结构已修改
```
2、修改innerText或textContent属性
```
// 获取<p id="p-id">...</p>
var p = document.getElementById('p-id');
// 设置文本:
p.innerText = '<script>alert("Hi")</script>';
// HTML被自动编码，无法设置一个<script>节点:
// <p id="p-id">&lt;script&gt;alert("Hi")&lt;/script&gt;</p>
```
#### 两者的区别在于读取属性时，innerText不返回隐藏元素的文本，而textContent返回所有文本。
## 六、插入DOM
使用appendChild，把一个子节点添加到父节点的最后一个子节点：
```
<!-- HTML结构 -->
<p id="js">JavaScript</p>
<div id="list">
    <p id="java">Java</p>
    <p id="python">Python</p>
    <p id="scheme">Scheme</p>
</div>
```
把`<p id="js">JavaScript</p>添加到<div id="list">`的最后一项：
```
var
    js = document.getElementById('js'),
    list = document.getElementById('list');
list.appendChild(js);
```
现在，HTML结构变成了这样：
```
<!-- HTML结构 -->
<div id="list">
    <p id="java">Java</p>
    <p id="python">Python</p>
    <p id="scheme">Scheme</p>
    <p id="js">JavaScript</p>
</div>
```
因为插入的js节点已经存在于当前的文档树，因此这个节点首先会从原先的位置删除，再插入到新的位置。
## 七、删除DOM
要删除一个节点，首先要获得该节点本身以及它的父节点，然后，调用父节点的removeChild把自己删掉：
```
// 拿到待删除节点:
var self = document.getElementById('to-be-removed');
// 拿到父节点:
var parent = self.parentElement;
// 删除:
var removed = parent.removeChild(self);
removed === self; // true
```
栗子

删除已有的 HTML代码 元素：
```
<div id="div1">
<p id="p1">这是一个段落。</p>
<p id="p2">这是另一个段落。</p>
</div>
```
```
<script>
var parent=document.getElementById("div1");
var child=document.getElementById("p1");
parent.removeChild(child);
</script>
```