# 今天是最后一堂讲C#的课了
## 淦٩(๑`^´๑)۶
## 回顾一下今天都讲了些啥东东
  ### 1. 递归：就是在过程或函数里调用自身
  练习：使用递归完成斐波那契数列，要求输入指定的个数，输出指定个数的数列
![当图片无法显示，展示的文本内容](./img/imgs5.png)
 ### 2. 字符串：太多了，还是直接做练习吧
  练习1：输入一串字符串，将字符串反转输出（打印）
     
    public static void main(String[] args) 
    
    {
        System.out.print("请输入一个字符串(please input a string)：");

        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();//scanner.nextLine()
        System.out.println("逆转前(before reverse):"+line);
        String result = "";
        for (int i=line.length()-1;i>=0;i--)
        {
            result = result + line.charAt(i);//result += line.charAt(i);
        }
        System.out.println("逆转后(after reverse)"+result);
    }


  练习2：输入一串字符串（英文语句），将每个单词反转输出； 如：输入hello world 输出 ollhe dlrow  
      public static void main(String[] args) 
      {
        
        Scanner scan= new Scanner(System.in);
        System.out.println("请输入需要反转的字符串：");
        String str=scan.next();
        char []c=new char[str.length()]; 
        for(int i=0;i<str.length();i++) 
            c[i]=str.charAt(str.length()-1-i); 
        System.out.println("反转后的字符串为："+String.valueOf(c)); 
    }


  练习3：输入一个身份证号，输出这个身份证号的出身日期，并判断其生日是否已经过完；

         public static void main(String[] args) 
        {

            console.WriteLine("请输入身份证号");
            string s = Console.ReadLine();
            string jqn = s.Substring(6, 4);
            string jqy = s.Substring(10, 2);
            string jqr = s.Substring(12, 2);
            Console.WriteLine("你的生日是：{0}年{1}月{2}日", jqn, jqy, jqr);
            int t = int.Parse(jqn);
            Console.WriteLine("请输入今年的年份");
            int j = int.Parse(Console.ReadLine());
            int sh = j - t;
            Console.WriteLine("你今年{0}岁了", sh);
    }  
      
练习4：输入电子邮箱地址，判断其是否为合法的地址； 如:abc@abc.com 为合法 abc@@.abc.com为不合法

	  public static void main(String[] args)
    {

		String regex="\\w+@\\w+(\\.\\w{2,3})*\\.\\w{2,3}";

		String str1="abc@@.abc.com";
	
		String str2="abc@abc.com";
		
		if(str1.matches(regex))
    {   
			System.out.println(str1+"是一个合法的E_mail地址");
		}
		if(str2.matches(regex))
    {
			System.out.println(str2+"是一个合法的E_mail地址");
		}
		
	}

### 4.日期时间:
  DateTime 类提供了 12 个构造方法来创建该类的实例，但经常使用不带参数的方法创建 DateTime 类的实例。
  在 DateTime 类中提供了常用的属性和方 法用于获取或设置日期和时间，如下表所示。

    方法       描述
    Date    获取实例的日期部分
    Day     获取该实例所表示的日期是一个月的第几天
    DayOfWeek 获取该实例所表示的日期是一周的星期几
    DayOfYear  获取该实例所表示的日期是一年的第几天
    Add(Timespan value)  在指定的日期实例上添加时间间隔值 value
    AddDays(double value)  在指定的日期实例上添加指定天数 value
    AddHours(double value)  在指定的日期实例上添加指定的小时数 value
    AddMinutes(double value)  在指定的日期实例上添加指定的分钟数 value
    AddSeconds(double value)  在指定的日期实例上添加指定的秒数 value
    AddMonths(int value)  在指定的日期实例上添加指定的月份 value
    AddYears (int value)  在指定的日期实例上添加指定的年份 value
  下面通过实例来演示 DateTime 类的使用。
  【实例】使用 DateTime 类获取当前时间，分别输出该日是当月的第几天、星期几以 及一年中的第几天，并计算 30 天后的日期。

     class Program
    {
      static void Main(string[] args)
      {
        DateTime dt = DateTime.Now;
        Console.WriteLine("当前日期为：{0}", dt);
        Console.WriteLine("当前时本月的第{0}天", dt.Day);
        Console.WriteLine("当前是：{0}", dt.DayOfWeek);
        Console.WriteLine("当前是本年度第{0}天", dt.DayOfYear);
        Console.WriteLine("30 天后的日期是{0}", dt.AddDays(30));
      }
     }
    
  ### 5.数据类型转换：

  【实例】使用 Convert 方法将分别将一个浮点型数据转换成整型和字符串型。

       class Program
     {
      static void Main(string[] args)
      {
         float num1 = 82.26f;
         int integer;
         string str;
         integer = Convert.ToInt32(num1);
         str = Convert.ToString(num1);
         Console.WriteLine("转换为整型数据的值{0}", integer);
         Console.WriteLine("转换为字符串{0},",str);
      }
     }
