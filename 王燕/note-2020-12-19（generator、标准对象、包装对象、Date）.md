## 今天心情不是很美丽，因为榕榕洗了头
## generator
### 定义
generator（生成器）跟函数很像

        function* foo(x) {
            yield x + 1;
            yield x + 2;
            return x + 3;
        }
        let p =foo(1);
        console.log(p);

        for(let item of p){
            console.log(item);
        }
运行结果：

![当图片无法显示，展示的文本内容](./img/imgs67.png)

generator和函数不同的是，generator由function*定义（注意多出的*号），并且，除了return语句，还可以用yield返回多次。
## 标准对象

在JavaScript的世界里，一切都是对象。

但是某些对象还是和其他对象不太一样。为了区分对象的类型，我们用typeof操作符获取对象的类型，它总是返回一个字符串：
```
typeof 123; // 'number'
typeof NaN; // 'number'
typeof 'str'; // 'string'
typeof true; // 'boolean'
typeof undefined; // 'undefined'
typeof Math.abs; // 'function'
typeof null; // 'object'
typeof []; // 'object'
typeof {}; // 'object'
```
可见，number、string、boolean、function和undefined有别于其他类型。特别注意null的类型是object，Array的类型也是object，如果我们用typeof将无法区分出null、Array和通常意义上的object——{}。

### 包装对象
number、boolean和string都有包装对象，字符串也区分string类型和它的包装类型。包装对象用new创建：
```
var n = new Number(123); // 123,生成了新的包装类型
var b = new Boolean(true); // true,生成了新的包装类型
var s = new String('str'); // 'str',生成了新的包装类型
```
虽然包装对象看上去和原来的值一模一样，显示出来也是一模一样，但他们的类型已经变为object了！所以，包装对象和原始值用===比较会返回false：
```
typeof new Number(123); // 'object'
new Number(123) === 123; // false

typeof new Boolean(true); // 'object'
new Boolean(true) === true; // false

typeof new String('str'); // 'object'
new String('str') === 'str'; // false
```
胡哥说这个有可能在有生之年都不会用到，为了体现我的专业，我还是写了

但下面的几条规则胡哥说要背下来🙃
+ 不要使用new Number()、new Boolean()、new String()创建包装对象；

+ 用parseInt()或parseFloat()来转换任意类型到number；

+ 用String()来转换任意类型到string，或者直接调用某个对象的toString()方法；

+ 通常不必把任意类型转换为boolean再判断，因为可以直接写if (myVar) {...}；

+ typeof操作符可以判断出number、boolean、string、function和undefined；

+ 判断Array要使用Array.isArray(arr)；

+ 判断null请使用myVar === null；

+ 判断某个全局变量是否存在用typeof window.myVar === 'undefined'；

+ 函数内部判断某个变量是否存在用typeof myVar === 'undefined'。
## Date
Date 对象用于处理日期和时间。

创建 Date 对象的语法：

        var myDate=new Date()
        
注释：Date 对象会自动把当前日期和时间保存为其初始值。
### Date 对象方法
        方法	                    描述
        Date()	                返回当日的日期和时间。
        getDate()	            从 Date 对象返回一个月中的某一天 (1 ~ 31)。
        getDay()	            从 Date 对象返回一周中的某一天 (0 ~ 6)。
        getMonth()	            从 Date 对象返回月份 (0 ~ 11)。
        getFullYear()	        从 Date 对象以四位数字返回年份。
        getYear()	            请使用 getFullYear() 方法代替。
        getHours()	            返回 Date 对象的小时 (0 ~ 23)。
        getMinutes()	        返回 Date 对象的分钟 (0 ~ 59)。
        getSeconds()	        返回 Date 对象的秒数 (0 ~ 59)。
        getMilliseconds()	    返回 Date 对象的毫秒(0 ~ 999)。
        getTime()	            返回 1970 年 1 月 1 日至今的毫秒数。
        getTimezoneOffset()	    返回本地时间与格林威治标准时间 (GMT) 的分钟差。
        getUTCDate()	        根据世界时从 Date 对象返回月中的一天 (1 ~ 31)。
        getUTCDay()     	    根据世界时从 Date 对象返回周中的一天 (0 ~ 6)。
        getUTCMonth()	        根据世界时从 Date 对象返回月份 (0 ~ 11)。
        getUTCFullYear()	    根据世界时从 Date 对象返回四位数的年份。
        getUTCHours()	        根据世界时返回 Date 对象的小时 (0 ~ 23)。
        getUTCMinutes() 	    根据世界时返回 Date 对象的分钟 (0 ~ 59)。
        getUTCSeconds()	        根据世界时返回 Date 对象的秒钟 (0 ~ 59)。
        getUTCMilliseconds()	根据世界时返回 Date 对象的毫秒(0 ~ 999)。
        parse()	                返回1970年1月1日午夜到指定日期（字符串）的毫秒数。
        setDate()	            设置 Date 对象中月的某一天 (1 ~ 31)。
        setMonth()	            设置 Date 对象中月份 (0 ~ 11)。
        setFullYear()	        设置 Date 对象中的年份（四位数字）。
        setYear()	            请使用 setFullYear() 方法代替。
        setHours()	            设置 Date 对象中的小时 (0 ~ 23)。
        setMinutes()	        设置 Date 对象中的分钟 (0 ~ 59)。
        setSeconds()	        设置 Date 对象中的秒钟 (0 ~ 59)。
        setMilliseconds()	    设置 Date 对象中的毫秒 (0 ~ 999)。
        setTime()	            以毫秒设置 Date 对象。
        setUTCDate()	        根据世界时设置 Date 对象中月份的一天 (1 ~ 31)。
        setUTCMonth()	        根据世界时设置 Date 对象中的月份 (0 ~ 11)。
        setUTCFullYear()	    根据世界时设置 Date 对象中的年份（四位数字）。
        setUTCHours()	        根据世界时设置 Date 对象中的小时 (0 ~ 23)。
        setUTCMinutes()	        根据世界时设置 Date 对象中的分钟 (0 ~ 59)。
        setUTCSeconds()	        根据世界时设置 Date 对象中的秒钟 (0 ~ 59)。
        setUTCMilliseconds()	根据世界时设置 Date 对象中的毫秒 (0 ~ 999)。
        toSource()	            返回该对象的源代码。
        toString()	            把 Date 对象转换为字符串。
        toTimeString()	        把 Date 对象的时间部分转换为字符串。
        toDateString()	        把 Date 对象的日期部分转换为字符串。
        toGMTString()	        请使用 toUTCString() 方法代替。
        toUTCString()	        根据世界时，把 Date 对象转换为字符串。
        toLocaleString()	    根据本地时间格式，把 Date 对象转换为字符串。
        toLocaleTimeString()	根据本地时间格式，把 Date 对象的时间部分转换为字符串。
        toLocaleDateString()	根据本地时间格式，把 Date 对象的日期部分转换为字符串。
        UTC()	                根据世界时返回 1970 年 1 月 1 日 到指定日期的毫秒数。
        valueOf()	            返回 Date 对象的原始值。

如果new Date()里面没有传任何参数，也没有用任何set系列方法，那么就指的是当前的值(本地计算时钟)，包括时分秒。js中可以很容易用这个特征来以任何形式显示当前时间。

        var date = new Date(),
            nowYear = date.getFullYear(),
            nowMonth = date.getMonth() + 1,  //注意getMonth从0开始，getDay()也是(此时0代表星期日)
            nowDay = date.getDate(),
            nowHour = date.getHours(),
            nowMinute = date.getMinutes(),
            nowSecond = date.getSeconds(),
            weekday = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
            nowWeek = weekday[date.getDay()];
        console.log(nowYear + '年' + nowMonth + '月' + nowDay + '日' + nowHour + '时' + nowMinute + '分' + nowSecond + '秒' + nowWeek);
运行结果：

        2020年12月19日17时9分2秒星期六