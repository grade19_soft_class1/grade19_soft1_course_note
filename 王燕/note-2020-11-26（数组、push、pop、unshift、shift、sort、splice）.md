### 数组
数组是一种特殊的变量，可以用一个单一的名称存放很多值，并且还可以通过引用索引号来访问这些值。

### push和pop
push() 方法（在数组结尾处）向数组添加一个新的元素：

![当图片无法显示，展示的文本内容](./img/imgs17.png)

结果为：

![当图片无法显示，展示的文本内容](./img/imgs18.png)

pop() 方法从数组中删除最后一个元素：

![当图片无法显示，展示的文本内容](./img/imgs19.png)

![当图片无法显示，展示的文本内容](./img/imgs20.png)

### unshift和shift
unshift() 方法（在开头）向数组添加新元素，并“反向位移”旧元素：

![当图片无法显示，展示的文本内容](./img/imgs21.png)

![当图片无法显示，展示的文本内容](./img/imgs22.png)

shift() 方法会删除首个数组元素，并把所有其他元素“位移”到更低的索引。

![当图片无法显示，展示的文本内容](./img/imgs23.png)

![当图片无法显示，展示的文本内容](./img/imgs24.png)

### sort
没啥好讲的，直接看练习吧！

![当图片无法显示，展示的文本内容](./img/imgs25.png)

![当图片无法显示，展示的文本内容](./img/imgs26.png)

### splice
splice() 方法可用于向数组添加新项也可以用于删除

![当图片无法显示，展示的文本内容](./img/imgs27.png)

![当图片无法显示，展示的文本内容](./img/imgs28.png)


