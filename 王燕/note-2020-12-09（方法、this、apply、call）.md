## 今天两点钟还在修仙，所以上课格外的困，下午还得上课，烦人😶
## 今日份笔记
## 方法
对象的定义

      var person={
          name:'榕榕',
          age：19,
          birth:2001,
          country:'开普勒星球',

      }
接下来，绑定一个函数来返回person的年龄：

       var person={
        name:'榕榕',
        birth:2001,
        country:'开普勒星球',
        age: function(){
       return new Date().getFullYear()-this.birth;
      
         }
      }
    console.log(person.age);
    console.log(person.age());
如果是这样的话，出来的结果就是下面的样子，所以只有第二种方法才能出来

![当图片无法显示，展示的文本内容](./img/imgs55.png)
## this关键词
在 JavaScript 中，被称为 this 的事物，指的是拥有该 JavaScript 代码的对象。

this 的值，在函数中使用时，是“拥有”该函数的对象。

请注意 this 并非变量。它是关键词。无法改变 this 的值。

        function getAge(){
        return new Date().getFullYear()-this.birth;
        }
        var person={
        name:'榕榕', 
        birth:2001,
        country:'开普勒星球',
        age:getAge
      }
    console.log(person.age());//正常
    console.log(getAge()); //报错
![当图片无法显示，展示的文本内容](./img/imgs56.png)

上图告诉我们不能直接引用getAge();不然直接报错
### 内部调用了this，this的指向
如果以对象的方法形式调用，比如person.age()，该函数的this指向被调用的对象

如果单独调用函数，比如getAge()，此时，该函数的this指向全局对象，也就是window。

        var person={
        name:'榕榕',
        birth:2001,
        country:'开普勒星球',
        age: function(){
        return new Date().getFullYear()-this.birth;
  
         }
        }
     var fn=person.age;
     console.log(fn());
如果直接这么写的话也会报错

        var person={
        name:'榕榕', 
         birth:2001,
        country:'开普勒星球',
         age:function (){
        function getFormBirth(){
            return new Date().getFullYear()-that.birth;
        }
        return getFormBirth();
     }
   
    }
    console.log(person.age());
像这样把方法重构一下的话结果还是报错了，这时候就应该用一个that变量首先捕获this

        var person={
         name:'榕榕', 
        birth:2001,
        country:'开普勒星球',
         age:function (){
         var that=this;
            function getFormBirth(){
         console.log(that);
            return new Date().getFullYear()-that.birth;
            }
        return getFormBirth();
         }
        }
        console.log(person.age());
结果就是这样的

![当图片无法显示，展示的文本内容](./img/imgs57.png)
## apply方法
用apply修复getAge()调用：

        function getAge(){
         return new Date().getFullYear()-this.birth;
            }
         var person={
         name:'榕榕', 
         birth:2001,
         country:'开普勒星球',
         age:getAge
            }
         console.log(person.age());
         console.log(getAge.apply(person,[]));
这样也是可以得出结果的

![当图片无法显示，展示的文本内容](./img/imgs58.png)

### apply()和call()的区别：
+ apply()把参数打包成Array再传入；

+ call()把参数按顺序传入。
### 调用Math.max(7, 8, 9)，分别用apply()和call()实现如下：

![当图片无法显示，展示的文本内容](./img/imgs60.png)

![当图片无法显示，展示的文本内容](./img/imgs61.png)