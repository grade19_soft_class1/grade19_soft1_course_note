# 今日份笔记
## 删除DOM

要删除一个节点，首先要获得该节点本身以及它的父节点，然后，调用父节点的removeChild把自己删掉
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="List">
        <p id="java">Java</p>
        <p id="python">Python</p>
        <p id="scheme">Scheme</p>
    </div>
    <script>
        var self=document.getElementById('java');
        var parent=self.parentElement;
        parent.removeChild(self);
    </script>
</body>
</html>
```
注意删除后的节点虽然不在文档树中了，但其实它还在内存中，可以随时再次被添加到别的位置。

遍历一个父节点的子节点并进行删除操作时，要注意，children属性是一个只读属性，并且它在子节点变化时会实时更新。

删除节点通过使用removeChild()方法来实现。
removeChild() 方法用来删除一个子节点。
obj.removeChild(oldChild)
oldChild:表示需要删除的节点
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>删除节点</h1>
    <div id="di">
        <p>第一行文本</p>
        <p>第二行文本</p>
        <p>第三行文本</p>
    </div>
    <form>
        <input type="button" value="删除" onclick="delNode();">
    </form>
    <script src="./js/js.js"></script>
</body>
</html>
```
```
'use strict'

function delNode(){
    var deleteN=document.getElementById('di');
    if(deleteN.hasChildNodes()){
        deleteN.removeChild(deleteN.lastChild);
    }
}
```
![图片无法显示](./imgs/2020-12-29_164124.png)

练习
JavaScript
Swift
HTML
ANSI C
CSS
DirectX
```
<!-- HTML结构 -->
<ul id="test-list">
    <li>JavaScript</li>
    <li>Swift</li>
    <li>HTML</li>
    <li>ANSI C</li>
    <li>CSS</li>
    <li>DirectX</li>
</ul>
```
把与Web开发技术不相关的节点删掉：
```
'use strict';
// TODO
var arr=['JavaScript','HTML','CSS'];
var parent = document.getElementById("test-list");
for(var i of parent.children){
    if(!arr.includes(i.innerText)){
        parent.removeChild(i);
    }
}

// 测试:
;(function () {
    var
        arr, i,
        t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 3) {
        arr = [];
        for (i = 0; i < t.children.length; i ++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['JavaScript', 'HTML', 'CSS'].toString()) {
            console.log('测试通过!');
        }
        else {
            console.log('测试失败: ' + arr.toString());
        }
    }
    else {
        console.log('测试失败!');
    }
})();
```

## 操作表单
用JavaScript操作表单和操作DOM是类似的，因为表单本身也是DOM树。

不过表单的输入框、下拉框等可以接收用户输入，所以用JavaScript来操作表单，可以获得用户输入的内容，或者对一个输入框设置新的内容。

HTML表单的输入控件主要有以下几种：

+ 文本框，对应的`<input type="text">`，用于输入文本；

+ 口令框，对应的`<input type="password">`，用于输入口令；

+ 单选框，对应的`<input type="radio">`，用于选择一项；

+ 复选框，对应的`<input type="checkbox">`，用于选择多项；

+ 下拉框，对应的`<select>`，用于选择一项；

+ 隐藏文本，对应的`<input type="hidden">`，用户不可见，但表单提交时会把隐藏文本发送到服务器。

1. form 对象常用属性
```
属性                描述
action              设置或返回表单的action属性
elements            表示包含表单中所有表单元素的数组，使用索引引用其中的元素
length              返回表单中的表单元素数目
method              设置或返回将数据发送到服务器的HTTP方法
name                设置或返回表单的名称
target              设置或返回表单提交的数据所显示的frame或窗口
onreset             在重置表单元素之前调用事件处理方法
onsubmit            在提交表单之前调用事件处理方法
```
2. form 对象常用方法
```
方法            描述
reset()         把表单的所有输入元素重置为它们的默认值
submit()        提交表单
```
3. 约束验证HTML输入属性
```
属性	        描述
disabled	    规定输入的元素不可用
max	            规定输入元素的最大值
min	            规定输入元素的最小值
pattern	        规定输入元素值的模式
required	    规定输入元素字段是必需的
type 	        规定输入元素的类型
```
4. 约束验证CSS伪类选择器
```
选择器	             描述
:disabled	        选取属性为 "disabled" 属性的 input 元素
:invalid	        选取无效的 input 元素
:optional	        选择没有"required"属性的 input 元素
:required	        选择有"required"属性的 input 元素
:valid	            选取有效值的 input 元素
```
### 1. 获取值
获得了一个`<input>`节点的引用,就可以直接调用value获得对应的用户输入值。
```
// <input type="text" id="email">
var input = document.getElementById('email');
input.value; // '用户输入的值'
```
这种方式可以应用于text、password、hidden以及select。但是对于单选框和复选框，value属性返回的永远是HTML预设的值，而我们需要获得实际是用户是否“勾上了”选项，所以用chencked判断。
```
// <label><input type="radio" name="weekday" id="monday" value="1"> Monday</label>
// <label><input type="radio" name="weekday" id="tuesday" value="2"> Tuesday</label>
var mon = document.getElementById('monday');
var tue = document.getElementById('tuesday');
mon.value; // '1'
tue.value; // '2'
mon.checked; // true或者false
tue.checked; // true或者false
```
### 2. 设置值
设置值和获取值类似，对于text、password、hidden以及select，直接设置value就可以。
```
// <input type="text" id="email">
var input = document.getElementById('email');
input.value = 'test@example.com'; // 文本框的内容已更新
```
对于单选框和复选框，设置checked或false即可。
### 3. HTML5控件
HTML5新增了大量标准控件，常用的包括date、datetime、datetime-local、color等，它们都使用`<input>`标签：
```
<input type="datetime-local" value="2020-12-29T02:15:24">

<input type="color" value="##ff0000">
```
![图片无法显示](./imgs/2020-12-29_172711.png)
### 4. 提交表单
1. submit提交
* submit 按钮式提交
缺点：在提交前不可修改提交的form表单数据
* onsubmit方式提交
优点：在请求提交操作(action)时，可对提交的数据进行处理
2. formData提交
3. 动态添加表单提交, js方式提交
* 动态追加的form表单
* 静态form表单，js方式提交

方式一是通过`<form>`元素`<submit>`方法提交一个表单。例如，响应一个`<button>`的click事件，在JavaScript代码中提交表单：
```
<!-- HTML -->
<form id="test-form">
    <input type="text" name="test">
    <button type="button" onclick="doSubmitForm()">Submit</button>
</form>

<script>
function doSubmitForm() {
    var form = document.getElementById('test-form');
    // 可以在此修改form的input...
    // 提交form:
    form.submit();
}
</script>
```

这种方式的缺点时扰乱了浏览器对form的正常提交。浏览器默认点击`<button type="submit">`时提交表单，或者用户在最后一个输入框按回车键。因此，第二种方式是响应`<form>`本身的onsubmit事件，在提交form时作修改：
```
<!-- HTML -->
<form id="test-form" onsubmit="return checkForm()">
    <input type="text" name="test">
    <button type="submit">Submit</button>
</form>

<script>
function checkForm() {
    var form = document.getElementById('test-form');
    // 可以在此修改form的input...
    // 继续下一步:
    return true;
}
</script>
```

在检查和修改`<input>`时，要充分利用`<input type="hidden">`来传递数据。
## 操作文件
在HTML表单中，可以上传文件的唯一控件就是`<input type="file">`。

注意：当一个表单包含`<input type="file">`时，表单的enctype必须指定为`multipart/form-data`，method必须指定为post，浏览器才能正确编码并以multipart/form-data格式发送表单的数据。

出于安全考虑，浏览器只允许用户点击`<input type="file">`来选择本地文件，用JavaScript对`<input type="file">`的value赋值是没有任何效果的。当用户选择了上传某个文件后，JavaScript也无法获得该文件的真实路径：

<input type="file">

## AJAX
AJAX不是JavaScript的规范，它只是一个哥们“发明”的缩写：Asynchronous JavaScript and XML，意思就是用JavaScript执行异步网络请求。

Web的运作原理：一次HTTP请求对应一个页面。

如果要让用户留在当前页面中，同时发出新的HTTP请求，就必须用JavaScript发送这个新请求，接收到数据后，再用JavaScript更新页面，这样一来，用户就感觉自己仍然停留在当前页面，但是数据却可以不断地更新。

最早大规模使用AJAX的就是Gmail，Gmail的页面在首次加载后，剩下的所有数据都依赖于AJAX来更新。

AJAX = Asynchronous JavaScript and XML（异步的 JavaScript 和 XML）。

AJAX 不是新的编程语言，而是一种使用现有标准的新方法。

AJAX 最大的优点是在不重新加载整个页面的情况下，可以与服务器交换数据并更新部分网页内容。

AJAX 不需要任何浏览器插件，但需要用户允许JavaScript在浏览器上执行。

AJAX 应用
* 运用XHTML+CSS来表达资讯；
* 运用JavaScript操作DOM（Document Object Model）来执行动态效果；
* 运用XML和XSLT操作资料;
* 运用XMLHttpRequest或新的Fetch API与网页服务器进行异步资料交换；
* 注意：AJAX与Flash、Silverlight和Java Applet等RIA技术是有区分的
## Promise
在JavaScript的世界中，所有代码都是单线程执行的。

AJAX就是典型的异步操作。

把回调函数success(request.responseText)和fail(request.status)写到一个AJAX操作里很正常，但是不好看，而且不利于代码复用。

Promise有各种开源实现，在ES6中被统一规范，由浏览器直接支持。先测试一下浏览器。
```
new Promise(function(){});

console.log('支持Promise!');
```
```
function test(reslove,reject) {
    let timeOut=Math.random()*2;
    console.log(timeOut);
    setTimeout(() => {
       if(timeOut<1){
           console.log('延迟时间小于1，执行reslove方法');
           reslove('成功');
       } else{
           console.log('延迟时间大于1，执行reject方法');
           reject('失败')
       }
    }, timeOut*1000);
}

let p=new Promise(test);

p.then((res)=>{
    console.log(res);
}).catch((res)=>{
    console.log(res);
})
```
```
function mulitply(x){
    return new Promise(function(resolve,reject){
        console.log("乘法运算开始，1.5秒后返回结果");
        setTimeout(resolve,1500,x*x);
    });
}

function add(x){
    return new Promise(function(resolve,reject){
        console.log("加法运算开始，0.6秒后返回结果");
        setTimeout(resolve,600,x+x);
    });
}

let p1=new Promise(function(resolve,reject){
    console.log("任务开始");
    resolve(5);
})

p1.then(mulitply)
.then(add)
.then(mulitply)
.then(add)
.then((res)=>{
    console.log('这个结果是：'+res);
})
```