# 今日份笔记
## 讲了一下上次作业的其它方法
1. 练习：在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：
```
'use strict';
var arr = ['小明', '小红', '大军', '阿黄'];
console.log('???');
```
* 第一种
```
var arr = ['小明', '小红', '大军', '阿黄'];
var sum = '欢迎';
for (var i = 0; i < arr.length; i++) {
    if (i == arr.length - 1) {
        sum += arr[i] + '同学';
    } else if (i == arr.length - 2) {
        sum += arr[i] + '和';
    } else {
        sum += arr[i] + ',';
    }
}
console.log(sum);
```
* 第二种
```
var arr = ['小明', '小红', '大军', '阿黄'];

if (arr.length<=1){
    console.log('欢迎'+arr[arr.length-1]+'同学');
}else{
    console.log('欢迎'+arr.slice(0,arr.length-1)+'同学'+'和'+arr[arr.length-1]+'同学');
}
console.log(arr.slice(0,arr.length-1));
```
* 第三种
```
var arr = ['小明', '小红', '大军', '阿黄'];
if (arr.length == 1) {
    console.log("欢迎" + arrarr[arr.length - 1] + "同学");
} else {
    var sum = '欢迎';
    for (var i = 0; i < arr.length; i++) {
        if (i < arr.length - 2) {
            sum += arr[i] + ',';
        }
        if (i == arr.length - 2) {
            sum += arr[i];
        }
    }
    sum += '和' + arr[arr.length - 1] + '同学';
}
console.log(sum);
```
## 循环
### for循环

1. for有三个表达式：①声明循环变量；②判断循环条件；③更新循环变量；三个表达式之间，用;分割，for循环三个表达式都可以省略，但是两个“;”缺一 不可。

2. for循环的执行特点：先判断再执行，与while相同　　

3. for循环三个表达式都可以有多部分组成，第二部分多个判断条件用&& ||连接，第一三部分用逗号分割；

4. 练习:

利用for循环计算1 * 2 * 3 * ... * 10的结果：
```
'use strict';
var x = ?;
var i;
for ...

if (x === 3628800) {
    console.log('1 x 2 x 3 x ... x 10 = ' + x);
}
else {
    console.log('计算错误');
}
```

```
var x=0;
for (var i=0;i<=10;i++){
    x+=i
}
console.log("x="+x);//输出55
```


for循环最常用的地方是利用索引来遍历数组：
```
var arr = ['Apple', 'Google', 'Microsoft'];
var i, x;
for (i=0; i<arr.length; i++) {
    x = arr[i];
    console.log(x);
}
```
for循环的3个条件都是可以省略的，如果没有退出循环的判断条件，就必须使用break语句退出循环，否则就是死循环：
```
var x = 0;
for (;;) { // 将无限循环下去
    if (x > 100) {
        break; // 通过if判断来退出循环
    }
    x ++;
}
```
### while循环   
1. while循环()中的表达式，运算结果可以是各种类型，但是最终都会转为真假，转换规则如下。
* Boolean：true为真，false为假；
* String：空字符串为假，所有非空字符串为真；
* Number：0为假，一切非0数字为真；
* null/Undefined/NaN:全为假；
* Object：全为真
```
var x=0;
var y=99;
while(y>0){
    x=x+y;
    y=y-2;
}
console.log(y);//-1
console.log(x);//2500
```
### do-while循环  

1. while循环特点：先判断后执行；

2. do-while循环特点：先执行再判断，即使初始条件不成立，do-while循环至少执行一次，也就是说do-while循环比while循环多执行一次。
```
var x=0;
do{
    x=x+1;
}
while(x<100);
console.log(x);//100
```

###  循环的嵌套

* 外层循环控制行
* 内层循环控制列
* 循环的嵌套：解决多行多列的结构

## 遍历方法
### for-in
* for-in语句用于对数组或者对象的属性进行循环操作。
* for-in循环中的代码每执行一次，就会对数组或者对象的属性进行一次操作。
```
var xiaohong={
    name:'小红',
    age:20,
    ciry:'广东',
    birth:'1998',
};
for(var key in xiaohong){
    console.log(key);
    console.log(xiaohong[key]);
}
```
![图片无法显示](./imgs/2020-12-01_170838.png)
### for - of
 1. for...of循环可以使用的范围包括数组Set和Map结构、某些类似数组的对象(比如arguments对象、DOM NodeList 对象)、后文的Generator对象，以及字符串。

 2. JavaScript 原有的for-in循环，只能获得对象的键名,不能直接获取键值。ES6 提供for...of循环，允许遍历获得键值
 * 数组操作
 ```
 var arr=['a','b','c','d','e'];

for (var a in arr){
    console.log(a);
}
console.log(' ');
for(var a of arr){
    console.log(a);
}
 ```
 ![图片无法显示](./imgs/2020-12-01_172721.png)
 * 类似数组的对象操作
```
var arr='Hello Word!!!';

for (var a of arr){
    console.log(a);
}
```
 ![图片无法显示](./imgs/2020-12-01_173048.png)
 ### 循环控制语句
1. break：跳出本层循环，继续执行循环后面的语句;如果循环有多层，则break只能跳出一层。

2. continue：跳过本次循环剩余的代码，继续执行下一次循环。
* 对与for循环，continue之后执行的语句，是循环变量更新语句i++；
* 对于while、do-while循环，continue之后执行的语句，是循环条件判断；
因此，使用这两个循环时，必须将continue放到i++之后使用，否则continue将跳过i++进入死循环。
```
for (var i=0;i<10;i++){
    if(i==6){
        break;
    }
    console.log(i);
}
```
![图片无法显示](./imgs/2020-12-01_163231.png)
```
for (var i=0;i<10;i++){
    if(i==6){
        continue;
    }
    console.log(i);
}
```
![图片无法显示](./imgs/2020-12-01_163447.png)