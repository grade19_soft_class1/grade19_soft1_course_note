## JavaScript数组常用方法练习

1. 题目描述
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
    ```
    示例
    输入  [ 1, 2, 3, 4 ], 3
    输出   2
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function indexOf(arr, item) {
                return arr.indexOf(item);
            }
        </code>
    </pre>
    </details>

2. 题目描述
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
    ```
    示例
    输入  [ 1, 2, 3, 4 ]
    输出  10
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function sum(arr) {
            var sum = 0;
            arr.forEach(function(item,index){
            sum = sum + item;
        })
        return sum
    }
        </code>
    </pre>
    </details>

3. 题目描述
移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4, 2], 2
    输出  [1, 3, 4]
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function remove(arr, item) {
                var result = [];
                arr.forEach(function(arrItem){
                    if(item != arrItem){
                        result.push(arrItem);
                    }
                })
                return result;
            }
        </code>
    </pre>
    </details>

4. 题目描述
移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
    ```
    示例
    输入  [1, 2, 2, 3, 4, 2, 2], 2
    输出  [1, 3, 4]
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function removeWithoutCopy(arr, item) {
                while(arr.indexOf(item) != -1){
                    arr.splice(arr.indexOf(item),1)
                }
                return arr;
            }
        </code>
    </pre>
    </details>    

5. 题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4],  10
    输出  [1, 2, 3, 4, 10]
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function append(arr, item) {
                var result = [];
                arr.forEach(function(arrItem){
                    result.push(arrItem);
                })
                result.push(item);
                return result;
            }
        </code>
    </pre>
    </details>       

6. 题目描述
删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4]
    输出  [1, 2, 3]
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function truncate(arr) {
                var result = [];
                var len = arr.length -1;
                while(len--){
                    result.unshift(arr[len]);
                }
                return result;
            }
        </code>
    </pre>
    </details>   

7. 题目描述
在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4], 10
    输出  [10, 1, 2, 3, 4]
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function prepend(arr, item) {
                var result = [];
                result.push(item);
                arr.forEach(function(arrItem){
                    result.push(arrItem);
                })
                return result;
            }
            // 或
            function prepend(arr, item) {
                var result = arr.slice(0);
                result.push(item);
                return result;
            }
        </code>
    </pre>
    </details>   

8. 题目描述
删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4]
    输出  [2, 3, 4]
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function curtail(arr) {
                var result = arr.slice(0);
                result.shift();
                return result;
            }
        </code>
    </pre>
    </details>   

9. 题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4], ['a', 'b', 'c', 1]
    输出  [1, 2, 3, 4, 'a', 'b', 'c', 1]
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function concat(arr1, arr2) {
                var currentArr1 = arr1.slice(0);
                var currentArr2 = arr2.slice(0);
                return currentArr1.concat(currentArr2)
            }
        </code>
    </pre>
    </details>   

10. 题目描述
在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4], 'z', 2
    输出  [1, 2, 'z', 3, 4]
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function insert(arr, item, index) {
                var result = arr.slice(0);
                result.splice(index,0,item);
                return result;
            }
        </code>
    </pre>
    </details>   

11. 题目描述
统计数组 arr 中值等于 item 的元素出现的次数
    ```
    示例
    输入  [1, 2, 4, 4, 3, 4, 3], 4
    输出  3
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function count(arr, item) {
                var num = 0;
                arr.forEach(function(arrItem){
                    if(item === arrItem){
                        num ++;
                    }
                })
                return num;
            }
        </code>
    </pre>
    </details>   

12. 题目描述
找出数组 arr 中重复出现过的元素
    ```
    示例
    输入  [1, 2, 4, 4, 3, 3, 1, 5, 3]
    输出  [1, 3, 4]
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function duplicates(arr) {
                var result = [];
                arr.forEach(function(item){
                    if((arr.indexOf(item) != -1)&&(arr.indexOf(item) != arr.lastIndexOf(item))){
                        if(result.indexOf(item) == -1){
                            result.push(item)
                        }
                    }
                });
                return result;
            }
        </code>
    </pre>
    </details>   

13. 题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4]
    输出  [1, 4, 9, 16]
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function square(arr) {
                var result = arr.slice(0);
                result.forEach(function(item,index){
                    result[index] = item * item;
                })
                return result;
            }
        </code>
    </pre>
    </details>   

14. 题目描述
在数组 arr 中，查找值与 item 相等的元素出现的所有位置
    ```
    示例
    输入  'abcdefabc'
    输出  [0, 6]
    ```
    <details>
    <summary>参考答案</summary>
    <pre>
        <code> 
            function findAllOccurrences(arr, target) {
                var result = [];
                for(var i =0;i<arr.length;i++){
                    if(arr[i] === target){
                        result.push(i);
                    }
                }
                return result;
            }
        </code>
    </pre>
    </details>   

备注：方法供参考，不唯一。