1. 题目描述
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
    ```
    示例
    输入  [ 1, 2, 3, 4 ], 3
    输出   2
    ```

 * 解：
 ```
 var arr=[1,2,3,4];

 var item=3;

 var itemindex=arr.indexOf(item);

 console.log(itemindex);
 ```

2. 题目描述
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
    ```
    示例
    输入  [ 1, 2, 3, 4 ]
    输出  10
    ```

* 解：

```
var arr=[1,2,3,4];

var arradd=0;

for(var i=0;i<arr.length;i++)
{
    arradd += arr[i];
}
console.log(arradd);
```

3. 题目描述
移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4, 2], 2
    输出  [1, 3, 4]
    ```

* 解

```
var arr=[1,2,3,4,2];

var resarr=[];

for(var i=0;i<arr.length;i++)
{
    if(arr[i]!=2)
    {
        resarr.push(arr[i]);
    }
}
console.log(resarr);
```

4. 题目描述
移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
    ```
    示例
    输入  [1, 2, 2, 3, 4, 2, 2], 2
    输出  [1, 3, 4]
    ```

* 解：
```
var arr=[1,2,2,3,4,2,2];

第一种：
for(var i=0;i<arr.length;i++)
{
    if(arr[i]==2)
    {
        var arritem=arr.indexOf(arr[i]);
        arr.splice(arritem,arritem+1);
    }
}
console.log(arr);

第二种：（更好）
while(arr.indexOf(2) != -1){
    arr.splice(arr.indexOf(2),1)
}

console.log(arr);
```

5. 题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4],  10
    输出  [1, 2, 3, 4, 10]
    ```

* 解：
```
var arr=[1,2,3,4];

var resarr=[];

for(var i=0;i<arr.length;i++)
{
    resarr.push(arr[i]);
}

resarr.push(10);

console.log(resarr);
```

6. 题目描述
删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4]
    输出  [1, 2, 3]
    ```

* 解：

```
var arr=[1,2,3,4];

var resarr=[];

for(var i=0;i<arr.length-1;i++)
{
    resarr.push(arr[i]);
}

console.log(resarr);
```

7. 题目描述
在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4], 10
    输出  [10, 1, 2, 3, 4]
    ```

* 解：
```
var arr=[1,2,3,4];

var resarr=[];

for(var i=0;i<arr.length;i++)
{
    resarr.push(arr[i]);
}

resarr.unshift(10);

console.log(resarr);
```

8. 题目描述
删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4]
    输出  [2, 3, 4]
    ```

* 解：
```
var arr=[1,2,3,4];

var resarr=[];

for(var i=0;i<arr.length;i++)
{
    resarr.push(arr[i]);
}

resarr.shift();

console.log(resarr);
```

9. 题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4], ['a', 'b', 'c', 1]
    输出  [1, 2, 3, 4, 'a', 'b', 'c', 1]
    ```

* 解：
```
var arr1=[1,2,3,4];

var arr2=['a', 'b', 'c', 1];

var addarr=arr1.concat(arr2);

console.log(addarr);
```

10. 题目描述
在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4], 'z', 2
    输出  [1, 2, 'z', 3, 4]
    ```

* 解：
```
var arr=[1,2,3,4];

var resarr=[];

for(var i=0;i<arr.length;i++)
{
    resarr.push(arr[i]);
}

resarr.splice(2,0,'z');

console.log(resarr);

```

11. 题目描述
统计数组 arr 中值等于 item 的元素出现的次数
    ```
    示例
    输入  [1, 2, 4, 4, 3, 4, 3], 4
    输出  3
    ```

* 解：
```
var arr=[1,2,4,4,3,4,3];

var itemcounts=0;

for(var i=0;i<arr.length;i++)
{
    var itemindex=arr.indexOf(4);

    if(itemindex!=-1)
    {
        itemindex+=1;
    }

}

console.log(itemindex);
```

12. 题目描述
找出数组 arr 中重复出现过的元素
    ```
    示例
    输入  [1, 2, 4, 4, 3, 3, 1, 5, 3]
    输出  [1, 3, 4]
    ```

* 解：
```
var arr1=[1,2,4,4,3,3,1,5,3];

var arr2=[];

console.log(arr1);

for(var i=0;i<arr1.length;i++)
{
  for(var j=i+1;j<arr1.length;j++)
  {
    if(arr1[i]===arr1[j]&&arr2.indexOf(arr1[i])==-1)
    {
        arr2.push(arr1[i]);
    }
  }
}

console.log(arr2);
```

13. 题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4]
    输出  [1, 4, 9, 16]
    ```

* 解：
```
var arr=[1,2,3,4];

var resarr=[];

for(var i=0;i<arr.length;i++)
{
    arr[i]*=arr[i];
    resarr.push(arr[i]);
}

console.log(resarr);
```

14. 题目描述
在数组 arr 中，查找值与 item 相等的元素出现的所有位置
    ```
    示例
    输入  'abcdefabc'
    输出  [0, 6]
    ```

* 解：
```
var arrs='abcdefabc';

console.log(arrs);

var arr1=[];

for(var i=0;i<arrs.length;i++)
{
    arr1.push(arrs[i]);
}

console.log(arr1);

var item='a';

var arr2=[];

for(var j=0;j<arr1.length;j++)
{
    if(arr1[j]==item)
    {
        arr2.push(j);
    }
}

console.log(arr2);
```