'use strict'
var arr = [10, 20, 1, 2];
arr.sort((x, y) => {
    if (x < y) {
        return -1;
    }
});
console.log(arr); // [1, 2, 10, 20]