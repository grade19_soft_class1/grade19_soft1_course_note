'use strict';
// // 原型对象:
// var Student = {
//     name: 'Robot',
//     height: 1.2,
//     HELLO: function () {
//         console.log(this.name + ' is ning...');
//     }
// };

// function createStudent(name) {
//     // 基于Student原型创建一个新对象:
//     var s = Object.create(Student);
//     // 初始化新对象:
//     s.name = name;
//     return s;
// }

// var xiaoming = createStudent('小明');
// xiaoming.HELLO(); // 小明 is ning...
// xiaoming.__proto__ === Student; // true


// function Cat(name) {
//     this.name = name;
// }
// Cat.prototype.say = function () {
//     return 'Hello, ' + this.name + '!';
// }

// // 测试:
// var kitty = new Cat('Kitty');
// var doraemon = new Cat('哆啦A梦');
// if (kitty && kitty.name === 'Kitty'
//     && kitty.say
//     && typeof kitty.say === 'function'
//     && kitty.say() === 'Hello, Kitty!'
//     && kitty.say === doraemon.say
// ) {
//     console.log('测试通过!');
// } else {
//     console.log('测试失败!');
// }
