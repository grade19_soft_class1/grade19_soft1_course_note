'use strict';

var selected = null;
selected = $('#para-1');//仅选择JavaScript
selected = $('.color-red.color-green');//仅选择Erlang
selected = $('.color-red');//选择JavaScript和Erlang
selected = $('[class^="color-"]');//选择所有编程语言
selected = $('input[name^=name]');//选择名字input
selected = $('input[name^=name],input[name=email]');//+ 选择邮件和名字input

// 高亮结果:
if (!(selected instanceof jQuery)) {
    console.log('不是有效的jQuery对象!');
}
$('#test-jquery').find('*').css('background-color', '');
selected.css('background-color', '#ffd351');