'use strict';

// function lazy_sum(arr) {
//     var sum = function () {
//         return arr.reduce(function (x, y) {
//             return x + y;
//         });
//     }
//     return sum;
// }

// console.log(lazy_sum([1, 2, 3, 4, 5]));

// var f = lazy_sum([1, 2, 3, 4, 5]);

// console.log(f());

function count() {
    var arr = [];
    for (let i = 1; i <= 3; i++) {//用了let 
        arr.push(function () {
            return i * i;
        });
    }
    return arr;
}

var results = count();
var f1 = results[0];
var f2 = results[1];
var f3 = results[2];

console.log(f1());
console.log(f2());
console.log(f3());