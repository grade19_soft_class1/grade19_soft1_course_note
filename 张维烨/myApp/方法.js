'use strict'

//第一种对象函数：
/* var Card01 = {
    name: 'zwy',
    OldMoney: 20000,
    Money: function (outMoney, inMoney) {
        if (outMoney === 0 && inMoney > 0) {
            let NewMoney = this.OldMoney + inMoney;
            this.OldMoney = NewMoney;
            return console.log('存钱成功！' + '当前所剩余额：' + NewMoney + '￥');
        } else if (inMoney === 0 && outMoney > 0) {
            let NewMoney = this.OldMoney - outMoney;
            if (NewMoney < 0) {
                return console.log('取钱失败，余额不足！！！' + '当前所剩余额：' + this.OldMoney + '￥');
            } else {
                this.OldMoney = NewMoney;
                return console.log('取钱成功！' + '当前所剩余额：' + NewMoney + '￥');
            }
        } else if (outMoney > 0 && inMoney > 0) {
            return console.log('传入参数有误');
        }
        else {
            return console.log('你没有取钱或者存钱');
        }
        return;
    }
}



Card01.Money(0, 10000);//存一万

Card01.Money(10000, 0);//取一万

Card01.Money(100000, 0);//取十万

Card01.Money(10000, 0);//取一万

Card01.Money(0, 0);//不存不取

Card01.Money(10, 10);//同时存十块又取十块 */

//第二种独立函数：
/* var Card02 = {
    name: 'zwy',
    OldMoney: 20000,
    Money: InOrOut
};

function InOrOut(outMoney, inMoney) {
    if (outMoney === 0 && inMoney > 0) {
        let NewMoney = this.OldMoney + inMoney;
        this.OldMoney = NewMoney;
        return console.log('存钱成功！' + '当前所剩余额：' + NewMoney + '￥');
    } else if (inMoney === 0 && outMoney > 0) {
        let NewMoney = this.OldMoney - outMoney;
        if (NewMoney < 0) {
            return console.log('取钱失败，余额不足！！！' + '当前所剩余额：' + this.OldMoney + '￥');
        } else {
            this.OldMoney = NewMoney;
            return console.log('取钱成功！' + '当前所剩余额：' + NewMoney + '￥');
        }
    } else if (outMoney > 0 && inMoney > 0) {
        return console.log('传入参数有误');
    }
    else {
        return console.log('你没有取钱或者存钱');
    }
    return;
}


Card02.Money(0, 10000);//存一万

Card02.Money(10000, 0);//取一万

Card02.Money(100000, 0);//取十万

Card02.Money(10000, 0);//取一万

Card02.Money(0, 0);//不存不取

Card02.Money(10, 10);//同时存十块又取十块 */

/* console.log(InOrOut(10000, 0))//单独调用，也会出错

var Io = Card02.Money;// 先拿到Card02的Money函数
console.log(Io(10000, 0));//这种更不行，因为this指向不正确 */

//用apply方法和call方法修复
//1。apply（）
//InOrOut.apply(Card02, [10000, 0]);//取一万

//2.call()
//InOrOut.call(Card02, 0, 10000);//存一万


//第三种方法重构：
var Card03 = {
    name: 'zwy',
    OldMoney: 20000,
    Money: function () {
        var that = this;
        function InOrOut(outMoney, inMoney) {
            if (outMoney === 0 && inMoney > 0) {
                let NewMoney = that.OldMoney + inMoney;
                that.OldMoney = NewMoney;
                return console.log('存钱成功！' + '当前所剩余额：' + NewMoney + '￥');
            } else if (inMoney === 0 && outMoney > 0) {
                let NewMoney = that.OldMoney - outMoney;
                if (NewMoney < 0) {
                    return console.log('取钱失败，余额不足！！！' + '当前所剩余额：' + that.OldMoney + '￥');
                } else {
                    that.OldMoney = NewMoney;
                    return console.log('取钱成功！' + '当前所剩余额：' + NewMoney + '￥');
                }
            } else if (outMoney > 0 && inMoney > 0) {
                return console.log('传入参数有误');
            }
            else {
                return console.log('你没有取钱或者存钱');
            }
            return;
        }
        InOrOut.apply(null, arguments);
    }
}



Card03.Money(0, 10000);//存一万

Card03.Money(10000, 0);//取一万

Card03.Money(100000, 0);//取十万

Card03.Money(10000, 0);//取一万

Card03.Money(0, 0);//不存不取

Card03.Money(10, 10);//同时存十块又取十块
