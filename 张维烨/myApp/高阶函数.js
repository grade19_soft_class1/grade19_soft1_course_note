'use strict';

//#region 高阶函数
// function add(x, y, f) {
//     return f(x) + f(y);
// }
// var x = add(-5, 6, Math.abs); // 11

// console.log(x);
//#endregion


//#region map() 
// function pow(x) {
//     return x * x;
// }
// var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// var results = arr.map(pow); // [1, 4, 9, 16, 25, 36, 49, 64, 81]

// console.log(results);
//#endregion


//#region reduce()

//#region  练习：利用reduce()求积：
// 解：
// function product(arr) {
//     return arr.reduce((x, y) => x * y)
// }


// // 测试:
// if (product([1, 2, 3, 4]) === 24 && product([0, 1, 2]) === 0 && product([99, 88, 77, 66]) === 44274384) {
//     console.log('测试通过!');
// }
// else {
//     console.log('测试失败!');
// }
// var arr = [1, 3, 5, 7, 9];
// console.log(product(arr));//945
//#endregion

//#region  练习：想办法把一个字符串13579先变成Array——[1, 3, 5, 7, 9]，再利用reduce()就可以写出一个把字符串转换为Number的函数。

// 注意：不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：

//解：
// function string2int(s) {
//     var arrStr = s.split('');
//     var arrInt=[];
//     arrInt=arrStr.map(function(b){
//         return +b;
//     })
//     return arrInt.reduce(function (x,y){
//         return  x*10+y;
//     })


// }
// var a = '13579';
// console.log(string2int(a));
// // 测试:
// if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
//     if (string2int.toString().indexOf('parseInt') !== -1) {
//         console.log('请勿使用parseInt()!');
//     } else if (string2int.toString().indexOf('Number') !== -1) {
//         console.log('请勿使用Number()!');
//     } else {
//         console.log('测试通过!');
//     }
// }
// else {
//     console.log('测试失败!');
// }
//#endregion

//#region  练习：请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。

//解：
// function normalize(arr) {
//     function onedaxie(x){
//         return x.substring(0,1).toUpperCase()+x.substring(1).toLowerCase();
//     }
//     return arr.map(onedaxie);

// }
// //测试:
// if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
//     console.log('测试通过!');
// }
// else {
//     console.log('测试失败!');
// }

//#endregion

//#region  小明希望利用map()把字符串变成整数，他写的代码很简洁：

// var arr = ['1', '2', '3'];
// var r;
// r = arr.map(parseInt);

// console.log(r);

// //修正后：
// r=arr.map(i=>i*1);
// console.log(r);

//#endregion

//#endregion


//#region filter()

//#region 练习：请尝试用filter()筛选出素数：

// 解:
// function get_primes(arr) {
//     function isPrimeNumber(element) {
//         if (element === 1) { return false; }
//         for (var x = 2; x < element; x++) {
//             if (element % x === 0) {
//                 return false;//这是合数
//             }
//         } return true;
//     }
//     return arr.filter(isPrimeNumber);
// }

// // 测试:
// var
//     x,
//     r,
//     arr = [];
// for (x = 1; x < 100; x++) {
//     arr.push(x);
// }
// r = get_primes(arr);
// if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
//     console.log('测试通过!');
// } else {
//     console.log('测试失败: ' + r.toString());
// }
//#endregion

//#endregion
