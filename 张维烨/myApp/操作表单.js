'use strict';
var checkRegisterForm = function () {
    // TODO:
    let username = document.getElementById("username");
    let pwd_1 = document.getElementById("password");
    let pwd_2 = document.getElementById("password-2");

    const reUsername = /^[0-9A-Za-z]{3,10}$/;
    const rePwd = /^[0-9A-Za-z]{6,20}$/;

    if (!reUsername.exec(username.value) || !rePwd.exec(pwd_1.value) || pwd_1.value !== pwd_2.value) {
        alert("please recheck your password or username!")
        return false;
    }
}

    // 测试:
    ; (function () {
        window.testFormHandler = checkRegisterForm;
        var form = document.getElementById('test-register');
        if (form.dispatchEvent) {
            var event = new Event('submit', {
                bubbles: true,
                cancelable: true
            });
            form.dispatchEvent(event);
        } else {
            form.fireEvent('onsubmit');
        }
    })();