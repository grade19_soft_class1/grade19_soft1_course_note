// 'use strict'

// // 选择<p>JavaScript</p>:
// var divtest = document.querySelector('#test-div');
// console.log(divtest.textContent);
// var js = divtest.querySelector('div.c-red>#test-p')
// console.log(js);
// // 选择<p>Python</p>,<p>Ruby</p>,<p>Swift</p>:
// var arr = [];
// var divcolor = document.getElementsByClassName('c-red c-green')[0];
// for (let i = 0; i < 3; i++) {
//     arr.push(divcolor.getElementsByTagName('p')[i])
// }
// // 选择<p>Haskell</p>:
// var divgreen = document.getElementsByClassName('c-green')[1];
// console.log(divgreen.innerText);
// var haskell = divgreen.getElementsByTagName('p')[1];

// // 测试:
// if (!js || js.innerText !== 'JavaScript') {
//     alert('选择JavaScript失败!');
// } else if (!arr || arr.length !== 3 || !arr[0] || !arr[1] || !arr[2] || arr[0].innerText !== 'Python' || arr[1].innerText !== 'Ruby' || arr[2].innerText !== 'Swift') {
//     console.log('选择Python,Ruby,Swift失败!');
// } else if (!haskell || haskell.innerText !== 'Haskell') {
//     console.log('选择Haskell失败!');
// } else {
//     console.log('测试通过!');
// }

// 返回ID为'top'的节点：
// var tops = document.getElementById('top');
// console.log(tops);
// console.log(tops.innerText);
// console.log(tops.textContent);
// // 先定位ID为'top'的节点，再返回其内部所有p节点：
// var tps = document.getElementById('top').getElementsByTagName('p');
// console.log(tps);
// // 先定位ID为'top'的节点，再返回其内部所有class包含im的节点：
// var reds = document.getElementById('top').getElementsByClassName('im');
// console.log(reds);
// // 获取节点Content下的所有直属子节点:
// var cs = Content.children;
// console.log(cs);
// // 获取节点Content下第一个、最后一个子节点：
// var first = Content.firstElementChild;
// console.log(first);
// console.log(first.innerText);
// console.log(first.textContent);
// var last = Content.lastElementChild;
// console.log(last);
// console.log(last.innerText);
// console.log(last.textContent);

// // 通过querySelector获取ID为top的节点：
// var q1 = document.querySelector('#top');
// console.log(q1);

// // 通过querySelectorAll获取q1节点内的符合条件的所有节点：
// var ps = q1.querySelectorAll('div.im>p')[1];
// console.log(ps);//我是2

// 获取<p>javascript</p>节点:
// var js = document.getElementById('test-js');
// console.log(js);
// // 修改文本为JavaScript:
// // TODO:
// js.innerHTML = 'JavaScript';

// // 修改CSS为: color: #ff0000, font-weight: bold
// // TODO:
// js.style.color = '#ff0000';
// js.style.fontWeight = 'bold';
// // 测试:
// if (js && js.parentNode && js.parentNode.id === 'test-div' && js.id === 'test-js') {
//     if (js.innerText === 'JavaScript') {
//         if (js.style && js.style.fontWeight === 'bold' && (js.style.color === 'red' || js.style.color === '#ff0000' || js.style.color === '#f00' || js.style.color === 'rgb(255, 0, 0)')) {
//             console.log('测试通过!');
//         } else {
//             console.log('CSS样式测试失败!');
//         }
//     } else {
//         console.log('文本测试失败!');
//     }
// } else {
//     console.log('节点测试失败!');
// }

// 'use strict';
// // sort list:
// var ol = document.getElementById('test-list');
// var scheme = ol.querySelectorAll('li')[0];
// ol.appendChild(scheme);
// var js = ol.querySelectorAll('li')[0];
// var haskell = ol.querySelectorAll('li')[3];
// ol.insertBefore(haskell, js);
// console.log(ol);
// // 测试:
// ; (function () {
//     var
//         arr, i,
//         t = document.getElementById('test-list');
//     if (t && t.children && t.children.length === 5) {
//         arr = [];
//         for (i = 0; i < t.children.length; i++) {
//             arr.push(t.children[i].innerText);
//         }
//         if (arr.toString() === ['Haskell', 'JavaScript', 'Python', 'Ruby', 'Scheme'].toString()) {
//             console.log('测试通过!');
//         }
//         else {
//             console.log('测试失败: ' + arr.toString());
//         }
//     }
//     else {
//         console.log('测试失败!');
//     }
// })();

// 'use strict';
// // TODO
// var parent = document.getElementById('test-list');
// parent.removeChild(parent.children[1]);
// parent.removeChild(parent.children[2]);
// parent.removeChild(parent.children[3]);
// // 测试:
// (function () {
//     var
//         arr, i,
//         t = document.getElementById('test-list');
//     if (t && t.children && t.children.length === 3) {
//         arr = [];
//         for (i = 0; i < t.children.length; i++) {
//             arr.push(t.children[i].innerText);
//         }
//         if (arr.toString() === ['JavaScript', 'HTML', 'CSS'].toString()) {
//             console.log('测试通过!');
//         }
//         else {
//             console.log('测试失败: ' + arr.toString());
//         }
//     }
//     else {
//         console.log('测试失败!');
//     }
// })();