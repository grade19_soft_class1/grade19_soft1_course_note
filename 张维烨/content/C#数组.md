数组详解


数组从字面上理解就是存放一组数，但在 C# 语言中数组存放的并不一定是数字，也可以是其他数据类型。

一个变量只能存放一个值，如果需要计算 10 个变量中的最大值，则需要定义 10 个变 量，非常麻烦。

数组的引入给编程带来了很多方便，它能通过一个变量存放多个相同类型的值，在存取数组中的值时直接根据数组中的下标即可完成。

在一个数组中存放的值都是同一数据类型的，并且可以通过循环以及数据操作的方法对数组的值进行运算或操作。

枚举和结构体是两个特殊的值类型，枚举类型用于定义某些列只能设置指定的值，结 构体是一种特殊的类型，在结构体中允许定义字段、属性、方法等成员。


所有的数组都是由连续的内存位置组成的。最低的地址对应第一个元素，最高的地址对应最后一个元素。

C# 中数组从零开始建立索引，即数组索引从零开始。

C# 中数组的工作方式与在大多数其他流行语言中的工作方式类似。但还有一些差异应引起注意。

声明数组时，方括号[]必须跟在类型后面，而不是标识符后面。

在 C# 中，将方括号放在标识符后是不合法的语法。
int[] table; // not int table[];
另一细节是，数组的大小不是其类型的一部分，这使得可以声明一个数组并向它分配 int 对象的任意数组，而不管数组长度如何。

数组分为一维数组和多维数组，我们将在接下来的两节中分别为大家讲解。


一、一维数组


一维数组在数组中最常用，即将一组值存放到一个数组中，并为其定义一个名称，通过数组中元素的位置来存取值。

创建一维数组的语法形式如下。

//定义数组
数据类型[]  数组名；

//初始化数组中的元素
数据类型[]  数组名 = new  数据类型[长度];
数据类型[]  数组名 = {值 1, 值 2, ...}
数据类型[]  数组名 = new  数据类型[长度]{值 1，值 2,...}

在定义数组时定义的数据类型代表了数组中每个元素的数据类型。

在使用数组前必须初始化数据，即为数组赋初值。

在初始化数组时指定了数组中的长度，也就是数组中能存放的元素个数。

在指定数组的长度后，数组中的元素会被系统自动赋予初始值，与类中段的初始化类似，数值类型的值为 0、引用类型的值为 null。

如果在初始化数组中直接对数组赋值了，那么数组中值的个数就是数组的长度。

由于在数组中存放了多个元素，在存取数组中的元素时要使用下标来存取，类似于取字符串中的字符。

例如有一个 int 类型的数组，输出数组中的第一个元素和最后一个元素，语句如下。

//定义 int 类型的数组
int[] a = {1,2,3};
//输岀数组中的一个元素
Console.WriteLine(a[0]);
//输出数组中的最后一个元素
Console.WriteLine(a[a.Length-1]);
获取数组的长度使用的是数组的Length属性，数组的下标仍然从 0 开始，数组中的最后一个元素是数组的长度减 1。

【实例 1】在 Main 方法中创建一个字符串类型的数组，并存入 5 个值，然后将数组中下标是偶数的元素输出。

根据题目要求，代码如下。
class Program
{
    static void Main(string[] args)
    {
        string[] strs = { "aaa", "bbb", "ccc", "ddd", "eee" };
        for(int i = 0; i < strs.Length; i = i + 2)
        {
            Console.WriteLine(strs[i]);
        }
    }
}


从上面的执行效果可以看出，输出的是数组中的第 1 个、第 3 个、第 5 个元素，但是下标却是 0、2、4。

【实例 2】在 Main 方法中创建 int 类型数组，并从控制台输入 5 个值存入该数组中，最后将数组中的最大数输出。

根据题目要求，代码如下。
class Program
{
    static void Main(string[] args)
    {
        int[] a = new int[5];
        Console.WriteLine("请输入5个整数：");
        for(int i = 0; i < a.Length; i++)
        {
            a[i] = int.Parse(Console.ReadLine());//将字符串类型转换成整型
        }
        int max = a[0];//这里假设a[0]是最大的
        for(int i = 1; i < a.Length; i++)
        {
            if (a[i] > max)
            {
                max = a[i];
            }
        }
        Console.WriteLine("数组中最大值为：" + max);
    }
}


二、多维数组


在 C# 语言里多维数组中比较常用的是二维数组，这也是本书中对多维数组介绍的重点。

定义多维数组的语法形式如下。

//定义多维数组
数据类型[ , , ...]  数组名;

//创建多维数组并初始化
数据类型[ , , ...]   数组名 = new  数据类型[m,n,...]  {{ , , ...},{ , , ...}};

从上面定义的语法可以看出，它与一维数组的定义非常类似，每多一个维度则在定义时的[]中增加一个“,”。

存取数组中的值也是将下标用“,”隔开。

【实例 1】在 Main 方法中定义一个存放学生成绩的二维数组，并将该数组中每个学生的成绩输出。

根据题目要求，定义二维数组为 double 类型的，代码如下。
class Program
{
    static void Main(string[] args)
    {
        double[,] points = { { 90, 80 }, { 100, 89 }, { 88.5, 86 } };
        for(int i = 0; i < points.GetLength(0); i++)
        {
            Console.WriteLine("第" + (i + 1) + "个学生成绩：");
            for(int j = 0; j < points.GetLength(1); j++)
            {
                Console.Write(points[i, j] + " ");
            }
            Console.WriteLine();
        }
    }
}

在遍历多维数组元素时使用 GetLength(维度) 方法能获取多维数组中每一维的元素，维度也是从 0 开始的，因此在该实例中获取数组中第一维的值时使用的是 points.GetLength(0)。

在 C# 语言中不仅支持上面给出的多维数组，也支持锯齿型数组，即在多维数组中的每一维中所存放值的个数不同。

锯齿型数组也被称为数组中的数组。定义锯齿型数组的语法形式如下。
数据类型[][]  数组名 = new 数据类型[数组长度][];
数组名[0] = new 数据类型[数组长度];

在这里，数据类型指的是整个数组中元素的类型，在定义锯齿型数组时必须要指定维度。

【实例 2】在 Main 方法中创建一个锯齿型数组，第一维数组的长度是 2、第二维数组的长度是 3、第三维数组的长度是 4，并直接向数组中赋值，最后输出数组中的元素。

根据题目要求，代码如下。
class Program
{
    static void Main(string[] args)
    {
        int[][] arrays = new int[3][];
        arrays[0] = new int[] { 1, 2 };
        arrays[1] = new int[] { 3, 4, 5 };
        arrays[2] = new int[] { 6, 7, 8, 9 };
        for(int i = 0; i < arrays.Length; i++)
        {
            Console.WriteLine("输出数组中第" + (i + 1) + "行的元素：");
            for(int j=0;j<arrays[i].Length; j++)
            {
                Console.Write(arrays[i][j] + " ");
            }
            Console.WriteLine();
        }
    }
}
执行上面的代码，效果如下图所示。

输出锯齿型数组中的元素

锯齿型数组中的值也可以通过循环语句来赋值，与输岀语句类似。

在上面的实例中, arrays 数组中的元素从控制台输入的具体语句如下。

int[][] arrays = new int[3][];
arrays[0] = new int[2];
arrays[1] = new int[3];
arrays[2] = new int[4];
for(int i = 0; i < arrays.Length; i++)
{
    Console.WriteLine("输入数组中第" + (i + 1) + "行的元素：");
    for(int j=0;j<arrays[i].Length; j++)
    {
        arrays[i][j] = int.Parse(Console.ReadLine());
    }
    Console.WriteLine();
}


三、foreach循环用法详解


foreach 循环用于列举出集合中所有的元素，foreach 语句中的表达式由关键字 in 隔开的两个项组成。

in 右边的项是集合名，in 左边的项是变量名，用来存放该集合中的每个元素。

该循环的运行过程如下：每一次循环时，从集合中取出一个新的元素值。放到只读变量中去，如果括号中的整个表达式返回值为 true，foreach 块中的语句就能够执行。

一旦集合中的元素都已经被访问到，整个表达式的值为 false，控制流程就转入到 foreach 块后面的执行语句。

foreach 语句经常与数组一起使用，在 C# 语言中提供了 foreach 语句遍历数组中的元素，具体的语法形式 如下。

foreach(数据类型  变量名  in  数组名)
{
    //语句块；
}

这里变量名的数据类型必须与数组的数据类型相兼容。

在 foreach 循环中，如果要输出数组中的元素，不需要使用数组中的下标，直接输出变量名即可。

foreach 语句仅能用于数组、字符串或集合类数据类型。

【实例】在 Main 方法中创建一个 double 类型的数组，并在该数组中存入 5 名学生的考试成绩，计算总成绩和平均成绩。

根据题目要求，使用foreach语句实现该功能，代码如下。
class Program
{
    static void Main(string[] args)
    {
        double[] points = { 80, 88, 86, 90, 75.5 };
        double sum = 0;
        double avg = 0;
        foreach(double point in points)
        {
            sum = sum + point;
        }
        avg = sum / points.Length;
        Console.WriteLine("总成绩为：" + sum);
        Console.WriteLine("平均成绩为：" + avg);
    }
}

在计算平均成绩时，通过数组的 Length 属性即可得到数组中元素的个数，使用总成绩除以元素的个数即为结果。


从上面的执行效果可以看出，在使用 foreach 语句时可以免去使用下标的麻烦，这也 给遍历数组中的元素带来很多方便。


四、Split：将字符串拆分为数组

Split 方法用于按照指定的字符串来拆分原有字符串，并返回拆分后得到的字符串数组。

下面用两个实例来演示字符串拆分方法的应用。

【实例 1】在 Main 方法中从控制台输入一个字符串，然后计算该字符串中包含的逗号的个数。

根据题目要求，如果要查询逗号的个数，通过拆分方法 Split 将拆分结果存放到字符串数组中，数组的长度减 1 即为字符串中含有逗号的个数，代码如下。

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("请输入一个字符串：");
        string str = Console.ReadLine();
        string[] condition = { "," };
        string[] result = str.Split(condition, StringSplitOptions.None);
        Console.WriteLine("字符串中含有逗号的个数为：" + (result.Length - 1));
    }
}


在使用 Split 方法时，需要注意该方法中传递的参数 (condition, StringSplitOptions.None)。

第一个参数是拆分的条件数组，可以在该数组中存放多个字符串作为拆分的条件。

第二个 参数 StringSplitOptions.None 是拆分的选项，表示如果在拆分时遇到空字符也拆分出一个元素。

如果在拆分时不需要包含空字符串，则可以使用 StringSplitOptions.RemoveEmptyEntries 选项，例如在上例中将 StringSplitOptions.None 更改成 StringSplitOptions.RemoveEmptyEntries, 语句如下。
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("请输入一个字符串：");
        string str = Console.ReadLine();
        string[] condition = { "," };
        string[] result = str.Split(condition, StringSplitOptions.RemoveEmptyEntries);
        Console.WriteLine("字符串中含有逗号的个数为：" + (result.Length - 1));
    }
}
仍然执行该实例中所输入的字符串，效果如下图所示。

使用StringSplitOptions.RemoveEmptyEntries参数拆分字符串

从上面的执行效果可以看出，逗号的个数为 3，而前面的例子中逗号的个数为 4，这是因为当拆分“a,b,c,d,”时，最后一个逗号拆分后逗号后面的值是一个空格，因此拆分结果中数组的元素个数为 4 而不是 5。

在实际应用中，用户应根据具体情况选择拆分选项的不同值。


五、冒泡排序（Sort方法）


冒泡排序在应聘的笔试题目中经常被考到，冒泡排序的原理是将数组元素中相邻两个元素的值进行比较，将较小的数放到前面，每一次交换都将最大的数放到最后，依次交换后最终将数组中的元素从小到大排序。

下面通过几个实例来演示 C# 中冒泡排序是如何实现的。

【实例 1】在 Main 方法中创建一个整型数组，并在该数组中存放 5 个元素，使用冒泡排序算法将数组中的元素从小到大排序。

根据题目要求，代码如下。

class Program
{
    static void Main(string[] args)
    {
        int[] a = { 5, 1, 7, 2, 3 };
        for(int i = 0; i < a.Length; i++)
        {
            for(int j = 0; j < a.Length - i - 1; j++)
            {
                if (a[j] > a[j + 1])
                {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                }
            }
        }
        Console.WriteLine("升序排序后的结果为：");
        foreach(int b in a)
        {
            Console.Write(b + "");
        }
        Console.WriteLine();
    }
}


如果要对数组中的元素从大到小排序，只需要将 if(a[j]>a[j+1]) 语句更改成 if(a[j]<a[j+1]) 即可。

System.Array 是所有数组的基类，其提供的属性和方法也可以被用到任何数组中。

例如前面使用的 Length 属性也是该基类中提供的。

数组中常用的方法如下表所示。

编号	方法	描述
1	Clear()	清空数组中的元素
2	Sort()	冒泡排序，从小到大排序数组中的元素
3	Reverse()	将数组中的元素逆序排列
4	IndexOf()	查找数组中是否含有某个元素，返回该元素第一次出现的位置，如果没有与之匹配的元素，则返回 -1
5	LastIndexOf()	查找数组中是否含有某个元素，返回该元素最后一次出现的位置

【实例 2】使用数组中的 Sort 方法完成对数组元素的排序。

根据题目要求，代码如下。
class Program
{
    static void Main(string[] args)
    {
        int[] a = { 5, 3, 2, 4, 1 };
        Array.Sort(a);
        Console.WriteLine("排序后的结果为：");
        foreach(int b in a)
        {
            Console.Write(b + " ");
        }
        Console.WriteLine();
    }
}


虽然在数组中并没有提供对其降序排序的方法，但可以先将数组中的元素使用 Sort 排序,再使用 Reverse 方法将数组中的元素逆序，这样就完成了从大到小的排序。