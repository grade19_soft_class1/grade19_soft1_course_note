# 循环、Map和Set

## __循环__

### 要计算1+2+3，我们可以直接写表达式：
```
1 + 2 + 3; // 6
```

### 要计算1+2+3+...+10，勉强也能写出来。

### 但是，要计算1+2+3+...+10000，直接写表达式就不可能了。

* 为了让计算机能计算成千上万次的重复运算，我们就需要 __循环语句__。

### JavaScript的循环有两种，一种是for循环，通过 __初始条件、结束条件和递增条件__ 来循环执行语句块：
```
var x = 0;
var i;
for (i=1; i<=10000; i++) {
    x = x + i;
}
x; // 50005000
```

* 让我们来分析一下for循环的控制条件：
```
i=1 这是初始条件，将变量i置为1；
i<=10000 这是判断条件，满足时就继续循环，不满足就退出循环；
i++ 这是每次循环后的递增条件，由于每次循环后变量i都会加1，因此它终将在若干次循环后不满足判断条件i<=10000而退出循环。
```

## __练习__

### 利用for循环计算1 * 2 * 3 * ... * 10的结果：
```
'use strict';
var x = ?;
var i;
for ...

if (x === 3628800) {
    console.log('1 x 2 x 3 x ... x 10 = ' + x);
}
else {
    console.log('计算错误');
}
```

* 解：
```
'use strict';

var x = 1;
var i=1;
for(i;i<=10;i++){
    x*=i
}

console.log(x);

if (x === 3628800) {
    console.log('1 x 2 x 3 x ... x 10 = ' + x);
}
else {
    console.log('计算错误');
}
```

### for循环最常用的地方是利用 __索引__ 来遍历数组：
```
var arr = ['Apple', 'Google', 'Microsoft'];
var i, x;
for (i=0; i<arr.length; i++) {
    x = arr[i];
    console.log(x);
}
```

### __for循环的3个条件都是可以省略的__，如果没有退出循环的判断条件，就必须使用 __break语句__ 退出循环，否则就是死循环：
```
var x = 0;
for (;;) { // 将无限循环下去
    if (x > 100) {
        break; // 通过if判断来退出循环
    }
    x ++;
}
```
## __for ... in__
```
写法格式：for(var v in arrObj){...}

当arrObj是数组时，v代表索引

当arrObj是对象时，v代表属性
```

### for循环的一个变体是for ... in循环，它可以把一个对象的所有属性 __依次循环__ 出来：
```
var o = {
    name: 'Jack',
    age: 20,
    city: 'Beijing'
};
for (var key in o) {
    console.log(key); // 'name', 'age', 'city'
}
```

### 要过滤掉对象继承的属性，用 __hasOwnProperty()__ 来实现：
```
var o = {
    name: 'Jack',
    age: 20,
    city: 'Beijing'
};
for (var key in o) {
    if (o.hasOwnProperty(key)) {
        console.log(key); // 'name', 'age', 'city'
    }
}
```

### 由于Array也是对象，而它的每个元素的索引被视为对象的属性，因此，for ... in循环可以直接循环出Array的索引：
```
var a = ['A', 'B', 'C'];
for (var i in a) {
    console.log(i); // '0', '1', '2'
    console.log(a[i]); // 'A', 'B', 'C'
}
```

* 请注意，for ... in对Array的循环得到的是 __String__ 而不是 __Number__。

## __while__

### for循环在已知循环的初始和结束条件时非常有用。而上述忽略了条件的for循环容易让人看不清循环的逻辑，此时用while循环更佳。

### while循环只有一个判断条件，条件满足，就不断循环，条件不满足时则退出循环。比如我们要计算100以内所有奇数之和，可以用while循环实现：
```
var x = 0;
var n = 99;
while (n > 0) {
    x = x + n;
    n = n - 2;
}
x; // 2500
```

### 在循环内部变量n不断自减，直到变为-1时，不再满足while条件，循环退出。

## __do ... while__

### 最后一种循环是do { ... } while()循环，它和while循环的唯一区别在于，不是在每次循环开始的时候判断条件，而是在每次循环完成的时候判断条件：
```
var n = 0;
do {
    n = n + 1;
} while (n < 100);
n; // 100
```

### 用 __do { ... } while()__ 循环要小心，循环体会 __至少执行1次__，而for和while循环则可能一次都不执行。

## __练习__

### 请利用循环遍历数组中的每个名字，并显示Hello, xxx!：

* 请尝试for循环和while循环，并以正序、倒序两种方式遍历。
```
'use strict';
var arr = ['Bart', 'Lisa', 'Adam'];
for ...
```

* 解：
```
'use strict';

var arr = ['Bart', 'Lisa', 'Adam'];

console.log('1.for循环正序')

for(var i=0;i<arr.length;i++){
    console.log('Hello'+','+arr[i]+'!');
}


console.log('')

console.log('2.for循环倒序')

for(var i=arr.length-1;i>=0;i--){
    console.log('Hello'+','+arr[i]+'!');
}


console.log('')

console.log('3.while循环正序')

var n=arr.length;

while(n>0){
    console.log('Hello'+','+arr[arr.length-n]+'!');
    n-=1;
}


console.log('')

console.log('4.while循环倒序')

var n=arr.length;

while(n>0){
    console.log('Hello'+','+arr[n-1]+'!');
    n-=1;
}
```
 


## 小结

* 循环是让计算机做重复任务的有效的方法，有些时候，如果代码写得有问题，会让程序陷入“死循环”，也就是永远循环下去。

> JavaScript的死循环会让浏览器无法正常显示或执行当前页面的逻辑，有的浏览器会直接挂掉，有的浏览器会在一段时间后提示你强行终止JavaScript的执行，因此，要特别注意死循环的问题。

### 在编写循环代码时，务必小心编写初始条件和判断条件，尤其是边界值。特别注意i < 100和i <= 100是不同的判断逻辑。

## __Map和Set

* 传送门

> ## [Map和Set](./content/Map和Set.md)