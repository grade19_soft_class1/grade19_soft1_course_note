##  装饰器
```
'use strict';

var count = 0;
var oldParseInt = parseInt; // 保存原函数

window.parseInt = function () {
    count += 1;
    return oldParseInt.apply(null, arguments); // 调用原函数
};
// 测试:
parseInt('10');
parseInt('20');
parseInt('30');
console.log('count = ' + count); // 3
```
1.  以上将parseInt赋值给oldParseInt，再将parseInt重新定义使parseInt获得记录使用次数的作用。其中apply(null , arguments);使对象指向null不改变parseInt原有的作用。
## 高阶函数
1. 定义：那么一个函数就可以接收另一个函数作为参数，这种函数就称之为高阶函数。例如：
```
function add(x, y, f) {
    return f(x) + f(y);
}
```
2. 其中f是函数(function)；编写高阶函数，就是让函数的参数能够接收别的函数。
## map
1. map能将函数作用于数组的每一个元素。例如：
```
'use strict';

function pow(x) {
    return x * x;
}
var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
var results = arr.map(pow); // [1, 4, 9, 16, 25, 36, 49, 64, 81]
console.log(results);
```
2. 注意！！！。。。map()传入参数是函数本身。