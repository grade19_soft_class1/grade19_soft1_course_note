## sort
Array的sort()方法默认把所有元素先转换为String再排序   
幸运的是，sort()方法也是一个高阶函数，它还可以接收一个比较函数来实现自定义的排序。   
最后友情提示，sort()方法会直接对Array进行修改，它返回的结果仍是当前Array   
```
var arr = [10, 20, 1, 2];
arr.sort(function (x, y) {
    if (x < y) {
        return 1;
    }
    if (x > y) {
        return -1;
    }
    return 0;
}); // [20, 10, 2, 1]
```
其中return为-1，则向后移动，return为0则不变，return为1则向前移动
## Array中的高阶函数
1. every() 判断函数是否满足测试条件
```
'use strict';
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.every(function (s) {
    return s.length > 0;
})); // true, 因为每个元素都满足s.length>0

console.log(arr.every(function (s) {
    return s.toLowerCase() === s;
})); // false, 因为不是每个元素都全部是小写
```
2. find()用于寻找符合条件的第一个元素；
```
'use strict';
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.find(function (s) {
    return s.toLowerCase() === s;
})); // 'pear', 因为pear全部是小写

console.log(arr.find(function (s) {
    return s.toUpperCase() === s;
})); // undefined, 因为没有全部是大写的元素
```
3. findIndex与find类似，不同在于返回的是元素的下标，未找到时返回-1；
```
'use strict';
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.findIndex(function (s) {
    return s.toLowerCase() === s;
})); // 1, 因为'pear'的索引是1

console.log(arr.findIndex(function (s) {
    return s.toUpperCase() === s;
})); // -1
```
4. forEach()用于遍历数组，传入函数不需要返回值；
```
'use strict';
var arr = ['Apple', 'pear', 'orange'];
arr.forEach(console.log); // 依次打印每个元素
```