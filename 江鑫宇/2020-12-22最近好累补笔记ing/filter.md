### filter
filter经常用于吧array的某些元素过滤掉，然后返回剩下的元素   
```
var arr = [1,2,4,5,6,9,10,15]
var r = arr.filter(function (){
    return x % 2 !==0
})
r;//[1,5,9,15]
```
filter函数会将满足条件的筛选出来filter();   
### 回调函数  
filter()接收的回调函数，其实可以有多个参数。通常我们仅使用第一个参数，表示Array的某个元素。回调函数还可以接收另外两个参数，表示元素的位置和数组本身：
```
var arr = ['A', 'B', 'C'];
var r = arr.filter(function (element, index, self) {
    console.log(element); // 依次打印'A', 'B', 'C'
    console.log(index); // 依次打印0, 1, 2
    console.log(self); // self就是变量arr
    return true;
});
```
利用filter，可以巧妙地去除Array的重复元素：
```
'use strict';

var
    r,
    arr = ['apple', 'strawberry', 'banana', 'pear', 'apple', 'orange', 'orange', 'strawberry'];
r = arr.filter(function (element, index, self) 
    {
        return self.indexOf(element) === index;
    });

    console.log(r.toString());
``` 
去除重复元素依靠的是indexOf总是返回第一个元素的位置，后续的重复元素位置与indexOf返回的位置不相等，因此被filter滤掉了。