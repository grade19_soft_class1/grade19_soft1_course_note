## 方法
```
function getAge() {
    var y = new Date().getFullYear();
    return y - this.birth;
}

var xiaoming = {
    name: '小明',
    birth: 1990,
    age: getAge
};

xiaoming.age(); // 30, 正常结果
getAge(); // NaN
// 这里xiaoming.age()的this指向被调用的对象；单独调用函数getAge()，此时，该函数的this指向全局对象，也就是window。
```

1. 方法重构时可能会发生错误，原因是this指针只在age方法的函数内指向xiaoming，在函数内部定义的函数，this又指向undefined了！（在非strict模式下，它重新指向全局对象window！）
```
'use strict';

var xiaoming = {
    name: '小明',
    birth: 1990,
    age: function () {
        function getAgeFromBirth() {
            var y = new Date().getFullYear();
            return y - this.birth;
        }
        return getAgeFromBirth();
    }
};
xiaoming.age(); // Uncaught TypeError: Cannot read property 'birth' of undefined
```
```
//上面的解决方法
'use strict';

var xiaoming = {
    let that = this
    name: '小明',
    birth: 1990,
    age: function () {
        function getAgeFromBirth() {
            var y = new Date().getFullYear();
            return y - that.birth;
        }
        return getAgeFromBirth();
    }
};
xiaoming.age();//30
```
# 在独立的函数调用中在strict模式，this指向undefined 否则指向 window 但我们可以同apply 或 call 控制this的指向

2. 可以用函数本身的apply方法，它接收两个参数，第一个参数就是需要绑定的this变量，第二个参数是Array，表示函数本身的参数。

3. 另一个与apply()类似的方法是call()，唯一区别是：
+ apply()把参数打包成Array再传入；
+ call()把参数按顺序传入。