## set
1. Set和Map类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在Set中，没有重复的key。
2. 要创建一个Set，需要提供一个Array作为输入，或者直接创建一个空Set
```
var  s1 = new set();//空
var s2 = new set([1,2,3,4]);//1，2，3，4
```
3. 重复元素在Set中自动被过滤,注意数字3和字符串'3'是不同的元素。
```
var s3 = new set([1,2,2,3,'3']);//1,2,3,'3'

```
4. 可以通过add(key)的方法添加到元素到Set中。
```
var s = new Set([1,2,3]);
s.add(4);
s//1,2,3,4
```
5. delete(key)可以删除元素
```
var s = new Set([1,2,3,4]);
s.delete(4);
s//1,2,3
```
# iterable
1. iterable 音标['itəreibl]：重音在['i]
2. Array、Map和Set都属于iterable类型，具有iterable类型的集合可以通过for ... of循环来遍历。
3. 用for ... of循环遍历集合，用法如下：
```
var a = ['A', 'B', 'C'];
var s = new Set(['A', 'B', 'C']);
var m = new Map([[1, 'x'], [2, 'y'], [3, 'z']]);
for (var x of a) { // 遍历Array
    console.log(x);
}
for (var x of s) { // 遍历Set
    console.log(x);
}
for (var x of m) { // 遍历Map
    console.log(x[0] + '=' + x[1]);
}
```
4. for...in中的x表示数组的下标，并且包含手动添加的属性，for...of中的x表示的是其中的值，但不包含手动添加的元素
```
       var a = ['A','B','C'];
        a.name = 'Hello';
        for(var x in a){
            console.log(a[x]);
        }
        for(var x of a){
            console.log(x);
        }
 
```
5. iterable的内置forEach;
```
    var a = ['A', 'B', 'C'];
    a.forEach(function (element, index, array) {
    // element: 指向当前元素的值
    // index: 指向当前索引
    // array: 指向Array对象本身
    console.log(element + ', index = ' + index);
    console.log(array);//['A','B','C']
});
```
6. 如果对某些参数不感兴趣，由于JavaScript的函数调用不要求参数必须一致，因此可以忽略它们。例如，只需要获得Array的element可以
```
var a = [1,2,3,5]
a.forEach(element){
    console.log(element);//1,2,3,5
}
```

## 公式丫

1. 确认过眼神，是我不会的东西
2. 在mark down 中写公式需要 mark down all in one 扩展包
3. $$ E = MC^2 $$
   
4. 图片示例: ![](./2020-12-03_074243.png)
