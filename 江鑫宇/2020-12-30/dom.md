## 练习题专区
### 第一题
```
//html内部结构
 <div id="test-div">
        <p id="test-js">javascript</p>
        <p>Java</p>
    </div>

//js内部
'use strict'

// 获取<p>javascript</p>节点:
var js = document.getElementById('test-js');

// 修改文本为JavaScript:
// TODO:
js.innerHTML = 'JavaScript';
// 修改CSS为: color: #ff0000, font-weight: bold
// TODO:
js.style.color = '#ff0000';
js.style.fontWeight = 'bold';
// 测试:
if (js && js.parentNode && js.parentNode.id === 'test-div' && js.id === 'test-js') {
    if (js.innerText === 'JavaScript') {
        if (js.style && js.style.fontWeight === 'bold' && (js.style.color === 'red' || js.style.color === '#ff0000' || js.style.color === '#f00' || js.style.color === 'rgb(255, 0, 0)')) {
            console.log('测试通过!');
        } else {
            console.log('CSS样式测试失败!');
        }
    } else {
        console.log('文本测试失败!');
    }
} else {
    console.log('节点测试失败!');
}
```
### 第二题
```
//html结构
    <ol id="test-list">
        <li class="lang">Scheme</li>
        <li class="lang">JavaScript</li>
        <li class="lang">Python</li>
        <li class="lang">Ruby</li>
        <li class="lang">Haskell</li>
    </ol>

//js内部代码
'use strict'
// sort list:
var arr = [];
var t = document.getElementById('test-list').children;

for (let i = 0; i < t.length; i++) {
    arr.push(t[i].innerHTML);

}

let test = arr.sort();

for (let i = 0; i < test.length; i++) {
    document.getElementById('test-list').children[i].innerHTML = test[i];
}
console.log(t[0].innerHTML);

(function() {
    var
        arr, i,
        t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 5) {
        arr = [];
        for (i = 0; i < t.children.length; i++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['Haskell', 'JavaScript', 'Python', 'Ruby', 'Scheme'].toString()) {
            console.log('测试通过!');
        } else {
            console.log('测试失败: ' + arr.toString());
        }
    } else {
        console.log('测试失败!');
    }
})();
```
### 第三题
```
 <!-- HTML结构 -->
    <ul id="test-list">
        <li>JavaScript</li>
        <li>Swift</li>
        <li>HTML</li>
        <li>ANSI C</li>
        <li>CSS</li>
        <li>DirectX</li>
    </ul>

//js内部代码
'use strict';
// TODO
var d1 = document.getElementById('test-list');
d1.removeChild(d1.children[1]);
d1.removeChild(d1.children[2]);
d1.removeChild(d1.children[3]);
console.log(d1);
// 测试:

(function() {
    var
        arr, i,
        t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 3) {
        arr = [];
        for (i = 0; i < t.children.length; i++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['JavaScript', 'HTML', 'CSS'].toString()) {
            console.log('测试通过!');
        } else {
            console.log('测试失败: ' + arr.toString());
        }
    } else {
        console.log('测试失败!');
    }
})();
```