'use strict'
//方案一
//         不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：

// 'use strict';

// function string2int(s) {
//     var arr = [];
//     for(let i = 0;i <s.length;i++){
//         arr.push(s[i]);
//         arr[i] + 0;
//         arr[i] = arr[i]*1; 
//     }
    
//     return arr.reduce(function (m,n)
//     {
//         return m*10+n;
//     })
// }

// function stringToInt(res){
//             res + 0;
//             res = res*1;
//         return res;
// }
//方案二
// function string2int(s){
//     var arr = [];
//     for(let i = 0;i<s.length;i++){
//         arr.push(s[i]);
//     }
//         if(arr.length === 0){
//             alert('未传入参数');
//         }else{
//         arr = arr.map(stringToInt);
//         }
//      let rest = arr.reduce((m,n) =>{ return(m * 10 + n) });
//     return rest;
// }


// 测试:
// if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
// if (string2int.toString().indexOf('parseInt') !== -1) {
//     console.log('请勿使用parseInt()!');
// } else if (string2int.toString().indexOf('Number') !== -1) {
//     console.log('请勿使用Number()!');
// } else {
//     console.log('测试通过!');
// }
// }
// else {
// console.log('测试失败!');
// }

//请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。

function normalize(arr) {
    var name = '';
    var sum;
    var res = [];
    for(let i =0;i<arr.length;i++){
        name = arr[i];
        sum = name[0].toUpperCase()+name.split(2).toLowerCase();
        res.push(sum);
    }
    return res;
}

var text =  normalize(['adam', 'LISA', 'barT']);
console.log(text);

// 测试:
if (normalize(['adam', 'LISA', 'barT']) === ['Adam', 'Lisa', 'Bart']) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}



