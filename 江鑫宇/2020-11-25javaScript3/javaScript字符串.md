## 字符串
# 多行字符串
```
'use strict'

console.log(
    `
    这就是
    多行
    字符串
    打印出来后是多行的
    `
);
```

# 字符串检索
```
//h[i] 用于检索字符串中每一个字符，从0开始，超出索引返回undefined
var h  ='Hello Word!'
console.log(h[0]);
console.log(h[13]);
h[2]='L'//字符串无法更改，h[2]依旧是l
console.log(h[2]);
```
# 字符串拼接
```
    // ${}用于连接字符串，{}中的值随变量变化类似于“+”号
var name = '小明';
var age = 20;
var message = `你好, ${name}, 你今年${age}岁了!`;
alert(message);

```

# 奇奇怪怪的函数
```
//toUpperCase将字符串中的英文字符变成大写
var a = 'myword';
console.log(a.toUpperCase());

//tiLowerCase将字符串中的英文字符变成小写
var b = 'MYWord';
console.log(b.toLowerCase());
```