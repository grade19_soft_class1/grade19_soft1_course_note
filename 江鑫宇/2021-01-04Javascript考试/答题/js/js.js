'use strict';
/*
1、题目描述
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例1
输入
[ 1, 2, 3, 4 ], 3
输出
2

function indexOf(arr, item) {

}
*/
function indexOf(arr, item) {
    return arr.indexOf(item);
}

var test1 = indexOf([1, 2, 3, 4], 3);
console.log('第一题：' + test1);

/*
2、题目描述
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

10

function sum(arr) {

}
*/
function sum(arr) {
    return arr.reduce(function(n, m) {
        return n + m;
    })
}

var test2 = sum([1, 2, 3, 4]);
console.log('第二题：' + test2);


/*
3、题目描述
移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4, 2], 2
输出

[1, 3, 4]

function remove(arr, item) {

}
*/

function remove(arr, item) {
    let res = [];
    for (let i = 0; i < arr.length; i++) {
        if (arr.indexOf(item) >= 0) {
            res = arr.slice(arr.indexOf(item) - 1, 3);
        }
        return res;
    }
}

let test3 = remove([1, 2, 3, 4, 2], 2);
console.log('第三题：' + test3);

/*
4、题目描述
移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
示例1
输入

[1, 2, 2, 3, 4, 2, 2], 2
输出

[1, 3, 4]

function removeWithoutCopy(arr, item) {

}
*/
function removeWithoutCopy(arr, item) {
    let sun = [];
    for (let i = 0; i < arr.length; i++) {
        console.log(arr.indexOf(item));
        if (arr[arr.indexOf(item)] != arr[i]) {
            sun.push(arr[i]);
        }

    }

    return sun;
}
var text4 = removeWithoutCopy([1, 2, 2, 3, 4, 2, 2], 2);
console.log(text4);

/*
5、题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4],  10
输出

[1, 2, 3, 4, 10]

function append(arr, item) {
    
}
*/

function append(arr, item) {
    let res = [];
    for (let i = 0; i < arr.length; i++) {
        res.push(arr[i]);
    }
    res.push(item);
    return res;

}

var test5 = append([1, 2, 3, 4], 10);
console.log('第五题：' + test5);


/*
6、题目描述
删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 2, 3]

function truncate(arr) {

}
*/
function truncate(arr) {
    let res = [];
    for (let i = 0; i < arr.length - 1; i++) {
        res.push(arr[i]);
    }

    return res;
}
var test6 = truncate([1, 2, 3, 4]);
console.log('第六题：' + test6);
/*
7、题目描述
在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], 10
输出

[10, 1, 2, 3, 4]

function prepend(arr, item) {

}
*/
function prepend(arr, item) {
    let res = [];
    res.push(item);
    for (let i = 0; i < arr.length; i++) {
        res.push(arr[i]);
    }

    return res;
}

var test7 = prepend([1, 2, 3, 4], 10);
console.log('第七题：' + test7);
/*
8、题目描述
删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[2, 3, 4]

function curtail(arr) {

}
*/
function curtail(arr) {
    let res = [];

    for (let i = 1; i < arr.length; i++) {
        res.push(arr[i]);
    }

    return res;
}

var test8 = curtail([1, 2, 3, 4]);
console.log('第八题：' + test8);

/*
9、题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], ['a', 'b', 'c', 1]
输出

[1, 2, 3, 4, 'a', 'b', 'c', 1]

function concat(arr1, arr2) {

}
*/

function concat(arr1, arr2) {
    return arr1.concat(arr2);
}

var test9 = concat([1, 2, 3, 4], ['a', 'b', 'c', 1]);
console.log('第九题：' + test9);
/*
10、题目描述
在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], 'z', 2
输出

[1, 2, 'z', 3, 4]

function insert(arr, item, index) {

}
*/
// function insert(arr, item, index) {
//     return arr.splic('item', 2);
// }

// var test10 = insert([1, 2, 3, 4], 'z', 2);

// console.log('第10题：' + test10);
/*
11、题目描述
统计数组 arr 中值等于 item 的元素出现的次数
示例1
输入

[1, 2, 4, 4, 3, 4, 3], 4
输出

3

function count(arr, item) {

}
*/
function count(arr, item) {
    let sun = 0;
    for (let i = 0; i < arr.length; i++) {
        if (item == arr[i]) {
            sun++;
        }
    }
    return sun;
}
var test11 = count([1, 2, 4, 4, 3, 4, 3], 4);
console.log('第11题：' + test11);

/*
12、题目描述
找出数组 arr 中重复出现过的元素
示例1
输入

[1, 2, 4, 4, 3, 3, 1, 5, 3]
输出

[1, 3, 4]

function duplicates(arr) {

}
*/
function duplicates(arr) {
    let arr1 = [];
    for (let i = 0; i < arr.length; i++) {

        if (arr.indexOf(arr[i]) !== arr.lastIndexOf(arr[i]) && arr.indexOf(arr[i]) != -1) {
            arr1.push(arr[i]);
        }
    }
    return arr1;
}
var test12 = duplicates([1, 2, 4, 4, 3, 3, 1, 5, 3]);
console.log('第12题：' + test12);

/*
13、题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 4, 9, 16]

function square(arr) {

}
*/
function square(arr) {
    return arr.map(function(n) {
        return n * n;
    })
}
var arr13 = [1, 2, 3, 4];
var test13 = square(arr13);
console.log(arr13);
console.log('第13题：' + test13);
/*
14、题目描述
在数组 arr 中，查找值与 item 相等的元素出现的所有位置
示例1
输入

['a','b','c','d','e','f','a','b','c'] 'a'
输出

[0, 6]

function findAllOccurrences(arr, target) {

}
*/


/*
15、题目描述
判断 val1 和 val2 是否完全等同

function identity(val1, val2) {

}
*/
function identity(val1, val2) {
    if (val1 === val2) {
        return true;
    } else {
        return false;
    }
}

var test15 = identity(1, 1);
console.log('第15题：' + test15);
/*
16、题目描述
定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14

function area_of_circle(r, pi) {

}
*/
function area_of_circle(r, pi) {
    let s = 0;

    if (pi === undefined) {
        s = 3.14 * r * r;
        return s;
    } else {
        s = r * r * pi;
        return s;
    }

}

var test16 = area_of_circle(1);
console.log('第16题：' + test16);
/*
17、题目描述

<!-- HTML结构 -->
<ul id="test-list">
    <li>JavaScript</li>
    <li>Swift</li>
    <li>HTML</li>
    <li>ANSI C</li>
    <li>CSS</li>
    <li>DirectX</li>
</ul>

针对以上文档结构，请把与Web开发技术不相关的节点删掉：

注意！！请分别使用原生JS和jQuery两种试实现！！！！！

*/
//原生js
// var swift = document.getElementById('test-list').children[1];
// swift.remove(swift);
// var ansi = document.getElementById('test-list').children[2];
// ansi.remove(ansi);
// var directx = document.getElementById('test-list').children[3];
// directx.remove(directx);

//jQuery
var swift = $("#test-list>li")[1];
swift.remove();
var ansi = $('#test-list>li')[2];
ansi.remove();
var directx = $('#test-list>li')[3];
directx.remove();





/*
18、题目描述

对如下的Form表单：

<!-- HTML结构 -->
<form id="test-form" action="test">
    <legend>请选择想要学习的编程语言：</legend>
    <fieldset>
        <p><label class="selectAll"><input type="checkbox"> <span class="selectAll">全选</span>
        <span class="deselectAll">全不选</span></label> <a href="#0" class="invertSelect">反选</a></p>
        <p><label><input type="checkbox" name="lang" value="javascript"> JavaScript</label></p>
        <p><label><input type="checkbox" name="lang" value="python"> Python</label></p>
        <p><label><input type="checkbox" name="lang" value="ruby"> Ruby</label></p>
        <p><label><input type="checkbox" name="lang" value="haskell"> Haskell</label></p>
        <p><label><input type="checkbox" name="lang" value="scheme"> Scheme</label></p>
		<p><button type="submit">Submit</button></p>
    </fieldset>
</form>

要求:
绑定合适的事件处理函数，实现以下逻辑：

当用户勾上“全选”时，自动选中所有语言，并把“全选”变成“全不选”；

当用户去掉“全不选”时，自动不选中所有语言；

当用户点击“反选”时，自动把所有语言状态反转（选中的变为未选，未选的变为选中）；

当用户把所有语言都手动勾上时，“全选”被自动勾上，并变为“全不选”；

当用户手动去掉选中至少一种语言时，“全不选”自动被去掉选中，并变为“全选”。


*/

var
    form = $('#test-form'),
    langs = form.find('[name= lang]'),
    invertSelect = form.find('.invertSelect'),
    all = form.find('#head-all'),
    deselectAll = form.find('.deselectAll');

var selectAll = $('#head');


deselectAll.hide();

function allSelect() {

    if (selectAll[0].checked) {
        langs.prop('checked', true);
        deselectAll.show();
        all.hide();

    } else {
        langs.prop('checked', false);
        deselectAll.hide();
        all.show();
    }
}

function selectOne() {
    let sun = 0;
    langs.each(function() {
        if (this.checked) {
            sun++;
        }
    })

    selectAll.prop('checked', langs.length === sun ? true : false);
    if (langs.length === sun) {
        allSelect();
    } else {
        deselectAll.hide();
        all.show();
    }
}


function invertSelect1() {
    langs.each(function() {
        if (this.checked) {
            $(this).prop('checked', false);
        } else {
            $(this).prop('checked', true);
        }
    })
    selectOne();
}