## generator
函数在执行过程中没有遇到return时就是隐含了 return undefined   
generator跟函数很像，定义如下：
```
function* foo(x) {
    yield x + 1;
    yield x + 2;
    return x + 3;
}
```
generator和函数不同的是，generator由function*定义并且，除了return语句，还可以用yield返回多次。   
因此，generator就是可以多次返回的函数，那么generator有什么用呢？？      
generator 可以用于返回多次数的函数，我们以一个著名的斐波那契数列为例，它由0，1开头：
```
0 1 1 2 3 5 8 13 21 34 ...
```
要编写一个产生斐波那契数列的函数，可以这么写：
```
function fib(max) {
    var
        t,
        a = 0,
        b = 1,
        arr = [0, 1];
    while (arr.length < max) {
        [a, b] = [b, a + b];
        arr.push(b);
    }
    return arr;
}

// 测试:
fib(5); // [0, 1, 1, 2, 3]
fib(10); // [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
```
函数只能返回一次，所以必须返回一个Array。但是，如果换成generator，就可以一次返回一个数，不断返回多次。用generator改写如下：
```
function* fib(max) {
    var
        t,
        a = 0,
        b = 1,
        n = 0;
    while (n < max) {
        yield a;
        [a, b] = [b, a + b];
        n ++;
    }
    return;
}
```
直接调用一个generator和调用函数不一样，fib(5)仅仅是创建了一个generator对象，还没有去执行它。

调用generator对象有两个方法，一是不断地调用generator对象的next()方法：
```
fib.next();
```

第二个方法是直接用for ... of循环迭代generator对象，这种方式不需要我们自己判断done：
```
'use strict'

function* fib(max) {
    var
        t,
        a = 0,
        b = 1,
        n = 0;
    while (n < max) {
        yield a;
        [a, b] = [b, a + b];
        n ++;
    }
    return;
}
for (var x of fib(10)) {
    console.log(x); // 依次输出0, 1, 1, 2, 3, ...
}
```
