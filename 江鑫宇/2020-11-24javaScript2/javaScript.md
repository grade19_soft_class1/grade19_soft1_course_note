## JavaScript
# 一些函数
```
//a.indexOf(i)查找i在数组a中的位置（返回下标），未查询到返回-1；
var arr1 =[1,2,6,3,4,5,'he'];
console.log(arr1.indexOf(1));
console.log(arr1.indexOf(2));
console.log(arr1.indexOf(3));
console.log(arr1.indexOf('he'))

var arr2=arr1.slice(1,3);//包含开始下标，不包含结束下标
console.log(arr1.reverse());//倒序输出数组
console.log(arr1.reverse());//用两次就变成正序了，奇奇怪怪

console.log(arr1.splice(2));//去掉前2个元素.提取剩下的组成数组
console.log(arr1);//1，2
console.log(arr1.splice(0,0,0));
console.log(arr1);
```
# 对象
```
//NaN表示无法计算结果，NaN不等于任何数，也不等于自己唯一能判断NaN的是函数IsNaN();
//Infinity 表示无穷大


//对象
var person ={
    name: 'jxy',
    age: 21,
    majot: '软件技术',
    city: 'fujian'
};

var listperson =[person.name,person.age,person.majot,person.city];
    console.log(person.name);//打印对象中的name
    console.log(listperson.splice(0));//打印数组

```
# strict模式
1. 为了修补JavaScript都不用var申明造成变量间互相影响，强制使用var申明变量
2. 'use strict'将这写在JavaScript代码的第一行进入strict模式
```
'use strict'
abc ='Hello javaScript';
console.log(abc);//将会报错

var a = 'Hello javaScript'
console.log(a);//将打印出Hello javaScript
```