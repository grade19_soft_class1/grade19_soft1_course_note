## javaScript
```
console.log('这是在控制台打印')

//这里定义一个变量
var a = 1;


//弱语言，这里的a可以代表这个类；有点类似与委托
a=function s(n){
    return n+1;
}

var i= a(2);
console.log(i);

//当console.log中的逻辑为bool类型时会变从true Or false
console.log(3!==3)//false
console.log(2>1)//true

//等于号 ===  和 ==
// ===先比较数据类型再比较值；==先转化数据类型再比较值

var ab = 3;
var AB ='3';

console.log(ab==Ab);//true
console.log(ab===Ab);//false

var x=[1,2,3,4];
var y=['x','y','z'];
var z=[['a','b','c'],[9,0]];

console.log(x.concat(y,z));
//1,2,3,4,'x','y','z',array[3],array[2]
console.log(x.concat(y,['a','b'],[55,32,78]));
//'x','y','z','a','b',55,32,78
console.log(x.join('_'));
//结果1_2_3_4

//join('_')中的 "_"只可以将join前面的 ","替换成 "_"
//concat()拼接数组只能拼接一维数组。拼接二维数组时会显示array[i];i表示array的长度
```
1. 且：&& 或：|| 非：!
2. null表示值还未赋值
3. undefined表示变量不存在
4. x.join将x数列中的","替换成"_"
5. x.concat将x数列和其它数列拼接
