## **<随堂笔记>**
### **今日内容如下：**
**高阶函数**
1. **Map()**
2. **reduce()**
3. **filter()**
**Map()函数：**
   
+ **不改变原来的数组的内容，返回一个新的数组**
    
例如：求立方📏
```
function cube(x){
    return x*x*x
}

var arr = [1,3,5,7,9];
var result = arr.map(cube);
console.log(result); // [1, 27, 125, 343, 729]
```
还能直接将数组里的数值转换成字符串类型₡
```
var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
var result = arr.map(String); 
console.log(result); // ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
```
**reduce()函数**
+ **用作求和求积非常方便,必须接受两个参数**
   
例如： 求数组的和
```
var array = [2, 4, 6, 8, 10];
var result = array.reduce(function (x, y) {
    return x + y;
})
console.log(result); // 30
```
PS: 求积就乘法得了
   
**filter()方法**
   
简介：
   
filter()方法会创建一个新数组，原数组的每个元素传入回调函数中，回调函数中有return返回值，若返回值为true，这个元素保存到新数组中；若返回值为false，则该元素不保存到新数组中；原数组不发生改变。
   
语法：
   
`array.filter(function(currentValue,index,arr), thisValue)`
   
![](./imgs/filter语法.png)
   
例子：在array中，保留偶数：
```
var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
var s = arr.filter(function (x) {
    return x % 2 == 0;
})
console.log(s);
```
去除Array中的空字符串：
   
```
var arr = ['A', '', 'B', null, undefined, 'C', '  '];
var r = arr.filter(function (s) {
    return s && s.trim(); 
});
console.log(r);// ['A', 'B', 'C']
```
**trim()方法：**
+ **trim()方法是用来删除字符串两端的空白字符并返回，trim方法并不影响原来的字符串本身，它返回的是一个新的字符串。**
+ **🈲缺陷：只能去除字符串两端的空格，不能去除中间的空格**
```
var str = " 6 6 ";
var str_1 = str.trim();
console.log(str_1); //6 6//输出左右侧均无空格
```
> **单独去除左侧空格则使用 str.trimLeft(); //var str_1 = str.trimLeft();**

> **单独去除右侧空格则使用 str.trimRight();//var str_1 = str.trimRight();**
    
**replace正则匹配方法**
**去除字符串内所有的空格：`str = str.replace(/\s*/g,"");`**
   
例如：
   
```
var str = " 6 6 ";
var str_1 = str.replace(/\s*/g,"");
console.log(str_1); //66
```
**利用filter()对数组进行去重**
```
var arr = [1,2,3,4,5,6,7,7,8,8,9,9,6,6,5,5,3,3];
var r = arr.filter(function(element,index,item){
    return item.indexOf(element) === index
})

console.log(r);
```
**sort()排序**
+ 只对首字符进行排序，若要对数字大小排序，如下：
```
var arr = [100, 99, 42, 2];
arr.sort(function (x, y) {
    if (x < y) {
        return -1;
    }
    if (x > y) {
        return 1;
    }
    return 0;
});
console.log(arr); // [2, 42, 99, 100]
```
   
**倒序则把两个返回值调换**
   
---
练习：
```   
//练习：不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：

function string2int(s) {
    function str2num(str) {
        var strArr = str.split('');               //把字符串分割成字符串数组
        function toInt(data) {
            return +data;                  //通过js的弱类型转换，实现字符类型到数字类型的转换
        }
        var numArr = strArr.map(toInt);           //通过map()把字符串数组转换成数字数组
        return numArr;
    }
    var num = str2num(s);
    var res = num.reduce(function (x, y) {        //通过reduce()把数字数组转换成数字量
        return x * 10 + y;
    });
    return res;

}
console.log(string2int("12345"));

// 测试:
if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        console.log('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        console.log('请勿使用Number()!');
    } else {
        console.log('测试通过!');
    }
}
else {
    console.log('测试失败!');
}
```
   
```
//练习 请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。

'use strict';

function normalize(arr) {
    function strs(str) {
        str = str.toLowerCase(); // 先全都小写
        let x1 = str.substring(0, 1).toUpperCase();  // 提取第一位进行大写
        let x2 = str.substring(1); // 提取该元素的后面元素
        return x1 + x2;
    }
    return arr.map(strs);
}



console.log(normalize(['adam', 'LISA', 'barT']));


// 测试:
if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}
```
   
```   
//小明希望利用map()把字符串变成整数，他写的代码很简洁：

'use strict';

var arr = ['1', '2', '3'];
var r;
r = arr.map(function (value) {
    return parseInt(value)
});

console.log(r);
```
   
```
//练习 请尝试用filter()筛选出素数：能被1和自身整除

'use strict';

function get_primes(arr) {
    return arr.filter(function (element, index, item) {
        var map = { index: 0 };
        for (var i = 2; i < element; i++) {
            if (element % i == 0) {
                map.index++
            }

        }
        //此代码意思：不算1
        if (map.index == 0) {
            return element > 1
        }
    })
}



// 测试:
var
    x,
    r,
    arr = [];
for (x = 1; x < 100; x++) {
    arr.push(x);
}
r = get_primes(arr);
if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + r.toString());
}
```