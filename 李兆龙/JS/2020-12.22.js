'use strict';

var xiaoming = {
    name: '小明',
    age: 14,
    gender: true,
    height: 1.65,
    grade: null,
    'middle-school': '\"W3C\" Middle School',
    skills: ['JavaScript', 'Java', 'Python', 'Lisp']
};



function convert(key, value) {
    if (typeof value === 'string') {
        value = value.toLowerCase()
        let x1 = value.substring(0,1).toUpperCase();
        let x2 = value.substring(1);
        return x1+x2
    }
    return value;
}

var se = JSON.stringify(xiaoming, convert, '  ');
console.log(se);

const person = {
    isHuman: false,
    printIntroduction: function() {
    console.log(`My name is ${this.name}. Am I human? ${this.isHuman}`);
    }
    };

    const me = Object.create(person);

    me.name = 'Matthew';

    me.isHuman = true; 

    console.log(me.printIntroduction());



var student ={
    name:'KD',
    gender:'famale',
    height:2.08,
    age:function (){
        console.log(this.name + 'is 32 years old.');
    }
};

var NBA = {
    name:"Kevin Durant"
};

NBA._proto_ = student;

console.log(NBA);