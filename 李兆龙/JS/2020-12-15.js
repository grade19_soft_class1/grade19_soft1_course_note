function count() {
    var arr = [];
    for (var i = 1; i <= 3; i++) {
        arr.push((function (n) {
            return n * n;

        })(i));
    }
    return arr;
}

var results = count();
var f1 = results[0];
var f2 = results[1];
var f3 = results[2];

console.log(f1); // 1
console.log(f2); // 4
console.log(f3); // 9


function user() {
    var name = 'wangxi'
    return function getName() {
        return name
    }
}

var userName = user()() // userName 变量中始终保持着对 name 的引用
console.log(userName) // wangxi

userName = null // 销毁闭包，释放内存

var name = 'Schopenhauer'

function getName() {
    var name = 'Aristotle'
    var intro = function () {  // 这是一个闭包
        console.log('I am ' + name)
    }
    return intro
}

function showMyName() {
    var name = 'wangxi'
    var myName = getName()
    myName()
}

showMyName() // I am Aristotle

// 定义数字0:
var zero = function (f) {
    return function (x) {
        return x;
    }
};

var one = function (f) {
    return function (x) {
        return f(x);
    }
};


function add(n, m) {
    return function (f) {
        return function (x) {
            return m(f)(n(f)(x));
        }
    }
}
var two = add(one, one)

var three = add(one, two);

var four = add(two, two);

var five = add(two, three);

var six = add(three, three);

(one(function () {
    console.log('print 1 times');
}))();

(two(function () {
    console.log('print 2 times');
}))();

(three(function () {
    console.log('print 3 times');
}))();

(four(function () {
    console.log('print 4 times');
}))();

(five(function () {
    console.log('print 5 times');
}))();

(six(function () {
    console.log('print 6 times');
}))();