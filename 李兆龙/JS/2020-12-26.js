'use strict';

let content = document.getElementById('content')

console.log(content);

let t = document.querySelector('#top');

console.log(t);

let l = document.querySelector('#left')

console.log(l);

let r = document.querySelector('#right');

console.log(r);

setTimeout(() => {
    var
        js = document.getElementById('js'),
        list = document.getElementById('list');
    list.appendChild(js);
}, 3000);

// var hes = document.createElement('p');

// hes.id = "hes";

// hes.innerText = "hes";

// list.appendChild(hes);

// var d = document.createElement('style');

// d.setAttribute('type', 'text/css');

// d.innerHTML = 'p { color: red }';

// document.getElementsByTagName('head')[0].appendChild(d);


//按字符串顺序重新排序DOM节点：
 // sort list:
        //获取父节点
        var list =document.getElementById("test-list");
        var arr = [];
        // console.log(list);

        //将父节点的孩子放进array数组中，js中数组元素的类型可以任意，这里数组元素类型为DOM节点
        for (let i = 0; i < list.children.length; i++){
            arr.push(list.children[i]);
        }
        // console.log(arr);

        //利用高阶函数sort()对数组排序，排序规则自定义一个函数传给sort
        //题目要求根据节点的文本内容按照字符串顺序排序
        arr.sort(function(x, y){
            if (x.innerText > y.innerText){
                return 1;
            }else if (x.innerText < y.innerText){
                return -1;
            }else {
            	return 0;  
            }
        });
        // console.log(arr);

        //将节点添插入父节点的中

        // 因为我们插入的js节点已经存在于当前的文档树，因此这个节点首先会从原先的位置删除，再插入到新的位置。
        for (let i = 0; i < arr.length; i++){
            list.appendChild(arr[i]);
        }
'use strict';
// sort list:

// 测试:
; (function () {
    var
        arr, i,
        t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 5) {
        arr = [];
        for (i = 0; i < t.children.length; i++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['Haskell', 'JavaScript', 'Python', 'Ruby', 'Scheme'].toString()) {
            console.log('测试通过!');
        }
        else {
            console.log('测试失败: ' + arr.toString());
        }
    }
    else {
        console.log('测试失败!');
    }
})();
