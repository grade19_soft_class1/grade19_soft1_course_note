'use strict';

// var id = new String("1050621252");
// var password = new String("133312");

// var checkRegisterForm = function () {
//     //限制：username必须是3-10位英文字母或数字
//     //限制：口令必须是6-20位
//     //限制：两次输入口令必须一致
//     var id = document.getElementById('username').value;
//     var password = document.getElementById('password').value;
//     var password2 = document.getElementById('password-2').value;

//     if (id.length >= 3 && id.length <= 10) {
//         if (password.length >= 6 && password.length <= 20) {
//             if (password.value == password2.value) {
//                 alert("登陆成功！")
//             } else {
//                 alert("密码错误，请重新输入！")
//             }

//         } else {
//             alert("密码长度必须是6-20位！");
//         }
//     } else {
//         alert("用户名长度必须是3-10位！");
//     }


//     // TODO:
//     return false;
// }

//     // 测试:
//     ; (function () {
//         window.testFormHandler = checkRegisterForm;
//         var form = document.getElementById('test-register');
//         if (form.dispatchEvent) {
//             var event = new Event('submit', {
//                 bubbles: true,
//                 cancelable: true
//             });
//             form.dispatchEvent(event);
//         } else {
//             form.fireEvent('onsubmit');
//         }
//     })();


// function m(x){
//     return new Promise(function(resolve,reject){
//         console.log('乘法，0.5后返回结果');
//         setTimeout(resolve,500,x*x);
//     });
// }

// function add(x){
//     return new Promise(function(resolve,reject){
//         console.log('加法，1后返回结果');
//     });
// }

// let p = new Promise(function(resolve,reject){
//     console.log('Action');
//     resolve(10);
// });

// p.then(m)
// .then(add)
// .then(m)
// .then(a)
// .then((res)=>{
//     console.log('结果为：'+res);
// })


function PromiseFn(callBack){
    try{
        callBack(resolve,reject)
    }catch(e){
        reject()
    }
    let self = this;
    self.resolveVal = undefined;
    self.rejectVal = undefined;
    self.status = 'pending'//默认是等待状态
    self.keepResolveFn = []//
    self.keepRejectFn = []//
     
    function resolve(val){
        if(self.status === 'pending'){
            self.resolveVal = val;
            self.status = 'resolve';
            self.keepResolveFn.forEach(fn=>fn());
        }
    }
    function reject(val){
        if(self.status === 'pending'){
            self.rejectVal = val;
            self.status = 'reject';
            self.keepRejectFn.forEach(fn=>fn());
        }
    }
    //执行先记录resolve和reject函数事件
     
}
PromiseFn.prototype.then = function(resolveFunction,rejectFunction){
    let self = this;
    if(self.status === 'resolve'){
        resolveFunction(self.resolveVal)
    }
    if(self.status === 'reject'){
        rejectFunction(self.rejectVal)
    }
    if(self.status === 'pending'){
        self.keepResolveFn.push(()=>{
            resolveFunction(self.resolveVal);
        });
        self.keepRejectFn.push(()=>{
            rejectFunction(self.rejectVal)
        });
    }
}
let promiseA = new PromiseFn((resolve,reject)=>{
    setTimeout(function(){
        resolve('成功啦')
    },2000)
})
promiseA.then((resolveVal)=>{
    console.log('resolve'+resolveVal)
},(rejectVal)=>{
    console.log('reject'+rejectVal)
})
