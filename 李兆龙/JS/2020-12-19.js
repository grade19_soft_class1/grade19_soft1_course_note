'use strict';

function* next_id() {
    let current_id = 0;
    while (!false) {
        current_id++;
        yield current_id;
    }
}

var res = next_id();

for (var i = 0; i < 3; i++) {
    console.log(res.next().value)
}


// 测试:
var
    x,
    pass = true,
    g = next_id();
for (x = 1; x < 100; x++) {
    if (g.next().value !== x) {
        pass = false;
        console.log('测试失败!');
        break;
    }
}
if (pass) {
    console.log('测试通过!');
}

function* fib(max) {
    var
        t,
        a = 0,
        b = 1,
        n = 0;
    while (n < max) {
        yield a;
        [a, b] = [b, a + b];
        n++;
    }
    return;
}

var d = fib(5);

console.log(d.next());
console.log(d.next());
console.log(d.next());
console.log(d.next());
console.log(d.next());
console.log(d.next());


function Car() {
    this.name = 'BMW',
        this.height = '1400',
        this.lang = '4900',
        this.height = 100,
        this.run = function () {
            this.height--;
        }
}

var newCar = new Car();

console.log(newCar.name);

var s = '123';

if (s) {
    console.log(1);
} else {
    console.log(2);
}

var arr = [1.2, 2, 3];

console.log(Array.isArray(arr));


console.log(123..toString()); // '123', 注意是两个点！
console.log((123).toString()); // '123'

//练习 小明为了和女友庆祝情人节，特意制作了网页，并提前预定了法式餐厅。小明打算用JavaScript给女友一个惊喜留言：

'use strict';
var today = new Date();
if (today.getMonth() === 2 && today.getDate() === 14) {
    alert('亲爱的，我预定了晚餐，晚上6点在餐厅见！');
}

console.log(today);