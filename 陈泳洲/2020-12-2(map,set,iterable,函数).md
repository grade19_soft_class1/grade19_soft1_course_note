# 课堂笔记
## Map和Set
### Map
Map是一组键值对的结构，具有极快的查找速度。
```
var s=new Map();//创建空的map
s.set('北京',1);//添加新的key-value
s.set('福建',2);
console.log(s.has('北京'));//是否存在北京 true
s.get('福建');// 2
s.delete('北京');//删除 key 北京
console.log(s.get('北京'));//undefined

```

### Set
Set和Map类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在Set中，没有重复的key。
要创建一个Set，需要提供一个Array作为输入，或者直接创建一个空Set：
```
var s1 = new Set(); // 空Set
var s2 = new Set([1, 2, 3]); // 含1, 2, 3
```
重复元素在Set中自动被过滤：
```
var s = new Set([1, 2, 3, 3, '3']);
s; // Set {1, 2, 3, "3"}
```
Map和Set是ES6标准新增的数据类型，请根据浏览器的支持情况决定是否要使用。

## iterable
遍历Array可以采用下标循环，遍历Map和Set就无法使用下标。为了统一集合类型，ES6标准引入了新的iterable类型，Array、Map和Set都属于iterable类型。

具有iterable类型的集合可以通过新的for ... of循环来遍历。

```
var a=[1,2,3];
var b=new Set(['a','b','c']);
var c=new Map([[1,'d'],[2,'f']]);

for(var x of a){
    console.log(x);
}
for(var x of b){
    console.log(x);
}
for(var x of c){
    console.log(x[0]+x[1]);
}
```
>![图片](./imgs/2020-12-2(map,set,iterable,函数).png)


for ... in循环由于历史遗留问题，它遍历的实际上是对象的属性名称。一个Array数组实际上也是一个对象，它的每个元素的索引被视为一个属性。
```
for ... in循环将把name包括在内，但Array的length属性却不包括在内。

for ... of循环则完全修复了这些问题，它只循环集合本身的元素：
```
iterable内置的forEach方法，它接收一个函数，每次迭代就自动回调该函数。以Array为例
```
'use strict';
var a = ['A', 'B', 'C'];
a.forEach(function (element, index, array) {
    // element: 指向当前元素的值
    // index: 指向当前索引
    // array: 指向Array对象本身
    console.log(element + ', index = ' + index);
});
```
Set与Array类似，但Set没有索引，因此回调函数的前两个参数都是元素本身：
```
var s = new Set(['A', 'B', 'C']);
s.forEach(function (element, sameElement, set) {
    console.log(element);
});
```
Map的回调函数参数依次为value、key和map本身：
```
var m = new Map([[1, 'x'], [2, 'y'], [3, 'z']]);
m.forEach(function (value, key, map) {
    console.log(value);
});
```

## 函数
### 定义函数
在JavaScript中，定义函数的方式如下：
```
function abs(x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
}

function pf(x){
    return x*x
}
```
由于JavaScript的函数也是一个对象，上述定义的abs()函数实际上是一个函数对象，而函数名abs可以视为指向该函数的变量。

因此，第二种定义函数的方式如下：
```
var abs = function (x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
};

var pf=function(x){
    return x*x
}
```
### 调用函数
调用函数时，按顺序传入参数即可：
```
abs(10); // 返回10
abs(-9); // 返回9
pf(4)//返回2
```
由于JavaScript允许传入任意个参数而不影响调用，因此传入的参数比定义的参数多也没有问题，虽然函数内部并不需要这些参数：
```
abs(10, 'blablabla'); // 返回10
abs(-9, 'haha', 'hehe', null); // 返回9
pf(2,'a');//返回2
```
传入的参数比定义的少也没有问题：
```
abs(); // 返回NaN
```