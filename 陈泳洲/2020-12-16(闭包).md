# 课堂笔记
## 什么是闭包
 1. 闭包就是函数内部的子函数—— 等于没说
 2. 当函数可以记住并访问所在的词法作用域时，就产生了闭包，即使函数是在当前词法作用域之外执行。——靠谱
 3. 闭包就是能够读取其他函数内部变量的函数，在本质上是函数内部和函数外部链接的桥梁——靠谱
 4. 函数和对其周围状态（词法环境）的引用捆绑在一起构成闭包（closure）——很靠谱

 我们来实现一个对Array的求和。通常情况下，求和的函数是这样定义的：
```
function sum(arr) {
    return arr.reduce(function (x, y) {
        return x + y;
    });
}

sum([1, 2, 3, 4, 5]); // 15
```
但是，如果不需要立刻求和，而是在后面的代码中，根据需要再计算怎么办？可以不返回求和的结果，而是返回求和的函数！
```
function lazy_sum(arr) {
    var sum = function () {
        return arr.reduce(function (x, y) {
            return x + y;
        });
    }
    return sum;
}
```
当我们调用lazy_sum()时，返回的并不是求和结果，而是求和函数：
```
var f = lazy_sum([1, 2, 3, 4, 5]); // function sum()
```
调用函数f时，才真正计算求和的结果：
```
f(); // 15
```
在这个例子中，我们在函数lazy_sum中又定义了函数sum，并且，内部函数sum可以引用外部函数lazy_sum的参数和局部变量，当lazy_sum返回函数sum时，相关参数和变量都保存在返回的函数中，这种称为“闭包（Closure）”的程序结构拥有极大的威力。

### 闭包就是能够读取其他函数内部变量的函数，在本质上是函数内部和函数外部链接的桥
```
function foo(x) {
    var a = '昨天要升旗';

    function bar() {
        console.log(a);
    }
    return bar;
}

var res = foo();
console.log(res());//输出还是昨天要升旗

 这是因为此时res是执行foo函数时返回的bar引用，bar函数得以保存了它饿词法环境
 ```
 ## 箭头函数
 ```
x => x * x
```
上面的箭头函数相当于：
```
function (x) {
    return x * x;
}
```
### this
箭头函数看上去是匿名函数的一种简写，但实际上，箭头函数和匿名函数有个明显的区别：箭头函数内部的this是词法作用域，由上下文确定。

回顾前面的例子，由于JavaScript函数对this绑定的错误处理，下面的例子无法得到预期结果：
```
var obj = {
    birth: 1990,
    getAge: function () {
        var b = this.birth; // 1990
        var fn = function () {
            return new Date().getFullYear() - this.birth; // this指向window或undefined
        };
        return fn();
    }
};
```