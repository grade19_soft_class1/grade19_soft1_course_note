# 今日练习
## 一股寒潮即将来临❄  考试换了新方式
## 删除DOM
html页面
```
    <div id="content">
        <ul id="uu">
            <li id="sun">太阳</li>
            <li id="moon">月亮</li>
            <li id="star">星星</li>
            <li id="masSun">九阳</li>
            <li id="maxMoon">九阴</li>
        </ul>
    </div>
```
js页面
```
'use strict'

let star = document.querySelector('#star');

let uu = star.parentElement;

let removed=uu.removeChild(star);

console.log(star===removed);
```
## 操作表单
html
```
    <div id="content">
        <input id="v8" type="radio" value="88"/>
        <input id="v9" type="checkbox" value="99"/>
    </div>
```
js
```
'use strict'

let v8=document.querySelector('#v8');
let v9=document.querySelector('#v9');

console.log(v8.value);
console.log(v9.value);
```

+ 文本框，对应的`<input type="text">`，用于输入文本；

+ 口令框，对应的`<input type="password">`，用于输入口令；

+ 单选框，对应的`<input type="radio">`，用于选择一项；

+ 复选框，对应的`<input type="checkbox">`，用于选择多项；

+ 下拉框，对应的`<select>`，用于选择一项；

+ 隐藏文本，对应的`<input type="hidden">`，用户不可见，但表单提交时会把隐藏文本发送到服务器。
## HTML5控件
```
    <input type="date" value="2015-07-01">

    2020/12/28
    <input type="datetime-local" value="2015-07-01T02:03:04">
    
    2020/12/28 02:03:04
    <input type="color" value="#ff0000">
```
## 练习
h5结构
```
<form id="test-register" action="#" target="_blank" onsubmit="return checkRegisterForm()">
    <p id="test-error" style="color:red"></p>
    <p>
        用户名: <input type="text" id="username" name="username">
    </p>
    <p>
        口令: <input type="password" id="password" name="password">
    </p>
    <p>
        重复口令: <input type="password" id="password-2">
    </p>
    <p>
        <button type="submit">提交</button> <button type="reset">重置</button>
    </p>
</form>
```
js结构
```
'use strict';
var checkRegisterForm = function () {
    // TODO:
    return false;
}

// 测试:
;(function () {
    window.testFormHandler = checkRegisterForm;
    var form = document.getElementById('test-register');
    if (form.dispatchEvent) {
        var event = new Event('submit', {
    		bubbles: true,
    		cancelable: true
  		});
        form.dispatchEvent(event);
    } else {
        form.fireEvent('onsubmit');
    }
})();
```
## setTimeout
```
function callback(){
    console.log('Done');
}
console.log('before setTimeout()');
setTimeout(callback,1000);//1秒后调用callba函数
console.log('after setTimeout()');
```
