# 今日课堂笔记
## 开始复习一下之前留的十四道练习题
## 今天新学习的内容
### 多维数组
```
var arr = [[1,2,3],[999,666,555],'🎶'];
var x = arr[1][1];
console.log(x);
```
### 对象  🤦‍♀️🤦‍♂️ 今天的内容感觉可以用一次直接完成
#### JavaScript的对象用于描述现实世界中的某个对象。
```
var duixiang={
    name:'ddd',
    birth: 1999,
    'now-school':'闽西职业技术学院',
    height:170,
    weight:57,
};
duixiang.age = 22;//22岁  新增
delete duixiang.age;//删除年龄
//用in查找duixiang是否拥有一个属性
var sc='name' in duixiang;//true
console.log(sc);
var ds='school' in duixiang;//false
console.log(ds);
//继承得到的也可以用in
var sb='toString' in duixiang;//true
console.log(sb);
//用hasOwnProperty检测是不是继承得到的属性  
console.log(duixiang.hasOwnProperty('name'));//true
console.log(duixiang.hasOwnProperty('toString'));//false
console.log(duixiang);
```
### 条件判断 就是if  else 语句
#### 直接练习习题
```
var height = parseFloat(prompt('请输入身高(m):'));
var weight = parseFloat(prompt('请输入体重(kg):'));
var bmi = (weight/(height*height));
if(bmi<18.5){
    console.log('体重过轻');
}else if(18.5<=bmi<=25){
    console.log('体重正常');
}else if(25<bmi<28){
    console.log('体重过重');
}else if(28<=bmi<=32){
    console.log('肥胖');
}else if(bmi>32){
    console.log('严重肥胖');
};
```
我自己的bmi  身高181,体重55;
![](./imgs/bmi.png);