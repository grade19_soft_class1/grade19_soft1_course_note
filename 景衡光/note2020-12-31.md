# 本学期最后的一次笔记了 
# 层级选择器（Descendant Selector）
### 如果两个DOM元素具有层级关系，就可以用`$('ancestor descendant')`来选择，层级之间用空格隔开。例如：
```
<!-- HTML结构 -->
<div class="testing">
    <ul class="lang">
        <li class="lang-javascript">JavaScript</li>
        <li class="lang-python">Python</li>
        <li class="lang-lua">Lua</li>
    </ul>
</div>
```
### 要选出JavaScript，可以用层级选择器：
```
 var js1=$('ul.lang li.lang-javascript')[0]; // [<li class="lang-javascript">JavaScript</li>]
  var js2=$('div.testing li.lang-javascript')[0]; // [<li class="lang-javascript">JavaScript</li>]
   
  console.log(js1);
  console.log(js2);

  console.log(js1===js2);  //true
```
  ![图片加载中...](./img/cj001.png)
## 因为`<div>`和`<ul>`都是`<li>`的祖先节点，所以上面两种方式都可以选出相应的`<li>`节点。这样div 和 Ul 可以和任意层查找子标签

# 子选择器（Child Selector）
### 子选择器`$('parent>child')`类似层级选择器，但是限定了层级关系必须是父子关系，表示：在父级标签与子集标签中间加一个大于号> ,就是`<child>`节点必须是`<parent>`节点的直属子节点。还是以上面的例子：

```
 var js1=$('ul.lang>li.lang-javascript')[0]; // [<li class="lang-javascript">JavaScript</li>]
  var js2=$('div.testing>li.lang-javascript')[0]; //undefiend
  console.log(js1);
  console.log(js2);

```
![图片加载中...](./img/cj002.png)

# 过滤器（Filter）
### 过滤器一般不单独使用，它通常附加在选择器上，帮助我们更精确地定位元素。观察过滤器的效果：
```
  var js=$('ul.lang li'); // 选出JavaScript、Python和Lua 3个节点
  
  var js1=$('ul.lang li:first-child'); // 仅选出JavaScript
  var lua=$('ul.lang li:last-child'); // 仅选出Lua
  var num2=$('ul.lang li:nth-child(2)'); // 选出第N个元素，N从1开始
  var even=$('ul.lang li:nth-child(even)'); // 选出序号为偶数的元素
  var odd=$('ul.lang li:nth-child(odd)'); // 选出序号为奇数的元素

     console.log(js);
     console.log(js1);
     console.log(lua);
     console.log(num2);
     console.log(even);
     console.log(odd);
```
效果如下图所示<br>
![图片加载中...](./img/cj003.png)

# 表单相关
# 对于表单元素，jQuery对象统一提供val()方法获取和设置对应的value属性;
### 针对表单元素，jQuery还有一组特殊的选择器：
```
:input：可以选择<input>，<textarea>，<select>和<button>；

:file：可以选择<input type="file">，和input[type=file]一样；

:checkbox：可以选择复选框，和input[type=checkbox]一样；

:radio：可以选择单选框，和input[type=radio]一样；

:focus：可以选择当前输入焦点的元素，例如把光标放到一个<input>上，用$('input:focus')就可以选出；

:checked：选择当前勾上的单选框和复选框，用这个选择器可以立刻获得用户选择的项目，如$('input[type=radio]:checked')；

:enabled：可以选择可以正常输入的<input>、<select> 等，也就是没有灰掉的输入；

:disabled：和:enabled正好相反，选择那些不能输入的。
```
### 此外，jQuery还有很多有用的选择器，例如，选出可见的或隐藏的元素：
```
$('div:visible'); // 所有可见的div
$('div:hidden'); // 所有隐藏的div
```

# 练习
针对如下HTML结构：
```
<!-- HTML结构 -->

<div class="test-selector">
    <ul class="test-lang">
        <li class="lang-javascript">JavaScript</li>
        <li class="lang-python">Python</li>
        <li class="lang-lua">Lua</li>
    </ul>
    <ol class="test-lang">
        <li class="lang-swift">Swift</li>
        <li class="lang-java">Java</li>
        <li class="lang-c">C</li>
    </ol>
</div>
```
选出相应的内容并观察效果：
```
'use strict';
var selected = null;

答案：
// 分别选择所有语言，
  var selected = $('div.test-selector');

所有动态语言，
 var selected = $('ul.test-lang');
  selected.find('li').css('list-style-type','none');

所有静态语言，
  var selected = $('ol.test-lang');

JavaScript，Lua，C等:
  var selected = $('div .lang-lua,div .lang-javascript,div .lang-c');

// 高亮结果:
if (!(selected instanceof jQuery)) {
    return console.log('不是有效的jQuery对象!');
}
$('#test-jquery').find('*').css('background-color', '');
selected.css('background-color', '#ffd351');
 
JavaScript
Python
Lua
Swift
Java
C
```
# 查找和过滤
  ### 通常情况下选择器可以直接定位到我们想要的元素，但是，当我们拿到一个jQuery对象后，还可以以这个对象为基准，进行查找和过滤。最常见的查找是在某个节点的所有子节点中查找，使用find()方法，它本身又接收一个任意的选择器。例如如下的HTML结构：
```
<!-- HTML结构 -->
  <ul class="lang">
        <li class="js dy">JavaScript</li>
        <li class="dy">Python</li>
        <li id="swift">Swift</li>
       
        <ul>
            <li class="dy">Scheme</li>
            <li name="haskell">Haskell</li>
        </ul>
    </ul>
```
### 用find()查找：
```
 var ul = $('ul.lang'); // 获得<ul>

  var dy = ul.find('.dy'); // 获得JavaScript, Python, Scheme
  var swf = ul.find('#swift')[0]; // 获得Swift
  var hsk = ul.find('[name=haskell]')[0]; // 获得Haskell
 
   console.log(dy);
   console.log(swf);
   console.log(hsk);
```
![图片加载中...](./img/find002.png)

###  如果要从当前节点开始向上查找，使用parent()方法:
```
var swf = $('#swift'); // 获得Swift
var parent = swf.parent(); // 获得Swift的上层节点<ul>
var a = swf.parent('.red'); // 获得Swift的上层节点<ul>，同时传入过滤条件。如果ul不符合条件，返回空jQuery对象
```
### 如果位于同一层级的节点，可以通过next()和prev()方法，他只会找下一个或上一个节点 不会跳层例如：

```
var swift = $('#swift');

swift.next(); // Scheme
swift.next('[name=haskell]'); // 空的jQuery对象，因为Swift的下一个元素Scheme不符合条件[name=haskell]

swift.prev(); // Python
swift.prev('.dy'); // Python，因为Python同时符合过滤器条件.dy
```
# filter()方法可以过滤掉不符合选择器条件的节点.
# map()方法把一个jQuery对象包含的若干DOM节点转化为其他对象：
如下所例：
```
 var langs = $('ul.lang li'); 
   
  var res=langs.map(function () {
       return this.innerText;
    }).get();

   console.log(res);   // 拿到JavaScript, Python, Swift, Scheme和Haskell
 

  var a = langs.filter('.dy');  //滤掉不符合选择器条件的节点
   for(var i=0;i<a.length;i++){
    console.log(a[i].innerText);    //输出JavaScript, Python, Scheme
   }
```
![图片加载中...](./img/filter001.png) 
### 或者传入一个函数，要特别注意函数内部的this被绑定为DOM对象，不是jQuery对象：
```
var a = langs.filter(function(){
     return this.innerText.indexOf('S')===0;
  }); 
  
  for(var i=0;i<a.length;i++){
    console.log(a[i].innerText);
  }
  ```
![图片加载中...](./img/filter002.png) 

# 练习
对于下面的表单：
```
<form id="test-form" action="#0" onsubmit="return false;">
    <p><label>Name: <input name="name"></label></p>
    <p><label>Email: <input name="email"></label></p>
    <p><label>Password: <input name="password" type="password"></label></p>
    <p>Gender: <label><input name="gender" type="radio" value="m" checked> Male</label> <label><input name="gender" type="radio" value="f"> Female</label></p>
    <p><label>City: <select name="city">
    	<option value="BJ" selected>Beijing</option>
    	<option value="SH">Shanghai</option>
    	<option value="CD">Chengdu</option>
    	<option value="XM">Xiamen</option>
    </select></label></p>
    <p><button type="submit">Submit</button></p>
</form>
```
### 输入值后，用jQuery获取表单的JSON字符串，key和value分别对应每个输入的name和相应的value，例如：`{"name":"Michael","email":...}
```
答案：

  'use strict';
  var res = {};
  res.Name=$('input[name="name"]').val();
  res.Email=$('input[name="email"]').val();
  res.Password=$('input[name="password"]').val();
  res.Gender=$('input[name="gender"]').val();
  res.City=$('select[name="city"]').val();
  
  var json=JSON.stringify(res,null,' ');

  // 显示结果:
  if (typeof (json) === 'string') {
    console.log(json);
  }
  else {
    console.log('json变量不是string!');
  }

```

# 操作DOM
## 修改Text和HTML 
### text() 获取节点的文本
### html()方法 获取原始HTML文本，例如，如下的HTML结构：

``` 
  <ul id="test-ul">
    
        <li name="book">Java &amp; JavaScript</li>
    </ul>

//js
   var res=$('#test-ul li[name=book]').text(); // 'Java & JavaScript'
  var res1=$('#test-ul li[name=book]').html(); // 'Java &amp; JavaScript'
    
  console.log(res);
  console.log(res1);
```

### 然后我们修改一下文本
```
'use strict';
var j1 = $('#test-ul li.js');
var j2 = $('#test-ul li[name=book]');
j1.html('<span style="color: red">JavaScript</span>');
j2.text('JavaScript & ECMAScript');  
```
![图片加载中...](./img/dom001.png) 

# 修改CSS
```
<!-- HTML结构 -->
<ul id="test-css">
    <li class="lang dy"><span>JavaScript</span></li>
    <li class="lang"><span>Java</span></li>
    <li class="lang dy"><span>Python</span></li>
    <li class="lang"><span>Swift</span></li>
    <li class="lang dy"><span>Scheme</span></li>
</ul>
```
### 要高亮显示动态语言，调用jQuery对象的css('name', 'value')方法，我们用一行语句实现：
```
  'use strict';
    var res=$('#test-css li.dy>span');
    res.css('background-color', '#ffd351').css('color', 'red');
```
![图片加载中...](./img/css001.png) 

## 为了和JavaScript保持一致，CSS属性可以用'background-color'和'backgroundColor'两种格式。

### css()方法将作用于DOM节点的style属性，具有最高优先级。如果要修改class属性，可以用jQuery提供的下列方法：
```
var div = $('#test-div');
div.hasClass('highlight'); // false， class是否包含highlight
div.addClass('highlight'); // 添加highlight这个class
div.removeClass('highlight'); // 删除highlight这个class
```
# 练习：分别用css()方法和addClass()方法高亮显示JavaScript：
```
<!-- HTML结构 -->
<style>
.highlight {
    color: #dd1144;
    background-color: #ffd351;
}
</style>

<div id="test-highlight-css">
    <ul>
        <li class="py"><span>Python</span></li>
        <li class="js"><span>JavaScript</span></li>
        <li class="sw"><span>Swift</span></li>
        <li class="hk"><span>Haskell</span></li>
    </ul>
</div>
//addClass()方法
'use strict';
  var div = $('#test-highlight-css');
  var res= div.find('.js');
  res.addClass('highlight');
//Class()方法
   'use strict';
  var div = $('#test-highlight-css');
  var res= div.find('.js');
 res.css('color','red').css('background-color','#ffd351');
```
# 显示和隐藏DOM
### 考虑到显示和隐藏DOM元素使用非常普遍，jQuery直接提供show()和hide()方法，我们不用关心它是如何修改display属性的，总之它能正常工作：
### 如下例子HTML同上
```

'use strict';
    
  var py=$('li.py');
   console.log(py);

   
   setTimeout(() => {
    py.hide();
   }, 2000);    //两秒后隐藏
  


  setTimeout(function(){
    py.css('color','red').css('fontSize','50px');
    py.show();
    
   },3000)  //三秒后显示
```
# 获取DOM信息
利用jQuery对象的若干方法，我们直接可以获取DOM的高宽等信息，而无需针对不同浏览器编写特定代码：
```
// 浏览器可视窗口大小:
$(window).width(); // 800
$(window).height(); // 600

// HTML文档大小:
$(document).width(); // 800
$(document).height(); // 3500

// 某个div的大小:
var div = $('#test-div');
div.width(); // 600
div.height(); // 300
div.width(400); // 设置CSS属性 width: 400px，是否生效要看CSS是否有效
div.height('200px'); // 设置CSS属性 height: 200px，是否生效要看CSS是否有效
```
## attr()和removeAttr()方法用于操作DOM节点的属性：
### attr() 用于查找DOM节点的属性
### removeAttr() 用于删除DOM节点的属性
```
// <div id="test-div" name="Test" start="1">...</div>
var div = $('#test-div');
div.attr('data'); // undefined, 属性不存在
div.attr('name'); // 'Test'
div.attr('name', 'Hello'); // div的name属性变为'Hello'
div.removeAttr('name'); // 删除name属性
div.attr('name'); // undefined
```
## prop()方法和attr()类似，attr()和prop()对于属性checked处理有所不同：
```
var radio = $('#test-radio');
radio.attr('checked'); // 'checked'
radio.prop('checked'); // true
```
## prop()返回值更合理一些。不过，用is()方法判断更好：
```
var radio = $('#test-radio');
radio.is(':checked'); // true
```
### 类似的属性还有selected，处理时最好用is(':selected')。

# 添加DOM
## 要添加新的DOM节点，除了通过jQuery的html()这种暴力方法外，还可以用append()方法，例如：
```
 <div id="test-highlight-css">
            <ul>
                <li class="py" name="编程语言" width="88" type="text"><span>Python</span></li>
                <li class="js"><span>JavaScript</span></li>
                <li class="sw"><span>Swift</span></li>
                <li class="hk"><span>Haskell</span></li>
            </ul>
          
        </div>
//js

  'use strict';
var div=$('#test-highlight-css');
div.append(' 北京<input type="checkbox" id="bj" checked >');//把DOM添加到最后
div.prepend(' <li class="java"><span>Java</span></li>');    //把DOM添加到最前。
 console.log(div);

```
![图片加载中...](./img/append001.png) 
## append()把DOM添加到最后，prepend()则把DOM添加到最前。

# 删除节点
要删除DOM节点，拿到jQuery对象后直接调用remove()方法就可以了。如果jQuery对象包含若干DOM节点，实际上可以一次删除多个DOM节点：
```
var li = $('#test-div>ul>li');
li.remove(); // 所有<li>全被删除
```
# 练习
除了列出的3种语言外，请再添加Pascal、Lua和Ruby，然后按字母顺序排序节点：
```
<!-- HTML结构 -->
<div id="test-div">
    <ul>
        <li><span>JavaScript</span></li>
        <li><span>Python</span></li>
        <li><span>Swift</span></li>
    </ul>
</div>

//js

use strict';
  //除了列出的3种语言外，请再添加Pascal、Lua和Ruby，然后按字母顺序排序节点：

  var ul = $('#test-div>ul');
  //添加
  var arr = ['Pascal', 'Lua', 'Ruby'].map(x => '<li><span>' + x + '</span></li>')
  ul.append(arr);


  var span = $('#test-div>ul span');

  var res = [];

  //获取文本
  span.map(function () {
    var txt = this.innerText;
    res.push(txt);
  });

  //排序
  res.sort();

  //将res的每一个选项都赋给span
  span.map(function (x) {

        this.innerText=res[x];
   
  });


// 测试:
;(function () {
    var s = $('#test-div>ul>li').map(function () {
        return $(this).text();
    }).get().join(',');
    if (s === 'JavaScript,Lua,Pascal,Python,Ruby,Swift') {
        console.log('测试通过!');
    } else {
        console.log('测试失败: ' + s);
    }
})();
 
JavaScript
Python
Swift
```
![图片加载中...](./img/text009.png) 
# 事件
### 因为JavaScript在浏览器中以单线程模式运行，页面加载后，一旦页面上所有的JavaScript代码被执行完后，就只能依赖触发事件来执行JavaScript代码。浏览器在接收到用户的鼠标或键盘输入后，会自动在对应的DOM节点上触发相应的事件。如果该节点已经绑定了对应的JavaScript处理函数，该函数就会自动调用。
### 例子，假设要在用户点击了超链接时弹出提示框，我们用jQuery这样绑定一个click事件：
```
/* HTML:
 *
 * <a id="test-link" href="#0">点我试试</a>
 *
 */

// 获取超链接的jQuery对象:
var a = $('#test-link');
a.on('click', function () {
    alert('Hello!');
});
```

## on方法用来绑定一个事件，我们需要传入事件名称和对应的处理函数。
### 另一种更简化的写法是直接调用click()方法：两者完全等价
```
a.click(function () {
    alert('Hello!');
});
```
## 鼠标事件
+ click: 鼠标单击时触发；
+ dblclick：鼠标双击时触发；
+ mouseenter：鼠标进入时触发；
+ mouseleave：鼠标移出时触发；
+ mousemove：鼠标在DOM内部移动时触发；
+ hover：鼠标进入和退出时触发两个函数，相当于mouseenter加上mouseleave。
## 键盘事件
键盘事件仅作用在当前焦点的DOM上，通常是`<input>`和`<textarea>`。

+ keydown：键盘按下时触发；
+ keyup：键盘松开时触发；
+ keypress：按一次键后触发。
其他事件
+ focus：当DOM获得焦点时触发；
+ blur：当DOM失去焦点时触发；
+ change：当`<input>`、`<select>`或`<textarea>`的内容改变时触发；
+ submit：当`<form>`提交时触发；
+ ready：当页面被载入并且DOM树完成初始化后触发。
其中，ready仅作用于document对象。由于ready事件在DOM完成初始化后触发，且只触发一次，所以非常适合用来写其他的初始化代码。假设我们想给一个`<form>`表单绑定submit事件，下面的代码没有预期的效果：

### 初始化 我们自己的初始化代码必须放到document对象的ready事件中，保证DOM已完成初始化：
```
<html>
<head>
    <script>
        $(document).on('ready', function () {
            $('#testForm).on('submit', function () {
                alert('submit!');
            });
        });
    </script>
</head>
<body>
    <form id="testForm">
        ...
    </form>
</body>
```
### 这样写就没有问题了。因为相关代码会在DOM树初始化后再执行。
### 由于ready事件使用非常普遍，所以可以这样简化：
```
$(function () {
    // init...
});

```
## 完全可以反复绑定事件处理函数，它们会依次执行：
```

$(function () {
  console.log('初始化a');
});
$(function () {
  console.log('初始化b');
});
```
![图片加载中...](./img/init001.png) 
# 事件参数
## 如下所例获取鼠标坐标
```
<html>
 <style>
        div{
            width: 500px;
            height: 500px;
            background-color: rgb(0, 174, 255);
        }
        span{
            width: 100px;
            height: 100px;
            background-color: rgb(255, 51, 0);
        }
        *{
            color: black;
            font-size: 20px;
        }
    </style>
   

     <div id="testMouseMoveDiv" >
     
     </div>
     <span id="testMouseMoveSpan"></span>
     //js
 $(function () {

    var div=$('#testMouseMoveDiv');
    var span=$('#testMouseMoveSpan');
   
   
    div.mousemove(function (e) {
         span.text('pageX = ' + e.pageX + ', pageY = ' + e.pageY);
    });
});
```
![图片加载中...](./img/mousemove001.png) 

# 取消绑定
### 一个已被绑定的事件可以解除绑定，通过off('click', function)实现：
```
<html>
 <button id="btn">点击</button>

//js
  
  var a=$('#btn');

  function hello() {
    alert('hello!');
  }

  a.click(hello); // 绑定事件

  // 3秒钟后解除绑定:
  setTimeout(function () {
    a.off('click', hello);
  }, 5000);
```
## 可以使用off('click')一次性移除已绑定的click事件的所有处理函数。
## 同理，无参数调用off()一次性移除已绑定的所有类型的事件处理函数。
# 事件触发条件
### 一个需要注意的问题是，事件的触发总是由用户操作引发的。例如，我们监控文本框的内容改动：
```
var input = $('#test-input');
input.change(function () {
    console.log('changed...');
});
```
### 当用户在文本框中输入时，就会触发change事件。但是，如果用JavaScript代码去改动文本框的值，将不会触发change事件：
```
var input = $('#test-input');
input.val('change it!'); // 无法触发change事件
```
### 有些时候，我们希望用代码触发change事件，可以直接调用无参数的change()方法来触发该事件：
```
var input = $('#test-input');
input.val('change it!');
input.change(); // 触发change事件
input.change()相当于input.trigger('change')，它是trigger()方法的简写。
```

# 练习
对如下的Form表单：
```
<!-- HTML结构 -->
<form id="test-form" action="test">
    <legend>请选择想要学习的编程语言：</legend>
    <fieldset>
        <p><label class="selectAll"><input type="checkbox"> <span class="selectAll">全选</span><span class="deselectAll">全不选</span></label> <a href="#0" class="invertSelect">反选</a></p>
        <p><label><input type="checkbox" name="lang" value="javascript"> JavaScript</label></p>
        <p><label><input type="checkbox" name="lang" value="python"> Python</label></p>
        <p><label><input type="checkbox" name="lang" value="ruby"> Ruby</label></p>
        <p><label><input type="checkbox" name="lang" value="haskell"> Haskell</label></p>
        <p><label><input type="checkbox" name="lang" value="scheme"> Scheme</label></p>
		<p><button type="submit">Submit</button></p>
    </fieldset>
</form>
```
+ 绑定合适的事件处理函数，实现以下逻辑：

+ 当用户勾上“全选”时，自动选中所有语言，并把“全选”变成“全不选”；

+ 当用户去掉“全不选”时，自动不选中所有语言；

+ 当用户点击“反选”时，自动把所有语言状态反转（选中的变为未选，未选的变为选中）；

+ 当用户把所有语言都手动勾上时，“全选”被自动勾上，并变为“全不选”；

+ 当用户手动去掉选中至少一种语言时，“全不选”自动被去掉选中，并变为“全选”。
```
'use strict';

var
    form = $('#test-form'),
    langs = form.find('[name=lang]'),
    selectAll = form.find('label.selectAll :checkbox'),
    selectAllLabel = form.find('label.selectAll span.selectAll'),
    deselectAllLabel = form.find('label.selectAll span.deselectAll'),
    invertSelect = form.find('a.invertSelect');

// 重置初始化状态:
form.find('*').show().off();
form.find(':checkbox').prop('checked', false).off();
deselectAllLabel.hide();
// 拦截form提交事件:
form.off().submit(function (e) {
    e.preventDefault();
    alert(form.serialize());
});

  // TODO:绑定事件

'use strict';

  var
      form = $('#test-form'),
      langs = form.find('[name=lang]'),
      selectAll = form.find('label.selectAll :checkbox'),
      selectAllLabel = form.find('label.selectAll span.selectAll'),
      deselectAllLabel = form.find('label.selectAll span.deselectAll'),
      invertSelect = form.find('a.invertSelect');
   

  // 重置初始化状态:
  form.find('*').show().off();
  form.find(':checkbox').prop('checked', false).off();
  //隐藏全不选文本
  deselectAllLabel.hide();
  // 拦截form提交事件:
  form.off().submit(function (e) {
      e.preventDefault();    //拦截当前所有默认事件
      alert(form.serialize());  //序列化
  });
  // TODO:绑定事件
    

    
  //全选不全选
    selectAll.change(function(){

      if(selectAll.prop('checked')==true && selectAllLabel.show()){
        langs.prop('checked','checked')
         
        selectAllLabel.hide();
        deselectAllLabel.show();
       }
      else{
        langs.prop('checked',false);
        selectAllLabel.show();
        deselectAllLabel.hide();
      }

    })
   
   
   //反选
    invertSelect.click(function(){
      for(var i=0;i<langs.length;i++){
           if (langs[i].checked) {
            langs[i].checked=false            
           }
           else{
            langs[i].checked=true
           }
      }

     if ( langs.prop('checked')==true) {
       selectAll.prop('checked',true)
       selectAllLabel.hide();
       deselectAllLabel.show();
     }
     else{
      selectAll.prop('checked',false)
      selectAllLabel.show();
      deselectAllLabel.hide();
     }
    });
  
  

    //自定义选择
    langs.change(function(){
      var count=0;

        langs.each(function(){
          if (this.checked) {
            count+=1;
      }
        })
          
       


          if (count==langs.length) {
             selectAll.prop('checked',':checked');        
             selectAllLabel.hide();
            deselectAllLabel.show();
          }
          else{
             selectAll.prop('checked',false);         
            selectAllLabel.show();
            deselectAllLabel.hide();
          }
    });
    
   

  // 测试:
  console.log('请测试功能是否正常。');
}
```

