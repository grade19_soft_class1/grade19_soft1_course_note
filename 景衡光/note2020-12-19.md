# Generator 
## 对于一般函数，当调用它时是是顺序执行的，直到最后一行或遇到return语句后返回。
## Generator只有在调用 next() （调用generator时一般不显式的使用next()方法，而是通过for循环调用，这样还可以避免StopIteration的错误）才执行，遇到yield语句返回，再次被next()调用时候从上次的返回yield语句处继续执行，也就是用多少，取多少。

看如下栗子：

```
function* foo(x){
    yield x+1;
    yield x+2;
    yield x+3;
    return x+4;
}
var res=foo(1);
console.log(res.next());
console.log(res.next());
console.log(res.next());
console.log(res.next());

```

## next()方法会执行generator的代码，然后，每次遇到yield x;就返回一个对象{value: x, done: true/false}，然后“暂停”。返回的value就是yield的返回值，done表示这个generator是否已经执行结束了。如果done为true，则value就是return的返回值。
 ![图片加载中...](./img/yield001.png)

 ## 当然还有第二个方法， 就是直接用for ... of循环迭代generator对象

 ```
function* foo(x){
    yield x+1;
    yield x+2;
    yield x+3;
    return x+4;
}
for(var x of foo(1)){
console.log(x);  //只会打印yield输出的值
}
 ```
 ![图片加载中...](./img/yield002.png)
 ## 练习
## 要生成一个自增的ID，可以编写一个next_id()函数：
// var current_id = 0;

// function next_id() {
//     current_id ++;
//     return current_id;
// }

##  由于函数无法保存状态，故需要一个全局变量current_id来保存数字。

##  不用闭包，试用generator改写：

```
'use strict';

function* next_id() {
    let current_id = 0;
    //让他一直循环
    while(true){
         current_id++;
        yield current_id;
       
        //加到100就返回
        if(current_id==100){
            return current_id
        }
    }
     
}



// 测试:
var
    x,
    pass = true,
    g = next_id();
for (x = 1; x < 100; x ++) {
    if (g.next().value !== x) {
        pass = false;
        console.log('测试失败!');
        break;
    }
}
if (pass) {
    console.log('测试通过!');
}
```
![图片加载中...](./img/yield003.png)

#  标准对象

## 在JavaScript的世界里，一切都是对象。
## 但是某些对象还是和其他对象不太一样。为了区分对象的类型，我们用typeof操作符获取对象的类型，它总是返回一个字符串：
```
typeof 123; // 'number'
typeof NaN; // 'number'
typeof 'str'; // 'string'
typeof true; // 'boolean'
typeof undefined; // 'undefined'
typeof Math.abs; // 'function'
typeof null; // 'object'
typeof []; // 'object'
typeof {}; // 'object'

```
# 包装对象 
## 除了以上这些类型外，JavaScript还提供了包装对象，number、boolean和string都有包装对象。包装对象用new创建：
```
var n = new Number(123); // 123,生成了新的包装类型
var b = new Boolean(true); // true,生成了新的包装类型
var s = new String('str'); // 'str',生成了新的包装类型

console.log(n);
console.log(b);
console.log(s);
```
![图片加载中...](./img/bz001.png)
## 虽然包装对象看上去和原来的值一模一样，显示出来也是一模一样，但他们的类型已经变为object了！
## 所以，包装对象和原始值用===比较会返回false;

```
var n = new Number(123); 
var res1 =n === 123
var res2 =n ==123
console.log('===的返回值为 '+res1);
console.log('==的返回值为  '+res2);
```
![图片加载中...](./img/bz002.png)
## ps:
```
1：不要使用new Number()、new Boolean()、new String()创建包装对象；
2：用parseInt()或parseFloat()来转换任意类型到number；
3：用String()来转换任意类型到string，或者直接调用某个对象的toString()方法；
4：通常不必把任意类型转换为boolean再判断，因为可以直接写if (myVar) {...}；
5：typeof操作符可以判断出number、boolean、string、function和undefined；
6：判断Array要使用Array.isArray(arr)；
7：判断null请使用myVar === null；
8：判断某个全局变量是否存在用typeof window.myVar === 'undefined'；
9：函数内部判断某个变量是否存在用typeof myVar === 'undefined'。
```

## 其实并不是所有的对象都有toString()方法吗，null和undefined就没有
## 但是number对象调用toString()报SyntaxError;因此正确的写法如下：
```
var res1=123..toString(); // '123', 注意是两个点！
var res2=(123).toString(); // '123' 
console.log(res1);
console.log(res2);
``` 

![图片加载中...](./img/next001.png)

# Date 
## Date 对象用于处理日期和时间。
## Date 对象方法
方法	描述
```
Date()	返回当日的日期和时间。
getDate()	从 Date 对象返回一个月中的某一天 (1 ~ 31)。
getDay()	从 Date 对象返回一周中的某一天 (0 ~ 6)。
getMonth()	从 Date 对象返回月份 (0 ~ 11)。
getFullYear()	从 Date 对象以四位数字返回年份。
getYear()	请使用 getFullYear() 方法代替。
getHours()	返回 Date 对象的小时 (0 ~ 23)。
getMinutes()	返回 Date 对象的分钟 (0 ~ 59)。
getSeconds()	返回 Date 对象的秒数 (0 ~ 59)。
getMilliseconds()	返回 Date 对象的毫秒(0 ~ 999)。
getTime()	返回 1970 年 1 月 1 日至今的毫秒数。
getTimezoneOffset()	返回本地时间与格林威治标准时间 (GMT) 的分钟差。
getUTCDate()	根据世界时从 Date 对象返回月中的一天 (1 ~ 31)。
getUTCDay()	根据世界时从 Date 对象返回周中的一天 (0 ~ 6)。
getUTCMonth()	根据世界时从 Date 对象返回月份 (0 ~ 11)。
getUTCFullYear()	根据世界时从 Date 对象返回四位数的年份。
getUTCHours()	根据世界时返回 Date 对象的小时 (0 ~ 23)。
getUTCMinutes()	根据世界时返回 Date 对象的分钟 (0 ~ 59)。
getUTCSeconds()	根据世界时返回 Date 对象的秒钟 (0 ~ 59)。
getUTCMilliseconds()	根据世界时返回 Date 对象的毫秒(0 ~ 999)。
parse()	返回1970年1月1日午夜到指定日期（字符串）的毫秒数。
setDate()	设置 Date 对象中月的某一天 (1 ~ 31)。
setMonth()	设置 Date 对象中月份 (0 ~ 11)。
setFullYear()	设置 Date 对象中的年份（四位数字）。
setYear()	请使用 setFullYear() 方法代替。
setHours()	设置 Date 对象中的小时 (0 ~ 23)。
setMinutes()	设置 Date 对象中的分钟 (0 ~ 59)。
setSeconds()	设置 Date 对象中的秒钟 (0 ~ 59)。
setMilliseconds()	设置 Date 对象中的毫秒 (0 ~ 999)。
setTime()	以毫秒设置 Date 对象。
setUTCDate()	根据世界时设置 Date 对象中月份的一天 (1 ~ 31)。
setUTCMonth()	根据世界时设置 Date 对象中的月份 (0 ~ 11)。
setUTCFullYear()	根据世界时设置 Date 对象中的年份（四位数字）。
setUTCHours()	根据世界时设置 Date 对象中的小时 (0 ~ 23)。
setUTCMinutes()	根据世界时设置 Date 对象中的分钟 (0 ~ 59)。
setUTCSeconds()	根据世界时设置 Date 对象中的秒钟 (0 ~ 59)。
setUTCMilliseconds()	根据世界时设置 Date 对象中的毫秒 (0 ~ 999)。
toSource()	返回该对象的源代码。
toString()	把 Date 对象转换为字符串。
toTimeString()	把 Date 对象的时间部分转换为字符串。
toDateString()	把 Date 对象的日期部分转换为字符串。
toGMTString()	请使用 toUTCString() 方法代替。
toUTCString()	根据世界时，把 Date 对象转换为字符串。
toLocaleString()	根据本地时间格式，把 Date 对象转换为字符串。
toLocaleTimeString()	根据本地时间格式，把 Date 对象的时间部分转换为字符串。
toLocaleDateString()	根据本地时间格式，把 Date 对象的日期部分转换为字符串。
UTC()	根据世界时返回 1970 年 1 月 1 日 到指定日期的毫秒数。
valueOf()	返回 Date 对象的原始值。
```
## 注意，当前时间是浏览器从本机操作系统获取的时间，所以不一定准确，因为用户可以把当前时间设定为任何值。
## 如果要创建一个指定日期和时间的Date对象，可以用：
```
var d = new Date(2020, 09, 27, 20, 15, 30, 123);
console.log(d);
```
![图片加载中...](./img/date001.png)

## 再一个要注意的是JavaScript的月份范围用整数表示是0~11，0表示一月，1表示二月……。

## 时区
```
var d = new Date(1435146562875);  //这里输入一个时间戳。不过有时间戳就可以很容易地把它转换为一个Date：
var res1=d.toLocaleString(); 
var res2=d.toUTCString(); 
console.log(res1);   // '2015/6/24 下午7:49:22'，本地时间（北京时区+8:00），显示的字符串与操作系统设定的格式有关
console.log(res2);    // 'Wed, 24 Jun 2015 11:49:22 GMT'，UTC时间，与本地时间相差8小时
```
![图片加载中...](./img/UTC001.png)


## 要获取当前时间戳，可以用：
```
'use strict';
var dd=new Date(Date.now());
if (Date.now) {
    console.log(Date.now()); //返回当前时间戳 老版本IE没有now()方法
    console.log(dd);         //返回当前时间
} else {
    console.log(new Date().getTime());
}

```
![图片加载中...](./img/UTC002.png)
# 练习
小明为了和女友庆祝情人节，特意制作了网页，并提前预定了法式餐厅。小明打算用JavaScript给女友一个惊喜留言：
结果女友并未出现。小明非常郁闷，请你帮忙分析他的JavaScript代码有何问题。
```
'use strict';
var today = new Date(2020, 2, 14, 10, 15, 30, 123);
if (today.getMonth() === 2 && today.getDate() === 14 && today.getHours()<18) {
    console.log(today);
    alert('亲爱的，我预定了晚餐，晚上6点在餐厅见！');
}

```
 ![图片加载中...](./img/text003.png)