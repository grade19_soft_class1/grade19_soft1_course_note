#  今天好热，龙岩的夏天要来了嘛？
#  操作表单

## HTML表单的输入控件主要有以下几种：

### 文本框，对应的`<input type="text">`，用于输入文本；

### 口令框，对应的`<input type="password">`，用于输入口令；

### 单选框，对应的`<input type="radio">`，用于选择一项；

### 复选框，对应的`<input type="checkbox">`，用于选择多项；

### 下拉框，对应的`<select>`，用于选择一项；

### 隐藏文本，对应的`<input type="hidden">`，用户不可见，但表单提交时会把隐藏文本发送到服务器。
## 如果我们获得了一个<'input'>节点引用，就可以直接调用value获得对应的用户输入值；
## 这种方式可以应用于text、password、hidden以及select。但是，对于单选框和复选框，value属性返回的永远是HTML预设的值，而我们需要获得的实际是用户是否“勾上了”选项，所以应该用checked判断：
如下所例：
```
var mon = document.getElementById('monday');
var tue = document.getElementById('tuesday');
var res1=mon.value; 
var res2=tue.value; 

console.log(res1); 
console.log(res2);
```
![图片加载中...](./img/input001.png)

```
//HTML
   <div style="font-family: 宋体;">
         <label>
             
             用户名：<input type="text"  id="username" /> <br/>
             密&nbsp;码：<input type="password"  id="pwd" />
        </label>
         
    </div>
 //js
 var username=document.getElementById('username');
var pwd=document.getElementById('pwd');

console.log(username);
console.log(pwd);   
```
![图片加载中...](./img/input002.png)



# js实现表单提交submit()，onsubmit
## 第一中方法是第一种方式：表单提交，在form标签中增加onsubmit事件来判断表单提交是否成功
```
//HTML

   <form id="test-form" onsubmit="return checkForm()">
        <input type="text" name="test" value="你的用户名">
        <button type="submit">提交</button>
    </form>
    

//js
function checkForm() {
    var form = document.getElementById('test-form');
    // 可以在此修改form的input...
    // 继续下一步:
    if(form.children[0].value=='admin'){
        alert('提交成功');
        return true;
    }
   
    
}

```
## 第二种方式：通过button按钮来触发表单提交事件onclick="submitForm();"，会忽略掉其他标签中的属性，比如form标签中的onsubmit属性就失效了。这时为了进行表单验证，可以将验证代码放在submitForm();方法中进行验证。
### 如下所例：

```
//html
    <div style="font-family: 宋体;">
        <label>

            用户名：<input type="text" id="username" /> <br />
            密&nbsp;码：<input type="password" id="pwd" />
        </label><br />
       <div style="margin-top: 20px;">
        <input type="button" id="put" value="提交" onclick="doSubmitForm()"/>
        <input type="button" id="qx" value="取消" style="margin-left: 100px;"/>

        <button>注册</button>

       </div>
  
//js
'use strict';

var username = document.getElementById('username');
var pwd = document.getElementById('pwd');

function doSubmitForm() {

    console.log(username.value);
    console.log(pwd.value);

    if (username.value == 'admin' && pwd.value == '123456') {
        alert('提交成功');
        // 提交form:
        form.submit();
    }
    else {
        alert('提交失败');
    }
}
// 可以在此修改form的input...
   
   
}
```