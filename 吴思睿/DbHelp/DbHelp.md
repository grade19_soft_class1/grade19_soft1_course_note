# DbHelp

* using System;
* using System.Collections.Generic;
* using System.Linq;
* using System.Web;
* using System.Data;
* using System.Data.SqlClient;

namespace DbHelp
{
    public class DbHelp
    {
        public readonly static string conString = "server=;uid=;pwd=;database=";

        public static DataTable GetData(string sql) {
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conString);
            DataTable data = new DataTable();
            adapter.Fill(data);
            return data;
        }

        public static int A(string sql) {
            SqlConnection connection = new SqlConnection(conString);
            SqlCommand command = new SqlCommand(sql,connection);
            connection.Open();
            int abc = command.ExecuteNonQuery();
            connection.Close();
            return abc;
        }
    }
}