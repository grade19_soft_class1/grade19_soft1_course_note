# 有关 LinQ 的语言特性
##  隐式类型(var)
    var a = 1;                          //等同于 int a = 1;
    var s = "abc";                      //等同于 string s = "abc";
    var list = new List<int>();         //等同于 List<int> list = new List<int>();
    var o = new object;                 //等同与 object o = new object();

    在使用var隐式类型时，必须在声明变量之初就要给变量赋值。
    隐式类型只能在方法内部、属性get或set访问器内进行声明，不能使用var来定义返回值、参数类型或类中的成员。
## 匿名类型
    匿名类型与隐式类型是相对的，在创建一个对象时同样无需定义对象的类型。

    var obj = new { DateTime.Now, Name = "名称", Values = new int[] { 1, 2, 3, 4, 5 } };
## 对象初始化器(实例化)
    var student = new Student() { ID = 1, Name = "张三", Age = 20 };
    
    对象初始化器特性并不需要相关的关键字，它只是一种初始化对象的方式。
    在结合隐式类型和匿名类型时它可以避免使用强制类型转换。
# Func 委托与匿名方法(泛型)
    显式    Func<int, int, string> calc = getCalc;
                public string getCalc(int a,int b)
                {
                    return "结果为:" + (a + b);
                }

    匿名    Func<int, int, string> calc =
                Delegate(int a,int b)
                {
                    return "结果为:" + (a + b);
                }
# Lambda 表达式
    Arg-List => expr - body
    参数列表 => 表达式体

    双参    Func<int, int, string> calc =
                (int a, int b) =>
                {
                    return "结果为:" + (a + b);
                };
    单参    Func<int, string> calc =
                a =>
                {
                    return "结果为:" + a;
                };
    无参    Func<string> calc =
                () =>
                {
                    return "无参数 Lamdba 表达式";
                };