## 
未定义的数据类型 undefined 表示未声明变量
Document 文档的根节点  
JavaScript 是HTML中的默认脚本语言  
JavaScript 不提供任何内建的打印或显示函数  
getElementByld 返回对拥有指定Id的引用 最有效的查找元素的方法
## JavaScript 能改变 HTML 内容
查找 Id = "demo"的HTML元素，并把元素内容（innerHTML）更改为Java Script  
    document.getElementByle("demo").innerHTML = "Hello JavaScript";
## JavaScript 能隐藏或显示 HTML 元素
隐藏:  
    document.getElementByle("demo").style.display = "none";  
显示:  
    document.getElementByle("demo").style.display = "block";
## < script > 标签
在HTML中JavaScript代码必须位于< script > 与 < /script >标签之间
## 外部脚本
    < script src = "myScript.js" >< /script >  
相同的脚本可用与许多不同的网页  
## 外部 JavaScript 的优势
* 分离了 HTML 和代码
* 使HTML 和 JavaScript 更易于阅读和维护
* 已缓存的 JavaScript 文件可加速页面加载
## JavaScript 显示方案
JavaScript 能够以不同方式"显示"数据:
* 使用 window.alert() 写入警告框
* 使用 document.write() 写入HTML输出
* 使用 innerHtml 写入HTML元素
* 使用 console.log()写入浏览器控制台
## 使用 innerHTML
如需访问 HTML 元素，JavaScript 可使用  
document.getElementById(id)方法  
id 属性定义 HTML 元素  
InnerHTML 属性定义 HTML 内容
## 比较运算符
==比较 会自动转换数据类型再比较  
===比较 它不会自动转换数据类型 如果数据类型不一致 返回false 如果一致 再比较
    < script >

    console.log(false==0);//true
    console.log(false===0);//false

    console.log(NaN==NaN);//false
    console.log(NaN===NaN);//false

    < /script >
## 数组
    < script >   

    var arr = [12,45,'你好',445];

    for(var a=0 ; a<arr.length ; a++){
    console.log(arr[a])
    }

    < /script >
## 对象
    var person = { name: 'Bob', age: 20, tags: ['js', 'web', 'mobile'], city: 'Beijing', hasCar: true, zipcode: null };

    console.log(person.name);
## strict
启用strict模式的方法是在JavaScript代码的第一行写上：

'use strict';
这是一个字符串，不支持strict模式的浏览器会把它当做一个字符串语句执行，支持strict模式的浏览器将开启strict模式运行JavaScript。

来测试一下你的浏览器是否能支持strict模式：

'use strict';
// 如果浏览器支持strict模式，
// 下面的代码将报ReferenceError错误:
    abc = 'Hello, world';
    console.log(abc);
运行代码，如果浏览器报错，请修复后再运行。如果浏览器不报错，说明你的浏览器太古老了，需要尽快升级。

不用var申明的变量会被视为全局变量，为了避免这一缺陷，所有的JavaScript代码都应该使用strict模式。我们在后面编写的JavaScript代码将全部采用strict模式。
## 操作字符串
indexOf
indexOf()会搜索指定字符串出现的位置：

substring
substring()返回指定索引区间的子串：

    < script >

    var a = '我怕时间哦';
    console.log( a.substring(1,3));

    < /script >
## slice与splice
slice与splice

    < script >

     //index of 来搜索一个指定的元素的位置   

    var arr = [10,20,'啊这',45];

    console.log(arr);

    console.log(arr.indexOf(50));

    console.log(arr.indexOf('啊这'));

    //slice与splice 

    var aCopy = arr.slice();

    console.log(aCopy);

    if(arr == aCopy){
    document.write("是相同的")
    }
    else{
    document.write("地址不同 所以是不相同的")
    }

    //slice()就是对应String的substring()的版本

    var arr2 = ['A','B',1,2,3,4];

    var x = arr2.slice(2,6);

    console.log(x);
    //不会改变原数组
    console.log(arr2);

    //splice 会改变原数组

    var arr3 = [11,22,33,44,55];

    arr3.splice(2,0,'我','恨','你');

    console.log(arr3);

    arr3.splice(3,1);

    console.log(arr3);

    < /script >   
## reverse与sort
reverse与sort
    < script >
    
    //reverse 把整个Array的元素给反转

    var arr = [1,2,3,4,5,6];
    arr.reverse();
    console.log(arr);

    //sort 把当前Array进行排序(降序)
    var arr1 =[1,5,9,4,3];
    arr1.sort(function(a,b){
        return b-a ;

    })
    console.log(arr1);

    < /script >   