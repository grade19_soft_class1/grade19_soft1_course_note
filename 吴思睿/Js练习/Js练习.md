题目描述 找出元素 item 在给定数组 arr 中的位置 输出描述: 如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例

输入 [ 1, 2, 3, 4 ], 3

输出 2


  function indexOfFn(arr ,item){
            if(arr.indexOf(item) >= 0){
                return arr.indexOf(item);
            }
            else{
                return -1 ;
            }
        }

     
        console.log(indexOfFn([1, 2, 3, 4], 1));

题目描述 计算给定数组 arr 中所有元素的总和 输入描述: 数组中的元素均为 Number 类型
示例 输入 [ 1, 2, 3, 4 ] 输出 10

    
    var arr =[1,2,3,4];
    function add(arr){
        var sum = 0;
        for(i = 0 ;i< arr.length ;i++){
            sum += arr[i];          
        }
        console.log(sum);
        return sum;
    }

    add(arr);

题目描述 移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
示例 输入 [1, 2, 3, 4, 2], 2 输出 [1, 3, 4]

\\方法一:利用for循环

    function remove(arr, item) {
     //声明一个新数组保存结果
     var a = [];
     //循环遍历
     for(var i = 0; i < arr.length ;i++){
         //如果arr[i]不等于item，就加入数组a
         if(arr[i] != item){
             a.push(arr[i]);
         }
     }
     return a;
 }

console.log(remove([1,2,3],3)) 

\\方法二:利用forEach循环数组


     function remove(arr ,item){
         var result =[];
         arr.forEach(function(e){
             if(e !=item){
                 result.push(e)
             }
         });
       return result;

     }
    
    console.log(remove([1,2,3,4],4))
      
题目描述 在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
    
    var arr1 = [1,2,3,4];
    //这是原数组
    console.log(arr1);
    //push在数组后面加若干元素
    arr1.push(7);
    console.log(arr1);

题目描述 删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
示例 输入 [1, 2, 3, 4] 输出 [1, 2, 3]

    
    var arr1 = [1,2,3,4];
    //这是原数组
    console.log(arr1);
    //pop方法删除数组最后一位
    arr1.pop();
    console.log(arr1);

题目描述 在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例 输入 [1, 2, 3, 4], 10 输出 [10, 1, 2, 3, 4]


    var arr1 = [1,2,3,4];
    //这是原数组
    console.log(arr1);
    //unshift()方法在数组的头部添加若干元素
    arr1.unshift(7);
    console.log(arr1);
    //shift()方法在数组的头部删除若干个元素
    arr1.shift();arr1.shift();
    console.log(arr1);


题目描述 删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
示例 输入 [1, 2, 3, 4] 输出 [2, 3, 4]


    var arr1 = [1,2,3,4];
    //这是原数组
    console.log(arr1);
    //unshift()方法在数组的头部添加若干元素
    arr1.unshift(7);
    console.log(arr1);
    //shift()方法在数组的头部删除若干个元素
    arr1.shift();arr1.shift();
    console.log(arr1);


题目描述 合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例 输入 [1, 2, 3, 4], ['a', 'b', 'c', 1] 输出 [1, 2, 3, 4, 'a', 'b', 'c', 1]

    
    var arr1 = [1,2,3,4];
    var arr2 = ['我','和','你','呀'];
    var add = arr1.concat(arr2);
    console.log(add);
    

题目描述 在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例 输入 [1, 2, 3, 4], 'z', 2 输出 [1, 2, 'z', 3, 4]



     function insert(arr, item, index) {
            var result = arr.slice(0);
            result.splice(index,0,item);
            return result;
        }
    console.log(insert([1,2,3,4],1,5));