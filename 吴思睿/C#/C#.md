## C#的回顾
* 递归，布置了一个关于递归的练习（使用递归完成斐波那契数列，要求输入指定的个数，输出指定个数的数列）  

用方法调用自身来实现的

public class Text
{

    public int Factorial(int n)
    {
        if (n < 3)
        {
            return 1;

        }
        else
        {
            return Factorial(n - 1) + Factorial(n - 2); 
        }

    }
}



 class Program


{


    static void Main(string[] args)
    {

        Text text = new Text();
        var a =text.Factorial(10);
        Console.WriteLine(a);
        Console.ReadKey();           
    }

}
* 分部类的内容与嵌套类演示  

部分类就是用partial(部分类) 表示一个类中的内容比较多时 分散到不同的类中 并且他们类的名字是一样的

class Program
{

    static void Main(string[] args)
    {

        Bus bus = new Bus();
        bus.Id = 1;
        bus.Name = "宝马";
        bus.PrintMes();
        
    }
}



public partial  class Bus

{

    public int Id { get; set; }

    public string Name { get; set; }


}


public partial class Bus 

{

    public void PrintMes()
    {
        Console.WriteLine("我的Id是" + Id);
        Console.WriteLine("我的名字是" + Name );

    }
}

* 嵌套类就是将一个类中写在另一个类中的内部 访问时必须时 外部类.嵌套类

public class Bus


{

    public class BusTest
    {

        public int Id { get; set; }

        public int Number { set; get; }

        public void PrintSomething()
        {
            Console.WriteLine("车的id是 " + Id);
            Console.WriteLine("车的数量有 " + Number);
        }

    }

}






   class program


   static void Main(string[] args)
    {

        Bus.BusTest test = new Bus.BusTest();

        test.Id = 1;

        test.Number = 10;
        
        test.PrintSomething(); 

    }
* 日期时间

Date 获取实例的日期部分 Day 获取该实例所表示的日期是一个月的第几天 DayOfWeek 获取该实例所表示的日期是一周的星期几 DayOfYear 获取该实例所表示的日期是一年的第几天 Add(Timespan value) 在指定的日期实例上添加时间间隔值 value AddDays(double value) 在指定的日期实例上添加指定天数 value AddHours(double value) 在指定的日期实例上添加指定的小时数 value AddMinutes(double value) 在指定的日期实例上添加指定的分钟数 value AddSeconds(double value) 在指定的日期实例上添加指定的秒数 value AddMonths(int value) 在指定的日期实例上添加指定的月份 value AddYears (int value) 在指定的日期实例上添加指定的年份 value

        //  计算时间的间隔

        DateTime dt1 = DateTime.Now;

        DateTime dt2 = new DateTime(2020, 09, 30);

        TimeSpan ts = dt2 - dt1;


        Console.WriteLine("间隔的天数为{0}天", ts.Days);
* 字符串

split 用法 class Program { static void Main(string[] args) { string a = "asfdasdf"; string[] aArray = a.Split('f'); foreach (string h in aArray ) { Console.WriteLine(h.ToString() ); }

    }
}
练习1：输入一串字符串，将字符串反转输出（打印）；

static void Main(string[] args)

    {
        string a = Console.ReadLine();
        for ( int i = a.Length -1; i>=0; i--)
        {
            Console.WriteLine(a[i]);
        }
        

    }
练习2：输入一串字符串（英文语句），将每个单词反转输出； 如：输入hello world 输出 ollhe dlrow

中文如输入：王俊 输出 俊王

//糟糕做不出来

练习3：输入一个身份证号，输出这个身份证号的出身日期，并判断其生日是否已经过完；

        //请输出你的身份证号

        string str = Console.ReadLine();

        var c = str.Substring(9, 4);

        Console.WriteLine("我的生日是:" + c);
练习4：输入电子邮箱地址，判断其是否为合法的地址； 如:abc@abc.com 为合法 abc@@.abc.com为不合法

//可用正则表达式去做

* 数据类型转换

隐式类型转换 这些转换是 C# 默认的以安全方式进行的转换，不会导致数据丢失。例如，从小的整数类型转换为大的整数类型，从派生类转换为基类。

显式类型转换 显式类型转换，即强制类型转换。显式转换需要强制转换运算符，而且强制转换会造成数据丢失。

     // int类型是可以转换为float类型的
     int c = 23434324;
     float d = c;
     // float类型转换为double要强转
     float i = 343423;
     double k = (float)i;
* 数组

       //一维数组的定义
        int[] array = new int[4];
        int[] array1 = new int[4] { 1, 2, 3, 4 };
        int[] array2 = { 34, 343, 4, 343, };

        //二维数组的定义
        int[,] array4 = { { 0, 1, 2 }, {2, 3, 2}};
        double[,] points =  new double [3,2]{ { 90, 80 }, { 100, 89 }, { 88.5, 86 } };
* 正则表达式

编号 字符 描述 1 . 匹配除换行符以外的所有字符 2 \w 匹配字母、数字、下画线 3 \s 匹配空白符（空格） 4 \d 匹配数字 5 \b 匹配表达式的开始或结束 6 ^ 匹配表达式的开始 7 $ 匹配表达式的结束

正则表达式中表示重复的字符

编 号 字 符 描 述 1 * 0次或多次字符 2 ? 0次或1次字符 3 + 1次或多次字符 4 {n} n次字符 5 {n,M} n到M次字符 6 {n, } n次以上字符

