#2020.11.19 课堂笔记

## 一.递归（完成斐波那契数列）
```
递归，就是在运行的过程中调用自己，不满足条件是递归前进，满足条件时递归返回；

构成递归需具备的条件：

1. 子问题须与原始问题为同样的事，且更为简单；

2. 不能无限制地调用本身，须有个出口，化简为非递归状况处理。

思路:当n比较大时，效率较低，使用两层递归，空间换时间，效率高；

1.public class Solution {
    public int RectCover(int target) {
      if(target  <= 1){
            return 1;
        }
        if(target*2 == 2){
            return 1;
        }else if(target*2 == 4){
            return 2;
        }else{
            return RectCover((target-1))+RectCover(target-2);
        }
    }
}
```

## 二.分布类的内容
```
1.要作为同一类型的各个部分的所有分部类型定义都必须使用partial 进行修饰。

public partial class A { }
public class A { } // Error, must also be marked partial

2.使用partial 关键字表明可在命名空间内定义该类、结构或接口的其他部分

3.所有部分都必须使用partial 关键字
```

## 三.日期时间
```
1.Day:返回一个月某一天
2.DayofWeek:返回一周星期几
3.DayofYear:返回一年的第几天
4.AddDays(double value);--添加实例天数；
5.AddHours(double value);--添加实例小时数；
6.AddMinutes(double value);--添加实例分钟数；
7.Addseconds(double value);--添加实例秒数；
8.AddYears(int value);--添加实例年数；
9.AddMonths(int value);--添加实例月数；
class program
{
    static void Main(string [] args)
    {
        Datime dt = Datime.now;
        Console.WriteLine("本月第{0}天",Day);
        Console.WriteLine("当前是:{0}",DayofWeek);
        Console.WriteLine("当前是:{0}",DayofYear);
        Console.WriteLine("当前是:{0}",dt.AddDays);
        Console.WriteLine("当前是:{0}",dt.AddHours);
        Console.WriteLine("当前是:{0}",dt.AddMinutes);
        Console.WriteLine("当前是:{0}",dt.Addseconds);
        Console.WriteLine("当前是:{0}",dt.AddMonths);
        Console.WriteLine("当前是:{0}",dt.AddYears);
    }
}
```

## 四.字符串
```
1、从字符串中提取子串 
　　　　StringBuilder 类没有支持子串的方法，因此必须用String类来提取。（关于StringBuilder 类，文末会提到）
 　　　　string mystring="My name is ynn."; 
　　　　//Displays "name is ynn." 
　　　　Console.WriteLine(mystring.Substring( 3 )); 
　　　　//Displays "ynn" 
　　　　Console.WriteLine(mystring.Substring( 11,3 ));
2、比较字符串 
　　　　String 类有四种方法：Compare( )、CompareTo( )、CompareOrdinal( )、Equals( )。 
　      int result; 
　　　　bool bresult; 
        s1="aaaa"; 
　　　　s2="bbbb"; 
　　　　//Compare( )method 
　　　　//result值为“0”表示等，小于零表示 s1 < s2，大于零表示 s1 > s2 
　　　　result=String.Compare(s1,s2); 
　　　　result=s1.CompareTo( s2 ); 
　　　　result=String.CompareOrdinal(s1,s2);
3.插入子串
　　　　字符串.Insert(插入位置，插入子串) ;
　　　　s.Insert(2,"ab");
4.移出子串
　　　　字符串.Remove（起始位置，移出数）;
　　　　s.Remove(3,2);
5.替换子串
　　　　字符串.Replace（源子串，替换为）;
　　　　s.Replace("-"," ");将-替换为空格
6.截取子串
　　　　字符串.Substring（截取起始位置，截取个数）;
　　　　"abcdef".Substring(2,3);结果为cde
7.去空格
 　　　　myString.Trim(); //同时删除字符串前后的空格
        myString = myString.Trim(trimChars); //删除所有指定字符
 　　　　myString = myString.TrimEnd(); //删除字符串后的空格
 　　　　myString = myString.TrimStart(); //删除字符串前的空格
 8.转换大小写
 　　　　str.ToLower()转化成小写字母
 　　　　str.ToUpper()转化成大写字母
 9.拆分字符串
 　　　　例1：string[] Strs = myString.Split(' ',3);
 10.使字串达到指定长度
 　　　　PadLeft()、PadRight()
11.得到字符串长度
 　　　　  len=str.Length;
12.查找子串
　　　　字符串.IndexOf(子串，查找起始位置) ;
　　　　字符串.LastIndexOf(子串) ;最后一次出现的位置
　　　　str.IndexOf("ab",0);
13.字符串遍历
    　　　　string myString = "This is a test!";
    　　　　foreach (char myChar in myString)
    　　　　{
    　　　　　　　　Console.Write("{0}",myChar);
    　　　　}
```

## 五.数据类型转换
```
1.ToBoolean
2.ToByte
3.Tochar
4.Todatatime
5.ToDecimal
6.ToDouble
7.ToInt16
8.ToInt32
9.ToInt64
10.ToSbyte
11.Tosingle
12.Tostring
13.Totype
14.ToUInt16
15.ToUInt32
16.ToUInt64
```

## 六.数组
```
using System;

namespace ArrayApplication
{
    class MyArray
    {
       static void Main(string[] args)
       {
          int []  n = new int[10]; /* n 是一个带有 10 个整数的数组 */


          /* 初始化数组 n 中的元素 */         
          for ( int i = 0; i < 10; i++ )
          {
             n[i] = i + 100;
          }

          /* 输出每个数组元素的值 */
          foreach (int j in n )
          {
             int i = j-100;
             Console.WriteLine("Element[{0}] = {1}", i, j);
          }
          Console.ReadKey();
       }
    }
}
输出:
Element[0] = 100
Element[1] = 101
Element[2] = 102
Element[3] = 103
Element[4] = 104
Element[5] = 105
Element[6] = 106
Element[7] = 107
Element[8] = 108
Element[9] = 109
```

## 七.正则表达式
```
1、定义一个Regex类的实例


Regex regex = new Regex(@"\d");
这里的初始化参数就是一个正则表达式，“\d”表示配置数字。

2、判断是否匹配

判断一个字符串，是否匹配一个正则表达式，在Regex对象中，可以使用Regex.IsMatch(string)方法。

regex.IsMatch("abc"); //返回值为false，字符串中未包含数字
regex.IsMatch("abc3abc"); //返回值为true，因为字符串中包含了数字

3、获取匹配次数

使用Regex.Matches(string)方法得到一个Matches集合，再使用这个集合的Count属性。

regex.Matches("abc123abc").Count; 
返回值为3，因为匹配了三次数字。

4、获取匹配的内容

使用Regex.Match(string)方法进行匹配。

regex.Match("abc123abc").Value;
返回值为1，表示第一个匹配到的值。

5、捕获

正则表达式中可以使用括号对部分值进行捕获，要想获取捕获的值，可以使用Regex.Match(string).Groups[int].Value来获取。

```