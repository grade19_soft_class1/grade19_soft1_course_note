//function getAge(){
 //   var x = new Date().getFullYear();
  //  return x - this.birth
//}
//var student = {
//    name:"xiaoming",
 //   birth:2000,
 //   age:getAge
//}

//student.age;
//getAge();

//console.log(student.age());
//console.log(getAge());

/* 'use strict'
var student = {
    name:"小红",
    birth: 2000,
    age:function(){
        let that = this;//捕捉this;
        function getAgeFromBirth(){
            console.log(that);
            var y  = new Date().getFullYear();
            return y - that.birth;
        }
        return getAgeFromBirth();
    }
}
console.log(student.age()); */


function getAge(){
    var x  = new Date().getFullYear();
    return x - this.birth;
}
var person = {
    name:"joker",
    birth:2000,
    age:getAge
}

//person.age();
console.log(person.age());
//getAge.apply(student,[]);
// console.log(getAge.apply(student,[a,b,c]));//打包传入
 //console.log(getAge.call(student,a,b,c));//顺序传入

 Math.max.apply(null,[1,2,3]);
 Math.max.call(null,1,2,3);
 console.log(Math.max.apply(null,[1,2,3]));
 console.log( Math.max.call(null,1,2,3));


/*  function person(anothername,name,age,eyecolor){
     this.anothername = anothername;
     this.name = name;
     this.age = age;
     this.eyecolor = eyecolor;
     this.exchangename = function(name){
         this.anothername = name;
     }
 }
 console.log(mymother.exchangename("joker")); */


 var message = "HelloWorld"
var x  = message.toUpperCase();
var y = message.toLowerCase();
console.log(x);
console.log(y);

var person = {
    fullname:function () {
        return this.firstname +" "+this.lastname;
    }
    
}
var person1 ={
    firstname : "bill",
    lastname :"gates",
}


person.fullname.call(person1);
console.log(person.fullname.call(person1));

var count = 0 ;
var oldparseInt = parseInt; //保存原函数

window.parseInt = function(){
    count += 1;
    return oldparseInt.apply(null,arguments);//调用原函数
}
parseInt('1');
parseInt('2');
parseInt('3');
console.log('count =' + count);

let count1 = 0;

parseInt(10);
count1 += 1;

parseInt(20);
count += 1;

//console.log(parseInt(20,100));

//console.log(count1);

window.parseInt = function(){
    count1 ++;
    return oldparseInt.apply(null,arguments);
}

console.log(parseInt(20,100));

parseInt(10);
parseInt(20);
parseInt('1');
parseInt('2');

console.log('count1 ='+count1);


function add(x,y,z){
    return z(x) + z(y);
}
//x = -2;
//y = 4;
//z = Math.abs
//z(x) + z(y) = Math.abs(-2) + Math.abs(4) ==> 11
add(-2,4,Math.abs);
console.log(add(-2,4,Math.abs));//11

function mul(x){
    return x * x;
}
var array = [1,2,3,4];

array.map(mul);//map()传入参数mul，也是函数本身
console.log(array.map(mul));//[1,4,9,16]


//[x1,x2,x3].reduce(f) = f(f(f(x1,x2),x3))

var array1 = [1,2,3,4];

let a = array.reduce(function(x,y){
    return x + y;
});

console.log(a);

var array = [1,2,3];
let b = array.map(String);
console.log(b);

var array2 = [1,2,3,4,5];
var r = array2.filter(function(x){
    return x % 2 == 0;//只留下偶数
})

console.log(r);//[2,4]


var array3 = ['a','','b',undefined,''];

var r1 = array3.filter(function(y){
    return y && y.trim();
})

console.log(r1);//['a','b']

var array4 = [3,7,5,9];

var r2 = array4.sort(function(x,y){
        if(x<y){
            return -1;
        }
        if(x>y){
            return 1;
        }
        return 0;
})

console.log(r2);//[3,5,7,9]

var array5 = ['apple','Microsoft','Google'];

var r3 = array5.sort(function(x1,x2){
    s1 = x1.toUpperCase();
    s2 = x2.toUpperCase();
        if(s1<s2){
            return -1;
        }
        if(s1>s2){
            return 1;
        }
        return 0;
})

console.log(r3);['apple','Google','Microsoft']


var a1  =  ['a','b','c'];
var a2 = a1.sort();
console.log(a1);//["a","b","c"]
console.log(a2);//["a","b","c"]

console.log(a1 == a2);//true;

var w1 = ['a','b','c'];
var w2 = w1.filter(function(element,index,self){
    console.log(element);//'a','b','c'
    console.log(index);0,1,2
    console.log(self);//w2
})


var numbers = [1,2,3,4,5];

var over18  = numbers.every(myfunction);

function myfunction(value,index,array){
    return value > 3
}

console.log(over18);

var arr  = ['Apple,banana,organge'];
console.log(arr.every(function(x){
    return  x.length > 0;
}));

console.log(arr.every(function(y){
    return y.toLowerCase === y;

}));

var numbers1 = [1,10,100];

var over50 = numbers1.every(myfunction1);

function myfunction1(y){
    return y > 50
}

console.log(over50);

var arr1 = ['A','B','C','D','E','F'];

var aa = arr1.indexOf('E');

console.log(a);

var fruits = ["Apple", "Orange", "Apple", "Mango"];
var f = fruits.indexOf("Apple");

console.log(f);

var arr2 = [1,2,3,4];

console.log(arr2.find(function(z){
    return z > 2
}));


var arr3 = ["a","b","c"];

console.log(arr3.findIndex(function(h){
    return h.toUpperCase = h;
}));

var arr3 = [2,4,6,8];

console.log(arr3.findIndex(function(h){
    return  h > 4 
}));

function count(){
    var arr4 = [];
    for(var i = 1;i <= 3; i++){
        arr4.push(function()
        {
            return i * i
        })
    }
    return arr4;
}

var results = count();
console.log(results);
var f1 = c[0];
var f2 = c[1];
var f3 = c[2];

console.log(f1);
console.log(f2);
console.log(f3);