# 今日份小笔记
## 闭包
### 闭包是个啥玩意儿?
    1.闭包是嵌套的内部函数.
    2.包含被引用变量(函数)的对象.
    闭包存在于嵌套的内部函数中.
    本质上，闭包就是将函数内部和函数外部连接起来的一座桥梁.

     var globalVar = "abc";
    //自调用的函数
    (function fatherFunction(fatPart) {//父函数fatherFunction作用域开始

        var fatVar = 'F';//在父函数中声明的变量
        //自调用函数

        (function childrenFunction(chiPart) {　//子函数children作用域开始

            var chiVar = 'C';　//在子函数中声明的变量
            console.log(
                "父函数参数：" + fatPart +
                "父函数中声明：" + fatVar +
                "子函数参数：" + chiPart +
                "子函数声明：" + "chiVar" +
                "全局声明：" + global
            )
        })(5)　//子函数作用域结束，将5作为参数

    })(7)　//父函数作用域结束，将7作为参数

    　　//父函数参数：7
    　　//父函数中声明：F
    　　//子函数参数：5
    　　//子函数中声明：C
    　　//全局声明：abc

    childrenFunction 是在 fatherFunction 中定义的闭包，可以访问在 fatherFunction 作用域内声明和定义的变量.此外，闭包还可以访问在全局命名空间中声明的变量.

    吃栗子：
    'use strict'
    function create_counter(initial){
        var x=initial || 0;
        var property={
            increase:function(){
                x+=1;
                return x;
            }
        }
        return property;
    }
    let xk=create_counter();
    console.log(xk.increase());//1

    栗子：
    'use strict'
    function create_counter(initial){
        let zero=function(){
            console.log('秋风萧瑟，洪波涌起');
            return 0;
        }
        var x=initial || zero();
        var property={
            increase:function(){
                x+=1;
                return x;
            }
        }
        return property;
    }
    let xk=create_counter(2);
    console.log(xk.increase());//3

    吃栗子：
    'use strict'
    function make_pow(n) {
        return function (x) {
            return Math.pow(x, n);
        }
    }
    // 创建两个新函数:
    var pow2 = make_pow(2);
    var pow3 = make_pow(3);

    console.log(pow2(4)); //16
    console.log(pow3(6)); //216
## 箭头函数(Arrow Function)
    箭头函数相当于匿名函数，并且简化了函数定义.
    箭头函数有两种格式：
    1)只包含一个表达式，连{...}和return都省略掉;
    example:
    x=>x*x;相等于
    funciton(x){
        return x*x;
    }
    2)包含多条语句，不能省略{...}和return.
    'use strict'
    var a= x => {
        if (x > 0) {
            return x * x;
        }
        else {
            return - x * x;
        }
    }
    console.log(a(3));//9

    基础语法：
    (参数1, 参数2, …, 参数N) => { 函数声明 }
    (参数1, 参数2, …, 参数N) => 表达式（单一）
    //相当于：(参数1, 参数2, …, 参数N) =>{ return 表达式; }

    // 当只有一个参数时，圆括号是可选的：
    (单一参数) => {函数声明}
    单一参数 => {函数声明}

    // 没有参数的函数应该写成一对圆括号。
    () => {函数声明}

    高级语法：
    //加括号的函数体返回对象字面表达式：
    参数=> ({foo: bar})

    //支持剩余参数和默认参数
    (参数1, 参数2, ...rest) => {函数声明}
    (参数1 = 默认值1,参数2, …, 参数N = 默认值N) => {函数声明}

    //同样支持参数列表解构
    let f = ([a, b] = [1, 2], {x: c} = {x: a + b}) => a + b + c;
    f();  // 6

    实心栗子：
    1.具有一个参数的简单函数
    2.没有参数的需要用在箭头前加上小括号
    3.多个参数需要用到小括号，参数间逗号间隔，例如两个数字相加
    4.函数体多条语句需要用到大括号
    5.返回对象时需要用到小括号包起来，因大括号被占用解释为代码块了
    6.直接作为事件处理
    7.作为数组排序回调

    注意点：
    1.typeof运算符和普通的function一样
    2.instanceof也返回true，表明也是Funciton的实例
    3.this固定，不再善变
    4.箭头函数不能用new
    5.不能使用argument
## this
    js中this会随着执行环境的改变而改变:
    1)在方法中，this表示该方法所属的对象.
    2)如果单独使用，this表示全局对象.
    3)在函数中，this表示全局对象.
    4)在函数中，在严格模式下，this是未定义的undefined.
    5)在事件中，this表示接收事件的元素.
    6)类似call()和apply()方法可以将this引用到任何对象.

    吃栗子：
    'use strict'
    var obj = {
        birth: 1990,
        getAge: function () {
            var b = this.birth; // 1990
            var fn = ()=>{
                return ()=>{
                    return new Date().getFullYear() - this.birth;
                }
            }// this指向window或undefined
            return fn();
        }
    };
    let age=obj.getAge();
    console.log(age());//30

    吃栗子：
    //声明位置
    var fn = (()=>{
        console.log(this.x)
    })  
    var x="2"
    var obj={
        x:"1",
        fn:fn
    }
    //调用位置
    obj.fn();//2
    //调用位置
    fn();//2
    //以上可以看出，箭头函数在定义时就绑定了this，而非取决与调用位置了，
    //同样用call,apply,bind都无法更改this。
    var globalObject = this;
    var foo = (() => this);
    console.log(foo() === globalObject); // true
    var obj = {foo: foo};
    // 尝试使用call来设定this
    console.log(foo.call(obj) === globalObject); // true
    // 尝试使用bind来设定this
    foo = foo.bind(obj);
    console.log(foo() === globalObject); // true
### 练习:请使用箭头函数简化排序时传入的函数：
    'use strict'
    var arr = [10, 20, 1, 2];
    arr.sort((x, y) => {
        return x-y;
    });
    console.log(arr); // [1, 2, 10, 20]
