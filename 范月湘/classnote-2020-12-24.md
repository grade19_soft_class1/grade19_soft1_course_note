# 今日份宸宸再次犯困的笔记
## 浏览器
    1)IE浏览器是世界上使用最广泛的浏览器，它由微软公司开发，预装在windows操作系统中.装windows系统就有IE浏览器.
    2)Safari浏览器由苹果公司开发，它也是使用得比较广泛的浏览器之一.Safari预装在苹果操作系统中,从2003年首发测试以来至今已11年，是苹果系统的专属浏览器.
    3)Firefox浏览器是一个开源的浏览器，由Mozilla资金会和开源开发者一起开发.由于是开源的，so集成了很多小插件，开源拓展很多功能.发布于2002年，它也是世界上使用率前五的浏览器.
    4)Opera浏览器是由挪威一家微软公司开发，创始于1995，有着快速小巧的特点，还有绿色版属于轻灵的浏览器.
    5)Chrome浏览器由谷歌公司开发，测试版本在2008年发布，虽然年纪小，但是它有良好的稳定性、快速、安全性，很棒棒就对了...
## 浏览器对象
    js可以获取浏览器提供的很多对象，并进行操作.
### window
    所有浏览器都支持window对象.它代表浏览器的窗口.
    所有全局js对象、函数和变量自动成为window对象的成员.
    全局变量是window对象的属性.
    全局函数是window对象的方法.
    甚至(HTML DOM)document对象也是window对象属性:
    window.document.getElementById("header");
    等于这：
    document.getElementById("header");

    window.innerHeight--浏览器窗口的内高度(以像素计)
    window.innerWidth--浏览器窗口的内宽度(以像素计)
    浏览器窗口(视口)不包括工具栏和滚动条.
    吃栗子:
    'use strict';
    let q=`
    内宽:${window.innerWidth},内高:${window.innerHeight}
    `;
    let w=`
    外宽:${window.outerWidth},外高:${window.outerHeight}
    `;
    console.log(q);//内宽:1365,内高:979
    console.log(w);//外宽:1920,外高:1050

    其它窗口方法：
    window.open()--打开新窗口
    window.close()--关闭当前窗口
    window.moveTo()--移动当前窗口
    window.resizeTo()--重新调整当前窗口
### navigator
    navigator对象包含有关浏览器的信息.
    没有应用于navigator对象的公开标准，不过所有浏览器都支持该对象.

    navigator对象属性：
    appCodeName:返回浏览器的代码名.
    appMinorVersion:返回浏览器的次级版本.
    appName:返回浏览器的名称.
    appVersion:返回浏览器的平台和版本信息.
    browserLanguage:返回当前浏览器的语言.
    cookieEnabled:返回指明浏览器中是否启用cookie的布尔值.
    cpuClass:返回浏览器系统的CPU等级.
    onLine:返回指明系统是否处于脱机模式的布尔值.
    platform:返回运行浏览器的操作系统平台.
    systemLanguage:返回OS使用的默认语言.
    userAgent:返回由客户机发送服务器的user-agent头部的值.
    userLanguage:返回OS的自然语言设置.

    navigator对象方法:
    javaEnabled():规定浏览器是否启用Java.
    taintEnabled():规定浏览器是否启用数据污点(data tainting).
### screen
    screen对象包含有关客户端显示屏幕的信息.
    没有应用于screen对象的公开标准,不过所有浏览器都支持该对象.

    screen对象属性：
    availHeight:返回显示屏幕的高度(除Windows任务栏之外).
    availWidth:返回显示屏幕的宽度(除Windows任务栏之外).
    bufferDepth:设置或返回调色板的比特深度.
    colorDepth:返回目标设备或缓冲器上的调色板的比特深度.
    deviceXDPI:返回显示屏幕的每英寸水平点数.
    deviceYDPI:返回显示屏幕的每英寸垂直点数.
    fontSmoothingEnabled:返回用户是否在显示控制面板中启用了字体平滑.
    height:返回显示屏幕的高度.
    logicalXDPI:返回显示屏幕每英寸的水平方向的常规点数.
    logicalYDPI:返回显示屏幕每英寸的垂直方向的常规点数.
    pixelDepth:返回显示屏幕的颜色分辨率(比特每像素).
    updateInterval:设置或返回屏幕的刷新率.
    width:返回显示器屏幕的宽度.
### location
    Location对象包含有关当前URL的信息.
    Location对象是Window对象的一个部分，可通过window.location属性来访问.

    location对象属性：
    hash:设置或返回从#开始的URL(锚).
    host:设置从返回主机名和当前URL的端口号.
    hostname:设置或返回当前URL的主机名.
    href:设置或返回完整的URL.
    pathname:设置或返回当前URL的路径部分.
    port:设置或返回当前URL的端口号.
    protocol:设置或返回当前URL的协议.
    search:设置或返回从？开始的URL(查询部分).

    location对象方法:
    assign():加载新的文档.
    reload():重新加载当前文档.
    replace():用新的文档替换当前文档.
### document
    每个载入浏览器的HTML文档都会成为Document对象.
    Document对象使我们可以从脚本中对HTML页面中的所有元素进行访问.

    document对象集合:
    all[]:提供对文档中所有HTML元素的访问.
    anchors[]:返回对文档中所有Anchor对象的引用.
    applets[]:返回对文档中所有Applet对象的引用.
    forms[]:返回对文档中所有Form对象引用.
    images[]:返回对文档中所有Image对象引用.
    links[]:返回对文档中所有Area和Link对象引用.

    document对象属性:
    body:提供对<body>元素的直接访问.
    cookie:设置或返回与当前文档有关的所有cookie.
    domain:返回当前文档的域名.
    lastModified:返回文档被最后修改的日期和时间.
    referrer:返回载入当前文档的文档的URL.
    title:返回当前文档的标题.
    URL:返回当前文档的URL.

    document对象方法:
    close():关闭用document.open()方法打开的输出流,并显示选定的数据.
    getElementById():返回对拥有指定id的第一个对象的引用.
    getElementsByName():返回带有指定名称的对象集合.
    getElementsByTagName():返回带有指定标签名的对象集合.
    open():打开一个流,以收集来自任何document.write()或document.writeln()方法的输出.
    write():向文档写HTML表达式或js代码.
    writeln():等同于write()方法,不同的是在每个表达式之后写一个换行符.


### history
    History对象包含用户(在浏览器窗口中)访问过的URL.
    History对象是window对象的一部分，可通过window.history属性对其进行访问.
    没有应用于History对象的公开标准，不过所有浏览器都支持该对象.

    history对象属性：
    length:返回浏览器历史列表中的URL数量.

    history对象方法:
    back():加载history列表中的前一个URL.
    forward():加载history列表中的下一个URL.
    go():加载history列表中的某个具体页面.

    下面一行代码执行的操作与单击后退按钮执行的操作一样：
    history.back();
    下面一行代码执行的操作与单击两次后退按钮执行的操作一样：
    history.go(-2)
## 操作DOM
    更新:更新该DOM节点的内容，相当于更新了该DOM节点表示的HTML的内容;
    遍历:遍历该DOM节点下的子节点，以便进行进一步操作;
    添加:在该DOM节点下新增一个子节点，相当于动态增加了一个HTML节点;
    删除:将该节点从HTML中删除，相当于删除了该DOM节点的内容以及它包含的所有子节点.

    getDOM节点常用方法:
    document.getElementById()
    document.getElementsByTagName()
    CSS选择器document.getElementsByClassName()

    由于ID在HTML文档中是唯一的，所以document.getElementById()可以直接定位唯一的一个DOM节点.
    document.getElementsByTagName()和document.getElementsByClassName()总是返回一组DOM节点.
    要精确地选择DOM，可以先定位父节点，再从父节点开始选择，以缩小范围.
    eg.
    //返回ID为'test'的节点:
    let test=document.getElementById('test');
    //先定义ID为'test-table'的节点，再返回其内部所有tr节点:
    let trs=document.getElementById('test-table').getElementsByTagName('tr');
    //先定义ID为'test-div'的节点，再返回其内部所有class包含red的节点:
    let reds=document.getElementById('test-div').getElementsByClassName('red');
    //获取节点test下的所有直属子节点:
    let cs=test.children;
    //获取节点test下第一个、最后一个子节点:
    let fist=test.firstElementChild;
    let last=test.lastElementChild;

    第二种方法用querySelector()和querySelectorAll(),了解语法,使用条件获取节点更方便:
    //通过querySelector获取ID为q1的节点:
    let q1=document.querySelector('#q1');
    //通过querySelectorAll获取q1节点内的符合条件的所有节点:
    let ps=q1.querySelectorAll('div.highlighted>p');

    低版本的IE<8不支持querySelector和querySelectorAll.IE8仅有限支持.

    DOM节点是指Element，但是DOM节点实际上是Node.
    在HTML中，Node包括Element、Comment、CDATA_SECTION等很多种，以及根节点Document类型.
    但是，绝大多数时候我们只关心Element，也就是实际控制页面结构的Node，其他类型的Node忽略即可.
    根节点Document已经自动绑定为全局变量document.

    嚼栗子：
    详见(js/index.html)
![pic](./weiyi/wei1.png)

