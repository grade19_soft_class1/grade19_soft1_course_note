# 今日份小笔记
## 装饰器
    装饰器是对类、函数、属性之类的一种装饰，可以针对其添加一些额外的行为.
    简单地理解为非侵入式的行为修改.
    使用装饰器的理由：有些时候对传入参数的类型判断、对返回值的排序、过滤，对函数添加节流、防抖或其他的功能性代码，
    基于多个类的继承，各种各样的与函数逻辑本身无关的、重复性的代码.
    工具类提供了一个获取数据的函数：
    class Model1{
        getData(){
            return[{
                id:1,
                name:'Niko'
            },{
                id:2,
                name:'Bellic'
            }]
        }
    }
    console.log(new Model1().getData());
    console.log(Model1.prototype.getData());
![picture](./ke/ke1.PNG)  

    吃不透的栗子...害...直接整神了
    'use strict'
    let count=0;
    parseInt(10);
    count++;

    console.log(parseInt(20,2325));
    count++;

    console.log(count);
    let oldParseInt=parseInt;

    window.parseInt=function(){
        count++;
        return oldParseInt.apply(null,arguments);
    }
    console.log(parseInt(20,4234));
    parseInt(30);
    parseInt(100);
    console.log(count);
![picture1](./ke/ke2.PNG)
## 高阶函数
    高阶函数需要满足两个条件：
    1.接受一个或多个函数作为输入
    2.输出一个函数
    栗子：
    'use strict'
    function add(x, y, f) {
        return f(x) + f(y);
    }
    var x = add(-5, 6, Math.abs);  
    console.log(x);//11
    编写高阶函数，就是让函数的参数能够接收别的函数.  

    数组转换为整数数组，每个元素表示原始数组中字符串的长度:
    'use strict'
    const strArray=['JavaScript','Python','PHP','Java','C'];
    function mapForEach(arr,fn){
        const newArray=[];
        for(let i=0;i<arr.length;i++){
            newArray.push(fn(arr[i]));
        }
        return newArray;
    }
    const lenArray=mapForEach(strArray,function(item){
        return item.length;
    });
    console.log(lenArray);//10,6,3,4,1
## map
    map()方法返回一个新数组，数组中的元素为原始数组元素调用函数处理后的值.  
    map()方法按照原始数组元素顺序依次处理元素.

    map()不会对空数组进行检测.
    map()不会改变原始数组.

    map语法：array.map(function(currentValue,index,arr),thisValue)
    currentValue是必须的，它是当前元素的值.
    index是可选择的，它是当前元素的索引值.
    arr也是可选择的，它是当前元素属于的数组对象.
    thisValue可选，对象作为该执行回调时使用，传递给函数，用作"this"的值.
    如果省略了thisValue，或者传入null、undefined，那么回调函数的this为全局对象.

    map对象的方法和描述：
    clear:从映射中移除所有元素.
    delete:从映射中移除指定的元素.
    forEach:对映射中的每个元素执行指定操作.
    get:返回映射中的指定元素.
    has:如果映射包含指定元素,则返回 true.
    set:添加一个新建元素到映射.
    toString:返回映射的字符串表示形式.
    valueOf:返回指定对象的原始值.

    栗子：
    'use strict'
    let m=new Map();
    m.set(1,"black");
    m.set(2,"red");
    m.set("colors",2);
    m.set({x:1},3);

    m.forEach(function(item,key,mapObj){
        console.log(item.toString()+"<br/>");
        
    });
    console.log("<br/>");
    console.log(m.get(2));
![picture2](./ke/ke3.png)
    



