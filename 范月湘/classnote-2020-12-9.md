# 今日份小笔记
## 嵌套解构赋值
    栗子：
    let key={
    type:"Identifier",
    name:"foo",
    loc:{
        start:{
            line:1,
            column:1
        },
        end:{
            line:1,
            column:4
        }
    }
    };
    let {loc:{start}}=key;
    console.log(start.line);//1
    console.log(start.column);//1
## 数组解构赋值
    栗子：
    let name=["宸宸","辰辰","琛琛"];
    firstName="沉沉";
    secondName="尘尘";
    [firstName,secondName]=name;
    console.log(firstName);//宸宸
    console.log(secondName);//辰辰
## 嵌套数组解构赋值
    栗子：
    let name=["宸宸",["辰辰","琛琛"],"尘尘"];
    let [firstName,[secondName]]=name;
    console.log(firstName);//宸宸
    console.log(secondName);//辰辰  
## 变量提升复习
    function miao(){
        let key=10;
        console.log(key);
    }
    miao();//10
    这意思应该是把变量都整前边放，后边输出.

    变量提升就是把变量置顶，而不可在输出后继续定义变量，这是个例外：
    function miao(){
        let key=10;
        console.log(key);
        if(true){
            let key=3;
            console.log(key);
        }else{
            console.log(key);
        }
    }
    miao();//true情况下输出10,3;false情况下输出10,10
## 方法
    方法是通过对象调用的js函数.也就是说方法也是函数，只是比较特殊的函数.
    将函数和对象和写在一起时，函数就变成了方法.
    js定义函数的格式：
    1.let funname=funciton(参数1，参数2...){具体方法动作};
    2.function funname(let1,let2){要执行的具体代码};
    小栗子：let foo=function(a,b){return a+b;}
    小栗子中，foo是函数名，而不是对象名.

    栗子：
    let chen={
        name:'chen',
        birth:2003,
        age:function(){
            let o=new Date().getFullYear();
            return o-this.birth;
        }

    };
    console.log(chen.age);//方法
    console.log(chen.age());//17   

    啃栗子：
    function getAge(){
            let o=new Date().getFullYear();
            return o-this.birth;
        }
    let chen={
        name:'chen',
        birth:2003,
        age:getAge
    };
    console.log(chen.age());//17
    console.log(getAge());//NaN
    this是结果再好不过了..但是单独输出getAge就会显示NaN,指向全局对象,就是window.

### js调用函数的四种方法
    1.通过对象调用方法
    定义一个对象，在对象的属性中定义方法，通过myobject.property来执行方法，this☞当前的myobject对象.
    'use strict'
    let blogInfo={
        blogId:123,
        blogName:"Kris",
        showBlog:function(){
            console.log(this.blogId); 
        }
    }
    blogInfo.showBlog();//123

    2.方法(函数)的直接调用模式
    定义一个函数，设置成一个变量名保存函数，this☞向window对象.
    'use strict'
    var myfunc=function(a,b){
        return a+b;
    }
    console.log(myfunc(3,4));//7

    3.构造器调用模式
    定义一个函数对象，在对象中定义属性，在其原型对象中定义方法.使用property方法时，必须实例化该对象才能调用其方法.
    'use strict'
    var myfunc=function(a){
        this.a=a;
    };
    myfunc.prototype={show:function(){
        console.log(this.a);
    }}
    var newfunc=new myfunc("41345");
    newfunc.show();//41345

    4.apply,call调用方法
    'use strict'
    let myobject={};
    let sum=function(a,b){
        return a+b;
    };
    let sum1=sum.call(myobject,5,3);
    let sum2=sum.apply(myobject,[5,3]);
    console.log(sum1);//8
    console.log(sum2);//8