# 今日份小记录
## 一、简略地介绍了cnblogs.com博客园 .net 3.1不能跨平台，但 .net 5.0可
****
## 二、Lambda表达式的扩展方法
    扩展方法能够向现有类型'添加'方法，而无需创建新的派生类型，重新编译或以其他方式修改原始类型.
    语法要求：
    1.扩展方法必须是一个静态方法;
    2.扩展方法必须放在静态类中;
    3.扩展方法的第一个参数必须以this开头，并且制定此方法是扩展自哪个类
    扩展方法的特点：
    1.扩展方法扩展自哪个类，就必须是此类型的变量来使用，其他类型无法使用;
    2.扩展方法中的this后面的参数不属于方法的参数;
    3.如果扩展方法和实例方法具有相同的签名，则优先调用实例方法;
    4.扩展自父类上的方法，可以被子类的对象直接使用;
    5.扩展方法最终还是被编译器译成:静态类、静态方法().
### 扩展方法一(1)：FirstOrDefault() 
    FirstOrDefault()方法：返回序列中的第一个元素；如果序列中不包含任何元素，则返回默认值.
![First](imgs/note1.png)
### 扩展方法一(2)：FirstOrDefault(Func<(Of<(UTF,Boolean>)>))
    FirstOrDefault(Func<(Of<(UTF,Boolean>)>)):返回序列中满足条件的第一个元素;如果未找到这样的元素，则返回默认值.
![Second](imgs/note2.png)
![Third](imgs/note3.png)
### 扩展方法二：Max()
    Max():返回泛型序列中的最大值.
![Fourth](imgs/note4.png)
![Fifth](imgs/note5.png)
### 扩展方法三：SingleOrDefault()
    SingleOrDefault(Func<(Of<(UTF,Boolean>)>)):返回序列中满足指定条件的唯一元素：如果这类元素不存在，则返回默认值;如果有多个元素满足该条件，此方法将引发异常.
    class Program
    {
        static void Main(string[] args){
            List<Book>lstBook=new List<Book>();
            Book b1=new Book(5,"墨辰",34);
            Book b2=new Book(6,"骅邺",53);
            Book b3=new Book(7,"曜黎",26);
            lstBook.Add(b1);
            lstBook.Add(b2);
            lstBook.Add(b3);
            //返回序列中满足指定条件的唯一元素；
            //如果这类元素不存在,则返回默认值；
            //如果有多个元素满足该条件，此方法将引发异常。
            Book book = lstBook.SingleOrDefault(b => b.BPrice > 50);
            Console.WriteLine("编号：" + book.BId + "书名：" + book.BName + "价格：" + book.BPrice);
        }
    }
    //如果条件改为：lstBook.SingleOrDefault(b=>b.BPrice>50);则将引发异常.
### 扩展方法四：Where()
    Where(Func<(Of<(UTP,Boolean>)>)):基于谓词筛选值序列
    class Program
    {
        static void Main(string[] args){
            List<Book>lstBook=new List<Book>();
            Book b1=new Book(5,"勋易",34);
            Book b2=new Book(6,"封闫",53);
            Book b3=new Book(7,"启唯",26);
            lstBook.Add(b1);
            lstBook.Add(b2);
            lstBook.Add(b3);
            ////基于谓词筛选值序列
            IEnumerable<Book>books=lstBook.Where(b=>b.BPrice>30);
            foreach(Book b in books){
                Console.WriteLine("编号：" + book.BId + "书名：" + book.BName + "价格：" + book.BPrice);       
            }        
        }
    }
### 扩展方法五：Select()
    Select():将序列中的每个元素投影到新表中
    class Program
    {
        static void Main(string[] args){
            List<Book>lstBook=new List<Book>();
            Book b1=new Book(5,"莫迪",19);
            Book b2=new Book(6,"秋璇",27);
            Book b3=new Book(7,"洛桀",32);
            lstBook.Add(b1);
            lstBook.Add(b2);
            lstBook.Add(b3);
            ////基于谓词筛选值序列
            IEnumerable<Book>books2=lstBook.Select(b=>b);
            foreach(Book b in books2){
                Console.WriteLine("编号：" + book.BId + ",书名：" + book.BName + ",价格：" + book.BPrice);       
            }        
        }
    }