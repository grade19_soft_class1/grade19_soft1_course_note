# 今日份宸宸的小笔记
## DOM
    DOM全称Document Object Model,文档对象模型,它允许脚本控制Web页面、窗口与文档.
    DOM标准被分为3个不同的部分:
    核心 DOM-针对任何结构化文档的标准模型
    XML DOM-针对XML文档的标准模型
    HTML DOM-针对HTML文档的标准模型

    DOM是棵倒长的树，文档对象模型就是基于这样的文档视图结构的一种模型所有的HTML页面都逃不开这个模型，也可以称为节点树.
![pic](./dom/dom1.PNG)

    练习：如下的HTML结构：
    JavaScript、Java、Python、Ruby、Swift、Scheme、Haskell
    <body>
    <div id="test-div">
        <div class="c-red">
            <p id="test-p">JavaScript</p>
            <p>Java</p>
          </div>
          <div class="c-red c-green">
            <p>Python</p>
            <p>Ruby</p>
            <p>Swift</p>
          </div>
          <div class="c-green">
            <p>Scheme</p>
            <p>Haskell</p>
          </div>
        </div>
    </body>
    </html>
    <script src="./js.js"></script>
    请选择出指定条件的节点：
    'use strict';
    // 选择<p>JavaScript</p>:
    var js = document.getElementById('test-p');
    var js1 = document.querySelector('#test-p');
    console.log(js);
    console.log(js1);
    // 选择<p>Python</p>,<p>Ruby</p>,<p>Swift</p>:
    var arr = document.getElementsByClassName('c-red')[1].children;
    var arr1 = document.getElementsByClassName('c-red')[1].getElementsByTagName('p');
    var arr2 = document.querySelectorAll('div.c-red.c-green > p');
    var arr3 = document.querySelectorAll('.c-red.c-green > p');
    var arr4 = document.querySelectorAll('div[class="c-red c-green"] > p');
    console.log(arr);
    console.log(arr1);
    console.log(arr2);
    console.log(arr3);
    console.log(arr4);
    // 选择<p>Haskell</p>:
    var haskell = document.getElementsByClassName('c-green')[1].lastElementChild;
    var haskell1= document.querySelectorAll('div[class="c-green"] > p')[1];
    var haskell2 = document.querySelectorAll('.c-green > p')[4];
    console.log(haskell);
    console.log(haskell1);
    console.log(haskell2);
    // 测试:
    if (!js || js.innerText !== 'JavaScript') {
        alert('选择JavaScript失败!');
    } else if (!arr || arr.length !== 3 || !arr[0] || !arr[1] || !arr[2] || arr[0].innerText !== 'Python' || arr[1].innerText !== 'Ruby' || arr[2].innerText !== 'Swift') {
        console.log('选择Python,Ruby,Swift失败!');
    } else if (!haskell || haskell.innerText !== 'Haskell') {
        console.log('选择Haskell失败!');
    } else {
        console.log('测试通过!');
    }
![pic1](./dom/dom2.PNG)

## 更新DOM
    直接修改节点的文本方法有两种：
    1)修改innerHTML属性
    这个就很秀了，阔以修改一个DOM节点的文本内容，还阔以直接通过HTML片段修改DOM节点内部的子树:
    //获取<p id="p-id">...</p>
    var p=document.getElementById('p-id');
    //设置文本ABC
    p.innerHTML='ABC';
    //设置HTML
    p.innerHTML='ABC'<span style="color:rgb(0, 255, 255)">K</span>XYZ;
    
    2)修改innerText和textContent属性,可自动对字符串进行HTML编码,保证无法设置任何HTML标签
    //获取<p id="p-id">...</p>
    let q=document.getElementById('p-id');
    //设置文本
    q.innerText='<script>alert("hey")</script>';
    //HTML被自动编码，无法设置一个<script>节点

    innerText不返回隐藏元素的文本，而textContent返回所有文本.
    emmm注意IE<9不支持textContent.

    DOM节点的style属性对应所有的CSS,可以直接获取或设置.
    CSS允许font-size这样的名称,并非js有效属性名,so需要在js改写为驼峰式命名fontSize
    let w=document.getElementById('p-id');
    w.style.color='#fff000';
    w.style.fontSize='40px';
    w.style.paddingTop='2em';

    吃栗子：
    <body>   
        <div id="test-div">
            <p id="test-js">javascript</p>
            <p>Java</p>
          </div>
    </body>
    </html>
    <script src="./js.js"></script>

    'use strict';
    // 获取<p>javascript</p>节点:
    var js = document.getElementById('test-js');
    // 修改文本为JavaScript:
    // TODO:
    js.innerHTML ='JavaScript';
    // 修改CSS为: color: #ff0000, font-weight: bold
    // TODO:
    js.style.color='#ff0000';
    js.style.fontWeight='bold';
    // 测试:
    if (js && js.parentNode && js.parentNode.id === 'test-div' && js.id === 'test-js') {
        if (js.innerText === 'JavaScript') {
            if (js.style && js.style.fontWeight === 'bold' && (js.style.color === 'red' || js.style.color === '#ff0000' || js.style.color === '#f00' || js.style.color === 'rgb(255, 0, 0)')) {
                console.log('测试通过!');
            } else {
                console.log('CSS样式测试失败!');
            }
        } else {
            console.log('文本测试失败!');
        }
    } else {
        console.log('节点测试失败!');
    }//测试通过!
![pic2](./dom/dom3.PNG)
## 插入DOM 
    appendChild():把要添加的节点添加至父级里面的最后面，也叫追加.
    语法：fatherdom.appendChild(insertdom).
    所有浏览器都支持此方法.

    insertBefore():把要插入的节点添加至指定父级里面的指定节点之前.
    语法：fatherdom.insertBefore(insertdom,chosendom).
    所有浏览器都支持此方法.
    但是如果第二个参数节点不存在，在IE和Safari下会把要添加的节点使用appendChild()方法追加到指定父级中，
    而其他主流浏览器(Firefox、Chrome、Opera)下会报错.so在插入节点的时候，需要先判断第二个参数节点是否存在.

    练习:对于一个已有的HTML结构：
    Scheme、JavaScript、Python、Ruby、Haskell
    <body>   
    <ol id="test-list">
        <li class="lang">Scheme</li>
        <li class="lang">JavaScript</li>
        <li class="lang">Python</li>
        <li class="lang">Ruby</li>
        <li class="lang">Haskell</li>
    </ol>
    </body>
    </html>
    <script src="./js.js"></script>

    'use strict';
    // sort list:
    var ol=document.getElementById('test-list');
    var listC=document.getElementsByClassName('lang');
    var list=Array.from(listC);
    var newList=list.map(function(a){
        return a.innerText;
        });
    newList.sort();
    ol.innerHTML="";
    for(let i=0;i<newList.length;i++){
    var li=document.createElement('li');
    li.className='lang';
    li.innerText=newList[i];
    ol.appendChild(li);
    }
    // 测试:
    ;(function () {
        var
            arr, i,
            t = document.getElementById('test-list');
        if (t && t.children && t.children.length === 5) {
            arr = [];
            for (i=0; i<t.children.length; i++) {
                arr.push(t.children[i].innerText);
            }
            if (arr.toString() === ['Haskell', 'JavaScript', 'Python', 'Ruby', 'Scheme'].toString()) {
                console.log('测试通过!');
            }
            else {
                console.log('测试失败: ' + arr.toString());
            }
        }
        else {
            console.log('测试失败!');
        }
    })();//测试通过!
![pic4](./dom/dom4.png)
    