# 宸宸的小笔记
## generator(生成器)
    是ES6提供的一种异步编程解决方案.generator语法行为和普通函数完全不同，可以把generator理解为一个包含了多个内部状态的状态机.

    执行generator函数会返回一个遍历器对象，也就是说generator函数除了提供状态机，还可以生成遍历器对象.
    generator可以一次返回多个遍历器对象，通过这个对象可以访问到generator函数内部的多个状态.

    形式上generator函数和普通函数有两点不同：
    1)function关键字后面，函数名前面有一个"*"符号;
    2)函数体内部使用yield定义(生产)不同的内部状态.

    执行generator函数返回的是一个遍历器对象，这对象有一个next方法，执行next方法会返回一个对象，对象上有俩属性：
    1)value是yield关键字后面的表达式的值;
    2)done布尔类型，true表示没有遇到return语句，可以继续往下执行，false表示遇到return语句.

    吃栗子：
    'use strict';
    function* hey(){
        yield 'how do you do?';
        yield 'fine';
        return 'all right';
    }
    var h=hey();
    console.log(h.next());
    console.log(h.next());
    console.log(h.next());
    console.log(h.next());
    console.log(h.next());
![pic](./yj/yj1.PNG)

    吃栗子：
    'use strict';
    function* sky(){
        console.log('秋弦');
    }
    var generator=sky();
    setTimeout(function(){
        console.log(generator.next());
    }, 2020);
![pic1](./yj/yj2.PNG)

    调用generator对象有两个方法:
    1)不断地调用generator对象的next()方法：
    'use strict'
    function* fib(max) {
        var
            t,
            a = 0,
            b = 1,
            n = 0;
        while (n < max) {
            yield a;
            [a, b] = [b, a + b];
            n ++;
        }
        return;
    }
    var f=fib(5);
    console.log(f.next());
    console.log(f.next());
    console.log(f.next());
    console.log(f.next());
    console.log(f.next());
![pic2](./yj/yj3.PNG)

    2)直接用for...of循环迭代generator对象，不用自个儿判断done:
    'use strict'
    function* fib(max) {
        var
            t,
            a = 0,
            b = 1,
            n = 0;
        while (n < max) {
            yield a;
            [a, b] = [b, a + b];
            n ++;
        }
        return;
    }
    for (var x of fib(10)) {
        console.log(x); 
    }
![pic3](./yj/yj4.PNG)

    除了for...of循环，扩展运算符（...），解构赋值和Array.from方法内部调用的都是遍历器接口.
    它们都可以将Generator函数返回的Iterator对象作为参数:
    吃栗子：
    'use strict'
    function* number(){
        yield 1;
        yield 2;
        return 3;
        yield 4
    }
    //扩展运算符
    console.log(...number());
    //Array.from方法
    console.log(Array.from(number()));
    //解构赋值
    let [x,y]=number();
    console.log(x,y);
    //for...of循环
    for(let h of number()){
        console.log(h);
    }
![pic4](./yj/yj5.PNG)

    
    练习:要生成一个自增的ID，可以编写一个next_id()函数：
    'use strict'
    function* next_id(){
        let current_id =0;
        while(true) {
            current_id++;
            yield current_id;
        }
    }
    
    function* next_id(){
        let current_id =0;
        while(true) {
            current_id++;
            yield current_id;
        }
    }

    let g = next_id();

    for( var i = 0; i < 10; i++ ){
        console.log( g.next().value )
    }//按顺序输出1~10
## 标准对象
    为了区分对象的类型，用typeof操作符获取对象的类型，它总是返回一个字符串：
    typeof 123; // 'number'
    typeof NaN; // 'number'
    typeof 'str'; // 'string'
    typeof true; // 'boolean'
    typeof undefined; // 'undefined'
    typeof Math.abs; // 'function'
    typeof null; // 'object'
    typeof []; // 'object'
    typeof {}; // 'object'
    number、string、boolean、undefined、object有别于其他类.值得注意的是：null、Array、{}都属于object.
### 包装对象
    js还提供了包装对象，number、boolean、string都有包装对象，包装对象用new创建，但是其类型已变成object.
    吃栗子：
    'use strict'
    var n = new Number(123); // 123,生成了新的包装类型
    var b = new Boolean(true); // true,生成了新的包装类型
    var s = new String('str'); // 'str',生成了新的包装类型
    n === 123;  
    b === true;  
    s === 'str';
    console.log(n);
    console.log(b);
    console.log(s);
![pic5](./yj/yj6.PNG)

    Number()、Boolean()、String()会被当成普通函数，把任何类型转换为number、boolean和string类型.
    几条规则需要遵守：
    1)不要使用new Number()、new Boolean()、new String()创建包装对象；
    2)用parseInt()或parseFloat()来转换任意类型到number；
    3)用String()来转换任意类型到string，或者直接调用某个对象的toString()方法；
    4)通常不必把任意类型转换为boolean再判断，因为可以直接写if (myVar) {...}；
    5)typeof操作符可以判断出number、boolean、string、function和undefined；
    6)判断Array要使用Array.isArray(arr)；
    7)判断null请使用myVar === null；
    8)判断某个全局变量是否存在用typeof window.myVar === 'undefined'；
    9)函数内部判断某个变量是否存在用typeof myVar === 'undefined'。
## Date
    Date对象用于处理日期和时间.
    语法：var myDate=new Date();
    Date对象会自动把当前日期和时间保存为其初始值.
    Date对象属性：
    constructor:返回对创建此对象的Date函数的引用.
    prototype:可以向对象添加属性和方法.
![pic6](./yj/yj7.PNG)
![pic7](./yj/yj8.PNG)

    valueOf():返回Date对象的原始值.
### 时区
    Date对象表示的时间总是按浏览器所在时区显示的，不过我们既可以显示本地时间，也可以显示调整后的UTC时间.
    吃栗子：
    'use strict'
    var d = new Date(1435146562875);
    console.log(d.toLocaleString());
    console.log(d.toUTCString());
![pic8](./yj/yj9.PNG)

    js中，将时间对象转换为时间戳的五种方法：
    'use strict'
    var dt = new Date("2020-12-19 23:59:59.979");
    console.log(dt.getTime());
    
    console.log(dt.valueOf());

    console.log(Number(dt));
    
    console.log(+dt);
    
    console.log(Date.parse(dt));
![pic9](./yj/yj10.PNG)

    获得当前时间的时间戳：
    'use strict'
    var dt = new Date("2019-07-04 23:59:59.999");
    console.log(new Date().getTime()); // 方法一
    console.log(new Date().valueOf());// 方法二
    console.log(Date.parse(new Date()));// 方法三
    console.log(Number(new Date()));// 方法四
    console.log(+new Date()); // 方法五
![pic10](./yj/yj11.PNG)
