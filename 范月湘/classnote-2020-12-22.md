# 今日份小笔记
## RegExp进阶
    做更精确的匹配，用[]表示范围:
    eg.
    [0-9a-zA-Z\_]可以匹配一个数字、字母或者下划线;
    ^表示行的开头，^\d表示必须以数字开头.
    $表示行的结束，\d$表示必须以数字结束.
    吃栗子：
    'use strict';
    let reg=/^[\u4e00-\u9fa5\w]+$/;
    let yao='5sfd3';
    let asw='12旬遥遥re32';
    console.log(reg.test(yao));//true
    console.log(reg.test(asw));//true
### RegExp
    RegExp对象表示正则表达式，它是对字符串执行模式匹配的强大工具.
    直接量语法：/pattern/attributes
    创建RegExp对象的语法：new RegExp(pattern,attributes);

    参数pattern是一个字符串，指定了正则表达式的模式或其他正则表达式.
    参数attributes是一个可选的字符串，包含属性'g'、'i'和'm'，分别用于指定全局匹配、区分大小写的匹配和多行匹配.
![pic](./mq/mq1.PNG)

    方括号用于查找某个范围内的字符：
![pic1](./mq/mq2.PNG)

    元字符(Metacharacter)是拥有特殊含义的字符：
![pic2](./mq/mq3.PNG)

    量词
![pic3](./mq/mq4.PNG)

    RegExp对象属性
![pic4](./mq/mq5.PNG)

    RegExp对象方法
![pic5](./mq/mq6.PNG)

    支持正则表达式的String对象的方法
![pic6](./mq/mq7.PNG)
### 切分字符串
    用正则表达式切分字符串比用固定的字符更灵活.
    直接啃栗子：
    'use strict';
    let str="你是青空一望无际...\r\n\r\n第二十四篇 你是云翼无法企及\r\n\r\n 风轻 云淡 只是一眼万年";
    str.split(/第.+篇\s+.+\s+你.+及\s+.+\r\n\r\n\s+/);
    console.log(str);
![pic7](./mq/mq8.PNG)
### 分组
    分组就是在正则表达式中用()包起来的内容代表了一个分组.like this:
    'use strict';
    let reg=/(\d{4})/;
    console.log(reg.test('4324')); //true
    reg中的(\d{4})表示一个分组，匹配到四位数字.
    分组有四种类型：
    捕获型()、非捕获型(?:)、正向前瞻型(?:)、反向前瞻型(?!)
    只有捕获型分组才会暂存匹配到的串.

    捕获与引用：
    'use strict';
    let reg=/(\d{4})-(\d{2})-(\d{2})/;
    let rw='2020-12-22';
    console.log(reg.test(rw));//true
    console.log(RegExp.$1);//2020
    console.log(RegExp.$2);//12
    console.log(RegExp.$3);//22

    结合replace方法做字符串自定义替换：
    'use strict';
    let rw='2020-12-22';
    let reg=/(\d{4})-(\d{2})-(\d{2})/;
    console.log(rw=rw.replace(reg,'$1-$2-$3'));//2020-12-22

    反向引用：
    'use strict';
    let reg=/(\w{4}) is \1/;
    console.log(reg.test('joke is joke'));//true
    console.log(reg.test('koje is koje'));//true
    console.log(reg.test('okje is okej'));//false
    console.log(reg.test('ekjo is okje'));//false

    非捕获型分组：
    'use strict';
    let reg=/(?:\d{4})-(\d{2})-(\d{2})/;
    let day='2020-12-22';
    console.log(reg.test(day));//true
    console.log(RegExp.$1);//12
    console.log(RegExp.$2);//22

    正向前瞻型分组：
    'use strict';
    let reg=/joke is a (?=joker)/;
    console.log(reg.test('joke is a joker'));//true
    console.log(reg.test('joke is a jenus'));//false

    反向前瞻性分组：
    'use strict';
    let reg=/joke is a (?!joker)/;
    console.log(reg.test('joke is a joker'));//false
    console.log(reg.test('joke is a jenus'));//true
### 贪婪匹配
    贪婪模式——在匹配成功的前提下，尽可能多的去匹配;
    'use strict';
    let reg="dawesffndsjmhfgsdu";
    let n=/.*ff/g;
    console.log(n.test(reg));//true
    console.log(n.test("dawesffndsjmhfgsdu"));//false
    console.log(n.test("dawesffndsjmhfgsd"));//true
    console.log(n.test("dawesffndsjmhfgs"));//false
    console.log(n.test("dawesffndsjmhfg"));//true
    惰性模式——在匹配成功的前提下，尽可能少的去匹配.
    'use strict';
    let reg="dawesffndsjmhfgsdu";
    let n=/.*?ff/g;
    console.log(n.test(reg));//true
    console.log(n.test("d"));//false
    console.log(n.test("da"));//false
    console.log(n.test("awe"));//false
    console.log(n.test("sffn"));//true
### 全局搜索
    js正则表达式还有几个特殊的标志，最常用的是g，表示全局匹配.
    'use strict';
    let reg=/test/g;
    let qwq=new RegExp('test','g');
    console.log(reg);//  /test/g
    console.log(qwq);//  /test/g
    全局匹配类似搜索，因此不能使用/^...$/，那样只会最多匹配一次.
    正则表达式还可以指定i标志，表示忽略大小写，m标志，表示执行多行匹配.
## JSON
    JSON是一种格式，基于文本，优于轻量，用于交换数据.
    1.一种数据格式
    规范数据要如何表示，比如有个人叫"昀箬"，身高"180cm"，体重"67kg"，要将信息以多种方式呈现：
    姓名"昀箬",身高"180cm"，体重"67kg"
    name="昀箬"&height="180cm"&weight="67kg"
    <person><name>昀箬</name><height>180</height><weight>67</weight></person>
    {"name:"昀箬","height":180,"weight":67}
    以上的数据是一样的，但是形式是各式各样的.JSON是其中一种表达方式.

    2.基于文本的数据格式
    JSON是基于文本的数据格式，相对于基于二进制的数据，所以JSON在传递的时候是传递符合JSON这种格式
    (至于JSON的格式是什么我们第二部分再说)的字符串，我们常会称为“JSON字符串”.

    3.轻量级的数据格式
    在JSON之前，有一个数据格式叫xml，至今仍广泛使用，但JSON更加轻量，
    如xml需要用到很多标签，明显看到xml格式的数据中标签本身占据了很多空间，
    而JSON比较轻量，即相同数据，以JSON的格式占据的带宽更小，
    在有大量数据请求和传递的情况下是有明显优势的.

    4.被广泛地用于数据交换
    轻量已经是一个用于数据交换的优势了，更重要的JSON是易于阅读、编写和机器解析的，
    即这个JSON对人和机器都是友好的，而且又轻，独立于语言（因为是基于文本的），所以JSON被广泛用于数据交换.

    以前端JS进行ajax的POST请求为例，后端PHP处理请求为例：

    1)前端构造一个JS对象，用于包装要传递的数据，然后将JS对象转化为JSON字符串，再发送请求到后端；

    2)后端PHP接收到这个JSON字符串，将JSON字符串转化为PHP对象，然后处理请求.

    相同的数据在这里有3种不同的表现形式，分别是前端的JS对象、传输的JSON字符串、后端的PHP对象，JS对象和PHP对象明显不是一个东西，
    但是由于大家用的都是JSON来传递数据，大家都能理解这种数据格式，
    都能把JSON这种数据格式很容易地转化为自己能理解的数据结构，较为方便，在其他各种语言环境中交换数据都是如此.
### 序列化
    对象序列化是指将对象的状态转换为字符串.
    序列化是将对象的状态信息转换为可以存储或传输的形式的过程.
    吃栗子：
    'use strict';
    let obj={
        n:1,
        s:'string',
        b:true,
        x:null,
        y:undefined,
        arr:[1,2,3],
        obj:{j:423,k:758}
    };
    let str=JSON.stringify(obj);
    let obj1=JSON.parse(str);
    console.log(obj);
    console.log(str);
    console.log(obj1);
![pic8](./mq/mq9.png)
## 面向对象编程
    创建一个或多个类的专门版本类方式称为继承(js只支持单继承).
    创建的专门版本的类通常叫做子类，另外的类通常叫做父类.
    在js中，继承通过赋予子类一个父类的实例并专门化子类来实现.
    js不检测子类的prototype.constructor,必须手动申明.
    啃栗子：
    'use strict';
    //定义Meet构造器
    function Meet(waiting){
        this.waiting=waiting;
    }
    //在Meet.prototype加方法
    Meet.prototype.quiet=function(){
        alert("I will go with you together");
    } 
    Meet.prototype.sameWay=function(){
        alert("Hello,I'm "+this.waiting);
    }
    //定义Whatever构造器
    function Whatever(waiting,never){
        //调用父类构造器，确保"this"在调用过程中设置正确
        Meet.call(this,waiting);
        //初始化Whatever类特有属性
        this.never=never;
    }
    //调用Meet的正确位置，从Whatever调用它
    Whatever.prototype=Object.create(Meet.prototype);//See note below
    //设置"constructor"属性指向Whatever
    Whatever.prototype.constructor=Whatever;
    //更换"sameWay"方法
    Whatever.prototype.sameWay=function(){
        console.log("Hey,I'm "+this.waiting+".I'm thinking "+this.never+".");  
    };
    //加入"shadow"方法
    Whatever.prototype.shadow=function(){
        console.log("maybe your world will colorful");   
    }
    //测试
    let thought=new Whatever("Jennifer","I need quiet");
    thought.sameWay();
    thought.quiet();
    thought.shadow();
    console.log(thought instanceof Meet);
    console.log(thought instanceof Whatever);
![pic9](./mq/mq10.png)

![pic10](./mq/mq11.png)

 
