# 今日份小笔记
## 多维数组
     JS中不能直接定义多维数组的，但是可以通过变通的方式来实现多维数组.
     栗子：
        let arr =[[1,2,3],[3,4,5],[5,6,7]];
        arr[0][0];
        arr[1][0];
        console.log(arr);
     练习：在新生欢迎会上，你已经拿到了新同学的名单，
     请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：
    'use strict';
    var arr = ['小明', '小红', '大军', '阿黄'];
    var w=arr.sort();
    var q=arr.join(',');
    console.log('欢迎'+arr.slice(0,3)+'和'+arr[3]+'同学');
     
## 对象
    js中所有事物都是对象：字符串、数字、数组、日期等.
    对象只是一种特殊的数据，对象拥有属性和方法.

    访问对象属性的语法：objectName.propertyName
    访问对象的方法:objectName,methodName()

    创建新对象的方法:
    1.使用Object定义并创建对象的实例.
    2.使用函数来定义对象，然后创建新的对象实例.
    举个栗子：
    <script>
        var person=new Object();
        person.firstname="Jane";
        person.lastname="Justin";
        person.age=27;
        person.eyecolor="blue";
        document.write(person.firstname+" is "+person.age+" years old");
    </script>
    对象构造器栗子：
     <script>
         function person(firstname,lastname,age,eyecolor){
             this.firstname=firstname;
             this.lastname=lastname;
             this.age=age;
             this.eyecolor=eyecolor;
         }
         joker=new person("Jane","Justin","27","blue");
         document.write(joker.firstname+" is "+joker.age+" years old");
     </script>

     属性名包括特殊字符($,下划线,字母+数字)要用单引号括起来.
     访问属性用[' '],检测变量名是否拥有同一属性，用in.
     判断属性是否是变量名自身拥有，而不是继承而来，用hasOwnProperty().
## 条件判断
    1.if(){}
    栗子：
    if(2==2 && 3==3){
    console.log('em');
    }else if(3==3){
        console.log('no');
    }else{
        console.log('k');
    }

    2.switch(a){}
    栗子：
     switch(a){
     case 1:
         console.log(1);
         break;
     case 2:
         console.log(2);
         break;
     default:
         console.log('default');
    }

    3.for循环
    var cue=['a','b','c','d'];
    for(var i in cue){
     console.log(cue[i]);
    }

    4.while
     while(true){
      continue;
      break;
    }

    练习：小明身高1.75，体重80.5kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数：
    低于18.5：过轻
    18.5-25：正常
    25-28：过重
    28-32：肥胖
    高于32：严重肥胖
    用if...else...判断并显示结果：
    var height=parseFloat(prompt('请输入身高(m):'));
    var weight=parseFloat(prompt('请输入体重(kg):'));
    var BMI=weight/(height^2);
    if(BMI<18.5){
        console.log('过轻'); 
    }else if(BMI<=18.5 && BMI<25){
        console.log('正常'); 
    }else if(BMI<=25 && BMI<28){
        console.log('过重');
    }else if(BMI<=28 && BMI<32){
        console.log('肥胖');
    }else 
    {
        console.log('严重肥胖');
    }


