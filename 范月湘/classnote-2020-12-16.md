# 今日份小笔记
##  练习：不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数
    'use strict'
    function string2int(str){
        str=str.split('');
        var strint=str.map(function(x){
            return +x;
        })
        return strint.reduce(function(x,y){
            return x*10+y;
        })
    }
    // 测试:
    if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
        if (string2int.toString().indexOf('parseInt') !== -1) {
            console.log('请勿使用parseInt()!');
        } else if (string2int.toString().indexOf('Number') !== -1) {
            console.log('请勿使用Number()!');
        } else {
            console.log('测试通过!');
        }
    }
    else {
        console.log('测试失败!');
    }//测试通过!
##  练习:请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。
    'use strict'
    function normalize(arr) {
        function strs(str){
            str=str.toLowerCase();//全部都先小写
            let x1 = str.substring(0, 1).toUpperCase();//提取第一个进行大写
            let x2 = str.substring(1);//提取该元素的后面
            return x1+x2;
        }
        return arr.map(strs);
    }
    // 测试: 
    if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
        console.log('测试通过!');
    }
    else {
        console.log('测试失败!');
    }//测试通过!
## 小明希望利用map()把字符串变成整数，他写的代码很简洁：
    'use strict';
    var arr = ['1', '2', '3'];
    var r;
    r = arr.map(parseInt);
    console.log(r);
    结果竟然是1, NaN, NaN，小明百思不得其解，请帮他找到原因并修正代码。
    修改：
    'use strict';
    var arr = ['1', '2', '3'];
    var r;
    r = arr.map(Number);
    console.log(r);//1,2,3
##  练习:请尝试用filter()筛选出素数：
    'use strict';
    function get_primes(arr) {
        function isPrimeNumber(x) {
            if (x === 1) {
                return false;//1不是素数，返回false
            }

            //i是2到x开平方根之间的整数
            //如果x对i取余等于0，则不是素数，返回false

            for (let i = 2; i <= Math.sqrt(x, 2); i++) {
                if (x % i === 0) {
                    return false;
                }
            }

            //如果上述条件都不满足，则是素数，返回true
            return true;
        }
        
        //用filter()返回新的只含有素数的数组
        return arr.filter(isPrimeNumber);
    }
    // 测试:
    var
        x,
        r,
        arr = [];
    for (x = 1; x < 100; x++) {
        arr.push(x);
    }
    r = get_primes(arr);
    if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
        console.log('测试通过!');
    } else {
        console.log('测试失败: ' + r.toString());
    }//测试通过! 
## Array.prototype.map()
    语法：const new_array = arr.map(callback[, thisArg])

    参数说明：
    callback
    生成新数组元素的函数，使用三个参数：
    currentValue
    callback的第一个参数，数组中正在处理的当前元素.
    index
    callback的第二个参数，数组中正在处理的当前元素的索引.
    array
    callback的第三个参数，map 方法被调用的数组.
    thisArg可选的。执行 callback 函数时 使用的this 值.

    吃栗子:
    'use strict'
    const array1 = [1, 4, 9, 16];
    // pass a function to map
    const map1 = array1.map(x => x * 2);
    console.log(map1);
    // expected output: Array [2, 8, 18, 32]
    
    吃栗子：
    'use strict'
    var str="aabccd";
    var obj={};
    [].map.call(str,function(x,y,z){
        if(!obj[x]){
            obj[x]=1
        }else{
            obj[x]+=1
        }
    });
    //直接使用字符串的遍历器接口 ES6
    for (let x of str) {console.log(x)}
    // obj is {a: 2, b: 1, c: 2, d: 1}
