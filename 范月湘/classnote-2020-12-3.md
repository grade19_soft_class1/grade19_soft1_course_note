# 今日份小笔记
## 定义函数
    老胡有言：<script src="./js/js.js"><script>
    ./是当前文件，/是子目录
     这样式儿的<script src="js.js"></script>也彳亍.

     js四种定义函数方法:
     1.function语句定义函数
     function q(w){
        if(w>=0){
            return w;
        }else{
            return -w;
        }
    }
    console.log(q);
     2.Function()构造函数来定义函数
     var e=new Function("q","w","return q+w");

    var r=function(q,w){
        return q+w;
    }
    console.log(e);
    console.log(r);
![图片](./eva/eva1.PNG) 

     3.函数表达式
     //函数表达式
    var functionName = function(args){
    //函数体
    };
     4.自调用函数
     (function (){
    console.log("一次性函数");  
    })();
    (function(win){
        var num=20;
        win.num=num;
    })(window);
    //把局部变量给父类就可
    console.log(num);
## 调用函数
    无意间发现了throw(创建或抛出异常)这个东东
    throw语法：throw exception
    栗子：
    <p>Please input a number between 5 and 10:</p>
    <input id="demo" type="text">
    <button type="button" onclick="myFunction()">Test Input</button>
    <p id="message"></p>
    <script>
    function myFunction() {
        var message, x;
        message = document.getElementById("message");
        message.innerHTML = "";
        x = document.getElementById("demo").value;
        try {
            if(x == "") throw "is Empty";
            if(isNaN(x)) throw "not a number";
            if(x > 10) throw "too high";
            if(x < 5) throw "too low";
        }
        catch(err) {
            message.innerHTML = "Input " + err;
        }
    }
    </script>
## arguments
    arguments对象能够模拟重载.
    Arguments对象实例arguments引用函数实参，用函数下标"[]"引用arguments元素
    arguments.length为函数实参个数，arguments.callee引用函数自身.

    arguments特性：
    1.arguments对象和Function是分不开的.
    2.argumrnts这个对象不能显式创建.
    3.arguments对象只有函数开始时才可用.
    栗子：
    function res(){
    var q="";
    for(var i=0;i<arguments.length;i++){
        alert(arguments[i]);
        q += arguments[i] + ",";
    }
        return q;
    }
    res("name", "age");

    callee属性是 arguments 对象的一个成员,仅当相关函数正在执行时才可用.
    callee属性的初始值就是正被执行的Function对象.实现匿名的递归函数.\
    var mem=function(m){
        if(1==m){
            return 1;
        }else{
            return m+arguments.callee(m-1);
        }
    }
    alert(mem(5));//输出15
## rest参数
    rest语法：function f(a, b, ...theArgs) {
                // ...
             }
    区分rest参数和参数对象：
    1.rest参数不会为每个变量给一个单独的名称，参数对象包含所有参数传递给函数.
    2.参数对象不是真正的数组，rest参数是真实的数组实例.
    例如数组sort、map、forEach、pop的方法都可以直接使用
    3.参数对象有自己额外的特性(例如callee属性)
    栗子：
    function fun1(...theArgs) {
    console.log(theArgs.length);
    }
    fun1();  // 0
    fun1(5); // 1
    fun1(5, 6, 7); //3

    练习1：
    var a=1;
    var b=4;
    var c=3;
    foo(a,[b], c)
    // 接收2~3个参数，b是可选参数，如果只传2个参数，b默认为null：
    function foo(a, b, c) {
        console.log(a);
        console.log(b);
        console.log(c);
        if (arguments.length === 2) {
            // 实际拿到的参数是a和b，c为undefined
            c = b; // 把b赋给c
            b = null; // b变为默认值
    }
    console.log(a);
    console.log(b);
    console.log(c);

    练习2：
    function sum(...rest) {
    //console.info(rest)
    li = 0;
    if(arguments.length>0){
        for(var i =0;i<rest.length;i++){
            li+=rest[i]
            console.info('第'+i+'次循环rest[i]的值:'+rest[i])
    //注释掉的两行和上面的循环效果一样
    //  for(var x of rest){  
        //    li+=x;a
        }return li;
        }else 
    return 0;
    }
    //测试:
    var i, args = [];
    for (i=1; i<=100; i++) {
    args.push(i);
    }
    if (sum() !== 0) {
    console.log('测试失败: sum() = ' + sum());
    } else if (sum(1) !== 1) {
    console.log('测试失败: sum(1) = ' + sum(1));
    } else if (sum(2, 3) !== 5) {
    console.log('测试失败: sum(2, 3) = ' + sum(2, 3));
    } else if (sum.apply(null, args) !== 5050) {
    console.log('测试失败: sum(1, 2, 3, ..., 100) = ' + sum.apply(null, args));
    } else {
    console.log('测试通过!');
    }

    练习3：
    定义一个计算圆面积的函数area_of_circle()，它有两个参数：
    r: 表示圆的半径；
    pi: 表示π的值，如果不传，则默认3.14
    'use strict';
    function area_of_circle(r, pi) {
         if(arguments.length===0){
             return 0;
         }else if(arguments.length===1){
             pi=3.14;
         }
         return pi*r*r;
    }
    // 测试:
    if (area_of_circle(2) === 12.56 && area_of_circle(2, 3.1416) === 12.5664) {
        console.log('测试通过');
    } else {
        console.log('测试失败');
    }
    
    练习4：
    小明是一个JavaScript新手，他写了一个max()函数，返回两个数中较大的那个：
    'use strict';
    function max(a, b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }
    console.log(max(15, 20));
