# 今日份笔记之数据类型和变量
## Number
    定义：Number对象是原始数值的包装对象.
    Number创建方式new Number().

    语法:var example=new Number(value);
    参数值不能转换为一个数字将返回NaN.

### Number对象属性：
    1.constructor:返回对创建此对象的Number函数的引用.
    2.max_value:可表示的最大的数.
    3.min_value:可表示的最小的数.
    4.negative_infinity:负无穷大，溢出时返回该值.
    5.positive_infinity:正无穷大，溢出时返回该值.
    6.NaN:非数字值.
    7.prototype:允许向对象添加属性和方法.
    8.epsilon:表示1和比最接近1且大于1的最小Number之间的差别
    9.min_safe_integer:JavaScript最小安全整数.
    10.max_safa_integer:JavaScript最大安全整数.
### Number对象方法：
    Number对象方法可以使用Number.的形式调用，也可以使用全局调用.
    1.isFinite:检测指定参数是否为无穷大.
    2.toExponential:把对象的值转换为指数计数法.
    3.toFixed:把数字转换为字符串,结果的小数点后有指定位数的数字.
    4.toPrecision:把数字格式化为指定的长度.
    5.toString():把数字转换为字符串,使用指定的基数.
    6.valueOf:返回一个Number对象的基本数字值.
    7.parseFloat():把字符串参数解析成浮点数，左右等效于一元运算法+.
    8.parseInt():把字符串解析成特定基数对应的整型数字.
    9.isInteger():判断传递的值是否为整数.
    10.isNaN():判断传递的值是否为NaN;
    11.isSafeInteger():判断传递的值是否为安全整数.
## 字符串
    1.JavaScript字符串用于存储和处理文本.
    2.字符串可以插入到引号中的任何字符，单双引号效果一样样的.
    栗子：var q='derderder'; var q="derderder";
    3.可以用索引位置来访问字符串中的每个字符.
    栗子：var position=q[4];
    4.在字符串中添加转义字符使用引号.
    举个栗子：
     <script>
           var r='It\'s alright';
           var y="He is called \"Jay\"";
           document.write(r+y);
    </script>
    5.字符串长度
    使用内置属性length计算字符串长度.
    栗子：var tes="fauegraedhsaufgwadh";
    var ice=tes.length;
    6.特殊字符
    在字符串中可以使用转义字符转义的几种特殊字符：
    1. \' 单引号
    2. \" 双引号
    3. \\ 反斜杠
    4. \n 换行
    5. \r 回车
    6. \t tab(制表符)
    7. \b 退格符
    8. \f 换页符
## 布尔值
    JavaScript中布尔值有true和false.
    栗子：<script>
           var i=5;
           var l=6;
           if(i>l){
               alter('i>l返回true');
           }
       </script>
    布尔值为false，其他转化为true的几种情况：
    1.undefined:未定义，找不到值时出现.
    2.null:空值
    3.false:布尔值的false,字符串false布尔值为true
    4.0:数字0，字符串0布尔值为true
    5.NaN:无法计算结果时出现，表示非数值,但typeofNaN==="number"
    6."":双引号或单引号
## 比较运算符
    比较运算符在逻辑语句中使用，以测定变量或值是否相等.
    1. ==等于
    2. ===绝对等于(值和类型均相等)
    3. !=不等于
    4. !==不绝对等于(值和类型有一个不相等，或两个都不相等)
    5. >大于
    6. <小于
    7. >=大于或等于
    8. <=小于或等于
## null和undefined
    null是js中的关键字,表示空值,null可以看作是object的一个特殊的值,如果一个object值为空,表示这个对象不是有效对象.

    undefined不是js中的关键字，其是一个全局变量，是global的一个属性，以下情况会返回undefined:
    1.使用了一个未定义的变量:var i;
    2.使用了已定义但为声明的变量;
    3.使用了一个对象属性，但该属于不存在或者未赋值;
    4.调用函数时，该提供的参数没有提供:
    function func(d){
        console.log(d);
    }
    func();//undefined
    5.函数没有返回值时，默认返回undefined:
    var silk=func();
    silk;//undefined
## 数组
    数组对象的作用是使用单独的变量名来存储一系列的值.
    举个栗子：
    1.常规方式：
    <script>
    var x;
    var star=new Array();
    star[0]="super";
    star[1]="moon";
    star[2]="sun";
    for(i=0;i<star.length;i++){
        document.write(star[i]+"<br />")
    }
    </script>
    2.简洁方式：
    var star=new Array("super","moon","sun");
    3.字面：
    var star=["super","moon","sun"];
## 对象
    布尔型可以是一个对象，
    数字型可以是一个对象，
    字符串可以是一个对象，
    日期是一个对象，数学和正则表达式也是对象，
    数组是一个对象，甚至函数也可以是对象.
    创建新对象有两个不同的方法：
    1.使用object定义并创建对象的实例.
     <script>
                var fruit=new Object();
                fruit.firstname="Melon";
                fruit.lastname="WaterMelon";
                fruit.weight=24;
                fruit.color="green"; 
                document.write(fruit.firstname + " is " + fruit.weight + " catties ");
        </script>
    2.使用函数来定义对象，然后创建新的对象实例.
    <script>
                function fruit(firstname,lastname,weight,color){
                    this.firstname=firstname;
                    this.lastname=lastname;
                    this.weight=weight;
                    this.color=color;
                }
                FruitType=new fruit("Melon","WaterMelon",24,"green");
                document.write(FruitType.firstname + " is " + FruitType.weight + " catties");
    </script>              
## 变量
    变量是用来存储信息的"容器".
    栗子：
    <script>
             var r=3;
             var s=4;
             var t=r+s;
             document.write(r+"<br>");
             document.write(s+"<br>");
             document.write(t+"<br>");
    </script>
## strict模式
    use strict的目的是指定代码在严格条件下执行.
    严格模式通过在脚本或函数的头部添加use strict表达式来声明.
    为啥要使用严格模式？
    1.消除代码运行的一些不安全之处，保证代码运行的安全;
    2.提高编译器效率，增加运行速度;
    3.为未来新版本的JavaScript做好铺垫.
### 严格模式的限制
    1.不允许使用未声明的变量;
    2.不允许删除变量或对象(对象也是一个变量);
    3.不允许删除函数;
    4.不允许变量重名;
    5.不允许使用八进制;
    6.不允许使用转义字符;
    7.不允许对只读属性赋值;
    8.不允许对一个使用getter方法读取的属性进行赋值;
    9.不允许删除一个不允许删除的属性;
    10.变量名不能使用"eval"字符串;
    11.变量名不能使用"arguments"字符串;
### 保留关键字
    为了向将来JavaScript的新版本过渡，严格模式新增了一些保留关键字：
    implements、interface、let、package、private、protected、public、static、yield

#### 今天，土狗，我记住你了！！！