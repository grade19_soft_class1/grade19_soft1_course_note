# 今日份笔记
## Set
    set中成员的值都是唯一的，没有重复的值.
    向set中添加成员时，不会发生类型转换.
    向set中添加的对象总是不相等.
### set常用的属性和方法
    属性：
    size:返回set实例的成员总数
    方法：
    add():添加某个值，返回set本身
    delete():删除某个值，返回一个布尔值，判断删除是否成功
    has():返回一个布尔值，表示该值是否为set成员
    clear():清除所有成员，没有返回值
    keys():返回键名的遍历器
    values():返回键值的遍历器
    entries():返回键值对的遍历器
    forEach():使用回调函数遍历每个成员
    栗子：
    <script type="text/javascript">
        const set=new Set();
        [1,2,3,3,4,5].forEach(x=>{
            set.add(x);
        })
        console.log(set);//输出1~5
        for(var i of set){
            console.log(i);
        }
    </script>
## Redis
    Redis 是一个高性能的key-value数据库。
    Redis的出现，很大程度补偿了memcached这类keyvalue存储的不足，在部分场合可以对关系数据库起到很好的补充作用。

## iterable
    遍历Array可以采用下标循环，遍历Map和Set就无法使用下标.
    为了统一集合类型，ES6标准引入了新的iterable类型，Array、Map和Set都属于iterable类型.
    for...of循环栗子：
    var q=['a','b','c'];
    var s=new Set(['d','e','f']);
    var m=new Map([[2,'r'],[4,'y'],[6,'k']]);
    for(var v of q){
        console.log(v);//遍历Array
    }
    for(var v of s){
        console.log(v);//遍历Set
    }
    for(var v of m){
        console.log(v);//遍历Map
    }
![图片](./que/que1.PNG)

    for...in循环将把name包括在内，但Array的length属性却不包括在内.
    for...of循环只循环集合本身的元素.
    直接使用iterable内置的forEach方法，接收一个函数，每次迭代就自动回调该函数，栗子：
    var k=['1','2','3'];
    k.forEach(function(el,i,m) {
    console.log(el+',i='+m);
    //el:指向当前元素的值
    //i:指向当前索引
    //m:指向Array对象本身
    });
![图片1](./que/que2.PNG)

## 函数
    js使用关键字function定义函数.
    函数可以通过声明定义，也可以是一个表达式.
    函数声明的语法：
    function functionName(parameters){
        执行的代码
    }
    js函数定义方式栗子：
    <script type="text/javascript">
    //函数的第一种定义方式
        function add(a,b){
            var sum=a+b;
            document.write(sum);
        }
        add(3,5);
    //函数的第二种定义方式
        var slight=function(a,b){
            var slight=a+b;
            document.write(slight);
        } 
        slight(5,6);
    //函数的第三种定义方式
        var sky=new Function("x,y","var ui;ui=x+y;return ui");
        alert(sky(7,8));
    </script>

    关于变量和参数文体：
    1.函数外面定义的变量是全局变量，函数内可以直接使用
    2.在函数内部没有使用var定义的=变量则为全局变量
    3.在函数内使用var关键字定义的变量是局部变量，即出了函数外边无法获取.函数支持默认值
    4.在函数内部定义的函数为局部函数，在函数外部不能获取到
## 调用函数
    方法调用模式
    var q={
    value:0,
    inc:function(){
            alert(this.value)
        }
    }
    q.inc()//输出0

    函数调用模式
    var add=function(a,b){
    alert(this)//this被绑顶到window
        return a+b;
    }
    var sum=add(3,4);
    alert(sum)//输出7

    构造器调用模式
    var miao=function(string){
    this.status=string;
    }
    miao.prototype.get_status=function(){
        return this.status;
    }
    var kx=new miao("quiet");
    alert(kx.get_status());//输出quiet

    call和apply调用模式
    function f(x,y) {  //定义求和函数
        return x + y;
    }
    console.log(f.call (null, 3, 4));  //返回7

    function f(x,y) {  //定义求和函数
        return x + y;
    }
    console.log(f.apply(null, [3,4]));  //返回7
## markdown mathjax
    添加数学公式方式：1.在Vscode添加Markdown math插件
    2.<script>网址<script>
![图片2](./que/que3.png)
