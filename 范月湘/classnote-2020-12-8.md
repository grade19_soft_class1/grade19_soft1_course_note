# 今日份小笔记(解构赋值)
## 局部作用域
    1.变量在函数内部声明，变量为局部作用域.
    2.局部变量:只能在函数内部访问.
    栗子：
    'use strict'
    var num=10;
    function nu(){
        var num=20;
        console.log(num);
    }
    nu();
    console.log(num);//先输出20再输出10
## var、let、const
    for(var i=0;i<3;i++){
        setTimeout(function(){
            console.log(i);
        },1000);
    }//3个3,而非1、2、3
    for(let i=0;i<3;i++){
        setTimeout(function(){
            console.log(i);
        },1000);
    }//0、1、2

    由此可见，let是更完美的var
    1.let声明的变量拥有块级作用域.
    2.let声明的全局变量不是全局变量的属性.
    3.形如for(let x...)的循环在每次迭代时都为x创建新的绑定.
    4.用let重定义变量会抛出一个语法错误(SyntaxError).

    ES6引入的第三个声明类关键词:const.const就是用来定义常量的.
## 常量
    js中一旦被定义就无法再被修改的变量，称之为常量.
    'use strict'
    var PI=Math.PI;
    PI=100;
    console.log(PI);//100  

    常量即只能读取不能编辑(删除、修改)的变量.
    const是可以创建变量(基本类型)的.
    栗子：
    const TEST_C={
    };
    console.log(TEST_C.a=1);
    console.log(TEST_C.b=2);
    delete TEST_C.a;
    console.log(TEST_C);
    console.log(TEST_C.b=3);
    console.log(TEST_C);
![pictrue](./dr/dr1.PNG)

### Object方法实现
    1)Object.defineProperty:该方法会直接在一个对象上定义一个新属性，或者修改一个对象的现有属性，并返回这个对象.
    2)Object.preventExtensions:该方法让一个对象变得不可扩展，也就是永远不能再添加新的属性.
    3)Object.seal:让一个对象密封并返回被密之后的对象，新对象将变得不可扩展，即不能添加新的属性但是可以修改原本可写状态的属性.
    4)Object.freeze:该方法可以冻结一个对象，冻结指的是不能向这个对象添加新的属性，并不能修改其已有属性的值，不能删除已有属性，以及不能修改该对象已有属性的可枚举性、可配置性、可写性.也就是说，这个对象永远是不可变的，该方法返回被冻结的对象.
## 解构赋值
    解构赋值是对赋值运算符的扩展.解构赋值主要分为数组的解构和对象的解构.
    栗子：
    'use strict'
    function buildDate({year, month, day, hour=0, minute=0, second=0}) {
    console.log(year);
    console.log(month);
    console.log(day);
    console.log(hour);
    console.log(minute);
    console.log(second);
    }
    buildDate({ year: 2017, month: 1, day: 1 });
    buildDate({ year: 2017, month: 1, day: 1, hour: 20, minute: 15 });

    指定的局部变量名称在对象中不存在，那么局部变量会被赋值为undefined
    let node={
    type:"Identifier",
    name:"foo"
    };
    let{type,name,value}=node;
    console.log(type);//Identifier
    console.log(name);//foo
    console.log(value);//undefined

    指定的属性不存在时，随意定义一个默认值，在属性名称后添加一个等号(=)和相应的默认值即可.
    栗子：
    let node={
        type:"Identifier",
        name:"foo"
    };
    let{type,name,value=true}=node;
    console.log(type);//Identifier
    console.log(name);//foo
    console.log(value);//true

    为非同名局部变量赋值:
    let node={
        type:"Identifier",
        name:"foo"
    };
    let{type:localType,name:localName}=node;
    console.log(localType);//Indefined
    console.log(localName);//foo    

    数组解构：
    let food=["noodles","rice","fans"];
    let[firstFood,secondFood]=food;
    console.log(firstFood);//noodles
    console.log(secondFood);//rice
    
    解构模式中，可以直接省略元素，为感兴趣的元素提供变量名：
    let colors=["blue","purple","brown"];
    let[,,thirdColor]=colors;
    console.log(thirdColor);//brown
## 憨批弟弟来蹭课了...