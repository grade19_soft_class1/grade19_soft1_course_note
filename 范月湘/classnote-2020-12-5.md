# 今日份小笔记(变量作用域与解构赋值)
## var声明变量
    1.var关键字声明变量，无论声明在何处，都会被视为声明在函数的最顶部(不在函数内即在全局作用域的最顶部).
    2.不带var定义的变量，必须是声明+赋值，变量的声明不会提升，否则会报错.
    a=5;
    console.log(a);//5

    console.log(a);
    a=5;//报错

    不同函数内部的同名变量互相独立，互不影响：
    栗子：
    'use strict'
    function abs(){
        var name="晨晨";
        function jdk(){
            var name="垚垚";
            console.log(name);
            console.log(name);
        }
        jdk();
    console.log(name);
    console.log(name);
    }
    abs();//先俩垚垚后俩晨晨
## 变量提升
    js中,函数及变量的声明都将被提升到函数的最顶部.
    js中，变量可以在使用后声明，也就是变量可以先使用再声明.
    栗子：
    console.log(v);
    var v=100;
    function foo(){
        console.log(v);
        var v=200;
        console.log(v);
    }
    foo();
    console.log(v);//先俩undefined后200再100

    js引擎自动提升了变量的声明，但不会提升变量的赋值.
    老栗子了：
    'use strict'
    function foo(){
        var y='Bob';
        var x="Hello,"+y;
        console.log(x);
        
    }
    foo();

## 全局作用域
     js作用域可以分为两大类：全局作用域、局部作用域(函数作用域).
     1.全局作用域：
     1)直接编写在script标签中的js代码，都是全局作用域;
     2)或是一个单独的js文件中.
     3)全局作用域在页面打开时创建，页面关闭时销毁;
     4)在全局作用域中有一个全局对象window(代表的是一个浏览器的窗口，由浏览器创建)，可以直接使用.
     在全局变量中，所有创建的变量都会作为window对象的属性保存.
    var a=10;
    alert(a);
    alert(window.a);//10
    所有创建的函数都会作为window对象的方法保存.
    function fn(){
    console.log('函数');
    }
    window.fn();//函数
### 局部作用域(函数作用域)
    在函数内部就是局部作用域，这个代码的名字只在函数的内部起作用.
    调用函数时创建函数作用域，函数执行完毕之后，函数作用域销毁.
    每调用一次函数就会创建一个新的函数作用域，它们之间是相互独立的.
    栗子：
    var num=10;
    function nu(){
        var num=20;
        console.log(num);
    }
    nu();
    console.log(num);//先20后10
## 名字空间(命名空间...)
    尽量不要使用全局变量，防止环境污染和命名冲突.
    将全局变量放在一个命名空间下是一个好的解决方案.
### 静态命名空间
    1.直接赋值
    虽然啰嗦繁杂but安全，不会产生歧义
    栗子：
     var myKrace={};
    myKrace.id=0;
    myKrace.jky=function(){
        return myKrace.id++;
    }
    myKrace.replace=function(){
        myKrace.id=0;
    }
    window.console&&console.log(
        myKrace.jky(),
        myKrace.jky(),
        myKrace.replace(),
        myKrace.jky()
    );//0,1,undefined,0
    可以在命名空间函数中使用this来指代对象，函数被重新分配是this将指向不明.

    2.对象字面量方法
    如果要改变命名空间的名字方便，but要注意用this存在一定风险.
    栗子：
    var myKrace={
        id:0,
        jky:function(){
            return this.id++;
        },
        replace:function(){
            this.id=0;
        }
    }
    window.console&&console.log(
        myKrace.jky(),
        myKrace.jky(),
        myKrace.replace(),
        myKrace.jky()
    );//0,1,undefined,0

    3.模块模式
    将全局变量保存在一个马上执行的函数包装器(a function wrapper)中，函数包装器将返回一个相当于模块公共接口的对象.
    同时没有在函数包装器中return的变量将是私有变量，只对引用它的公共函数可见.
    栗子：
     var myKrace=(function(){
        var id=0;
        return {
            jky:function(){
                return id++;
            },
            replace:function(){
                id=0;
            }
        };
    })();
    window.console&&console.log(
        myKrace.jky(),
        myKrace.jky(),
        myKrace.replace(),
        myKrace.jky()
    );//0,1,undefined,0
### 动态命名空间
    命名空间被代理(proxy)表示，代理直接指向了函数包装器(the function wrapper)内部，
    意味着不再需要绑定一个return给命名空间.使命名空间的定义更加灵活，
    动态命名空间有模块命名模式的所有好处，并且比模块模式更加直观和易于阅读.
    1.应用一个命名空间参数Supple a Namespace Arguments
    栗子：
     var myKrace={};
    (function(content){
        var id=0;
        content.jky=function(){
            return id++;
        };
        content.replace=function(){
            id=0;
        }
    })(myKrace);
    window.console&&console.log(
        myKrace.jky(),
        myKrace.jky(),
        myKrace.replace(),
        myKrace.jky()
    );//0,1,undefined,0
    2.可以将context设置成全局对象
    栗子：
     var myKrace={};
    (function(context){
        var id=0;
        context.jky=function(){
            return id++;
        };
        context.replace=function(){
            id=0;
        }
    })(this);
    window.console&&console.log(
        jky(),
        jky(),
        replace(),
        jky()
    );//0,1,undefined,0
    3.用this关键字作为命名空间代理(a Namespace Proxy)
    好处是this关键字是在给予的执行环境中是静态的，它不会偶然改变.
    栗子：
     var myKrace={};
    (function(){
        var id=0;
        this.jky=function(){
            return id++;
        };
        this.replace=function(){
            id=0;
        }
    }).apply(myKrace);
    window.console&&console.log(
        myKrace.jky(),
        myKrace.jky(),
        myKrace.replace(),
        myKrace.jky()
    );//0,1,undefined,0
    apply和call函数是将this和指定的函数对象绑定，(funciton(){...}).apply(命名空间)，
    匿名函数里面的this一定指向命名空间.
    栗子：
    var exe1 = {},  exe2 = {};
    var jkyIdMod = function (startId) {
        var id = startId || 0;
        this.jky = function () {
            return id++;
        };
        this.replace = function () {
            id = 0;
        }
    };
    jkyIdMod.call(exe1);
    jkyIdMod.call(exe2, 1000);
    window.console && console.log(
        exe1.jky(),
        exe1.jky(),
        exe2.jky(),
        exe1.replace(),
        exe2.jky(),
        exe1.jky()
    ) //0,1,1000,undefined,1001,0
    和全局环境绑定：
    jkyIdMod();   
    window.console && console.log(
        jky(),
        jky(),
        replace(),
        jky()
    ) //0, 1, undefined, 0
    使用apply和call的最大好处是可以随便使用任何你想绑定的环境对象.
## 那些日子，舍友们一起掉的头发...

  

 
    
 
        
