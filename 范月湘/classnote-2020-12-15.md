# 今日份小笔记
## Array
    Array对象用于在变量中存储多个值.
![picture1](./ck/ck2.PNG)
### every
    every()方法用于检测数组所有元素是否都符合指定条件(通过函数提供).
    every()方法使用指定函数检测数组中的所有元素:
    1)如果数组中检测到有一个元素不满足，则整个表达式返回false，且剩下的元素不会再进行检测.
    2)如果所有元素都满足条件，则返回true.
    every()不会对空数组进行检测.
    every()不会改变原始数组.
    语法：array.every(function(currentValue,index,arr), thisValue)

    吃栗子：
    let arr=[1,2,3,4,5,6,7];
    let every=arr.every(function(val,index,arr){
        console.log('val:'+val);
        return val>4;
    });
    console.log(every);
![picture](./ck/ck1.PNG)
### find
    find()方法返回通过测试(函数内判断)的数组的第一个元素的值.
    find()方法为数组中的每个元素都调用一次函数执行:
    1)当数组中的元素在测试条件时返回true时，find()返回符合条件的元素，之后的值不会再调用执行函数.
    2)如果没有符合条件的元素返回undefined.
    find()对于空数组，函数是不会执行的.
    find()并没有改变数组的原始值.
    语法：array.find(function(currentValue, index, arr),thisValue)

    吃栗子：
    'use strict'
    let arr=[1,2,3,4,5,6];
    let r=arr.find(item=>item>3);
    console.log(r);//4
    let g=arr.find(item=>item==0);
    console.log(g);//undefined
### findIndex
    findIndex()方法返回传入一个测试条件(函数)符合条件的数组第一个元素位置.
    findIndex()方法为数组中的每个元素都调用一次函数执行:
    1)当数组中的元素在测试条件时返回true时，findIndex()返回符合条件的元素的索引位置，之后的值不会再调用执行函数.
    2)如果没有符合条件的元素返回-1.
    findIndex()对于空数组，函数是不会执行的.
    findIndex()并没有改变数组的原始值.
    语法:array.findIndex(function(currentValue, index, arr), thisValue)

    吃栗子：
    'use strict'
    const nameArr=[
        {id:1,username:"铭🌤",age:16},
        {id:2,username:"曜栎",age:19},
        {id:3,username:"洛杏",age:18},
        {id:4,username:"郴封",age:21}
    ];
    let d=nameArr.findIndex((value)=>value.age==18);
    console.log(d);//满足条件，返回下标2
    let d1=nameArr.findIndex((value)=>value.age==25);
    console.log(d1);//不满足条件，返回下标-1
### forEach
    forEach()方法用于调用数组的每个元素，并将元素传递给回调函数.
    forEach()对于空数组是不会执行回调函数的.
    语法：array.forEach(function(currentValue, index, arr), thisValue)

    吃栗子：
    'use strict'
    let a=[{id:1,age:14},{id:2,age:16},{id:3,age:17}];
    let b=[{id:3,sex:'女'},{id:1,sex:'男'},{id:2,sex:'女'}];
    let c=[];
    a.forEach(function(val) {
        b.forEach(function (val1) {
            if(val.id===val1.id){
                c.push({
                    id:val.id,
                    age:val.age,
                    sex:val1.sex
                })
            }
        })
    });
    console.log(c);
![picture2](./ck/ck3.PNG)
## 闭包
    闭包是指有权访问另一个函数作用域中的变量的函数.
### 如何从外部读取函数内部的变量.Why?
    闭包：f2可以读取f1中的变量，只要把f2作为返回值，就可以在f1外读取f1内部变量.
    原因：f1是f2的父函数，f2被赋给了一个全局变量，f2始终存在内存中，f2的存在依赖f1，因此f1也始终存在内存中，不会在调用结束后，被垃圾回收机制回收.
### js链式作用域
    js链式作用域：子对象会一级一级向上寻找所有父对象的变量，反之不行.
    js中函数内部可以读取全局变量，函数外部不能读取函数内部的局部变量.
### 函数作为返回值
    吃栗子：
    'use strict'
    function f(){
        let s=10;
        return function(){
            console.log(s);
        }
    }
    let fn=f();
    let s=20;
    fn();//10
    调用fn函数，输出s的值，fn并没有定义s，所以会向上找s，在fn函数的作用域中，有s值为10，输出10，不会输出20.
    全局作用域的s和f函数作用域的s不相同.体现闭包的一个好处：不会造成全局变量污染.
### 闭包
    闭包经典例子：
    'use strict'
    function fn(){
        let num=4;
        return function(){
            let k=0;
            console.log(++k);
            console.log(++num);
        }
    }
    let fn1=fn();
    fn1();
    fn1();
![picture3](./ck/ck4.PNG)

    吃定时器与闭包栗子：
    'use strict'
    for(var i=0;i<5;++i){
        setTimeout(function(){
            console.log(i+' ');
        },100);
    }//5次5
    如果将var替换成let，就会按顺序输出下标0~4.
### 闭包的好处与坏处
    好处：
    ①保护函数内的变量安全 ，实现封装，防止变量流入其他环境发生命名冲突
    ②在内存中维持一个变量，可以做缓存（但使用多了同时也是一项缺点，消耗内存）
    ③匿名自执行函数可以减少内存消耗
    坏处：
    ①其中一点上面已经有体现了，就是被引用的私有变量不能被销毁，增大了内存消耗，造成内存泄漏，解决方法是可以在使用完变量后手动为它赋值为null；
    ②其次由于闭包涉及跨域访问，所以会导致性能损失，我们可以通过把跨作用域变量存储在局部变量中，然后直接访问局部变量，来减轻对执行速度的影响