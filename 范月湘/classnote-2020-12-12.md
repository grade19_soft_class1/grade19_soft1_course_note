# 今日份小笔记
## reduce
    reduce()方法接收一个函数作为累加器，数组中的每个值(从左到右)开始缩减，最终计算为一个值.
    reduce()可以作为一个高阶函数，用于函数的compose.
    reduce()对于空数组是不会执行回调函数的.

    语法：array.reduce(function(total,currentValue,currentIndex,arr),initialValue)
    total:必需.初始值，或者计算结束后的返回值.
    currentValue:必需.当前元素.
    currentIndex:可选择.当前元素的索引.
    arr:可选择.当前元素所属的数组对象.
    
    练习：利用reduce()求积：
    'use strict';

    function product(arr) {
        function fn(x,y){
            return x*y;
        }
        return arr.reduce(fn);
    }

    // 测试:
    if (product([1, 2, 3, 4]) === 24 && product([0, 1, 2]) === 0 && product([99, 88, 77, 66]) === 44274384) {
        console.log('测试通过!');
    }
    else {
        console.log('测试失败!');
    }//测试通过!

    栗子：
    var arr = [1, 3, 5, 7, 9];
    arr.reduce(function (x, y) {
        return x * 10 + y;
    }); 
    console.log(arr);
![picture](./wf/wf1.png)
        
     
    练习：不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：
    'use strict'
    function string2int(s) {
        var arr=[];
        for(var i=0;i<s.length;++i){
            arr[i]=s[i];
        }
        var newarr=[];
        newarr=arr.map(function(element){
            return +element;
        });
        return newarr.reduce(function(x,y){
            return x*10+y;
        });
    }
    // 测试:
    if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
        if (string2int.toString().indexOf('parseInt') !== -1) {
            console.log('请勿使用parseInt()!');
        } else if (string2int.toString().indexOf('Number') !== -1) {
            console.log('请勿使用Number()!');
        } else {
            console.log('测试通过!');
        }
    }
    else {
        console.log('测试失败!');
    }//测试通过!

 
    练习
    请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。
    'use strict'
    function normalize(arr) {
        function strs(str){
            str=str.toLowerCase();//全部都先小写
            let x1 = str.substring(0, 1).toUpperCase();//提取第一个进行大写
            let x2 = str.substring(1);//提取该元素的后面
            return x1+x2;
        }
        return arr.map(strs);
    }
    // 测试: 
    if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
        console.log('测试通过!');
    }
    else {
        console.log('测试失败!');
    }//测试通过!

    小明希望利用map()把字符串变成整数，他写的代码很简洁： 
    'use strict';
    var arr = ['1', '2', '3'];
    var r;
    r = arr.map(parseInt);
    console.log(r);
    结果竟然是1, NaN, NaN，小明百思不得其解，请帮他找到原因并修正代码。
    修改：
    'use strict';
    var arr = ['1', '2', '3'];
    var r;
    r = arr.map(Number);
    console.log(r);
![picture1](./wf/wf2.png)
## filter
    filter()方法创建一个新的数组，新数组中的元素是通过检查指定数组中符合条件的所有元素.
    filter()不会对空数组进行检测.
    filter()不会改变原始数组.

    语法：array.filter(function(currentValue,index,arr),thisValue)
    currentValue:必需.当前元素的值.
    index:可选择.当前元素的索引值.
    arr:可选择.当前元素属于的数组对象.
    thisValue:可选.对象作为该执行回调时使用，传递给函数，用作"this"的值.
    如果省略了thisValue,"this"的值为"undefined".

    filter筛选出数组里所有偶数:
     'use strict'
    let arr1=[3,6,9,8,2,4,7];
    let arr2=arr1.filter(item=>{
        return item % 2 ==0
    });
    console.log(arr2);
![picture2](./wf/wf3.png)

    filter删掉数组中的空字符串:
     'use strict'
    var u=[undefined,undefined,1,'','false',false,true,null,'null'];
    console.log( u.filter(d=>d));
![picture5](./wf/wf6.png)

    filter实现数组去重:
     'use strict'
    let arr5 = ['11', 'df', 'bb', 'tu', 'bb']
    let arr6 = []
    arr6 = arr5.filter((item, index, self) => {
        return self.indexOf(item) === index
    })
    console.log(arr6);
![picture3](./wf/wf4.png)

    filter过滤掉数组里的小值:
     'use strict'
    let arr7 = [1, 2, 5, 8, 16, 32, 60]
    let arr8 = arr7.filter(item => {
        return item >= 4
    })
    console.log(arr8);
![picture4](./wf/wf5.png)
 
## 回调函数
    练习
    请尝试用filter()筛选出素数：
   'use strict';
    function get_primes(arr) {
        function isPrimeNumber(x) {
            if(x===1) {
                return false;//1不是素数，返回false
            }
            //i是2到x开平方根之间的整数
            //如果x对i取余等于0，则不是素数，返回false
            for(let i=2;i<=Math.sqrt(x,2);i++) {
                if(x%i===0) {
                    return false;
                }
            }
            //如果上述条件都不满足，则是素数，返回true
            return true;
        }
        //用filter()返回新的只含有素数的数组
        return arr.filter(isPrimeNumber);
    }
    // 测试:
    var
        x,
        r,
        arr = [];
    for (x = 1; x < 100; x++) {
        arr.push(x);
    }
    r = get_primes(arr);
    if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
        console.log('测试通过!');
    } else {
        console.log('测试失败: ' + r.toString());
    }//测试通过! 
## sort
    sort() 方法用于对数组的元素进行排序.
    排序顺序可以是字母或数字，并按升序或降序.
    默认排序顺序为按字母升序.
    语法：array.sort(sortfunction)
    sortfunction：可选.规定排序顺序.必须是函数.
    栗子：
    'use strict'
    var points = [40,100,1,5,25,10];
    points.sort(function(a,b){return a-b});
    console.log(points);
![picture6](./wf/wf7.png)

    栗子：
    'use strict'
    var fruits = ["Banana", "Orange", "Apple", "Mango"];
    console.log(fruits.sort());
    console.log(fruits.reverse());
![picture7](./wf/wf8.png)

## 排序算法
![picture8](./wf/wf9.png)