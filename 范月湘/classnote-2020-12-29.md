# 今日份宸宸方张的小笔记
## 考试文件夹
    新建一个命名文件夹，再来俩夹中夹，一个视频，一个源码；
    将运行演示过程用屏幕截图中的屏幕录像机选择全屏无任务栏或全屏模式进行录制；
    保存至视频文件夹，两文件夹压缩成zipped文件夹.
![pic9](./HTML/en.jpg)
## 删除DOM
    删除一个节点，先要获得节点本身以及父节点，调用父节点removeChild删自己.
    HTML:
     <div>
        <ul id=sky>
            <li id="moon">月</li>
            <li id="sun">阳</li>
            <li id="wind">风</li>
            <li id="rain">雨</li>
        </ul>
    </div>
    js:    
    'use strict';
    // 拿到待删除节点:
    var sun = document.getElementById('sun');
    console.log(sun);
    // 拿到父节点:
    var sky = sun.parentElement;
    console.log(sky);
    // 删除:
    var removed = sky.removeChild(sun);
    console.log(removed===sun); // true

    练习：JavaScript、Swift、HTML、ANSI C、CSS、Direct X
    HTML:
    <div id="test-list">
        <li>JavaScript</li>
        <li>Swift</li>
        <li>HTML</li>
        <li>ANSI C</li>
        <li>CSS</li>
        <li>DirectX</li>
    </div>
    把与Web开发技术不相关的节点删掉：
    js:
    'use strict';
    // TODO
    let arr=['JavaScript','HTML','CSS'];
    let parent=document.getElementById('test-list');
    for(let i of parent.children){
        if(!arr.includes(i.innerText)){
            parent.removeChild(i);
        }
    }
    // 测试:
    ;(function () {
        var
            arr, i,
            t = document.getElementById('test-list');
        if (t && t.children && t.children.length === 3) {
            arr = [];
            for (i = 0; i < t.children.length; i ++) {
                arr.push(t.children[i].innerText);
            }
            if (arr.toString() === ['JavaScript', 'HTML', 'CSS'].toString()) {
                console.log('测试通过!');
            }
            else {
                console.log('测试失败: ' + arr.toString());
            }
        }
        else {
            console.log('测试失败!');
        }
    })();//测试通过!
## 操作表单
    js操作表单和操作DOM是类似的，因表单本身也是树.
    吃栗子：
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>js操作form表单</title>
    <script src="./js.js/js.js">
        function test() {
            let ki = document.getElementById('ki');
            let ko = document.ko;
            ki.action = "http://www.baidu.com/s";
        }
    </script>
    </head>

    <body>
        <h3>js操作form表单</h3>
        <input type="button" name="" id="" value="测试form表单" onclick="test()" />
        <hr />
        <form action="#" method="get" id="ki" name="ko">
            <b>用户名：</b><input type="text" name="wr" id="uname" value="" readonly="readonly" /> <br/><br>
            密码：<input type="password" name="pwd" id="pwd" value="" disabled="disabled" /> <br/>
            <br>
            <input type="submit" value="登录">
            <input type="submit" value="注册">
            <br>
            <input type="radio" value=""><br>
            <input type="checkbox" value=""><br>
            <select name="" id=""></select><br>
            <input type="hidden" value="断桥残雪">
        </form>
    </body>
![pic](./HTML/html1.PNG)

### 获取值
    input标签：
    <input type="text" name="username" id="name">
    let input=document.getElementById('email');
    input.value;
   
    HTML：
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>js操作form表单</title>
    <script src="./js.js/js.js"></script>
    </head>

    <body>
        <form action="*" method="POST" name="frm">
            <input type="text" id="time" name="time" value="" >
            <input type="button" name="submit" value="submit" onclick="subs()">
        </form>
    </body>

    js:
    'use strict';
    function subs(){
        let time=frm.time.value;
        if(time==''){
            alert('时间不为空');
            console.log(true);//true
        }else{
            alert(time);
            console.log(false);//false
        }
    }
![pic1](./HTML/html2.PNG)
### 设置值
    <input type="text" name="username" id="name">
    let input=document.getElementById('email');
    input.value='test@example.com';
    对于单选框和复选框，设置checked为true或false即可.
    
### HTML5控件
    <body>
        <form action="demo_form.asp" method="get">
                First name: <input type="text" name="fname" /><br />
                Last name: <input type="text" name="lname" /><br />
                <input type="submit" value="Submit" /><br>
                Email: <input type="text" name="email" /><br />
                Pin: <input type="text" name="pin" /><br>
                <input type="button" value="Click me" onclick="msg()" /><br>
                <input type="checkbox" name="vehicle" value="Bike" /> I have a bike<br />
                <input type="checkbox" name="vehicle" value="Car" /> I have a car<br/>
                Color: <input type="color" name="user_color" /><br>
                Date: <input type="date" name="user_date" /><br>
                E-mail: <input type="email" name="user_email" /><br>
                Select images: <input type="file" name="img" /><br>
                <input type="hidden" name="country" value="Norway" /><br>
                <input type="image" src="en.jpg" alt="Submit" size="50px"/><br>
                Points: <input type="number" name="points" min="1" max="10" /><br>
                <input type="password" name="pwd" /><br>
                <input type="radio" name="sex" value="male" /> Male<br />
                <input type="radio" name="sex" value="female" /> Female<br/>                
                <input type="range" name="points" min="1" max="10" /><br>
                <input type="reset" />
              </form>
              <form action="form_action.asp" method="get">
                    Email: <input type="text" name="email" /><br />
                    <input type="submit" />
                </form>
                Mobile: <input type="tel" name="user_mobile" />
                Homepage: <input type="url" name="user_url" />
    </body>
![pic3](./HTML/html4.png)
### 提交表单
    练习：利用JavaScript检查用户注册信息是否正确，在以下情况不满足时报错并阻止提交表单：
    HTML:
    <body>
    <form id="test-register" action="#" target="_blank" οnsubmit="return checkRegisterForm()">
        <p id="test-error" style="color:red"></p>
        <p>
        用户名: <input type="text" id="username" name="username">
        </p>
        <p>
        口令: <input type="password" id="password" name="password">
        </p>
        <p>
        重复口令: <input type="password" id="password-2">
        </p>
        <p>
        <button type="submit">提交</button> <button type="reset">重置</button>
        </p>
        </form>
    </body>

    js:
    'use strict';
    function checkRegisterForm(){
        var usrname = document.getElementById('username');
        var gz = /^[_0-9a-zA-Z]{3,10}$/;
        var pwd1 = document.getElementById('password');
        var pwd2 = document.getElementById('password-2');
        if(gz.test(usrname.value) == true && (pwd1.value.length > 6 && pwd1.value.length < 20) && (pwd1.value == pwd2.value)){
        return true;
        }else{
        return false;
        }
    }
![pic2](./HTML/html3.PNG)
## 操作文件
    对文件进行的操作要比以上介绍的驱动器（Drive）和文件夹（Folder）操作复杂些，基本上分为以下两个类别：
    1)对文件的创建、拷贝、移动、删除操作
    2)对文件内容的创建、添加、删除和读取操作
### File API
    <input type="file" id="file" onchange="handleFiles(this.files)">
## AJAX
    吃它就对了...(4和200是固定的数字)
    var Ajax={
        get: function(url, fn) {
        // XMLHttpRequest对象用于在后台与服务器交换数据   
        var xhr = new XMLHttpRequest();            
        xhr.open('GET', url, true);
        xhr.onreadystatechange = function() {
            // readyState == 4说明请求已完成
            if (xhr.readyState == 4 && xhr.status == 200 || xhr.status == 304) { 
            // 从服务器获得数据 
            fn.call(this, xhr.responseText);  
            }
        };
        xhr.send();
        },
        // datat应为'a=a1&b=b1'这种字符串格式，在jq里如果data为对象会自动将对象转成这种字符串格式
    post: function (url, data, fn) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        // 添加http头，发送信息至服务器时内容编码类型
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");  
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 304)) {
            fn.call(this, xhr.responseText);
            }
        };
        xhr.send(data);
        }
    }
![pic4](./HTML/html5.png)
## Promise
    'use strict';
    function test(reslove, reject) {
        let timeOut = Math.random() * 2;
        console.log(timeOut);
        setTimeout(() => {
            if (timeOut < 1) {
                console.log('延时小于1，执行reslove方法');
                reslove('success');
            } else {
                console.log('延时大于1，执行reject方法');
                reject('lose');
            }
        }, timeOut * 1000);
    }
    let p=new Promise(test);
    p.then((res)=>{
        console.log(res);
    }).catch((res)=>{
        console.log(res);
    })
![pic5](./HTML/html6.png)

![pic6](./HTML/html7.png)

    'use strict';
    function multiply(x){
        return new Promise(function(reslove,reject){
            console.log('乘法运算开始，1.5s后返(回结果');
            setTimeout(reslove,1500,x*x);
        });
    }
    function add(x){
        return new Promise(function(reslove,reject){
            console.log('加法运算开始，0.6s后返回结果');
            setTimeout(reslove,600,x*x);
        });
    }
    let p1=new Promise(function(reslove,reject){
        console.log('任务开始');
        reslove(123);
    });
    p1.then(multiply)
    .then(add)
    .then(multiply)
    .then(add)
    .then((res)=>{
        console.log('这个结果是:'+res);    
    })
![pic7](./HTML/html8.png)
## Canvas
    <body>
    <canvas id="myCanvas" width="200" height="100" style="border:1px solid #d3d3d3;">
        您的浏览器不支持 HTML5 canvas 标签</canvas>      
        <script>
        var c=document.getElementById("myCanvas");
        var ctx=c.getContext("2d");
        // Create gradient
        var grd=ctx.createLinearGradient(0,0,200,0);
        grd.addColorStop(0,"red");
        grd.addColorStop(1,"white");
        // Fill with gradient
        ctx.fillStyle=grd;
        ctx.fillRect(10,10,150,80);
        </script>
    </body>
![pic8](./HTML/html9.png)
