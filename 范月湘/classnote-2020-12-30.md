# 今日份宸宸迷瞪的小笔记
## jQuery
    jQuery库可以通过一行简单的标记被添加到网页中.
    jQuery是一个JavaScript函数库.
    jQuery是一个轻量级的“写的少，做的多”的JavaSctipt库.
### 使用jQuery
    使用jQuery先添加压缩或未压缩的jQuery至文件夹.
    Download the compressed, production jQuery 3.5.1
    Download the uncompressed, development jQuery 3.5.1
    嚼栗子：
    <script src="./jquery-3.5.1.js"></script>
    <script>
        $(document).ready(function(){
            $("p").click(function(){
                $(this).hide();
            });
        });
    </script>
    <body>
        <p>巴啦啦棱亮，乌鸡麻黑，变没</p>
        <p>当场逝世</p>
        <p>emmmm</p>
    </body>
    <script src="./js/js.js"></script>
### $符号
    首先得加上压缩或者未压缩的jQuery
    <script src="./jquery-3.5.1.js"></script>
    <body>
    <span id="span">漫天雪花不及一片冰心</span>
    <button onclick="getobj()">获取内容</button>
    <script>
        function getobj(){
            let neirong=$('span').text();
            alert(neirong);
        }
    </script>
    </body>
![miao](./cj/cj3.png)
![miao1](./cj/cj4.png)

    在js标签内创建函数，在函数内使用$符号通过id(span)获得span标签对象，再使用text()方法获得span标签之内的内容.
    $符号支持多种对象选择器，如id,class,元素名称等.
## 选择器
    选择器允许你对DOM元素组或单个元素(DOM节点)进行操作.
    jQuery CSS选择器可用于改变HTML元素的CSS属性.
    <script type="text/javascript">
        $(document).ready(function(){
            $("button").click(function(){
                $("p").css("background-color","skyblue");
            });
        });
    </script>
    <body>
    <h2>title</h2>
    <p>paragraph</p>
    <p>ano</p>
    <button type="button">Click me</button>
    </body>
![miao2](./cj/cj5.png)
### 按ID查找
    匹配开头的元素：
    $("div[id^='aaa']") 
    匹配结尾的元素：
    $("div[id$='aaa']") 
    匹配包含的元素：
    $("div[id*='aaa']")
    别忘了加jQuery
    <body>
     <a id="test1" href="javascript:void(0)">test1</a>
     <a id="test2" href="javascript:void(0)">test2</a>
     <a id="test3" href="javascript:void(0)">test3</a>
     <a id="test4" href="javascript:void(0)">test4</a>
     <script type="text/javascript"> 
        for(let i=1;i<=$("a[id^='test']").length;i++){
            $("#test"+i).attr("href","http://cctv"+i+".com");
        }
     </script>
    </body>
    有网站就会进行跳转页面....没得就查无此站
### 按tag查找
    只要写上tag标签名即可.
    var ps = $('p'); // 返回所有<p>节点
    ps.length; // 数一数页面有多少个<p>节点
### 按class查找
    jQuery语法:$("选择器").action
    let divclass=$(".div1");//类选择器
    console.log(divclass);//得到一个类似数组的jQuery对象
    不要省略.在class名前$(".div1")
    所有包含div1的DOM节点都会被返回.
### 按属性查找
    方法一:使用name属性选择器:
    name属性选择器可用于按name属性选择元素.此选择器选择值与指定值完全相等的元素.
    语法:[name=“nameOfElement”]
    <body>
    <center>
        <p>下面的文本框中有<b>name属性'address'.</b>
            <form>
                <textarea name="address" id="" cols="40" rows="4"></textarea>
            </form>
        </p>
        <p>下面的文本框中有<b>name属性'area'.</b>
            <form>
                <input type="text" name="area">
            </form>
        </p>
        <p>单击按钮，隐藏name为"area"的input输入框</p>
        <button onclick="selectByName()">隐藏</button>
        <script type="text/javascript">
            function selectByName() {
                element = $('[name="area"]');
                element.hide();
            }
        </script>
    </center>
    </body>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
![miao3](./cj/cj6.png)
![miao4](./cj/cj7.png)

    方法二:使用javascript按名称获取元素并将其传递给jQuery
    可以使用javascript getElementsByName()方法选择所需的元素，将其传递给jQuery函数，进一步用作jQuery对象
    语法：selector = document.getElementsByName(‘nameOfElement’);element = $(selector);
    <center>
         <p>下面的文本框中有<b>name属性'address'.</b>
            <form>
                <textarea name="address" id="" cols="40" rows="4"></textarea>
            </form>
        </p>
        <p>下面的文本框中有<b>name属性'area'.</b>
            <form>
                <input type="text" name="area">
            </form>
        </p>
        <p>单击按钮，隐藏name为"address"的文本框</p>
        <button onclick="selectByName()">隐藏</button>
        <script type="text/javascript">
            function selectByName(){
                selector=document.getElementsByName('address');
                element=$(selector);
                element.hide();
            }
        </script>
     </center>
![miao5](./cj/cj8.png)
![miao6](./cj/cj9.png)

    练习:
    仅选择JavaScript
    仅选择Erlang
    选择JavaScript和Erlang
    选择所有编程语言
    选择名字input
    选择邮件和名字input
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="./js.js"></script>
    <body>
        <!-- HTML结构 -->
        <div id="test-jquery">
            <p id="para-1" class="color-red">JavaScript</p>
            <p id="para-2" class="color-green">Haskell</p>
            <p class="color-red color-green">Erlang</p>
            <p name="name" class="color-black">Python</p>
            <form class="test-form" target="_blank" action="#0" onsubmit="return false;">
                <legend>注册新用户</legend>
                <fieldset>
                    <p><label>名字: <input name="name"></label></p>
                    <p><label>邮件: <input name="email"></label></p>
                    <p><label>口令: <input name="password" type="password"></label></p>
                    <p><button type="submit">注册</button></p>
                </fieldset>
            </form>
        </div>
    </body>
    'use strict'
    var selected=null;
    //选择JavaScript
    selected = $('#para-1');
    //选择Erlang
    selected=$('.color-red.color-green');
    //选择JavaScript和Erlang
    selected=$('.color-red');
    //选择所有编程语言(属性选择器)
    selected=$("[class^='color-']")
    //选择名字input
    selected=$("input[name='name']")
    //选择邮件和名字input
    selected=$("input[name='email'],input[name='name']");
    function jQu(){
        if(!(selected instanceof jQuery)){
        return console.log('不是有效的Jquery对象');   
    }
    $('#test-jquery').find('*').css('background-color','');
    selected.css('background-color','#ffd351');
    }
![miao7](./cj/cj10.png)
## 层级选择器
    层级选择器就是通过元素之间的层次关系来选择元素的一种基础选择器.
    层次选择器在实际开发中也是相当重要的.常见的层次关系包括：父子、后代、兄弟、相邻.
### 层级选择器
    后代选择器，用于选择元素内部的所有某一种元素，包括子元素和其他后代元素.
    语法：$("M N")
    “M元素”和“N元素”之间用空格隔开，表示选中 M 元素内部的后代 N 元素（即所有 N 元素）.

    子代选择器，用于选中元素内部的某一种子元素.子代选择器与后代选择器虽然很相似，但是也有着明显的区别.
    后代选择器，选取的是元素内部所有的元素（包括子元素、孙元素等）;
    子代选择器，选取的是元素内部的某一种子元素（只限子元素）.
    语法：$("M>N")
    “M 元素”和“N 元素”之间使用>选择符，表示选中 M 元素内部的子元素 N.

    兄弟选择器，用于选中元素后面（不包括前面）的某一类兄弟元素.
    语法：$("M~N")
    “M 元素”和“N 元素”之间使用~选择符，表示选中 M 元素后面所有的兄弟元素 N.

    相邻选择器，用于选中元素后面（不包括前面）的某一个“相邻”的兄弟元素.
    相邻选择器与兄弟选择器也非常相似，不过也有明显的区别.
    兄弟选择器选取元素后面“所有”的某一类元素;
    相邻选择器选取元素后面“相邻”的某一个元素.
    语法：$("M+N")
    “M 元素”和“N 元素”之间使用+选择符，表示选中 M 元素后面的相邻的兄弟元素 N.
### 子选择器
    :nth-child(n) 选择器选取属于其父元素的不限类型的第 n 个子元素的所有元素.
    语法::nth-child(n|even|odd|formula)
    参数	描述
    n	    要匹配的每个子元素的索引.必须是一个数字.第一个元素的索引号是 1.
    even	选取每个偶数子元素.
    odd	    选取每个奇数子元素.
    formula	规定哪个子元素需通过公式 (an + b) 来选取。
    实例：p:nth-child(3n+2) 选取每个第三段，从第二个子元素开始.

    选取属于其父元素的第三个子元素的每个 <p> 元素：
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script>
        $(document).ready(function(){
            $("p:nth-child(3)").css("background-color","skyblue");
        });
    </script>
    
    <body>
        <h1>body title</h1>
        <p>body first para</p>
        <p>body sec para(body第三个子元素)</p>
        <div style="border: 1px soild;">
            <span>div一个span元素</span>
            <p>div first para</p>
            <p>div sec para(div第三个子元素)</p>
            <p>div last para</p>
        </div><br>
        <div style="border: 1px soild;">
            <p>another para</p>
            <p>another sec para</p>
            <p>another last para</p>
        </div>
        <p>body last para</p>
    </body>
![miao8](./cj/cj11.png)
### 过滤器(Filter)
    筛选元素集合中匹配表达式或通过传递函数测试的那些元素集合.
    .filter( selector )
    selector
    类型: Selector                
    一个用于匹配元素的选择器字符串.

    .filter( function(index) )
    function(index)
    类型: Function()
    一个函数用作测试集合中的每个元素.this是当前DOM元素.

    .filter( element )
    element
    类型: Element                
    一个或多个DOM元素来匹配当前元素集合.

    .filter( jQuery object )        
    jQuery object
    类型: Object                
    现有jQuery对象，用于进一步筛选当前元素集合.

    如果一个jQuery对象表示一个DOM元素的集合，.filter()方法构造了一个新的jQuery对象的子集.
    所提供的选择器是对每个元素进行测试;这个选择器匹配的所有元素将包含在结果中.

    吃栗子：
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <style>
    div{
        width: 60px; height: 60px; margin: 5px; float: left; border: 2px white solid; 
    }
    </style>

    <body>
        <div></div>
        <div class="middle"></div>
        <div class="middle"></div>
        <div class="middle"></div>
        <div class="middle"></div>
        <div></div>
        <script>
        $("div").css("background","#c8ebcc")
        .filter(".middle")
        .css("border-color","red");
        </script>
    </body>
![miao9](./cj/cj12.png)
### 表单相关
    嚼栗子:
    <title>jQueryStudy</title>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <style type="text/css">
        .int{ height: 30px; text-align: left; width: 600px; }
        label{ width: 200px; margin-left: 20px; }
        .high{ color: red; }
        .msg{ font-size: 13px; }
        .onError{ color: red; }
        .onSuccess{ color: green; }
    </style>

    <body>
        <form method="post" action="">
                <div class="int">
                 <label for="name">名称：</label>
                 <input type="text" id="name" class="required" />
                </div>
                <div class="int">
                 <label for="email">邮箱：</label>
                 <input type="text" id="email" class="required" />
                </div>
                <div class="int">
                 <label for="address">地址：</label>
                 <input type="text" id="personinfo" />
                </div>
                <div class="int">
                 <input type="submit" value="提交" id="send" style="margin-left: 70px;" />
                 <input type="reset" value="重置" id="res" />
                </div>
        </form>
        <script type="text/javascript">
            //为表单的必填文本框添加提示信息（选择form中的所有后代input元素）
            $("form :input.required").each(function () {
            //创建元素
            var $required = $("<strong class='high'>*</strong>");
            //将它追加到文档中
            $(this).parent().append($required);
            });
        </script>
    </body>
![miao10](./cj/cj13.png)
 