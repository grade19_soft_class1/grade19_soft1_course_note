# 今日份小笔记
## 一、git如何避免冲突
    1.将仓库先克隆到本地
    2.添加git add .
    3.进入文件夹cd 
    4.命名git commit -m ""
    5.提交git push
    or
    在本地克隆，只有一个默认分支master，不要直接上手
    1.建立一个自己的分支，取名为working,  git branch working
    2.切换到这个新分支 git checkout working 
    3.至此便可自由修改代码并保存
    提交代码步骤：
    1.git checkout working  --force 确保使用的是工作分支
    2.git add .
    3.git commit -m "$1" -a 提交代码到本地，工作分支增加一个版本，这里的$1是运行脚本的第一个参数  
    4.git checkout master
    5.git pull origin master 切换回默认分支，并将默认分支和中央最新版本合并
    6.git merge working 在本地合并这次修改到默认分支
    7.git push origin master 提交到中央版本库，要切换回工作分支
    8.git checkout working --force
    如果不小心动了生产环境（就是只从中央版本库pull到本地）的文件，只好将本地版本退回一个，再从中央代码库pull代码合并。
## 二、如何完成随堂笔记emmm
    1.fork班级仓库
    2.克隆仓库到本地
    3.使用vsCode打开README.md
    4.新建一个文件夹并赋予名称
    5.#后要空一格 
    6.插图：！[当图片无法显示，展示的文本内容](./imgs/imgs001.png)
    7.完成编辑后提交
## 三、lambda表达式
    lambda表达式的本质就是方法(匿名方法).
    具体的语法形式如下:
    访问修饰符  修饰符  返回值类型  方法名(参数列表)    => 表达式;
    public     static     int     Add(int a, int b) =>  a + b;
    ```
    Func <int,int,int> a1 = (a,b) => a+b;
    ```
## 四、委托
    一般lambda表达式用于创建委托或表达式树类型。
    delegate int del(int i);
    del myDelegate = x => x*x;  //这里的x代表的就是int类型的参数，c#会自动对其锁定类型。
    ```
    delegate void Say();
    function void SayHello(){

    }
    Say s=SayHello;
    s();
    ```
## 五、内置委托
    1.Func<>
    Func类的委托最少可以传入输入泛型参数(in，逆变) 1个，最多可以传入输入泛型参数(in，逆变) 16个，传入的输出泛型参数(out，协变)有且只有一个，这个类型是此委托封装的方法的返回值类型。
    2.Action<>
    Action类的委托最少可以传入0个参数，最多可以传入16个参数，参数类型皆为逆变，并且不返回值。
    3.Predicate<>
    Predicate<T>委托封装一个方法，该方法传入一个类型参数，这个参数是指要比较的对象的类型，此类型参数是逆变，同时接收一个参数(该参数就是要按照由此委托表示的方法中定义的条件进行比较的对象，参数的类型就是传入的类型参数的类型)，该方法始终返回bool类型的值。如果该对象符合由此委托表示的方法中定义的条件，则为 true；否则为 false。
    4.Comparison<>
    Comparison<T>委托封装一个方法，该方法传入一个类型参数，这个参数是指要比较的对象的类型，此类型参数是逆变，同时接收两个同类型的参数(这两个参数就是要比较的两个对象，参数的类型就是传入的类型参数的类型)，始终返回int类型的值，即一个有符号整数，指示 x 与 y 的相对值，如下表所示。
            值	    含义
            小于0	x 小于y 
            0	    x 等于y 
            大于0	x 大于y 
## 六、Linq及Linq用法、优势
    1.Linq:（Language Integrated Query）即语言集成查询。
    2.Linq两种常用用法：where查询、order排序
    3.Linq优势:
        1.熟悉的语言：开发人员不必为每种类型的数据源或数据格式学习新的语言。
        2.更少的编码：相比较传统的方式，LINQ减少了要编写的代码量。
        3.可读性强：LINQ增加了代码的可读性，因此其他开发人员可以很轻松地理解和维护。
        4.标准化的查询方式：可以使用相同的LINQ语法查询多个数据源。
        5.类型检查：程序会在编译的时候提供类型检查。
        6.智能感知提示：LINQ为通用集合提供智能感知提示。
        7.整形数据：LINQ可以检索不同形状的数据。

 