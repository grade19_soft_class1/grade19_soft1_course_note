# 土猪想跟土狗玩的一天
## 神奇的题解方法又多了几种
    学委的操作：
     var arr=['小明','小红','大军','阿黄'];
    var sum='';
    for(var i=0;i<arr.length;i++){
        if(i==arr.length-1){
            sum+=arr[i]+'同学';
        }else if(i==arr.length-2){
            sum+=arr[i]+'和';
        }else{
            sum+=arr[i]+',';
        }
    }
    console.log(sum);
    江某人的操作：
     var arr=['小明','小红','大军','阿黄'];
    if(arr.length<=1){
        console.log('欢迎'+arr[arr.length-1]+'同学');
    }else{
        console.log('欢迎'+arr.slice(0,arr.length-1)+'同学'+'和'+arr[arr.length-1]+'同学');
    }
    console.log(arr.slice(0,arr.length-1));
    讲道理的陈某人的操作：
    var arr=['小明','小红','大军','阿黄'];
    if(arr.length==1){
        console.log('欢迎'+arr[arr,length-1]+'同学');
    }else{
        var sum='欢迎';
        for(var i=0;i<arr.length-1;i++){
            if(i<arr.length-2){
                sum+=arr[i]+',';
            }
            if(i==arr.length-2){
                sum+=arr[i];
            }
        }
        sum+='和'+arr[arr.length-1]+'同学';
    }
    console.log(sum);
## 循环一笔带过
### for循环
    for的仨表达式：声明循环变量;判断循环条件;更新循环变量
    for循环的执行特点:先判断再执行，与while相同
    栗子：
    for(var mem=1;mem<10;mem++){
    console.log(mem);
    }//输出1~9
### while循环
    while循环()的表达式，运算结果可以是各种类型，最终都会转为真假.
    boolean:true为真，false为假.
    string:空字符串为假，所有非空字符串为真.
    number:0为假,一切非0数字为真.
    null/undefined/nan:全为假.
    object:全为真.
    栗子：
    var cue=4;
    while(cue<10){
    console.log(cue);
    cue++;
    }//输出4~9
### do-while循环
    do-while循环特点：先执行后判断，即使初始条件不成立,do-while循环至少执行一次，emm就是do-while循环比while循环多执行一次.
    栗子：
    var mem=10;
    do{
    console.log(mem);//倒着输出0~10
    mem--;
    }while(mem>=0);
    console.log(mem);//输出-1
### 循环的嵌套
    外层循环控制行
    内层循环控制列
    循环的嵌套:解决多行多列的结构
    栗子：
    for(var i=1;i<9;i++){
    for(var j=0;j<i+1;j++){
        console.log("*"); 
    }
    console.log("<br>");
    }
![图片](./sunshine/sunshine1.PNG)
### 遍历方法
    for-in语句用于对数组或者对象的属性进行循环操作.
    for-in循环的代码每执行一次，就会对数组或者对象的属性进行一次操作.
    栗子：
    var cj={'name':'宸杰','age':19,'height':'179'};
    for(var q in cj){
    console.log(q,cj[q]);
    }

    for-of循环可以使用的范围包括数组、Set和Map结构、某些类似数组的对象(arguments、DOM NodeList)、Generator以及字符串.
    JavaScript原有的for-in循环，只能获得对象的键名，不能直接获取键值.ES6提供for-of循环，允许遍历获得键值.
### 循环控制语句
    break:跳出本层循环，继续执行循环后面的语句.
    如果有多层循环，那么只能跳出一层.
    continue:跳过本次循环的剩余代码，继续执行下一次代码.
    break栗子：
    for(var i=0;i<10;i++){
    if(i==6){
        break;
    }
    console.log(i);//输出0~5
    }
    continue栗子：
    for(var i=0;i<10;i++){
    if(i==6){
        continue;
    }
    console.log(i);//输出0~9
    }
## Map
    map() 方法返回一个新数组，数组中的元素为原始数组元素调用函数处理后的值。
    map() 方法按照原始数组元素顺序依次处理元素。
    map()不会对空数组进行检测.
    map()不会改变原始数组.
    map语法：array.map(function(currentValue,index,arr), thisValue)
    栗子：
    <p>获取数组元素的平方根。</p>
    <button onclick="myFunction()">点这儿</button>
    <p id="demo"></p>
    <script>
    var numbers = [4, 9, 16, 25];
    function myFunction() {
        x = document.getElementById("demo")
        x.innerHTML = numbers.map(Math.sqrt);
    }
    </script>
![图片](./sunshine/sunshine2.PNG)