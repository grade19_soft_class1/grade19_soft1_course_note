# 今日份头脑💫的小笔记
## Array数组
    1.数组对象用来在单独的变量名中存储一系列的值.
    var MCure=new Array();
    2.向数组赋值可以任意添加多的值，亦或是定义整数自变量控制数组容量.
    栗子：                  or 栗子：
    var ck=new Array();             var ck=new Array(3);
    ck[0]="mike";                   ck[0]="mike";
    ck[1]="jane";                   ck[1]="jane";
    ck[2]="polo";                   ck[2]="polo";
    console.log(ck);                console.log(ck);
### 获取数组长度(length)
    length属性可读可写，是一个动态属性.length属性值也会随数组元素的变化而自动更新.
    同时，如果重置length属性值，也将影响数组的元素.

    1)如果length属性被设置了一个比当前length值小的值，
    则数组会被截断，新长度之外的元素值都会丢失.

    2)如果length属性被设置了一个比当前length值大的值，那么空数组就会被添加到数组末尾，
    使得数组增长到新指定的长度，读取值都为undefined.
## indexOf
    定义：indexOf()方法可返回某个指定的字符串值在字符串中首次出现的位置.
    如果没有找到匹配的字符串则返回-1.

    语法：string.indexOf(searchvalue,start)

    参数值:searchvalue:必需，规定需检索的字符串值.
    start:可选的整数参数.规定在字符串中开始检索的位置.
    它的合法取值是0到string object.length-1.
    如省略该参数，则将从字符串的首字符开始检索.
## slice和splice
    slice()定义：从已有的数组中返回你选择的某段数组元素.
    slice()语法:arrayObject.slice(start,end)
    栗子：
    取值范围是0-2(start-end):
    var fei=["mountain","river","tree","jungo"];
    console.log(fei.slice(0,2)); 

    只有start,会选择start开始到end之间的所有数组元素：
    var fei=["mountain","river","tree","jungo"];
    console.log(fei.slice(1)); 

    start是负数，从数组尾部开始算起，只有start无end且start为负数，只能获取到最后一个数组元素:
    var fei=["mountain","river","tree","jungo"];
    console.log(fei.slice(-1)); 

    start和end都是-1，结果为空
    var fei=["mountain","river","tree","jungo"];
    console.log(fei.slice(-1,-1)); 


    splice()定义：从数组中添加或删除元素，然后返回被删除的数组元素.
    splice()语法:arrayObject.splice(index,howmany,item1,...,itemX)
    栗子：
    从第三个元素开始且不删除元素，并在第三个元素前新增一个元素
    var silk=["der","mie","miao","aowu"];
    silk.splice(2,0,'hey'); 
    console.log(silk);

    从第三个元素开始且删除一个元素，并在原来第三个元素的位置新增一个元素
    var silk=["der","mie","miao","aowu"];
    silk.splice(2,1,'hey'); 
    console.log(silk);

    从最后一个元素开始并删除最后一个元素，在删除末尾元素后新增一个元素
    var silk=["der","mie","miao","aowu"];
    silk.splice(-1,1,'hey'); 
    console.log(silk);

    从最后一个元素开始且不删除元素，在末尾元素前新增一个元素
    var silk=["der","mie","miao","aowu"];
    silk.splice(-1,0,'hey'); 
    console.log(silk);

## push和pop
    push()定义：该方法用于向数组末尾添加一个或多个元素，并返回新的长度.
    push()语法:arrayObject.push(newelement1,newelement2,...,newelementX)
    栗子：
    var i=[1,4,5,6,2,8];
    i.push(42,67);
    console.log(i);
    
    pop()定义:该方法用于删除数组的末尾元素，并返回被删除的元素.
    pop()语法:arrayObject.pop()
    栗子：
    var i=[1,4,5,6,2,8];
    i.pop(42,67);
    console.log(i);

## unshift和shift
    unshift()定义:该方法用于向数组的开头添加一个或多个元素，并返回新的长度.
    unshift()语法:arrayObject.unshift(element1,element2,...,elementX)
    栗子：
    var i=[1,4,5,6,2,8];
    i.unshift(42,67);
    console.log(i);

    shift()定义：该方法用于删除数组的首位元素，并返回被删除的元素.
    shift()语法:arrayObject.shift()
    栗子：
    var i=[1,4,5,6,2,8];
    i.shift(42,67);
    console.log(i);

## sort
    sort()方法用于对数组的元素进行排序.
    sort语法：arrayObject.sort(sortby)
    var i=[1,4,5,6,2,8];
    i.sort();
    console.log(i);
    此可用于较大数的排列:
    var i=[1,4,5,6,2,14,25];
    i.sort();
    console.log(i.sort((a, b) => a - b));

## reverse
    reverse()方法用于颠倒数组中元素的顺序.
    reverse()语法：arrayObject.reverse()
    栗子：
    <script type="text/javascript"> 
        var lk=new Array(3);
        lk[0]="Mi";
        lk[1]="Si";
        lk[2]="Xi";
        document.write(lk+"<br/>")
        document.write(lk.reverse())
    </script>

## concat
    concat()方法用于连接两个或多个数组.
    该方法不会改变现有数组，仅会返回被连接数组的一个副本.
    concat()语法:arrayObject.concat(arrayX,arrayX,...,arrayX)
    栗子：
     <script type="text/javascript">
         var w=[1,2,3];
         document.write(w.concat(4,5));
    </script>

## join
    join()方法用于把数组中的所有元素放入一个字符串.
    元素是通过指定的分隔符进行分隔的.
    join语法：arrayObject.join(separator)
    <script type="text/javascript">
          var k=new Array(3);
          k[0]="joy";
          k[1]="fun";
          k[2]="cue";
          document.write(k.join("."));
    </script>

## javascript练习
![练习](./pho/pho1.png)
![练习](./pho/pho2.png)
![练习](./pho/pho3.png)
![练习](./pho/pho4.png)

