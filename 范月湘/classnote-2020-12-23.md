# 今日份宸宸犯困的笔记
## 创建对象
### 工厂模式
    工厂模式很直观，将创建对象的过程抽象为一个函数，用函数封装以特定接口创建对象的细节.
    吃栗子：
    'use strict';
    function createTree(name,age,height){
        let tree=new Object();
        tree.name= name;
        tree.age=age;
        tree.height=height;
        tree.others=function(){
            console.log(this,null,' ');
        }
        return tree;
    }
    let ak=createTree("荞",76,18);
    console.log(ak);
![pic](./cj/cj1.PNG)

    通俗地讲，工厂模式就是将创建对象的语句放在一个函数里，通过传入参数来创建特定对象，最后返回创建的对象.

###  构造函数模式
    构造函数模式是java语言创建对象的通用模式.
    在js中，不必显式声明某个类，直接创建构造函数即可，类的方法和属性在构造函数中(或原型对象上)处理.
    吃栗子：
    'use strict';
    function Flowers(name,age,category){
        this.name=name;
        this.age=age;
        this.category=category;
        this.sunshine=function(){
            console.log(this.name);
        }
    }
    let kq=new Flowers('曼珠沙华',1643,"石蒜科");
    console.log(kq);
![pic1](./cj/cj2.PNG)

    较之于工厂模式，构造函数创建对象有几点不同：
    1)没有显示地创建对象
    2)直接将属性和方法赋给this对象
    3)没有return语句

    创建Student实例，必须要使用new操作符，创建的实例对象将有一个constructor(构造器)属性，指向Person构造函数.
    调用构造函数创建对象经历以下过程:
    1)创建一个新对象
    2)将构造函数的作用域赋给新对象(so this就指向了这个新对象)
    3)执行构造函数中的代码
    4)返回新对象(不需要显式返回)

    构造函数的主要缺点：每个方法都要在每个实例上创建一遍.

### 原型模式
    js中，每个函数都有一个prototype属性，它是一个指针，指向一个对象，叫原型对象.
    原型对象包含了可以由特定类型的所有实例对象共享的属性和方法.
    原型对象有一个与生俱来的属性constructor，指向创建对象的构造方法.
    使用原型模式可以让所有的实例共享原型对象中的属性和方法，就是说不必在构造函数中定义对象实例的信息.
    吃栗子：
    'use strict';
    function Animal__ao(){
        Animal__ao.prototype.name='Tiger';
        Animal__ao.prototype.sex='male';
        Animal__ao.prototype.age=4;
        Animal__ao.prototype.sound=function(){
            console.log(this.name);
        }
    }
    let yp=new Animal__ao();
    yp.sound();//Tiger
    let rw=new Animal__ao();
    rw.sound();//Tiger

    Object.getPrototypeOf(object)方法返回参数对象的原型对象.
    Object.keys(object)方法返回对象上课枚举的实例属性.

    组合使用构造函数和原型模式：
    构造函数模式用于定义实例属性，原型模式则用于定义方法和共享的属性.
    这种混合模式不仅支持向构造函数传入参数，还最大限度地节约了内存，就很奈斯！
    吃栗子：
    'use strict';
    function Animal__ao(){
        Animal__ao.prototype.name='Tiger';
        Animal__ao.prototype.sex='male';
        Animal__ao.prototype.age=4;
    
    }
    Animal__ao.prototype.sound=function(){
            console.log(this.name);
    }
    Animal__ao.prototype.slight='Why do you think so?';
    let yp=new Animal__ao();
    yp.sound();//Tiger
    let rw=new Animal__ao();
    rw.sound();//Tiger
    let tq=new Animal__ao();
    tq.sound();//Tiger

    实现原型继承链:
    inherits(PrimaryStudent, Student);

    绑定其他方法到PrimaryStudent原型:
    PrimaryStudent.prototype.getGrade = function () {
        return this.grade;
    };
### 其它模式
    动态原型模式：仅在第一次调用构造函数时，将方法赋给原型对象的相应属性，其他示例的处理方式同构造函数模式

    寄生构造函数模式：仅仅封装创建对象的代码，然后再返回新创建的对象，仍使用new操作符调用

    稳妥构造函数模式：没有公共属性，只有私有变量和方法，以及一些get/set方法，用以处理私有变量
### 忘记写new咋办
    调用忘记写new的话，在strict模式下，this.name=name将报错，this绑定为undefined;
    在非strict模式下，this.name=name不报错，this绑定为window,
    无意间创建了全局变量name，并返回undefined，oh...It's too terrible!

    一些语法检查工具如jslint将可以帮你检测到漏写的new.

    吃栗子：
    'use strict';
    function Student(props){
        this.name=props.name || '匿名';
        this.grade=props.grade || 1;
    }
    Student.prototype.hello=function(){
        alert('hello,'+this.name+'!');
    }
    function createStudent(props){
        return new Student(props || {});
    }
    let millar=createStudent({
        name:'米勒',
    });
    console.log(millar.grade);//1   
## class继承
    extends关键字用于类声明或者类表达式中，以创建一个类，该类是另一个类的子类.
    语法：class ChildClass extends ParentClass { ... }
    extends关键字用来创建一个普通类或者内建对象的子类.
    继承的.prototype必须是一个Object或者null.

    练习:请利用构造函数定义Cat，并让所有的Cat对象有一个name属性，并共享一个方法say()，返回字符串'Hello, xxx!'：
    'use strict';
    function Cat(name) {
        this.name=name;
    }
    Cat.prototype.say = function () {
        return ('Hello, ' + this.name + '!');
    }

    // 测试:
    var kitty = new Cat('Kitty');
    var doraemon = new Cat('哆啦A梦');
    if (kitty && kitty.name === 'Kitty'
        && kitty.say
        && typeof kitty.say === 'function'
        && kitty.say() === 'Hello, Kitty!'
        && kitty.say === doraemon.say
    ) {
        console.log('测试通过!');
    } else {
        console.log('测试失败!');
    }//测试通过!
 