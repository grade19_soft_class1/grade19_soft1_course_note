# 宸宸通宵了，好困.的笔记
## 表单相关
    针对表单元素，jQuery还有一组特殊的选择器：
    :input：可以选择<input>，<textarea>，<select>和<button>;
    :file：可以选择<input type="file">，和input[type=file]一样;
    :checkbox：可以选择复选框，和input[type=checkbox]一样;
    :radio：可以选择单选框，和input[type=radio]一样;
    :focus：可以选择当前输入焦点的元素，例如把光标放到一个<input>上，用$('input:focus')就可以选出;
    :checked：选择当前勾上的单选框和复选框，用这个选择器可以立刻获得用户选择的项目，如$('input[type=radio]:checked');
    :enabled：可以选择可以正常输入的<input>、<select> 等，也就是没有灰掉的输入
    :disabled：和:enabled正好相反，选择那些不能输入的
    此外，jQuery还有很多有用的选择器，例如，选出可见的或隐藏的元素：
    $('div:visible'); // 所有可见的div
    $('div:hidden'); // 所有隐藏的div

    练习:
    HTML:
    <body>
        <!-- HTML结构 -->
        <div class="test-selector">
            <ul class="test-lang">
                <li class="lang-javascript">JavaScript</li>
                <li class="lang-python">Python</li>
                <li class="lang-lua">Lua</li>
            </ul>
            <ol class="test-lang">
                <li class="lang-swift">Swift</li>
                <li class="lang-java">Java</li>
                <li class="lang-c">C</li>
            </ol>
        </div>
    </body>
    'use strict';
    var selected = null;
    // 分别选择所有语言，所有动态语言，所有静态语言，JavaScript，Lua，C等:
    selected = $('div.test-selector li');//所有语言
    selected = $('ul.test-lang li');//动态语言
    selected = $('ol.test-lang li');//静态语言
    selected = $('ul.test-lang li.lang-javascript');//JavaScript
    selected = $('ul.test-lang li:last-child');//Lua
    selected = $('ol.test-lang li:last-child');//c
    // 高亮结果:
    function jq() {
        if (!(selected instanceof jQuery)) {
            return console.log('不是有效的jQuery对象!');
        }
        $('#test-jquery').find('*').css('background-color', '');
        selected.css('background-color', '#ffd351');
    }
![pic](./ck/ck5.PNG)
## 查找和过滤
    'use strict';
    //用find()查找：
    var ul = $('ul.lang'); // 获得<ul>
    var dy = ul.find('.dy'); // 获得JavaScript, Python, Scheme
    var swf = ul.find('#swift'); // 获得Swift
    var hsk = ul.find('[name=haskell]'); // 获得Haskell
    //如果要从当前节点开始向上查找，使用parent()方法：
    var swf=$('swift');//获得Swift
    var parent=swf.parent();//获得Swift的上层节点<ul>
    var a = swf.parent('.red'); // 获得Swift的上层节点<ul>，同时传入过滤
    //对于位于同一层级的节点，可以通过next()和prev()方法
    var swift=$('#swift');
    swift.next();//Scheme
    swift.next('[name=haskell]');//Haskell,因为Haskell是后续第一个符合选择器条件的节点
    swift.prev();//Python
    swift.prev('.js');//JavaScript,因为JavaScript是往前第一个符合选择器条件的节点
![pic1](./ck/ck6.PNG)
### 过滤
    filter() 方法返回符合一定条件的元素.
    该方法让您规定一个条件.不符合条件的元素将从选择中移除，符合条件的元素将被返回.
    该方法通常用于缩小在被选元素组合中搜索元素的范围.
    语法：$(selector).filter(criteria,function(index))
    filter()方法可以过滤掉不符合选择器条件的节点：
    <script>
        $(document).ready(function(){
            $("p").filter(".int").css("background-color","skyblue");
        });
    </script>
    <body>
        <h1>莫问归期</h1>
        <p>零落成泥碾作尘</p>
        <p class="int">青空</p>
        <p class="int">洛璃</p>
        <p>遥知不是雪，为有暗香来</p>
    </body>
![pic2](./ck/ck7.PNG)

    $.map()函数用于使用指定函数处理数组中的每个元素(或对象的每个属性)，并将处理结果封装为新的数组返回.
    语法：$.map( object, callback )
    map()方法把一个jQuery对象包含的若干DOM节点转化为其他对象：
    <style>
        div{color: aqua;}
        p{color: coral; margin: 0px;}
        span{color: cornsilk;}
    </style>
    <body>
        <div></div>
        <p></p>
        <span></span>
        <script>
            $(function(){
                var arr=["🐕","🐏","🐖","🦌","🐱"];
                $("div").text(arr.join(","));
                arr=$.map(arr,function(x,k) {
                    return(x.toUpperCase()+k);
                });
                $("P").text(arr.join(","));
                arr=$.map(arr,function(r) {
                    return r+r;
                });
                $("span").text(arr.join(","));
            })
        </script>
    </body>
![pic3](./ck/ck8.PNG)
### ⭐练习
    <body>
        <form id="test-form" action="#0" onsubmit="return false;">
            <p><label>Name: <input name="name"></label></p>
            <p><label>Email: <input name="email"> </label></p>
            <p><label>Password: <input name="password" type="password"> </label></p>
            <p>Gender: <label> <input name="gender" type="radio" value="m" checked>Male</label> 
            <label><input name="gender" type="radio" value="f">Female</label>
            </label> </p>
            <p><label>City: <select name="city" id="">
                <option value="BJ" selected>Beijing</option>
                <option value="SH">Shanghai</option>
                <option value="CD">Chengdu</option>
                <option value="XM">Xiamen</option>
            </select></label></p>
            <p><button type="submit">Submit</button></p>
        </form>
    </body>
    输入值后,用jQuery获取表单的JSON字符串,key和value分别对应每个输入的name和相应的value,例如：`{"name":"Michael","email":...}`
    'use strict';
    var json=null;
    var arr=$('#test-form input,select');
    var obj={};
    arr.map(function(){
        if(this.type==='radio'&&this.checked!=true){
            return;
        }
        obj[this.name]=this.value;
    })
    json=JSON.stringify(obj);

    //显示结果
    function wj(){
        if(typeof(json)==='string'){
            console.log(json);
        }else{
            console.log('json变量不是string!');
        }
    }
![pic4](./ck/ck9.PNG)
## 使用jQuery操作DOM
    样式操作:css()获取和设置样式、addClass()追加样式、removeClass()移除样式、toggleClass()切换样式
    内容操作：html()代码操作、text()内容操作、val()属性值操作
    节点操作:查找节点、创建节点元素、插入节点、删除节点、替换节点、复制节点
    属性操作:attr()获取和设置元素属性、removeAttr()删除元素属性
    节点遍历:
    1)遍历子元素:children()
    2)遍历同辈元素:next()、prev()、siblings()
    3)遍历前辈元素:parent()、parents()
    4)其他遍历方法:each()、end()、find()、eq()、first()...
### 修改Text和HTML
    jQuery对象的text()和html()方法分别获取节点的文本和原始HTML文本:
     <!-- HTML结构 -->
    <ul id="test-ul">
        <li class="js">JavaScript</li>
        <li name="book">Java &amp; JavaScript</li>
    </ul>
    'use strict';
    $('#test-ul li[name=book]').text(); // 'Java & JavaScript'
    $('#test-ul li[name=book]').html(); // 'Java &amp; JavaScript'
![pic5](./ck/ck11.png)   
### 修改CSS
    <body>
        <!-- HTML结构 -->
        <ul id="test-css">
            <li class="lang dy"><span>JavaScript</span></li>
            <li class="lang"><span>Java</span></li>
            <li class="lang dy"><span>Python</span></li>
            <li class="lang"><span>Swift</span></li>
            <li class="lang dy"><span>Scheme</span></li>
        </ul>
    </body>
    'use strict';
    $('#test-css li.dy>span').css('background-color', '#ffd351').css('color', 'red');
![pic6](./ck/ck12.png)

    jQuery对象的css()方法可以这么用：
    var div = $('#test-div');
    div.css('color'); // '#000033', 获取CSS属性
    div.css('color', '#336699'); // 设置CSS属性
    div.css('color', ''); // 清除CSS属性

    为了和JavaScript保持一致，CSS属性可以用'background-color'和'backgroundColor'两种格式.

    css()方法将作用于DOM节点的style属性，具有最高优先级.如果要修改class属性，可以用jQuery提供的下列方法：
    var div = $('#test-div');
    div.hasClass('highlight'); // false， class是否包含highlight
    div.addClass('highlight'); // 添加highlight这个class
    div.removeClass('highlight'); // 删除highlight这个class
### 练习:分别用css()方法和addClass()方法高亮显示JavaScript：
     <!-- HTML结构 -->
    <style>
        .highlight {
            color: #dd1144;
            background-color: #ffd351;
        }
    </style>
    <body>
        <div id="test-highlight-css">
            <ul>
                <li class="py"><span>Python</span></li>
                <li class="js"><span>JavaScript</span></li>
                <li class="sw"><span>Swift</span></li>
                <li class="hk"><span>Haskell</span></li>
            </ul>
        </div>
    </body>

    'use strict';
    var div = $('#test-highlight-css');
    // div.find('.js').addClass('highlight');
    div.find('.js').css('color','#dd1144').css('background-color','#ffd351');
![pic5](./ck/ck10.png)
### 显示和隐藏DOM
    jQuery直接提供show()和hide()方法,甭管如何运作,能用就行.
    var a = $('a[target=_blank]');
    a.hide(); // 隐藏
    a.show(); // 显示
### 获得DOM信息
    借用this获取当前元素信息
    <body>
        <ul>
            <li class="a" id="ongoing">正在进行</li>
            <li id="about_to_begin">即将开始</li>
            <li id="past_activities">往期活动</li>
        </ul>
    </body>    
    'use strict';
    let jq = $(document).ready(function () {
        $("li").click(function () {//点击可通过标签，id，类名等
            // alert($(this).attr("id"));//此处可以拿到点击元素的id,取到其value值及用attr()方法        
            alert($(this).html()); //拿到点击元素的内容<br>,拿到内容后即可使用ajax 方法与后台交换数据，进行交互
            var code = $(this).attr("id");
            // console.log(code);
            $("#a div").remove();//配合点击事件拿到元素的同时删掉HTML页面部分内容,同时也可用添加元素的方法添加元素
            $("#b div").remove();
            $("#c div").remove();
        })
    });
![pic6](./ck/ck13.png)
### 操作表单
    val() 方法返回或设置被选元素的值.
    元素的值是通过value属性设置的.该方法大多用于input元素.
    如果该方法未设置参数，则返回被选元素的当前值.
    语法：$(selector).val(value)
    <script type="text/javascript">
    $(document).ready(function(){
        $("button").click(function(){
            $(":text").val("sunshine");
        });
    });
    </script>
    <body>
        <p>Name: <input type="text" name="user" value="light"></p>
        <button>改变文本框的值</button>
    </body>
![pit](./ck/ck14.png)
![pict](./ck/ck15.png)

    一个val()就统一了各种输入框的取值和赋值的问题.
 
## 蜜汁焦虑....后期再添加或更改(土拨鼠尖叫...)



 