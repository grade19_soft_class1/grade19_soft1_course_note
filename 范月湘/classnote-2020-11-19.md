# 回顾C#
## 使用递归实现斐波那契数列
     递归定义：方法自个儿调用自个儿
     递归特点：1.递归一直会调用自己直到某种条件被满足
	     2.递归方法会有些参数，会把一些新的参数传给自己
![斐波那契](pic/pic.png)
## 嵌套类
    嵌套类的基本概念：在一个类中定义另一个类，分为静态嵌套类(使用较少)和非静态嵌套类(内部类)
    内部类定义：1.在一个类中直接定义类；
               2.在一个方法中定义类；
               3.匿名内部类.
### 嵌套类的使用
    1.(1)外部类只能访问嵌套类中修饰符为public、internal的字段、方法、属性.
    1.(2)嵌套类可以访问外部类的方法、属性、字段而不受访问修饰符的限制.
    2.嵌套类访问外部类实例的方法、字段、属性时候.一般在采取构造函数输入外部类.
    3.继承类可以再定义一个内嵌类并从继承父类中嵌套类.
    4.单例模式就采用一种为：延迟加载初始化实例.
    5.反射内嵌类需要使用"+"，而不是我们常用的".".
## partial分部类的content
    1.partial是一个关键字修饰符，可以将类或结构、接口或方法的定义拆分到两个或更多个源文件中.每个源文件包含类型或方法定义的一部分，编译使用程序时将把所有部分组合起来.修饰符不可用于委托或枚举声明中.
    2.在以下几种情况下需要拆分类定义：
    (1)处理大型项目时，使一个类分布于多个独立文件中可以让多位程序员同时对该类进行处理。
    (2)使用自动生成的源时，无需重新创建源文件便可将代码添加到类中。 Visual Studio 在创建 Windows 窗体、Web 服务包装器代码等时都使用此方法。 无需修改 Visual Studio 创建的文件，就可创建使用这些类的代码。
    (3)编译时会对分部类型定义的属性进行合并；将从所有分部类型定义中对以下内容进行合并：XML 注释、接口、泛型类型参数属性、class 特性、成员
    3.分部方法
     分部类或结构可以包含分部方法。 类的一个部分包含方法的签名。 分部方法声明由两个部分组成：定义和实现。 但是它的限制很多，只能在部分场景下使用。
    (1)分部方法声明必须以上下文关键字 partial 开头，并且方法必须返回 void。
    (2)分部方法可以有 ref 参数，但不能有 out 参数。
    (3)分部方法为隐式 private 方法，因此不能为 virtual 方法。
    (4)分部方法不能为 extern 方法，因为主体的存在确定了方法是在定义还是在实现。
    (5)分部方法可以有 static 和 unsafe 修饰符。
    (6)分部方法可以是泛型的。 约束将放在定义分部方法声明上，但也可以选择重复放在实现声明上。 参数和类型参数名称在实现声明和定义声明中不必相同。
    (7)你可以为已定义并实现的分部方法生成委托，但不能为已经定义但未实现的分部方法生成委托。
## Random类
    C# Random 类是一个产生伪随机数字的类，它的构造函数有两种。
    New Random()；
    New Random(Int32)。
    方法	描述
    Next()	每次产生一个不同的随机正整数
    Next(int max Value)	产生一个比 max Value 小的正整数
    Next(int min Value,int max Value)	产生一个 minValue~maxValue 的正整数，但不包含 maxValue
    NextDouble()	产生一个0.0~1.0的浮点数
    NextBytes(byte[] buffer)	用随机数填充指定字节数的数组
##  DataTime类☆
    在 DateTime 类中提供了常用的属性和方 法用于获取或设置日期和时间，如下表所示。
    方法	                            描述
    Date	                            获取实例的日期部分
    Day	                                获取该实例所表示的日期是一个月的第几天
    DayOfWeek	                        获取该实例所表示的日期是一周的星期几
    DayOfYear	                        获取该实例所表示的日期是一年的第几天
    Add(Timespan value)	                在指定的日期实例上添加时间间隔值 value
    AddDays(double value)	            在指定的日期实例上添加指定天数 value
    AddHours(double value)	            在指定的日期实例上添加指定的小时数 value
    AddMinutes(double value)	        在指定的日期实例上添加指定的分钟数 value
    AddSeconds(double value)	        在指定的日期实例上添加指定的秒数 value
    AddMonths(int value)	            在指定的日期实例上添加指定的月份 value
    AddYears (int value)	            在指定的日期实例上添加指定的年份 value
    class Program
    {
        static void Main(string[] args)
        {
            DateTime date = DateTime.Now;
            Console.WriteLine("当前日期为：{0}", date);
            Console.WriteLine("当前时本月的第{0}天为", date.Day);
            Console.WriteLine("当前是：{0}", date.DayOfWeek);
            Console.WriteLine("当前是本年度第{0}天", date.DayOfYear);
            Console.WriteLine("30天后的日期是{0}", date.AddDays(30));
            DateTime dt1 = DateTime.Now;
            DateTime dt2 = new DateTime(2016, 11, 19);
            TimeSpan ts = dt2 - dt1;
            Console.WriteLine("间隔的天数为{0}天", ts.Days);
        }

    }
## string字符串
    创建string对象方法：
    1.通过给string变量指定一个字符串;
    2.通过使用string类构造函数;
    3.通过使用字符串串联运算符(+);
    4.通过检索属性或调用一个返回字符串的方法;
    5.通过格式化方法来转换一个值或对象为它的字符串表示形式
    class Program
    {
        static void Main(string[] args)
        {
            //字符串连接
            string fname, lname;
            fname = "Justin";
            lname = "Bieber";
            string fullname = fname + lname;
            Console.WriteLine("Full name:{0}", fullname);
            //通过使用string构造函数
            char[] number = { 'a', 'b', 'c', 'd', 'e' };
            string count = new string(number);
            Console.WriteLine("Count:{0}", count);
            //方法返回字符串
            string[] array = { "Hello", "Your", "world", "is", "beautiful" };
            string message = String.Join(" ", array);
            Console.WriteLine("Message:{0}", message);
            //用于转换值的格式化方法
            DateTime num = new DateTime(2020, 11, 19, 22, 44, 33);
            string chat = String.Format("Message sent at {0:t} on {0:D}",num);
            Console.WriteLine("Message:{0}", chat);
            Console.ReadKey();
        }
    }
## 数据类型转换
   1.数据类型在一定的条件下是可以相互转换的，如将int型数据转换成double型数据。C#允许使用两种转换方式：隐式转换和显式转换.

   (1)隐式类型转换：这些转换是 C# 默认的以安全方式进行的转换, 不会导致数据丢失。
   例如，从小的整数类型转换为大的整数类型，从派生类转换为基类。
   int a=10;
   double b=a;//隐式转换

   (2)显式类型转换：显式类型转换，即强制类型转换。
   显式转换需要强制转换运算符，而且强制转换会造成数据丢失。
   double c=3.14;
   int d=(int)c;//显式转换

   2.通过方法进行类型转换
   (1)ToString():所有类型都继承了object基类,所以都有ToString这个转换成字符串的方法.
   (2)int.Parse():参数类型只支持string类型.
   使用该方法转换时string的值不能为null，不然无法通过转换；
   另外string类型参数也只能是各种整型，不然也无法通过转换.

   (3)int.TryParse():该方法与int.Parse()转换方法类型，不同于在于int.Parse()方法无法转换成功的情况该方法能正常执行并返回0.也就是说int.TryParse()方法比int.Parse()方法多了一个异常处理，如果出现异常则返回false，并且将输出参数返回0.
        int i;
        string s = null;
        int.TryParse(s,out i);

   (4)Convert: Convert类中提供了很多转换的方法。
   使用这些方法的前提是能将需要转换的对象转换成相应的类型，如果不能转换则会报格式不对的错误。
   使用Convert.ToInt32(double value)时，如果 value 为两个整数中间的数字，则返回二者中的偶数；即 4.5 转换为 4，而 5.5 转换为 6.

   Convert 类常用的类型转换方法如下表所示。
    方法	说明
    Convert.ToInt16()	转换为整型(short)
    Convert.ToInt32()	转换为整型(int)
    Convert.ToInt64()	转换为整型(long)
    Convert.ToChar()	转换为字符型(char)
    Convert.ToString()	转换为字符串型(string)
    Convert.ToDateTime()	转换为日期型(datetime)
    Convert.ToDouble()	转换为双精度浮点型(double)
    Conert.ToSingle()	转换为单精度浮点型(float)

   (5)实现自己的转换，通过继承接口IConvertible或TypeConverter类，从而实现自己的转换.

   3.使用AS操作符转换
   使用AS操作符转换，但是AS只能用于引用类型和可为空的类型.使用as有很多好处，当无法进行类型转换时，会将对象赋值为NULL,避免类型转换时报错或是出异常.

   4.装箱和拆箱
   装箱：装箱是指将一个值类型的数据隐式地转换成一个对象类型(object)的数据.执行装箱操作时不可避免的要在堆上申请内存空间，并将堆栈上的值类型数据复制到申请的堆内存空间上，这肯定是要消耗内存和cpu资源的.注意：在执行装箱转换时，也可以使用显式转换.

   拆箱：拆箱是指将一个对象类型的数据显式地转换成一个值类型数据.拆箱过程是装箱的逆过程，是将存储在堆上的引用类型值转换为值类型并赋给值类型变量.拆箱操作分为两步：一是检查对象实例，确保它是给定值类型的一个装箱值；而是将该值从实例复制到值类型变量中.
## 数组
     (1)数组引入的命名空间：using System;
     (2)Array：用法基本与数组同，引入命名空间：using System;
     (3)ArrayList:引入命名空间： using System.Collections
     (4)List:引入命名空间：using System.Collections.Generic;
     数组 string[]s=new string[2];
     赋值 s[0]="a"; s[1]="b";
     修改 s[1]="a1";
     ArrayList
    ArrayList list1 = new ArrayList();
    新增数据
    list1.Add("cde");
    list1.Add(5678);    
    修改数据
    list[2] = 34;
    移除数据
    list.RemoveAt(0);
    插入数据
    list.Insert(0, "qwe");
## 正则表达式
![数组](pic/pic1.png)
![数组](pic/pic2.png)
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("请输入一个邮箱：");
        string email = Console.ReadLine();
        Regex regex = new Regex(@"^(\w)+(\.\w)*@(\w)+((\.\w+)+)$");
        if (regex.IsMatch(email))
        {
            Console.WriteLine("邮箱格式正确。");
        }
        else
        {
            Console.WriteLine("邮箱格式不正确。");
        }
    }
}
## 练习题
    练习1：输入一串字符串，将字符串反转输出（打印）；
    练习2：输入一串字符串（英文语句），将每个单词反转输出；
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入需要反转的字符串！");
            string a = Console.ReadLine();
            for (int i = a.Length - 1; i >= 0; i--)
            {
                Console.Write(a.Substring(i, 1));
            }
            Console.ReadKey();
        }
    }
    练习3：输入一个身份证号，输出这个身份证号的出身日期，并判断其生日是否已经过完；
    练习4：输入电子邮箱地址，判断其是否为合法的地址；
    如:abc@abc.com 为合法
    abc@@.abc.com为不合法





    