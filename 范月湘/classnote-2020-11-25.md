# 今日份小笔记
## Js声明全局变量
    1.使用var+变量名的方式在function外部声明，即为全局变量，否则在function声明的是局部变量.
        var we=3;//全局变量
    function q(){
        var qe=4;//局部变量
        alert(we);
    }
    function s(){
        alert(we);
    }
    console.log(q);
    console.log(s);
    2.不加var，直接给标识符赋值，隐式声明全局变量.即使该语句是在一个function中，当该function被执行后标识符变成了全局变量.
        test=2;//全局变量
    function a(){
        as=23;//全局变量
        alert(test);
    }
    console.log(a);
    3.使用window全局对象来声明，全局对象的属性对应也是全局变量.较少用.
    window.test=6;
    console.log(window);
    这方式经常被用到一个匿名函数执行后将一些函数公开到全局.
    jQuery=1;
    window.jQuery=window.$=jQuery;
    console.log(jQuery);
### 全局变量的优缺点
    优点:可减少变量的个数，减少由于实际参数和形式参数的数据传递带来的时间消耗.

    缺点：(1)全局变量保存在静态存贮区，程序开始运行时为其分配内存，程序结束释放该内存。
    与局部变量的动态分配、动态释放相比，生存期比较长，因此过多的全局变量会占用较多的内存单元.

    (2)全局变量破坏了函数的封装性能.函数像一个黑匣子，一般是通过函数参数和返回值进行输入输出，函数内部实现相对独立.
    但函数中如果使用了全局变量，那么函数体内的语句就可以绕过函数参数和返回值进行存取，
    这种情况破坏了函数的独立性，使函数对全局变量产生依赖.同时，也降低了该函数的可移植性.
    
    (3)全局变量使函数的代码可读性降低.由于多个函数都可能使用全局变量，函数执行时全局变量的值可能随时发生变化，
    对于程序的查错和调试都非常不利.因此，如果不是万不得已，最好不要使用全局变量.
## 避免js全局变量冲突
    原则：(1)用匿名函数将脚本包起来
          (2)使用多级命名空间
    改进过程:
    1.原始数据(test.js和test1.js都有全局变量window.
    test导致冲突，全局变量属于window)
    栗子：
    <script type="text/javascript">
        var q=45,w="not sure";
    </script>
    <script type="text/javascript">
        var a,c="i'm sure";
    </script>
    //test.js
    window.w=4;
    console.log(window);
    //test1.js
    window.test=5;
    console.log(window);
    2.使用匿名函数(test.js和test1.js中的e都不是全局变量，但是test1.js中无法访问test.js中的g,没法通信)
    栗子：
    //test.js
    (function(){
    var e=13,g="Is that sure?";
    })();
    //test1.js
     (function(){
      var e,h="maybe";
    })();
    3.使用后全局变量进行通信(使用window.str作为全局变量，会导致全局变量越来越好，不好维护)
    var str;
    //test.js
    (function(){
     var r=213,t="if you";
     window.str=r;
    })();
    //test1.js
    (function(){
     var r,y="that's right";
     alert(window.str);
    })();
    4.使用命名空间
    var GLOBAL={};
    //test.js
    (function(){
    var r=45,s="sunshine";
    GLOBAL.r=r;
    })();
    //test1.js
    (function(){
    var r,t="yet";
    alert(GLOBAL.r);
    })();
## 字符串
### 字符串属性：
    constructor:返回创建字符串属性的函数;
    length:返回字符串的长度;
    prototype:允许向对象添加属性和方法.
### 字符串方法：
    chatAt():返回指定索引位置的字符;
    charCodeAt():返回指定索引位置字符的 Unicode 值;
    concat():连接两个或多个字符串，返回连接后的字符串;
    fromCharCode():将 Unicode 转换为字符串;
    indexOf():返回字符串中检索指定字符第一次出现的位置;
    lastIndexOf():返回字符串中检索指定字符最后一次出现的位置;
    localeCompare():用本地特定的顺序来比较两个字符串;
    match():找到一个或多个正则表达式的匹配;
    replace():替换与正则表达式匹配的子串;
    search():检索与正则表达式相匹配的值;
    slice():提取字符串的片断，并在新的字符串中返回被提取的部分;
    split():把字符串分割为子字符串数组;
    substr():从起始索引号提取字符串中指定数目的字符;
    substring():提取字符串中两个指定的索引号之间的字符;
    toLocaleLowerCase():根据主机的语言环境把字符串转换为小写，只有几种语言（如土耳其语）具有地方特有的大小写映射;
    toLocaleUpperCase():根据主机的语言环境把字符串转换为大写，只有几种语言（如土耳其语）具有地方特有的大小写映射;
    toLowerCase():把字符串转换为小写;
    toString():返回字符串对象值;
    toUpperCase():把字符串转换为大写;
    trim():移除字符串首尾空白;
    valueOf():返回某个字符串对象的原始值.