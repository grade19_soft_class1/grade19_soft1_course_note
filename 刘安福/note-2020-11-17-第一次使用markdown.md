# 第一次使用VS Code O(∩_∩)O嗯!

## 二级标题

# VS快捷键
    1 https://blog.csdn.net/seayehin/article/details/92964450
    2.Ctrl+] / [	缩进/缩进行 Indent/outdent line

# Lambda表达式

Func<int,int,int> a1=(a,b)=>a+b;

# 委托
    
# 保安日记
### 我是一个保安
#### 1.保安日记
        （1）今天我是一个保安
        （2）今天我学了Git的合并冲突，
        （3）使用vsCode做随堂笔记
        
##### （4）Lambda表达式:
        Func<int, int, int> func = (a, b) => a + b ;
            Console.WriteLine(func(3,4));//输出结果为7
            Console.ReadLine();
##### 代码如下图            
![当图片无法显示，展示的文本内容](./img/img_Lambad表达式.png)

```     


```
##### （5）委托
![当图片无法显示，展示的文本内容](./img/img_委托.png)

#### 2.保安的日常工作
        （1）读书
        （2）读书
        （3）还是读书
#### 3.保安的日常收获
        （1）习得知识
        （2）Get
        （3）Get
