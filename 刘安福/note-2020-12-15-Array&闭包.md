# 这两天格外的冷 但是呢教室格外的暖和😄

## Array
对于数组，除了map()、reduce、filter()、sort()这些方法可以传入一个函数
外，Array对象还提供了很多非常实用的高阶函数。

### every()方法可以判断数组的所有元素是否满足测试条件。
```
var arr=['Apple','Banan','abcdefg']

console.log(arr.every(function (a){
    return a.toUpperCase() === a ;//判断所有的元素是否都是大写
})); //false 
```

### find()方法用于查找符合的第一个元素，如果找到了就返回该元素 否则就返回undefined;
```
let arr=['Apple','Banan','abcdefg','ABCDEe']

console.log(arr.find(function (s){
    return s.toLowerCase();//返回数组中全小写的元素
}));//'abcdefg'

console.log(arr.find(function(s){
    return s.toUpperCase();//返回全大写的元素
}));//undefined，没有
```

###  findIndex()和find()类似，也是查找符合条件的第一个元素，不同之处在于findIndex()会返回这个元素的索引，如果没有找到，返回-1：

```
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.findIndex(function (s) {
    return s.toLowerCase() === s;
})); // 1, 因为'pear'的索引是1

console.log(arr.findIndex(function (s) {
    return s.toUpperCase() === s;
})); // -1
```

### ！！forEach   遍历数组 将元素一个个打印出来
    
## 闭包 
### 函数作为返回值
```
let arr = [2022, 22, 11, 10, 1, 9, 2]
function sum(arr) {
    return arr.reduce(function (x, y) {
        return x + y;
    })
}
console.log(sum(arr));//2077

//但是，如果不需要立刻求和，而是在后面的代码中，根据需要再计算怎么办？可以不返回求和的结果，而是返回求和的函数！

function lazy_sum(arr) {
    //用一个变量装着先
    var sum = function () {
        return arr.reduce(function (x, y) {
            return x + y;
        });
    }
    return sum;
}
//当我们调用lazy_sum()时，返回的并不是求和结果，而是求和函数：

var f = lazy_sum([1, 2, 3, 4, 5]); // function sum()
//调用函数f时，才真正计算求和的结果：
f(); // 15
console.log(f());
```
### 闭包的坑点
#### 用闭包模仿块级作用域 :es6没出来之前，用var定义变量存在变量提升问题，eg:
```

for(var i=0;i<10; i++){
    console.info(i)
}
alert(i)  // 变量提升，弹出10

//为了避免i的提升可以这样做
(function () {
    for(var i=0; i<10;i++){
         console.info(i)
    }
})()
alert(i)   // underfined   因为i随着函数的退出，执行环境销毁，变量回收
```
#### 闭包还可以把多参数的函数变成单参数的函数。例如，要计算xy可以用Math.pow(x, y)函数，不过考虑到经常计算x2或x3，我们可以利用闭包创建新的函数pow2和pow3：
```
//计算2^10
function make_pow(x){
    return function(n){
        return Math.pow(n,x)
    }

}
let pow10=make_pow(10)//Math.pow(n,10)
console.log(pow10(2));//1024

```











































