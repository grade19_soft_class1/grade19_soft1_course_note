# 今天双十二但是跟我这个不(qióng)喜(kùn)购(liáo)物(dǎo)的好像没关系
## reduce
·对一个Array求和，就可以用reduce实现
```
let arr=[1,2,3,4,5,6,7,8,9,10];
let a=arr.reduce((x,y)=>{
    return x+y;
})
console.log(a);//55
```
·利用reduce()求积
```
let arr=[1,2,3,4,5,6,7,8,9,10];
let a=arr.reduce((x,y)=>{
    return x*y;
})
console.log(a);//3628800
```
·练习：不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：
```
//方法一
function string2int(String) {
    let newArr = [];
    for (let i = 0; i < String.length; i++) {
        newArr.push(String[i])
    }

    function convert(x) {
        return x * 1
    }
    let c= newArr.map(convert);

    function b(x, y) {
        return x * 10 + y;
    }
    return c.reduce(b);
}

//其他
function string2int(string) {
    //return string.split('').join('')*1//方法二
    return string * 1//最简单的方法
}


// 测试:
if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        console.log('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        console.log('请勿使用Number()!');
    } else {
        console.log('测试通过!');
    }
}
else {
    console.log('测试失败!');
}
```
·
```
//js字母大小写转换方法：
//1、转换成大写：toUpperCase()
//2、转换成小写：toLowerCase()

function normalize(arr) {

    function convert(x) {
        let firstWord = x.substring(0, 1);
        let surplusWord = x.substring(1);
        let c = firstWord.toUpperCase() + surplusWord.toLowerCase();
        return c;
    }
    return arr.map(convert);
}
//或
function normalize(arr) {
    return arr.map((x)=>{
        return x.substring(0, 1).toUpperCase() + x.substring(1).toLowerCase();
    });
}


// 测试:
if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}

```

## 小明的map()修改
![图没了自己猜猜吧](./img/小明的Map.png)
```
var arr = ['1', '2', '3'];
var r;
r = arr.map(convert=>parseInt(convert,0));
console.log(r);
/*parseInt 通常使用parseInt时，只需要传递一个参数。但实际上，parseInt可以有两个参数，第二个参数是进制数，可以通过语句“alert(parseInt.length) === 2”来验证。0则表示以十进制来解析*/
```
## filter 课件指向💨 8.3
### 在一个Array中，删掉偶数，只保留奇数，
```
let arr = [1, 2, 4, 5, 6, 9, 10, 15];

let r=arr.filter(function(a){
    return a%2 !== 0;
});

console.log(r);
```
### Array中的空字符串删掉
```
var arr = ['A', '', 'B', null, undefined, 'C', '  '];

/*trim() 函数用于去除字符串两端的空白字符。
注意：$.trim()函数会移除字符串开始和末尾处的所有换行符，空格(包括连续的空格)和制表符。如果这些空白字符在字符串中间时，它们将被保留，不会被移除。*/

var r = arr.filter(function(a){
    return a && a.trim();
})
console.log(r);
```
### 回调函数
#### filter()接收的回调函数，其实可以有多个参数。通常我们仅使用第一个参数，表示Array的某个元素。回调函数还可以接收另外两个参数，表示元素的位置和数组本身：
```
var arr = ['A', 'B', 'C'];
var r = arr.filter(function (element, index, self) {
    console.log(element); // 依次打印'A', 'B', 'C'
    console.log(index); // 依次打印0, 1, 2
    console.log(self); // self就是变量arr
    return true;
});
```

#### 请尝试用filter()筛选出素数
```
function get_primes(arr) {
    return arr.filter(function (x) {
        if (x >= 9) {
            return x % 2 !== 0 && x % 3 !== 0 && x % 5 !== 0 && x % 7 !== 0 && x % 9 !== 0;
        } else if (x > 1) {//2-8
            return x % 4 !== 0&&x%6!==0 ;
        }
    })
}


// 测试:
var
    x,
    r,
    arr = [];
for (x = 1; x < 100; x++) {
    arr.push(x);
}
r = get_primes(arr);
if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + r.toString());
}

```






















