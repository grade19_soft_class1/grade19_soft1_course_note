# 保安日记之天冷了多加衣服
### 每天看看我的公安备案过了没
    2020-12-03 时隔15天了 还没过 
###  关于路径的问题
```
    /表示网站根目录    ./表示当前路径  ../表示上一层目录
```
#### arguments
    1.它只在函数内部起作用，并且永远指向当前函数的调用者传入的所有参数arguments类似Array但它不是一个Array：
```
    function xxo(a){
    for(var i=0;i<arguments.length;i++){
        console.log(arguments[i]);
    }
    };
    xxo(2,3,4)//相当于打印数组[2,3,4]里面的每个元素

```
    2.即使函数不定义任何参数，还是可以拿到参数的值：
    
    function xxo(a){
        if(arguments.length===0){
            console.log('你没有传入任何东西');
        }else{
            for(var i=0;i<arguments.length;i++){
                console.log(arguments[i]);
            }
        }
    };
    xxo();
    xxo(2,3,4)

#### 函数定义
```
    函数关键字(function)语句：

    function fnMethodName(x){alert(x);}
    函数字面量(Function Literals)：

    var fnMethodName = function(x){alert(x);}
    Function()构造函数：

    var fnMethodName = new Function(‘x','alert(x);') // 由Function构造  函数的参数个数可变。最后一个参数写函数体，前面的参数写入参。

    上面三种方法定义了同一个方法函数fnMethodName，第1种就是最常用的方法，后 两种都是把一个函数复制给变量fnMethodName，而这个函数是没有名字的，即匿名 函数
```

#### rest参数（形式为“…变量名”）
1.  用rest写一个sum()函数
2.  当传入的参数不确定时，使用rest运算符
3.  建议只计算纯数字 
```
    
function sum(...rest){
    var a=0;//初始化
    for (var i=0;i<arguments.length;i++){
        a += arguments[i];
    }
    return a ;
}

console.log(sum(1,2,3));//结果6
```
#### 练习
```
//定义一个计算圆面积的函数area_of_circle()，它有两个参数：
//r: 表示圆的半径； pi: 表示π的值，如果不传，则默认 Math.PI

function area_of_circle(r,pi){
    if(arguments.length===0){
        return '你没传任何东西进去';
    }else{
         pi=Math.PI;
        return pi*r*arguments[0];
    }
}

console.log(area_of_circle(1));

```