# '巅峰产生虚伪的拥护'
# '黄昏见证真正的信仰'
![显示失败](./imgs/sll.png)

# 操作DOM
> * ## document.getElementById
> * ## getElementsByClassName
> * ## document.querySelector
>>![显示失败](./imgs/domgg.png)
>>![显示失败](./imgs/domgg1.png)
>> ![显示失败](./imgs/domgg2.png)
# 更新DOM
> * ## 更新DOM
> * ## 其实不管是查找更新插入删除
> * ## 首先要做的就是需要操作对象的位置
> * ## so...
>> ![显示失败](./imgs/gengx1.png)
>> ![显示失败](./imgs/gengx2.png)
# 插入DOM
>* ## firstChild（写上）
>* ## appendChild（列表末尾）
>>![显示失败](./imgs/Dom1.png)
>>![显示失败](./imgs/Dom2.png)
>>  ![显示失败](./imgs/Dom3.png)
> * ## createElement(建立子节点)
> * ## document.createElement('新建样式')
> * ## 现在新建一个子节点👇
>> ![显示失败](./imgs/xjzjd1.png)
>> ![显示失败](./imgs/xjzjd2.png)
>> ![显示失败](./imgs/xjzjd3.png)
# 删除DOM
>> ![显示失败](./imgs/domdelelte.png)
>> ![显示失败](./imgs/domdelete1.png)
>> ![显示失败](./imgs/domdelete2.png)
# 操作表单
> * ## 文本框，对应的`<input type="text">`，用于输入文本
> * ## 口令框，对应的`<input type="password">`，用于输入口令；
> * ## 单选框，对应的`<input type="radio">`，用于选择一项；
> * ## 复选框，对应的`<input type="checkbox">`，用于选择多项
> * ## 下拉框，对应的`<select>`，用于选择一项；
> * ## 隐藏文本，对应的`<input type="hidden">`，用户不可见，但表单提交时会把隐藏文本发送到服务器。
>> ![显示失败](./imgs/check1.png)
>> ![显示失败](./imgs/check2.png)
>> ![显示失败](./imgs/check3.png)
# 操作文件
# AJAX
# promise

# Canvas