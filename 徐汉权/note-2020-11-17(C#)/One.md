# Lambda表达式
## https://www.jianshu.com/p/ef7dc7d411c6
![无法显示](../imgs/LambdaTow.png)
![无法显示](../imgs/lambda.png)
# 委托

# 内置委托
## 1.func<>
![无法显示](../imgs/func.png)

## 2.action<>
![无法显示](../imgs/action.png)
## Func和Action的区别
Func和Action作用几乎是一模一样的。
But: → → → Func<t>是有返回类型的而Action只有参数类型不能传返回类型。
所以，Action的委托函数都是没有返回值的。
# Linq
## http://cw.hubwiz.com/card/c/558cf07ac086935f4a6fb7fb/1/1/1/
![无法显示](../imgs/Linq.png)

# Linq用法