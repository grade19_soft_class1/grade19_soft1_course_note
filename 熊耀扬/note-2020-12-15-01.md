## every
every()方法可以判断数组的所有元素是否满足测试条件。
![](./images/i70.png)
## find
find()方法用于查找符合条件的第一个元素，如果找到了，返回这个元素，否则，返回undefined。
![](./images/i71.png)
## findIndex
findIndex()和find()类似，也是查找符合条件的第一个元素，不同之处在于findIndex()会返回这个元素的索引，如果没有找到，返回-1。
![](./images/i72.png)
## 闭包
有权访问另一个函数作用域内变量的函数都是闭包
```
'use strict'

function a(){
    var num = 1;
    function b(){
        console.log(num++);
    }
    b();
    b();
}

a();//这里先输出1，再输出2


function a(){
    var num = 1;
    this.b = function(){
        console.log(num++);
    }
}

var c = new a();

c.b();//1

c.b();//2
```
这里b函数访问了构造a函数里的num变量，所以形成了一个闭包。
![](./images/i73.png)
从上图我们可以看到，两次输出结果都是1，为什么呢？因为上图我们用的是局部变量，局部变量每次使用都会销毁，下次运行又要重新创建那个变量，所以虽然我们num++了，但是其实是第一次执行完就被销毁了。那么有没有办法让它不被销毁呢？办法是有的，使用闭包就可以让它不被销毁，具体请看下图。
![](./images/i74.png)
如上图所示，里面的变量并没有被销毁，因为函数a被外部的变量c引用，所以变量b没有被回收。如果某个函数被它的父函数之外的一个变量引用，就形成了一个闭包。

##### 闭包的另一种写法
```
'use strict'

let d = (function a(){
    var num = 1;
    function b(){
        console.log(num++);
    }
    return b;
})();

d();//1
d();//2
d();//3
```
首先我们是把一个自执行函数赋值给了d，这个自执行函数运行完之后的值变成了
```
function b(){
    console.log(num++);
}
```
因为我们在上面的代码return回了b,再因为这个自执行函数被d引用所以里面的变量num并没有因为这个自执行函数执行完而销毁，而是保存到了内存当中，，所以我们多次打印d()结果分别是1，2，3。
