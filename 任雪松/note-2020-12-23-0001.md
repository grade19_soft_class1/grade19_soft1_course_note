# 今日份的上课总结
## **距离到家还有十七天**

# 今日收获
1. 安利电影《神探夏洛克》
2. 头晕目眩找原型
3. 预习了一嗖嗖浏览器

## 练习题
```
function Cat(name) {
    this.name=name;
};
Cat.prototype.say=function(){
    return 'Hello, '+this.name+'!';
}


var kitty = new Cat('Kitty');
var doraemon = new Cat('哆啦A梦');
if (kitty && kitty.name === 'Kitty'
    && kitty.say
    && typeof kitty.say === 'function'
    && kitty.say() === 'Hello, Kitty!'
    && kitty.say === doraemon.say
) {
    console.log('测试通过!');
} else {
    console.log('测试失败!');
}
```
