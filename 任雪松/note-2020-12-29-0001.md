# 今日份的上课总结
## **距离到家还有十一天**

# 今日收获
## 1.删除DOM
```
删除节点后节点数会时刻发生变化
因此要注意小细节
```
```
<body>
    <div id="content">
        <ul id="uu">
            <li id="sun">🌞</li>
            <li id="moon">🌙</li>
            <li id="star">⭐</li>
            <li id="maxSun">九阳</li>
            <li id="maxMoon">九阴</li>
        </ul>
    </div>
    
</body>
<script src="./js/js.js"></script>
```
```
'use strict';

let star = document.querySelector('#star');

console.log(star);
let uu =star.parentElement;

let removed = uu.removeChild(star);

console.log(star===removed);
```
效果图如下:

![](./imgs/2020122901.png)

## 2.操作表单
```
1.文本框，对应的`<input type="text">`，用于输入文本；

2.口令框，对应的`<input type="password">`，用于输入口令；

3.单选框，对应的`<input type="radio">`，用于选择一项；

4.复选框，对应的`<input type="checkbox">`，用于选择多项；

5.下拉框，对应的`<select>`，用于选择一项；

6.隐藏文本，对应的`<input type="hidden">`，用户不可见，但表单提交时会把隐藏文本发送到服务器。
```
```
<body>
    <div id="content">
        <input id="a1" type="radio" value="88"/>
        <input id="a2" type="checkbox" value="99"/>
    </div>
</body>
<script src="./js/js.js"></script>
```
```
let a1 = document.querySelector('#a1');
let a2 = document.querySelector('#a2');
console.log(a1.value);
console.log(a2.value);
```
效果图如下:

![](./imgs/2020122902.png)

```
    <input type="date" value="2020-12-29">

    2020/12/29
    <input type="datetime-local" value="2020-12-29T17:13:00">
    
    2020/12/29 17:13:00
    <input type="color" value="#ff0000">
```
效果图如下:

![](./imgs/2020122903.png)