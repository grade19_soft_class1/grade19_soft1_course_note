### 😭⏰早课太困了
## 方法
```
在一个对象中绑定函数，称为这个对象的方法:
如下：
var xiaoming = {
    name: '小明',
    birth: 2000,
    age: function () {
        var y = new Date().getFullYear();
        return y - this.birth;
    }
};
console.log(xiaoming.age());//20

当我们把函数getAge拆出来看：
function getAge() {
    var y = new Date().getFullYear();
    return y - this.birth;
}
var xiaoming={
    name: '小明',
    birth: 2000,
    age:gertAge
}
xiaoming.age(); // 20
getAge(); // NaN

this指向的始终是被调用的函数的那个对象


``` 
```
1.this变量如果以对象的方法形式调用该函数的this指向被调用的对象，如果单独调用函数，该函数的this指向全局对象;
2. 如果是call()、apply()、with()，指定的this是谁，就是谁，普通的函数调用，函数被谁调用，this就是谁。
可以这么理解：
Javascript 的 this 很花心，在哪个对象的家里，就是那个对象的。而 C，C++，C# 的 this 很专一，无论在哪，都属于原配！
并且 Javascript 的函数作用域 则像出生地，出生在哪里，出生地就是哪里！和运行环境无关
```
获取this变量的方法
```
用一个that变量捕获this：
var xiaoming = {
    name: '小明',
    birth: 1990,
    age: function () {
        var that = this; // 在方法内部一开始就捕获this
        function getAgeFromBirth() {
            var y = new Date().getFullYear();
            return y - that.birth; // 用that而不是this
        }
        return getAgeFromBirth();
    }
};
xiaoming.age(); // 25
```
```
用apply修复getAge()调用：

function getAge() {
    var y = new Date().getFullYear();
    return y - this.birth;
}

var xiaoming = {
    name: '小明',
    birth: 1990,
    age: getAge
};

xiaoming.age(); // 25
getAge.apply(xiaoming, []); // 25, this指向xiaoming, 参数为空

```

call()方法与apply类似，他们的区别是这样子的
```
1.apply()把参数打包成Array再传入
2.call()把参数按顺序传入

Math.max.apply(null, [3, 5, 4]); // 5
Math.max.call(null, 3, 5, 4); // 5
```