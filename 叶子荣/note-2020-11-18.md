# 2020-11-18 随堂笔记

## 1.Git合并冲突的问题
```
1.原因：直接修改了Fork下来的文件并提交
2.解决方法：（1）找出被修改的文件，并删除
           （2）在整个库文件下面添加进去原先未修改的文件之后提交即可。

```
## 2.拓展方法
```
图片如下
```
![图裂了](./imgs/imgs002.png)
![图裂了](./imgs/imgs003.png)

## 3.总结
```
1、CollectionExtension类要改成public以及static
```
![图裂了](./imgs/imgs004.png)
```
2、看图
```
![图裂了](./imgs/imgs005.png)
```
3、看图
```
![图裂了](./imgs/imgs006.png)
## 3.对于拓展方法的个人理解以及使用场景
理解以及使用场景：现在有一个机器，功能只有一个就是说话。我想给他加个新的功能吃饭，但是这个机器已经被封装好了我没有权限直接修改，想实现机器吃饭的功能就得另外给它加上去。这就是拓展方法。
注意：拓展方法和重写（override）完全是两个不同的东西
