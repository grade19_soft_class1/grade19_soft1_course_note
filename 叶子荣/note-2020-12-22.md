## 2020-12-22笔记
```
腰部情况好转，本来心情还⑧错。
但听胡哥说二班有转班的人称呼二班为你们班，说实话这沾多少就不用我多说了八🐮^^。你说你既然老惦记你那原来的班那还转啥班啊^^恶心坏了
还好自己班没有这些人 
```
## RegExp 正则表达式
给字符串定义一个规则如果字符串符合规则我们才会认为它匹配，否则该字符串则不合法。
```
\d:匹配一个数字
\w:匹配一个数字或者字母
'00\d' : 可以匹配00+数字 不能匹配 00+字符
'\d\d\d' : 可以匹配三个数字
'\w\w' : 可以匹配两个数字或者两个字母或者一个数字一个字母
. 可以匹配任意字符：'js.' 可以匹配 jss、jsp、js!
. 还有一种情况:
var res = /00./;
console.log(res.test('00sssss')); //结果为true
00. 后面还有字符输出结果依然是 true 意思是 . 后面可以任意字符任意长度？

用* 表示后面任意个字符包括0个
举个例子：
var res = /\d*/;
console.log(res.test(1)); //结果为true

用+表示至少一个字符
举个例子：
var res = /\d+/;
console.log(res.test(1)); //至少一个字符意思是只要有一个字符就能匹配结果一样为true

用?表示0个或1个字符
举个例子：
var res = /\w?/;
console.log(res.test('s')); //结果为true

用{n}表示n个字符
举个例子：
var res = /\d{5}/;
console.log(res.test('12345')); //'1234'为false因为要求为5个数字，但'123456'为true。意思是说至少要五个数字吗

用{n,m}表示n-m个字符
举个例子：
var res = /\d{3,5}/;
console.log(res.test('12345'));  //'12'false，'123456'为true 意思是说只要大于或者等于3个数字就可以了吗

\s可以匹配一个空格（也包括Tab等空白符），所以\s+表示至少有一个空格，例如匹配' '，'\t\t'等

```
## 进阶
要做更精确地匹配，可以用[]表示范围，比如：
```
[0-9a-zA-Z\_]可以匹配一个数字、字母或者下划线；

[0-9a-zA-Z\_]+可以匹配至少由一个数字、字母或者下划线组成的字符串，比如'a100'，'0_Z'，'js2015'等等；

[a-zA-Z\_\$][0-9a-zA-Z\_\$]*可以匹配由字母或下划线、$开头，后接任意个由一个数字、字母或者下划线、$组成的字符串，也就是JavaScript允许的变量名；

[a-zA-Z\_\$][0-9a-zA-Z\_\$]{0, 19}更精确地限制了变量的长度是1-20个字符（前面1个字符+后面最多19个字符）解释：可以匹配由字母或者下划线或者$开头后接19个任意数字字母以及_$

A|B可以匹配A或B，所以(J|j)ava(S|s)cript可以匹配'JavaScript'、'Javascript'、'javaScript'或者'javascript'。

^表示行的开头，^\d表示必须以数字开头。

$表示行的结束，\d$表示必须以数字结束。

你可能注意到了，js也可以匹配'jsp'，但是加上^js$就变成了整行匹配，就只能匹配'js'了。
```
## JavaScript中正则表达式的写法
写法有两种
```
一种是：
var res = /ABC\-001/;

还有一种是：
var res = new RegExp('/ABC\\-001/');

注意：如果使用第二种写法，因为字符串的转义问题，两个\\实际上是一个\

test()用来测试给定的字符串是否匹配
如匹配的话为true
如不匹配的话则为false
```
## 切分字符串
用正则表达式切分字符串比用固定的字符更灵活，请看正常的切分代码
```
var res = 'a b   c'.split(' '); // ['a', 'b', '', '', 'c']
console.log(res); 
无法识别连续的空格
```
用正则表达式试试
```
var res = 'a b   c'.split(/\s+/); // ['a', 'b', 'c']
console.log(res);
```
无论多少个空格都可以正常分割。加入,试试：
```
'a,b, c  d'.split(/[\s\,]+/); // ['a', 'b', 'c', 'd']
```
再加入;试试：
```
'a,b;; c  d'.split(/[\s\,\;]+/); // ['a', 'b', 'c', 'd']
```
## Json
JSON是JavaScript Object Notation的缩写，它是一种数据交换格式。
```
究竟是啥俺也整不太明白
```
## 序列化
让我们先把小明这个对象序列化成JSON格式的字符串
```
var xiaoming = {
    name: '小明',
    age: 14,
    gender: true,
    height: 1.65,
    grade: null,
    'middle-school': '\"W3C\" Middle School',
    skills: ['JavaScript', 'Java', 'Python', 'Lisp']
};
var s = JSON.stringify(xiaoming,null,' ') //这是加上参数，按缩进输出的结果并原来的结果好看一点
console.log(s); 
```
![图裂了](./imgs/imgs081.png)
第二个参数用于控制如何筛选对象的键值，如果我们只想输出指定的属性，可以传入Array
```
var s = JSON.stringify(xiaoming,['name','skills'],' ')
console.log(s);
```
![图裂了](./imgs/imgs082.png)
### 还可以传入一个函数，这样对象的每个键值对都会被函数先处理
```
function convert(key, value) {
    if (typeof value === 'string') {
        return value.toUpperCase();
    }
    return value;
}
```
上面的代码把所有属性值都变成大写
```
var res = JSON.stringify(xiaoming, convert, '  ');
console.log(res);
```
![图裂了](./imgs/imgs083.png)
### 如果我们还想要精确控制如何序列化小明，可以给xiaoming定义一个toJSON()的方法，直接返回JSON应该序列化的数据
```
var xiaoming = {
    name: '小明',
    age: 14,
    gender: true,
    height: 1.65,
    grade: null,
    'middle-school': '\"W3C\" Middle School',
    skills: ['JavaScript', 'Java', 'Python', 'Lisp'],
    toJSON: function () {
        return { // 只输出name和age，并且改变了key：
            'Name': this.name,
            'Age': this.age
        };
    }
};

console.log(JSON.stringify(xiaoming)); //{"Name":"小明","Age":14}
```
## 反序列化
拿到一个JSON格式的字符串，我们直接用JSON.parse()把它变成一个JavaScript对象
```
JSON.parse('[1,2,3,true]'); // [1, 2, 3, true]
JSON.parse('{"name":"小明","age":14}'); // Object {name: '小明', age: 14}
JSON.parse('true'); // true
JSON.parse('123.45'); // 123.45

JSON.parse()还可以接收一个函数，用来转换解析出的属性
```
```
var obj = JSON.parse('{"name":"小明","age":14}', function (key, value) {
    if (key === 'name') {
        return value + '同学';
    }
    return value;
});
console.log(JSON.stringify(obj)); // {name: '小明同学', age: 14}
```
## 面向对象编程
在学过的Java和C#里面面向对象的基本概念为
类：类是对象的模板，类本身是一种类型，例如定义了一个Student类，类表示学生类型但不表示某个具体的学生
实例：实例是根据类创建的对象，根据Student类创造出来的xiaoming，xiaohong，xiaojun，每个实例代表一个具体的学生
这是在大多数面向对象编程语言类和实例的概念
但在JavaScript中这个概念得换一换，JavaScript不区分类和实例的概念。
而是通过原型（prototype）来实现面向对象编程

原型是指当我们想要创建xiaoming这个具体的学生时，我们并没有一个Student类型可用。那怎么办？恰好有这么一个现成的对象
```
var robot = {
    name: 'Robot',
    height: 1.6,
    age: function () {
        console.log(this.name + ' is ning...');
    }
};
```
这个跟小明很像就直接用这个来创建小明对象
```
var Student = {
    name: 'Robot',
    height: 1.2,
    age: function () {
        console.log(this.name + ' is ning...');
    }
};

var xiaoming = {
    name: '小明'
};

xiaoming.__proto__ = Student;
```
注意最后一行代码把xiaoming的原型指向了对象Student，看上去xiaoming仿佛是从Student继承下来的
```
console.log(xiaoming.name); 小明
console.log(xiaoming.age()); 小明 is ning
```
xiaoming有自己的name属性，但并没有定义()方法。不过，由于小明是从Student继承而来，只要Student有()方法，xiaoming也可以调用
```
xiaoming-prototype
```
JavaScript的原型链和Java的Class区别就在，它没有“Class”的概念，所有对象都是实例，所谓继承关系不过是把一个对象的原型指向另一个对象而已

如果把xiaoming的原型指向其他对象
```
xiaoming.__proto__ = Bird;
```
```
var Bird = {
    fly: function () {
        console.log(this.name + ' is flying...'); //小明 is flying
    }
};

xiaoming.__proto__ = Bird; 
console.log(xiaoming.fly()); //undefind
console.log(xiaoming.age()); //undefind
现在xiaoming已经无法()了，他已经变成了一只鸟
```
在JavaScrip代码运行时期，你可以把xiaoming从Student变成Bird，或者变成任何对象

### 请注意，上述代码仅用于演示目的。在编写JavaScript代码时，不要直接用obj.__proto__去改变一个对象的原型，并且，低版本的IE也无法使用__proto__。Object.create()方法可以传入一个原型对象，并创建一个基于该原型的新对象，但是新对象什么属性都没有，因此，我们可以编写一个函数来创建xiaoming
```
var Student = {
    name: 'Robot',
    height: 1.2,
    : function () {
        console.log(this.name + ' is ning...');
    }
};

function createStudent(name) {
    // 基于Student原型创建一个新对象:
    var s = Object.create(Student);
    // 初始化新对象:
    s.name = name;
    return s;
}

var xiaoming = createStudent('小明');
xiaoming.(); // 小明 is ning...
xiaoming.__proto__ === Student; // true
```