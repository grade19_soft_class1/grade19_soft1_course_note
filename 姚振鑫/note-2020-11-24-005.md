 # 第五次课堂笔记

 # javaScript
 # 1.Number  
  javaScript 不区分整数和浮点数，统一用Number表示


 # 2.字符串
 字符串是以'或"包围起来的一串字符。

 3.布尔值和比较运算符
D:\grade19_soft1_course_note\姚振鑫\imgs\2020-11-24_171409.png

![图片](imgs/img11.png)
![图片](imgs/img10.png)

  false == 0; // true
  false === 0; // false 
 1.相等运算符==。JavaScript在设计时，有两种比较运算符：

  第一种是==比较，它会自动转换数据类型再比较，很多时候，会得到非常诡异的结果；

  第二种是===比较，它不会自动转换数据类型，如果数据类型不一致，返回false，如果一致，再比较。

  由于JavaScript这个设计缺陷，不要使用==比较，始终坚持使用===比较。

 2.NaN这个特殊的Number与所有其他值都不相等，包括它自己：
  唯一能判断NaN的方法是通过isNaN()函数：

# 3.isNaN(NaN); // true 
 浮点数的相等比较：
 1 / 3 === (1 - 2 / 3); // false
 这不是JavaScript的设计缺陷。浮点数在运算过程中会产生误差，因为计算机无法精确表示无限循环小数。要比较两个浮点数是否相等，只能计算它们之差的绝对值，看是否小于某个阈值：

 Math.abs(1 / 3 - (1 - 2 / 3)) < 0.0000001; // true
# 4.underfined和null
  null 表示为空 underfined表示为未定义。underfined和null区别意义不大。通常我们都用null,underfined仅仅在判断函数参数是否传递的情况下有有用。
  5.数组
  数组是一组按顺序排列的集合，集合的每个值称为元素。JavaScript的数组可以包括任意数据类型。例如：

 [1, 2, 3.14, 'Hello', null, true];
 上述数组包含6个元素。数组用[]表示，元素之间用,分隔。

 另一种创建数组的方法是通过Array()函数实现：

 new Array(1, 2, 3); // 创建了数组[1, 2, 3]
 然而，出于代码的可读性考虑，强烈建议直接使用[]。

 数组的元素可以通过索引来访问。请注意，索引的起始值为0：

 var arr = [1, 2, 3.14, 'Hello', null, true];
 arr[0]; // 返回索引为0的元素，即1
 arr[5]; // 返回索引为5的元素，即true
 arr[6]; // 索引超出了范围，返回undefined

![图片](imgs/img9.png)
![图片](imgs/img11.png)
# 5.对象
   javaScript的对象是一组有键-值组成的无序集合
   var person=
   {
    name:"Bob",
    age:20, 
    tags:["js","wed","mobile "],
    city:"Beijing",
    hasCar:true,
    zipcode:null,
   };
   如何获取一个对象的属性，我们用对象变量，属性名的方式：
   person.name;//"Bob"           person.zipcode;//null
   #  8.变量
   声明一个变量用var语句
   var a; 此时的a的值为underfined
   var $b=1;

    动态变量 
   变量本身类型不固定的语言称之为动态语言

    静态变量
    在定义变量时必须指定变量类型，如赋值的时候不匹配，就会报错
  # 9.strict模式
   为了修补JavaScript这一严重设计缺陷，ECMA在后续规范中推出了strict模式，在strict模式下运行的JavaScript代码，强制通过var申明变量，未使用var申明变量就使用的，将导致运行错误。
   



