# 第十五次课堂笔记

# 高阶函数

写一个简单的高阶函数：


      function add(x,y,f){
        return f(x)+f(y);
     }



编写高阶函数，就是让函数的参数能够接收别的函数。
      "use strict";
      function add (x,y,f){
       return f(x) +f(y);
      }
      var x= add(-5,6,Math.abs);//11
      console.log(x);

