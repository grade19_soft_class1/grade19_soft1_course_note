# 哈！笔记一周月啦！！！掌声鼓励！！！！！
## 今天主要讲了什么呢！
## 没错！就是我们最熟悉的箭头函数！！！

```
    这手操作不就及其细节啦！！！

    箭头函数就相当于函数中的匿名函数！
    这种的话非常方便的简化了函数的格式！！
    如果是一个函数的话根本就不用括号！就一行就结束了！

    x => x*x ; 
    没错！这就是一个函数！！！
    它的完整体是
    function (x) {
    return x * x;
    }
```
```
    如果表达是含多种语句就要有大括号阔起来！！！！
    不然会出问题！
    x => {
        if(x>0){
            return x* x;
        }
        else{
            return - x * x ;
        }
    }

```
```
    而且如果参数不止一个的话还要用括号括起来！

    (x,y)=> 2*x+y-x

    箭头函数的话没有参数也是可以的！
    但一般都是立即调用的函数！！
    ()=>123;
    还有一个就是变化参数！！！
    这个也是不同的！
    (x,y,...rest)=>{
        var i ;
        sum = x+y;
        for(i=0;i<rest.length;i++){
            sun+=rest[i];
        }
        return sum;
    }
```

```
     还有箭头函数和this的关系！！！
     箭头函数中的this直接就是词法作用域！！而且是根据上下文来定义的！！！他会直接指向！！！而且还可以直接调用起来！！！


    'use strict'

    var obj = {
    birth: 1990,
    getAge: function () {
        var b = this.birth;
        var fn = () => new Date().getFullYear() - this.birth;
        return fn();
    }
    };
    console.log(obj.getAge());//30
    
    就算给他个call
    var obj = {
    birth: 1990,
    getAge: function (year) {
        var b = this.birth;
        var fn = (y) => y - this.birth;
        return fn.call({birth:2000}, year);
    }
    };
    console.log(obj.getAge(2020));//30
    结果也是不变的！！！！！！！
```
#  拜拜呀！！！！！！！！