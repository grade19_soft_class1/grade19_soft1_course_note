## 笔记

## 11.19

# 主要内容(C#)

```
一、
递归

可以通过调用自身的形式执行代码循环
练习
//在类中的方法
public static int Recursion(int a)
{
    if (a == 0)
    {
        return 0;
    }
    else if (a == 1)
    {
        return 1;
    }
    else
    {
        return Recursion(a - 2) + Recursion(a - 1);
    }
}

//在主函数中输出
var re = Console.ReadLine();
var num = int.Parse(re);
var n = RecursionClass.Recursion(num);
Console.WriteLine(n);

```


```
二、
分部类

1.定义方法
访问修饰符   修饰符   partial class   类名{……}

2.内容
部分类主要用于当一个类中的内容较多时将相似类中的内容拆分到不同的类中，并且部分类的名称必须相同。

```


```
三、
日期时间

DateTime dt1 = DateTime.Now;//获取当前时间
DateTime dt2 = new DateTime(2019, 6, 1);//设置一个时间
TimeSpan ts = dt2 - dt1;//两个时间的时间间隔
Console.WriteLine("间隔的天数为{0}天", ts.Days);//输出
```


```
四、
字符串

1.
获取字符串长度（string.Length）

2.
IndexOf和LastIndexOf：查找字符串中的字符

3.
Replace：字符串替换

4.
Substring：字符串截取

5.
Insert：字符串插入

//练习
1.字符串反转
var s = Console.ReadLine();

char[] arr = s.ToArray();

var arrRe=arr.Reverse();

var sum="";

foreach (var str in arrRe)
{
    sum += str;
}

Console.WriteLine(sum);
```


```
五、
数据类型转换

1.
分为隐式类型转换和显式类型转换

显示类型转换为强转换
方法：
数据类型变量名 = (数据类型)  变量名或值


2.字符串类型转换
Parse方法：
数据类型.Parse(字符串类型的值)

3.数据类型转换
Convert方法：
convert.To数据类型(变量名)

```


```
六、
正则表达式

主要作用：
验证字符串的值是否满足一定的规则
```





