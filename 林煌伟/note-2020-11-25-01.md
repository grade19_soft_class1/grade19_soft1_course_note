## 笔记

## 2020.11.25

# 主要内容(JavaScript)

## 一、
```
    讲解了全局变量 (详情见24号笔记)
```

## 二、
```
    多行字符串
        
    两种使用方法
    1.使用反引号
    var a = `
    第一行，真不错

    第二行，很有精神，

    第三行，年轻人不讲武德

    第四行，我劝你耗子尾汁
    `

    console.log(a);
        
```
![图片出错了！](./imgs/Text1-2020.11.25.png)

```
    2.使用\n
    var b = '一念永恒\n大主宰\n完美世界';

    console.log(b);

```
![图片出错了！](./imgs/Text2-2020.11.25.png)

## 三、
```
    模板字符串（只能在多行字符串中使用）(使用``的方式使用)

    通过${}的方式将变量中的字符串添加到字符串中

    var str = {
    name : "小明",
    age : 12,
    sex : '男'
    }

    var s = `${str.name}，你好

    今年你${str.age}岁了
    `

    console.log(s);
    
```
![图片出错了！](./imgs/Text3-2020.11.25.png)

## 四、
```
    操作字符串
    
    要获取字符串某个指定位置的字符，使用类似Array的下标操作

    对于字符串中的字符进行单独修改不会报错，但是字符串不会被修改。
    var s = '大梦千年';
    s[0] = '一';

    alert(s)

    结果还是"大梦千年"

```

## 五、
```
    toUpperCase（将字符串中的字母全部转换成大写）（对于中文无效）

    var te = 'Hello Word';

    console.log(te.toUpperCase());
    


    toLowerCase（将字符串中的字母全部转换成小写）（对于中文无效）


```

## 六、
```
    indextOf 方法
    搜索指定字符串出现的位置

    var te = 'Hello Word';

    console.log(te.indexOf('Word'));//搜索Word字符串

    结果为 6 ，如果没有找到指定字符串结果为-1; 
```

## 七、
```
    substring 方法
    返回指定区域的字串内容

    var te = 'Hello Word';

    console.log(te.substring(1,6));//返回1到6的子串（不包括6）

    console.log(te.substring(2));//从2开始到结束

```


