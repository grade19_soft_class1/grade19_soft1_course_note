1、 找出元素 item 在给定数组 arr 中的位置 输出描述: 如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
```
 let arr = [1, 2, 3, 4 ];
        var item = arr.indexOf(3);
        if(item)
        {
            console.log(item);
        }
        else{
            console.log(-1);
        }
```
![](./images/31.png)

2、 计算给定数组 arr 中所有元素的总和 输入描述: 数组中的元素均为 Number 类型
```
function Sumout(arr) {
    var sum = 0;
    arr.forEach((item) => {
        sum += item;
    })
    return sum;
}
    console.log(Sumout([ 1, 1, 1, 1 ])); 

```
![](./images/32.png)

3、 移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
```
function noxiugai(arr, item) {
    let Arr = [];
    for(let i in arr){
        if(arr[i] != item){
            Arr.push(arr[i])
        }
    }
    return Arr;
}
console.log(noxiugai([1, 2, 3, 4, 2], 2)); 
```
![](./images/33.png)

4、 移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
```
function xiugai(arr, item) {
    for(var i = 0; i< arr.length; i++) {
        if(arr[i]===item) {
            arr.splice(i,1);
            i--;
        }
    }
    return arr;
}

console.log(xiugai([1,2,2,3,4,2,2],2));
```
![](./images/34.png)

5、在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
```
let arr = [1, 2, 3, 4];
console.log("原数组：" + arr);
arr.push(10);
console.log("新增item：" + arr);
```
![](./images/35.png)

6、删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
```
let arr = [2, 3, 1, 9, 7, 5, 8, 22];
console.log(arr);
console.log(arr.pop());
console.log(arr);
```
![](./images/25.png)

7、在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
```
let arr = [2, 3, 1, 9, 7, 5, 8, 22];
 
console.log(arr);
arr.unshift("new1");
console.log(arr);
```
![](./images/36.png)

8、删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
```
let arr = [2, 3, 1, 9, 7, 5, 8, 22];
console.log(arr);
arr.shift();
console.log(arr);
```
![](./images/27.png)

9、合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
```
let arr = [2, 3, 1, 9, 7, 5, 8, 22];
let arr2 = ['这是arr2'];
console.log(arr.concat(arr2)); 
```
![](./images/23.png)

10、在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
```
let arr = [2, 3, 1, 9, 7, 5, 8, 22];
console.log(arr);
console.log(arr.splice(2, 3, "这是新添加的元素"));
console.log(arr);
```
![](./images/30.png)


11、统计数组 arr 中值等于 item 的元素出现的次数
```
let arr1 = [1,1,1,1,1,1,1,2,3,4,5];
let item=1;
let  count=0;
for(i=0;i<arr1.length;i++){
    if(arr1[i]==item){
        count++
    }
}
console.log(count);
```

12、找出数组 arr 中重复出现过的元素
```

```

13、数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
```
```

14、在数组 arr 中，查找值与 item 相等的元素出现的所有位置
```

```