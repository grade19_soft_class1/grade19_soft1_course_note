# 11月21号的课堂笔记

## 一、SSL证书申请流程图
![无法显示图片](./imgs/SSL证书.png)

## 二、把备案号放在网站底部
```
<p><a href="https://beian.miit.gov.cn/#/Integrated/index">赣ICP备2020013255号</a></p>
```
## 三、Javascript
    1.Javascript是啥：JavaScript是世界上最流行的脚本语言。
    2.为啥要学Javascript：在Web里，只有JavaScript能跨平台、跨浏览器驱动网页，与用户交互。
    3.啥是弱类型语言：Javascript作为弱类型语言可以定义多个同名的类型或者方法。
    4.JavaScript严格区分大小写。
### 在HTML中使用JavaScript
    1.把代码写到<script>...</script>标签中，将标签插入到网页中。
    2.<script src="...">在src属性中指定.js文件。
#### 嵌入式    
```
    <body>
        <script>
            alert("hello word");//alert方法能弹出一个提示框
        </script>
    </body>
```