# 今天是12月19号
## generator 生成器

生成器就是一个可以多次返回的函数

``` JavaScript
    function* foo(x) {
    yield x + 1;
    yield x + 2;
    return x + 3;
}
```
调用时可以用foo(3).next()或者使用for of 循环
```JavaScript
function* foo(x) {
    yield x + 1;
    yield x + 2;
    return x + 3;
}

for(let item of foo(3)){
    console.log(item);
}
```
### 练习
要生成一个自增的ID，可以编写一个next_id()函数：
```JavaScript
var current_id = 0;

function next_id() {
    current_id ++;
    return current_id;
}
```
由于函数无法保存状态，故需要一个全局变量current_id来保存数字。

不用闭包，试用generator改写：
```JavaScript
'use strict';
function* next_id() {
    let current_id = 0;
    for(let x= 1;x<100;x++){
        yield ++current_id; 
    }
}

// 测试:
var
    x,
    pass = true,
    g = next_id();
for (x = 1; x < 100; x ++) {
    if (g.next().value !== x) {
        pass = false;
        console.log('测试失败!');
        break;
    }
}
if (pass) {
    console.log('测试通过!');
}
```
## 对象的类型
但是某些对象还是和其他对象不太一样。为了区分对象的类型，我们用typeof操作符获取对象的类型，它总是返回一个字符串：
``` JavaScript
typeof 123; // 'number'
typeof NaN; // 'number'
typeof 'str'; // 'string'
typeof true; // 'boolean'
typeof undefined; // 'undefined'
typeof Math.abs; // 'function'
typeof null; // 'object'
typeof []; // 'object'
typeof {}; // 'object'
```
可见，number、string、boolean、function和undefined有别于其他类型。特别注意null的类型是object，Array的类型也是object，如果我们用typeof将无法区分出null、Array和通常意义上的object——{}。
## 包装对象
包装对象用new创建
```javaScript
    var a = new Number(123);
    var b = new String('123');
```
包装对象的类型是object

如果包装对象时没有用new就会是一个普通函数，将变量转换成定义的类型
```
    var n = Number(123); //就相当于是parseInt
```
## 重点来了
+ 包装对象能别用就尽量别用就行

+ 用parseInt()或parseFloat()来转换任意类型到number

+ 用String()来转换任意类型到string，或者直接调用某个对象的toString()方法

+ 通常不必把任意类型转换为boolean再判断，因为可以直接写if (myVar) {...}

+ typeof操作符可以判断出number、boolean、string、function和undefined

+ 判断Array要使用Array.isArray(arr)；

+ 判断null请使用myVar === null；

+ 判断某个全局变量是否存在用typeof window.myVar === 'undefined'

+ 函数内部判断某个变量是否存在用typeof myVar === 'undefined'
## Date
要获取系统当前时间，用：
```javaScript
var now = new Date();
now; // Wed Jun 24 2015 19:49:22 GMT+0800 (CST)
now.getFullYear(); // 2015, 年份
now.getMonth(); // 5, 月份，注意月份范围是0~11，5表示六月
now.getDate(); // 24, 表示24号
now.getDay(); // 3, 表示星期三
now.getHours(); // 19, 24小时制
now.getMinutes(); // 49, 分钟
now.getSeconds(); // 22, 秒
now.getMilliseconds(); // 875, 毫秒数
now.getTime(); // 1435146562875, 以number形式表示的时间戳
```
注意，当前时间是浏览器从本机操作系统获取的时间，所以不一定准确，因为用户可以把当前时间设定为任何值。

如果要创建一个指定日期和时间的Date对象，可以用：
```
var d = new Date(2015, 5, 19, 20, 15, 30, 123);
d; // Fri Jun 19 2015 20:15:30 GMT+0800 (CST)
```
值得注意的是月份表示的范围是0~11，所以要表示6月份需要传入5

## 时区
Date对象表示的时间总是按浏览器所在时区显示的，不过我们既可以显示本地时间，也可以显示调整后的UTC时间：
```javaScript
var d = new Date(1435146562875);
d.toLocaleString(); // '2015/6/24 下午7:49:22'，本地时间（北京时区+8:00），显示的字符串与操作系统设定的格式有关
d.toUTCString(); // 'Wed, 24 Jun 2015 11:49:22 GMT'，UTC时间，与本地时间相差8小时
```
那么在JavaScript中如何进行时区转换呢？实际上，只要我们传递的是一个number类型的时间戳，我们就不用关心时区转换。任何浏览器都可以把一个时间戳正确转换为本地时间。

时间戳是个什么东西？时间戳是一个自增的整数，它表示从1970年1月1日零时整的GMT时区开始的那一刻，到现在的毫秒数。假设浏览器所在电脑的时间是准确的，那么世界上无论哪个时区的电脑，它们此刻产生的时间戳数字都是一样的，所以，时间戳可以精确地表示一个时刻，并且与时区无关。

所以，我们只需要传递时间戳，或者把时间戳从数据库里读出来，再让JavaScript自动转换为当地时间就可以了。

要获取当前时间戳，可以用：
```javaScript
'use strict';
console.log(Date.now());
```
## 练习
小明为了和女友庆祝情人节，特意制作了网页，并提前预定了法式餐厅。小明打算用JavaScript给女友一个惊喜留言：
```javaScript
'use strict';
var today = new Date();
if (today.getMonth() === 2 && today.getDate() === 14) {
    alert('亲爱的，我预定了晚餐，晚上6点在餐厅见！');
}
```

结果女友并未出现。小明非常郁闷，请你帮忙分析他的JavaScript代码有何问题。

JavaScript中2月份用1表示