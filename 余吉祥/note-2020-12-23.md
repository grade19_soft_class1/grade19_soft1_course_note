# 12月23号的笔记

## 今天了解了一下面向对象编程

## 原型链
``` JavaScript
'use strict';

var Student = {
    name : 'xiaoming',
    age : 18 ,
    fn:function(){
        console.log(this.name + this.age);
    }
}

var xiaohong = {
    name : '小红'
}

xiaohong.__proto__=Student;//将小红指向Student

xiaohong.fn();
```
+ 将xiaohong指向Student后xiaohong就可以调用fn方法，反过来则不行，Student就是xiaohong的原型
+ arr的原型是Array.prototype,indexOf(),shift(),都是定义在Array.prototype中的因此所有数组都可以调用
+ function的原型是Function.prototype由于Function.prototype定义了apply()等方法，因此，所有函数都可以调用apply()方法。
## 构造函数
``` JavaScript
var xiaohong =function(name){
    this.name = name;
    this.Hello = console.log('Hello'+this.name+'!');
}


var d = new xiaohong('小红');
```
写了new之后this指向新创建的对象，并默认返回return this