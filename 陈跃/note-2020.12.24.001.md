# 知道3月21号开学的我眼泪掉下来🤣

# 浏览器
目前主流的浏览器分这么几种：
+ IE 6~11：国内用得最多的IE浏览器，历来对W3C标准支持差。从IE10开始支持ES6标准；
+ Chrome：Google出品的基于Webkit内核浏览器，内置了非常强悍的JavaScript引擎——V8。由于Chrome一经安装就时刻保持自升级，所以不用管它的版本，最新版早就支持ES6了；
+ Safari：Apple的Mac系统自带的基于Webkit内核的浏览器，从OS X 10.7 Lion自带的6.1版本开始支持ES6，目前最新的OS X 10.11 El Capitan自带的Safari版本是9.x，早已支持ES6；
+ Firefox：Mozilla自己研制的Gecko内核和JavaScript引擎OdinMonkey。早期的Firefox按版本发布，后来终于聪明地学习Chrome的做法进行自升级，时刻保持最新；


# 浏览器对象

## 1.window
1. window对象有innerWidth和innerHeight属性，可以获取浏览器窗口的内部宽度和高度。内部宽高是指除去菜单栏、工具栏、边框等占位元素后，用于显示网页的净宽高
```
console.log('window inner size: ' + window.innerWidth + ' x ' + window.innerHeight);
```
2. 还有一个outerWidth和outerHeight属性，可以获取浏览器窗口的整个宽高
```
console.log('window outer size: ' + window.outerWidth + ' x ' + window.outerHeight);
```
![](./img/50.png)

## navigator
navigator对象表示浏览器的信息
```
console.log('appName = ' + navigator.appName); //浏览器名称
console.log('appVersion = ' + navigator.appVersion); //浏览器版本
console.log('language = ' + navigator.language); //浏览器设置的语言
console.log('platform = ' + navigator.platform); //操作系统类型
console.log('userAgent = ' + navigator.userAgent); //浏览器设定的User-Agent字符串
```
![](./img/51.png)

## screen
screen对象表示屏幕的信息
```
console.log(screen.width); //屏幕宽度，以像素为单位
console.log(screen.height); //屏幕高度，以像素为单位
console.log(screen.colorDepth); //返回颜色位数
```

## location
location对象表示当前页面的URL信息
要获得URL各个部分的值，可以这么写：
```
location.protocol; // 'http'
location.host; // 'www.example.com'
location.port; // '8080'
location.pathname; // '/path/index.html'
location.search; // '?a=1&b=2'
location.hash; // 'TOP'
要加载一个新页面，可以调用location.assign()。如果要重新加载当前页面，调用location.reload()方法非常方便。
```

## document
document对象表示当前页面。由于HTML在浏览器中以DOM形式表示为树形结构，document对象就是整个DOM树的根节点。
```
var menu = document.getElementById('drink-menu');
var drinks = document.getElementsByTagName('dt');
console.log(menu);
console.log(drinks);
var i, s;
s = '提供的饮料有:';
for (i=0; i<drinks.length; i++) {
    s = s + drinks[i].innerHTML + ',';
}
console.log(s);
```
![](./img/52.png)
### ⚠要注意的是![](./img/53.png)

## history
history对象保存了浏览器的历史记录，JavaScript可以调用history对象的back()或forward ()，相当于用户点击了浏览器的“后退”或“前进”按钮。

## 操作DOM
![](./img/54.png)
![](./img/555.png)
![](./img/56.png)