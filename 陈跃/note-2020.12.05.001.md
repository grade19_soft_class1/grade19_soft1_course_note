# 12月份的第四次笔记

## 开始先给我们介绍了web的相关内容,并给我们看了码云上某位大神的笔记
## 再给我们介绍了前端和后端等等等等等
## 然后今天上课的内容有点少,讲的知识点很少,但有点难以理解

# 变量的作用域
## 局部变量的作用域:在函数内部定义的变量称为局部变量，其作用域为该函数内部，在该函数外部不能被访问
## 全局变量的作用域:定义在函数外部的变量称为全局变量，其作用域是整个JavaScript代码块

```
var name = "小明";
function setName(){
    name = "小红";   //去掉var变成了name全局变量
}
setName();
console.log(name);//输出小红
```

```
var name = "小明";
function setName(name){    //通过传参，也是局部变量
    console.log(name);
}
setName("小红");   //输出小红
console.log(name);//输出小明
```

# 变量的提升
## JavaScript的函数定义有个特点，它会先扫描整个函数体的语句，把所有申明的变量“提升”到函数顶部

```
console.log(v1);      //输出undefined
var v1 = 100;
function foo() {
    console.log(v1);  //输出undefined
    var v1 = 200;
    console.log(v1);  //输出200
}
foo();
console.log(v1);      //输出100
```

## ⚠总结:无论作用域中的声明出现在什么地方，都将在代码本身被执行前首先进行处理，可以将这个过程形象地想象成所有的声明（变量和函数）都会被“移动”到各自作用域的最顶端，这个过程被称为提升。

# 名字空间
## 名字空间的最大好处,就是避免名字冲突了
```
var wa = new Object();
wa.name = 'Jack';
wa.job = 'Student';
wa.sayHello = function(){console.log('Hello my name is '+this.name);} //输出Hello my name is Jack
wa.sayHello();
wa.Student = function(name,age){
	this.name = name;
	this.age = age;
}
wa.Student.prototype.sayHello = function(){console.log('我的名字叫'+this.name+",今年"+this.age+"岁！"); } //输出我的名字叫小明,今年13岁！
var stu = new wa.Student('小明',13);
stu.sayHello();  
```
