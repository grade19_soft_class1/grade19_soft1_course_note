## 单纯敲代码的一天
### 一、操作DOM
* 1.修改Text和HTML
    * 无参数调用text()是获取文本
    * 传入参数就变成设置文本

* 2.修改CSS:css()方法和addClass()方法
```
var div = $('#test-div');
div.css('color'); // '#000033', 获取CSS属性
div.css('color', '#336699'); // 设置CSS属性
div.css('color', ''); // 清除CSS属性
```

**练习：分别用css()方法和addClass()方法高亮显示JavaScript：**
```
<style>
.highlight {
    color: #dd1144;
    background-color: #ffd351;
}
</style>
<div id="test-highlight-css">
            <ul>
                <li class="py"><span>Python</span></li>
                <li class="js"><span>JavaScript</span></li>
                <li class="sw"><span>Swift</span></li>
                <li class="hk"><span>Haskell</span></li>
            </ul>
        </div>

<script>
        var d =$('#test-highlight-css');
        // d.css('backgroundColor','yellow');
        d.addClass('highlight');
</script>
```

* 3.显示和隐藏DOM:show()和hide()方法
```
var a = $('a[target=_blank]');
a.hide(); // 隐藏
a.show(); // 显示
```

> 隐藏DOM节点并未改变DOM树的结构,它只影响DOM节点的显示。这和删除DOM节点是不同的。

* 4.获取DOM信息
attr()和removeAttr()方法用于操作DOM节点的属性
```
// <div id="test-div" name="Test" start="1">...</div>
var div = $('#test-div');
div.attr('data'); // undefined, 属性不存在
div.attr('name'); // 'Test'
div.attr('name', 'Hello'); // div的name属性变为'Hello'
div.removeAttr('name'); // 删除name属性
div.attr('name'); // undefined
```

> 总结:attr()一个参数时判断属性是否存在,两个参数时,修改属性的值


**区分**
attr()和prop()类似
```
var radio = $('#test-radio');
radio.is(':checked'); // true
```
> 判断属性checked、selected用prop()更合理

* 5.操作表单
   * val()方法获取和设置对应的value属性：

> val()没有传入参数时表示获取值
> 一个参数表示修改值
> 一个val()就统一了各种输入框的取值和赋值的问题。


### 二、修改DOM结构
* 1.添加新的DOM节点: append()方法
```
var ul = $('#test-div>ul');
ul.append(<li><span>Haskell</span></li>);
```

* 2.删除节点: remove()方法
```
var li = $('#test-div>ul>li');
li.remove(); // 所有<li>全被删除
```
### 三、事件
jQuery能够绑定的事件主要包括：

<mark>鼠标事件</mark>


|属性       |                       情景                                  |
|   ---     |                       ---                                      |
|click:     |鼠标单击时触发；                                               
|dblclick： |鼠标双击时触发；                                                 |
|mouseenter|鼠标进入时触发；                                                    |
|mouseleave|鼠标移出时触发；                                                    |
|mousemove：| 鼠标在DOM内部移动时触发；                                       |           
|hover：    | 鼠标进入和退出时触发两个函数,相当于mouseenter加上mouseleave。  |

<mark>键盘事件</mark>

键盘事件仅作用在当前焦点的DOM上,通常是`<input>`和`<textarea>`。

|  属性     |   情景     |
|  ---    |      ---    |
|keydown：键盘按下时触发；|
|keyup：键盘松开时触发；   |
|keypress：按一次键后触发|

<mark>其他事件</mark>

|属性     |         情景                                    |
|   ---   |             ---                                 |
| focus： |当DOM获得焦点时触发；                               | 
| blur：  |当DOM失去焦点时触发；                               |             
| change：|当`<input>、<select>或<textarea>`的内容改变时触发；   |             
| submit：|当`<form>`提交时触发；                                |                 
| ready： |当页面被载入并且DOM树完成初始化后触发。             |     

```
<form id="testForm"></form>
<script>
$(document).ready(function () {
    // on('submit', function)也可以简化:
    $('#testForm).submit(function () {
        alert('submit!');
    });
});
</script>
```

> 遇到$(function () {...})的形式,牢记这是document对象的ready事件处理函数。