## 这么多天没喝奶茶了 我不甜了 <img src="imgs/dog1.jpg" width="35" height="35" align="center">
#### 闭包强大的功能
##### 创建计数器 
```
//   initial(最初的/第一个)
//   关于 '|' 与 '||' 符号
//  '|' 对不对都会执行一下后面的;'||' 只要有定义就不会执行后面的

    function create_counter(initial) {
        let x= initial || 0;
        return  {
            inc : function() {
                x += 1;
                return x;
            }
        }
    }
    let c1=create_counter();
    console.log(c1.inc()); //1
    console.log(c1.inc()); //2
    console.log(c1.inc()); //3 
    
    let c2= create_counter(10);
    console.log(c2.inc()); // 11
    console.log(c2.inc()); // 12
    console.log(c2.inc()); // 13 
```

在返回的对象中,实现了一个闭包,该闭包携带了局部变量x,且从外部代码根本无法访问到变量x。
换句话说,闭包就是携带状态的函数,并且它的状态可以完全对外隐藏起来。



#### 箭头函数---不同参数 
* 两个参数
```
  (x,y) => x*x + y*y;
```
  
* 无参
```
() => 3.14;
```

* 可变参数    
```     
(x,y,...rest) => {
    var i,sum = x+y;
    for (i=0;i<rest.length;i++) {
        sum += rest[i];
    }
    return sum;
} 
```

PS:要注意返回的是一个对象,如果是单表达式需要用括号包起来;否则会和函数结构体{...}有冲突。
```
   x => ( {foo: x} );
```

∵ this在箭头函数中已经按照**词法作用域**绑定了,
∴ 用call()或者apply()调用箭头函数时,无法对this进行绑定,即传入的第一个参数被忽略: 
```
  var obj = {
    birth: 1990,
    getAge: function (year) {
        var b = this.birth; // 1990
        var fn = (y) => y - this.birth; // this.birth仍是1990
        return fn.call({birth:2000}, year);
    }
  };
  obj.getAge(2015); // 25  
```

###### PS:关于词法作用域除此之外还牵扯出来的有关变量、函数、闭包的问题。

#### 练习 请使用箭头函数简化排序时传入的函数:
```
    let arr =[10,20,1,2];
    arr.sort( (x,y) => {
        return x - y;
    });
    console.log(arr); //[1,2,10,20]
```
