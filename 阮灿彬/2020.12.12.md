### map
```
function pow(x){
    return x * x ;
}

var arr = [1,2,3,4,5,6,7,8,9];
var result = arr.map(pow)
console.log(result);
```
![qwe](./photo/2020-12-15_160807.png)
### reduce Array的reduce()把一个函数作用在这个Array的[x1, x2, x3...]上，这个函数必须接收两个参数，reduce()把结果继续和序列的下一个元素做累积计算
```
用reduce求和
var arr = [1,2,3,4,5];
var pow =  arr.reduce(function(x,y){
    return x + y;
})
console.log(pow);
```
![qwe](./photo/2020-12-15_161440.png)
小明希望利用map()把字符串变成整数，他写的代码很简洁：
```
'use strict';

var arr = ['1', '2', '3'];
var r;
r = arr.map(parseInt);

console.log(r);
```
结果竟然是1, NaN, NaN，小明百思不得其解，请帮他找到原因并修正代码。
```
原因：parseInt方法只有字符串中的第一个数字会被返回。

修正:
'use strict'

let a = ['1' , '2' , '3'];

function c(x){
    return x.map(Number);
}


console.log(c(a));
```
## filter filter也是一个常用的操作，它用于把Array的某些元素过滤掉，然后返回剩下的元素。
```
'use strict'

var arr = [12,48,9,53];
arr.sort(function (x,y){
    if(x<y){
        return -1;
    }
    if(x>y){
        return 1;
    } 
    return 0;
})
console.log(arr);
```
![qwe](./photo/2020-12-15_170401.png)