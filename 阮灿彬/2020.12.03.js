'use strict'
// function foo(x) {
//     console.log('x = ' + x); // 10
//     for (var i=0; i<arguments.length; i++) {
//         console.log('arg ' + i + ' = ' + arguments[i]); // 10, 20, 30
//     }
// }
  foo(10, 20, 30);

//  function abs() {
//     if (arguments.length === 0) {
//         return 0;
//     }
//     var x = arguments[0];
//     return x >= 0 ? x : -x;
// }

// abs(); // 0
// abs(10); // 10
// abs(-9); // 9

function foo(a, b) {
    var i, rest = [];
    if (arguments.length > 2) {
        for (i = 2; i<arguments.length; i++) {
            rest.push(arguments[i]);
        }
    }
    console.log('a = ' + a);
    console.log('b = ' + b);
    console.log(rest);
}