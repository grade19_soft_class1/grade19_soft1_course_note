# 2020-11-28笔记内容
## 1、多维数组

### 多维数组
1.  当数组中的某个元素又是一个Array，则可形成多维数组，列如：

    ```
    var arr = [[1, 2, 3], [400, 500, 600], '-'];
    ```

-   练习：如何通过索引取到500这个值：

    ```
    我的答案：
    var arr=[[1, 2, 3], [400, 500, 600], '-'];
    var x=arr[1][1];
    console.log(x);
    ```
    
2.  练习：在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：
    ```
    练习示列：
    'use strict';
    var arr = ['小明', '小红', '大军', '阿黄'];
    console.log('???');
    ```

    ```
    我的答案：
    var arr=['小明','小红','大军','阿黄'];
    var str='';
    for(var i=0;i<arr.length;i++){
        if(i===arr.length-2)
        {
            str = str + arr[i] + '和' + arr[i+1];
            break;
        }
        else 
        {
            str=str+arr[i]+',';
        }
    }
    console.log('欢迎'+str);
    ```

### 对象
1.  JavaScript的对象是一种无序的集合数据类型，它由若干键值对组成，JS 当中对象的键是由字符串型式构成的
    ```
    创建一个对象：
    var stu={
        name:'小张',
        age:20,
        highschool:'No.1_Middol_Schllo',
        sex:'男'
    };
    ```
    JavaScript用一个{...}表示一个对象，键值对以xxx: xxx形式申明，用,隔开。注意，最后一个键值对不需要在末尾加,，如果加了，有的浏览器（如低版本的IE）将报错
    ```
    获取对象属性值：
    var name =stu.name;
    var age=stu.age;
    ```
    访问属性是通过.操作符完成的，但这要求属性名必须是一个有效的变量名。如果属性名包含特殊字符，就必须用''括起来：
    ```
    var stu={
        name:'小张',
        'high-school':'No.1_Middol_Schllo'     //就像这样
    };
    ```
    ```
    stu的属性名high-school不是一个有效的变量，就需要用''括起来。访问这个属性也无法使用.操作符，必须用['xxx']来访问：
    stu["high-school"];
    ```
    也可以用stu['name']来访问stu的name属性，不过stu.name的写法更简洁。我们在编写JavaScript代码的时候，属性名尽量使用标准的变量名，这样就可以直接通过object.prop的形式访问一个属性了。

    实际上JavaScript对象的所有属性都是字符串，不过属性对应的值可以是任意数据类型。

    由于JavaScript的对象是动态类型，你可以自由地给一个对象添加或删除属性：
    ```
    var xiaoming = {
        name: '小明'
    };
    xiaoming.age; // undefined
    xiaoming.age = 18; // 新增一个age属性
    xiaoming.age; // 18
    delete xiaoming.age; // 删除age属性
    xiaoming.age; // undefined
    delete xiaoming['name']; // 删除name属性
    xiaoming.name; // undefined
    delete xiaoming.school; // 删除一个不存在的school属性也不会报错
    ```
    如果我们要检测xiaoming是否拥有某一属性，可以用in操作符：
    ```
    var xiaoming = {
        name: '小明',
        birth: 1990,
        school: 'No.1 Middle School',
        height: 1.70,
        weight: 65,
        score: null
    };
    'name' in xiaoming; // true
    'grade' in xiaoming; // false
    ```
    不过要小心，如果in判断一个属性存在，这个属性不一定是xiaoming的，它可能是xiaoming继承得到的：
    ```
    'toString' in xiaoming; // true
    ```
    因为toString定义在object对象中，而所有对象最终都会在原型链上指向object，所以xiaoming也拥有toString属性。

    要判断一个属性是否是xiaoming自身拥有的，而不是继承得到的，可以用hasOwnProperty()方法：
    ```
    var xiaoming = {
        name: '小明'
    };
    xiaoming.hasOwnProperty('name'); // true
    xiaoming.hasOwnProperty('toString'); // false
    ```    