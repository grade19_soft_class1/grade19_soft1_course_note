# 2020.11.19 第三次课堂笔记

## 一·回顾C#————递归，嵌套类，partial分布类，DateTime类，正则表达式等等。

### 1.递归
    1).方法调用自身的形式，让程序代码循环执行
    2).常用与推算阶乘，斐波那契数列等等。。
    

 #### 例：计算 10 的阶乘
![10的阶乘](./img/2020.11.19/1.png)


### 2.嵌套类
    1).在一个类的内部中再定义一个类，内部的类被称为嵌套类
    2).嵌套类相当于类中的成员，能使用类成员的访问修饰符和修饰符
    3).在访问嵌套类中的成员时必须加上外层类的名称



### 3.partial分布类
    1).同一个类分布在不同文件名下，便于修改，一般用于大工程
    2).引用这个分布类时，所有的分布类成员都可以响应号召




### 4.DateTime类
    1). DateTime 类获取当前时间


#### 例：
![DateTime方法的用法](./img/2020.11.19/2.png)
![DateTime方法的用法](./img/2020.11.19/3.png)


### 5.正则表达式
    1).正则表达式是用来检验和操作字符串的工具
    2).字符匹配语法：
![](./img/2020.11.19/4.png)
    3).重复匹配语法：
![](./img/2020.11.19/5.png)
    4).字符定位语法：
![](./img/2020.11.19/6.png)