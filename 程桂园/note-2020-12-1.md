# 今日份课堂笔记
## 练习题的一些其它算法
        
    练习：在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：
```
    'use strict';
    var arr = ['小明', '小红', '大军', '阿黄'];
    console.log('???');
```

    代码如下：
```
    'use strict'
    var arr = ['小明', '小红', '大军', '阿黄']; 
    //第一种算法
    // var sum ='欢迎';
    // for(var i=0;i<arr.length;i++){
    //     if (i==arr.length-1){
    //         sum +=arr[i] +'同学！';
    //     }else if(i ==arr.length-2){
    //         sum += arr[i] +'和';
    //     }else{
    //         sum += arr[i ]+',';
    //     }
    // }
    // console.log(sum);
    /*第二种算法
    if (arr.length <= 1){
        console.log('欢迎'+arr[arr.length-1]+'同学！');
    }
    else{
        console.log('欢迎'+arr.slice(0,arr.length-1)+'同学'+'和'+arr[arr.length-1]+'同学！');
    }*/
    //第三种算法
    if (arr.length==1){
        console.log('欢迎'+arr[arr.length-1]+'同学！');
    }else{
        var sum ='欢迎';
        for(var i =0;i <arr.length;i++){
            if(i <arr.length-2){
                sum += arr[i]+',';
            }
            if(i ==arr.length-2){
                sum += arr[i];
            }
        }
        sum += '和'+arr[arr.length-1]+'同学！';
    }
    console.log(sum);
```
## 循环
    当计算机需要算成千上万个数时，我们就需要用到循环的方法。。
    for循环最常用的方法就是用索引来遍历数组。。
### for... in
    for...in 循环是for循环的一个变体，可以把一个对象的所以属性都依次循环出来。。
    例如：
```
    'use strict'
    var stu ={
        name :'韦付佳',
        age :21,
        birth: 1999,
        city :'Beijing'
    }
    for (var o in stu){
        console.log(o);
        console.log(stu[o]);
    }
```
    注意： for...in 对Array的循环得到的是 String而不是Number。
### while 
    while循环会在指定条件为真时循环执行代码块。。
    while循环是一个条件判断，条件满足时不断循环，条件不满足时，则退出当前循环。。
    例如：
```
    'use strict'
    var x=0;
    var n =99;
    while (n>0){
        x =x+n ;
        n =n-2;
    }
    console.log(x);
```

    在循环时n不断自减，直到变为-1时，将不再满足循环条件，则会退出循环。。
### do... while
    do...while 是while 循环的一种变体。它和while循环唯一的区别在于，它不是在每次循环开始时判断，而是在循环完成时判断条件。。
    例如：
```
    'use strict'
    var i =0;
    do{
        i =i+1;
    }
    while(i<60);
    console.log(i);
```
    用do...while 循环时要注意，循环至少会执行一次，但是for 和while循环可能会一次都不执行。。
### 练习题

    请利用循环遍历数组中的每个名字，并显示Hello, xxx!：
    ```
    'use strict';
    var arr = ['Bart', 'Lisa', 'Adam'];
    for ...
    ```
    
    请尝试for循环和while循环，并以正序、倒序两种方式遍历。
```
  //for循环
    'use strict'
    var arr = ['Bart', 'Lisa', 'Adam'];
    // arr.sort();//正序
    arr.reverse();//倒序
    for(var i in arr){
        console.log('Hello,'+arr[i]+'!');
    }
```
```
    //while循环
    'use strict'
    var arr = ['Bart', 'Lisa', 'Adam'];
    var i=0 ;
    arr.sort();//正序
    arr.reverse();//倒序
    while(i<arr.length){
        console.log('Hello,'+arr[i]+'!');
        i +=1;
    }
```