# 今日份课堂笔记
## Array
对于数组，除了map()、reduce、filter()、sort()这些方法可以传入一个函数外，Array对象还提供了很多非常实用的高阶函数。
### every
every()方法用于检测数组所有元素是否都符合指定条件（通过函数提供）。

every()方法使用指定函数检测数组中的所有元素.
* 如果数组中检测到有一个元素不满足，则整个表达式返回false，且剩下的元素不会再进行检测。
* 如果所有元素都满足，则返回true。
注意：every()不会对空数组进行检测，也不会改变原始数组。
every()方法可以判断数组中的元素是否满足条件。
```
'use strict'
var arr=['apple','Pear','orange'];
console.log(arr.every(function(s){
    return s.length>0;
}));
console.log(arr.every(function (s){
    return s.localeCompare() ===s;
}));
```
### find
find()方法返回通过测试（函数内判断）的数组的第一个元素的值。

find()方法为数组中的每个元素都调用一次函数执行。
* 当数组中的元素在测试条件时返回true时，find()返回符合条件的元素，之后的值不会再调用执行。
* 如果没有符合条件的元素返回undefined。
注意：find()对于空数组，函数不会执行；没有改变数组的原始值。

find()方法返回数组中符合测试函数条件的第一个元素。否则返回undefined。
```
'use strict';
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.find(function (s) {
    return s.toLowerCase() === s;
})); 
console.log(arr.find(function (s) {
    return s.toUpperCase() === s;
})); 
```
### findIndex
findIndex()方法返回传入一个测试条件（函数）符合条件的数组第一个元素位置。

findIndex()方法为数组中的每个元素都调用一次函数执行。
* 当数组中的元素在测试条件时返回true时，findIndex()返回符合条件的元素的索引位置，之后的值不会再调用执行函数。
* 如果没有符合条件的元素返回-1.
findIndex()函数也是查找目标元素，找到就返回元素的位置，找不到就返回-1

注意：findIndex()对于空数组，函数不会执行；没有改变数组的原始值。
```
'use strict';
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.findIndex(function (s) {
    return s.toLowerCase() === s;
})); 
console.log(arr.findIndex(function (s) {
    return s.toUpperCase() === s;
})); 
```
### forEach
forEach() 方法用于调用数组的每个元素，并将元素传递给回调函数。

注意: forEach() 对于空数组是不会执行回调函数的。

forEach()和map()的用法相似，都可以遍历到数组的每个元素且参数一致。

不同点：
* forEach()方法对数组的每个元素执行一次提供的函数。总是返回undefined。
* map()方法创建一个新数组，其结果是该数组中的每个元素都调用一个提供的函数后返回的结果。返回值是一个新的数组。
```
'use strict';
var arr = ['Apple', 'pear', 'orange'];
arr.forEach(console.log);
```
## 闭包
### 函数作为返回值
高阶函数除了可以接受函数作为参数外，还可以把函数作为结果值返回。
```
'use strict';
let arr =[1,10,100,1000,10000];
function sum(arr){
    return arr.reduce(function(x,y){
        return x+y;
    });
}
console.log(sum(arr));
```
```
'use strict';
let arr =[1,10,100,1000,10000];
function lazy_sum(arr){
    function sum(){
        return arr.reduce(function(x,y){
            return x+y;
        });
    }
}
let a1=lazy_sum(arr);
let a2 =lazy_sum(arr);
console.log(a1===a2);
console.log(a1);
console.log(a2);
```
在上述代码中a1与a2的调用结果互不影响。
### 闭包
1. 当内部函数在定义他的作用域的外部被引用时，就创建了该内部函数的闭包，如果内部函数引用了位于外部函数的变量，当外部函数调用完毕后，这些变量在内存不会被释放，因为闭包需要它们。
2. 闭包的最大用处：
* 可以读取函数内不得变量。
* 让这些变量始终保持在内存中，即闭包可以使得它诞生环境一直存在。

闭包的内存消耗很大，所以不能滥用闭包，否则会造成网页的性能问题。

相关代码如下：
```
'use strict';
function count (){
    var arr=[];
    for(var i =1;i<=3;i++){
        arr.push(function (){
            return i * i;
        });
    }
    return arr;
}
var res =count();
console.log(res);

var a1 =res[0];
var a2 =res[1];
var a3 =res[2];
console.log(a1());
console.log(a2());
console.log(a3());
```