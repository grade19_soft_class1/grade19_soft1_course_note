# 今日份课堂笔记
## Redis
Redis是一种内存数据库，并且还支持集群、分布式、主从同步等配置，原则上可以无限延展还支持一定的事务能力。这保证了高并发的场景下的数据的安全性和一致性。
## Map和Set
Map和Set是ES6标准新增的数据类型
### Map
Map 是一组键值对的结构具有极快的查找速度。
```
    'use strict'
    var name =['Mary','Bob','Tracy'];
    var scores =[81,68,95];
    var m =new Map([['Mary',81],['Bob',68],['Tracy',95]]);
    console.log(m.get('Bob'));
```
由于一个key对应一个value,如果对一个key放入多个value那么前面的值会被后面的值覆盖掉。

例如：
```
    'use strict'
    var name =['Mary','Bob','Tracy'];
    var scores =[81,68,95];
    var m =new Map([['Mary',81],['Bob',68],['Tracy',95]]);
    m.set('Bob',99);
    m.set('Bob',25);
    console.log(m.get('Bob'));//最终结果为25
```
### Set
1. Set 也是一组key的集合，但是它不储存value，在Set中没有重复的key。
```
    'use strict'
    var s1 =new Set();
    var s2 =new Set([1,5,63,98]);
    console.log(s1);
    console.log(s2);//{1, 5, 63, 98}
```
2. 在Set中，重复的元素会被过滤掉。。
```
    'use strict'
    var s =new Set([1,2,3,3,4,4,5,5,5,5]);
    console.log(s);//{1, 2, 3, 4, 5},重复的3,4,5都被过滤掉了。
```
3. 重复添加的元素不会有效果。。（add(key)的方法可以添加元素）
```
    'use strict'
    var s =new Set([1,2,3]);
    s.add(4);// {1, 2, 3, 4}
    s.add(4);//无效。
    console.log(s);
```
4. 通过delete(key)方法可以删除元素。
```
    'use strict'
    var s =new Set([1,2,3,4,999]);
    s.delete(999);
    s.delete(999);
    console.log(s);//{1, 2, 3, 4},重复删除无效。
```
## iterable
遍历数组可以使用下标循环，然而Map和Set确不可以使用下标。为了统一集合类型，ES6标准引入了新的iterable类型，Array、Map和Set都属于iterable类型。
for...of循环
```
    'use strict'
    var arr =[1,2,3,4];
    var name ='谁诀别相思成疾';
    for(var el of arr){
        console.log(el);//[1,2,3,4]
    }
```
### for...in循环和for...of循环的区别
for ... in循环将把name包括在内，但Array的length属性却不包括在内。
for ... of循环则是只循环集合本身的元素。
```
    'use strict'
    var arr =[1,2,3,4];
    var name ='谁诀别相思成疾';
    for(var el of arr){
        console.log(el);//[1,2,3,4]
    }
    for(var el in arr){
        console.log(el);//[0,1,2,3]下标
    }
```
更好的方法是直接使用iterable内置的ForEach方法。它接收一个函数，每次迭代就自动回调该函数。
```
    'use strict'
    var arr =[1,2,3,4];
    var name ='谁诀别相思成疾';
    arr.forEach((el ,index,arr) => {
        console.log(el);//指向当前元素的值
        console.log(index);//指向当前索引
        console.log(arr);//指向数组对象本身
    });
```
注意：forEach()方法是ES5.1标准引入的
```
    'use strict'
    var arr =[1,2,3,4];
    var m =new Map();
    m.set('name','方法');
    m.set('name1','方法1');
    m.set('name2','方法2');
    m.set('name3','方法3');
    var s =new Set(['a','b','c','d']);
    var name ='谁诀别相思成疾';
    arr.forEach((el ,index,arr) => {
        console.log(el);
        console.log(index);
        console.log(arr);
    });
    m.forEach((el,index,arr)=>{
        console.log(el);
        console.log(index);
        console.log(arr);
    });
    s.forEach((el,index,arr)=>{
        console.log(el);
        console.log(index);
        console.log(arr);
    });
```
## 函数
JavaScript 使用关键字 function 定义函数。函数可以通过声明定义，也可以是一个表达式。

JavaScript 函数是被设计为执行特定任务的代码块。

JavaScript 函数会在某代码调用它时被执行。

分号是用来分隔JavaScript语句，由于函数声明不是一个可执行语句，所以不用分号分隔。

注意：JavaScript 对大小写敏感。关键词 function 必须是小写的，并且必须以与函数名称相同的大小写来调用函数。

注意：函数体内部的语句在执行时，一旦执行到return时，函数就执行完毕，并将结果返回。因此，函数内部通过条件判断和循环可以实现非常复杂的逻辑。如果没有return语句，函数执行完毕后也会返回结果，只是结果为undefined。

例如：
```
    'use strict'
    function abs(x){
        if(x >=0){
            return x;
        }
        else{
            -x;
        }
    }
    console.log(abs(0));//0
    console.log(abs(-1));//由于没有用return，返回undefined
```
如果如果传入的参数比定义少，函数的参数将收到undefined，计算结果为NaN。
```
    'use strict'
    function abs(x){
        if(x >=0){
            return x;
        }
        else{
        return -x;
        }
    }

    console.log(abs());NaN
```
由于JavaScript允许传入任意个参数而不影响调用，因此传入的参数比定义的参数多也没有问题。

例如：
```
    'use strict'
    function abs(x){
        if(x >=0){
            return x;
        }
        else{
        return -x;
        }
    }
    console.log(abs(3,'dffdgtr','tycdsrty'));//返回3
    console.log(abs(-5,'fggfd','jiuiki'));//返回5
```