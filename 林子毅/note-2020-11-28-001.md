# 第八次课堂笔记（JavaScript）

### 今天讲解了上次课的课后练习题（数组的）

### 今天学了对象、条件判断

## JavaScript的对象是一种无序的集合数据类型，它由若干键值对组成

## 下面是练习题
### 1.题目描述

    小明身高1.75，体重80.5kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数：
    低于18.5：过轻
    18.5-25：正常
    25-28：过重
    28-32：肥胖
    高于32：严重肥胖
    用if...else...判断并显示结果：
<details>
    <summary>代码程序</summary>
    <pre>
        <code> 
            var height = parseFloat(prompt('请输入身高(m):'));
            var weight = parseFloat(prompt('请输入体重(kg):'));
            var bmi = weight/(Math.pow(height,2));
            if(bmi < 18.5){
                console.log('过轻');
            }else if(bmi >=18.5 && bmi < 25){
                console.log('正常');
            }else if(bmi >= 25 && bmi < 28){
                console.log('过重');
            else if(bmi >=28 &&　bmi < 32){
                console.log('肥胖');
            }else if(bmi >= 32){
                console.log('严重肥胖');
            }
        </code>
    </pre>
</details>

### 2.题目描述

    在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：
<details>
    <summary>代码程序</summary>
    <pre>
        <code>
            var arr = ['小明','小红','小军','阿黄'];
            console.log('欢迎'+ arr.slice(0,arr.length-1).join(',') + '和' + arr[arr.length-1] + '同学！');
        </code>
    </pre>
</details>