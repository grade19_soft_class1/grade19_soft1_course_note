# 高阶函数(续)

## every
every()方法可以判断数组的**所有元素**是否满足测试条件  
例：
![出错了](./img/HOF.PNG)
***

## find
find()方法用于查找符合条件的第一个元素，如果找到了，返回这个元素，否则，返回undefined
例： 
![出错了](./img/HOF2.PNG)
***

## findIndex
findIndex()和find()类似，也是查找符合条件的第一个元素，不同之处在于findIndex()会返回这个元素的索引，如果没有找到，返回-1  
例：
![出错了](./img/HOF3.PNG)
***

## forEach
forEach()常用于遍历数组，因此，传入的函数不需要返回值
例：
![出错了](./img/HOF4.PNG)
***