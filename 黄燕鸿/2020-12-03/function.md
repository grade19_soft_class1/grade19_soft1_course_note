# 函数
今天的函数是昨天的延续，就像我们现在活着，是为了以后或者。

## arguments
JS里有一个关键字\[arguments]，它的作用是指向当前函数的调用者传入的所有参数，类似于一个数组。  
也就是说，即使函数不定义任何参数，还是可以拿到参数的值。例如：
```
function abs() {
    if (arguments.length === 0) {
        return 0;
    }
    var x = arguments[0];
    return x >= 0 ? x : -x;
}

abs(); // 0
abs(10); // 10
abs(-9); // 9
```

## rest参数
虽然说JS允许我们传入无数个参数，但有些时候，我们只需要其中的若干个参数，其他的都是多余的。  
那么我们如何把多余的参数归为一坨呢?答案是rest参数。  

```
function foo(a, b, ...rest) {
    console.log('a = ' + a);
    console.log('b = ' + b);
    console.log(rest);
}
foo(1, 2, 3, 4, 5);
```
如上述代码，rest参数只能写在最后，前面用...标识，从运行结果可知，传入的参数先绑定a、b，多余的参数以数组形式交给变量rest。  
而如果传入的参数连正常定义的参数都没填满的话，rest参数会自动接受一个**空数组**。

## return语句
在JS中，return语句的规则与C#等的规则不一样，JS有时不会将某些代码自动格式化。例如：
```
function foo() {
    return
        { name: 'foo' };
}
foo();
```
如果是在C#中，上述代码并不会报错，但在JS中，虽然也不会报错，但它显示的foo()的结果却是undefined。  
这显然不符合我们的想法。为什么会这样？这是因为，在JS引擎有一个在行末自动添加分号的机制。  
所以上述代码实际上是这个亚子的：
```
function foo() {
    return ;
        { name: 'foo' };
}
foo();
```
显而易见，由于JS的机制，return后面直接带；啥也没有，自然是undefined了。  
所以当我们要return多行代码时，应该这样子写：
```
function foo() {
    return { 
        name: 'foo'
    };
}
foo();
```
把{放在return的后面，表示语句尚未结束