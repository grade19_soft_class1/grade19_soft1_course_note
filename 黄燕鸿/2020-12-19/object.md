# 标准对象

## typeof
它会获取对象的类型，它总是返回一个字符串
例：
![出错了](./img/object.PNG)
***

## 包装对象
在JS中，每种类型都区分它本身的类型和它的包装类型
例：
![出错了](./img/object2.PNG)
***
看起来就和C#的new一个对象一模一样  
虽然包装对象看上去和原来的值一模一样，显示出来也是一模一样，但他们的类型已经变为object了！所以，包装对象和原始值用===比较会返回false：
![出错了](./img/object3.PNG)
***
所以不要随意使用包装类型！不要随意使用！不要随意使用！

总结一下，有这么几条规则需要遵守：

- 不要使用new Number()、new Boolean()、new String()创建包装对象；

- 用parseInt()或parseFloat()来转换任意类型到number；

- 用String()来转换任意类型到string，或者直接调用某个对象的toString()方法；

- 通常不必把任意类型转换为boolean再判断，因为可以直接写if (myVar) {...}；

- typeof操作符可以判断出number、boolean、string、function和undefined；

- 判断Array要使用Array.isArray(arr)；

- 判断null请使用myVar === null；

- 判断某个全局变量是否存在用typeof window.myVar === 'undefined'；

- 函数内部判断某个变量是否存在用typeof myVar === 'undefined'。