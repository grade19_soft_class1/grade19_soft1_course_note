# generator

generator的意思是生成器  
最大特点就是可以交出函数的执行权（即暂停执行）。一个generator看上去像一个函数,区别就是函数名前面多了一个星号 *，但可以返回多次。与yield命令配合，可以实现暂停执行的功能

## 基本用法

![出错了](./img/generator.PNG)
![出错了](./img/generator2.PNG)
***

  
## next方法

1. 遇到yield语句，就暂停执行后面的操作，并将紧跟在yield后面的那个表达式的值，作为返回的对象的value属性的值。

2. 再次调用next方法时，再继续往下执行，直到遇到下一个yield语句。

3. 如果没有再遇到新的yield语句，就一直运行到函数结束，直到return语句为止，并将return语句后面的表达式的值，作为返回的对象的value属性值。

4. 如果该函数没有return语句，则返回的对象的value属性值为undefined。

5. next方法可以有参数, next方法参数的作用，是覆盖掉上一个yield语句的值。
![出错了](./img/generator3.PNG)
***
