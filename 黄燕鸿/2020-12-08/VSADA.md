# 变量作用域与解构赋值(续)

## 常量
如果要申明一个常量，那就要用关键字const来定义常量。例：const a=1;

## 解构赋值
意思是可以同时对一组变量进行赋值。这没啥好解释的上图：
![出错了](./VSADA.PNG)
***
有些时候，如果变量已经被声明了，再次赋值的时候，正确的写法也会报语法错误:
![出错了](./VSADA2.PNG)

这是因为JS把{开头的语句当作了块处理，于是=不再合法。解决方法是用小括号括起来：
![出错了](./VSADA3.PNG)
***

快速获取当前页面的域名和路径:
```
var {hostname:domain, pathname:path} = location;
```
