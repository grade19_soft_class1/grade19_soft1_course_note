# 方法
在一个对象中绑定函数，称为这个对象的方法。

## this
![出错了](./method.PNG)
***
由上图可以看到，对象laohu里有一个age方法，和普通函数也没啥区别，但是它在内部使用了一个this关键字  
那这个this是什么呢？  
在一个方法内部，this是一个特殊变量，它始终指向当前对象  
也就是说上图的this指向的是laohu这个对象  
现在我们把方法提取出来：
![出错了](./method2.PNG)
***
可以看出，这两种形式的方法都不会报错，实际上逻辑上也没有错误。  
但奇怪的是，调用两种形式的方法时，结果却是不一样的： 
***
![出错了](./method3.PNG)
***
报错显示birth找不到？这是为什么？birth不是在对象laohu里么？如果这个this没有指向laohu，那又指向谁了？  
实际上，this关键字在JS里有自己的一套逻辑。  
如果以对象的方法形式调用，比如laohu.age()，该函数的this指向被调用的对象，也就是laohu，这是符合我们预期的。  
但如果单独调用函数，比如getAge()，此时，该函数的this指向全局对象，也就是window。  
所以才导致调用报错，因为在我们的window，也就是全局变量里并没有定义birth，所以此时的this指向的是undefined  
所以这就是this的神奇之处，在运用时要注意它正确的指向  

那既然这样，那我就在对象里面使用this不就不会报错了呗~  
答案是：视情况而定  
正所谓万事无绝对，来看下面的代码：
![出错了](./method4.PNG)
***
上述代码咋一看是没什么问题，this也在对象里，所以这个this会指向laohu，也没有问题，完美~ 

但……你以为你以为的就是你以为的啊  
实际上此时的this指向的还是undefined 

这是啥情况？？？  

其实，this只在对象的内部方法里才会指向对象，但如果this的位置在对象的内部的方法里的一个嵌套方法，this又指向undefined了  
也就是说，this只会指向上一层的对象，否则就会指向undefined(在非strict模式下，它重新指向全局对象window)

既然这样的话岂不是不能用嵌套方法了？答案是可以的，不过要做一些"手脚"：
![出错了](./method5.PNG)
***
`var that = this`的意义就在于在方法内部一开始就捕获this  
这样的话你就可以放心地在方法内部定义其他函数，而不是把所有语句都堆到一个方法中

## apply和call
就算我们用捕获的方法使得this可以在对象内随意使用了  
但如果我想像图2一样把方法提取出来怎么办？
这时候捕获的方法就行不通了  
所以我们就要用到apply和call这两个方法了

apply方法是函数本身自带的一个方法，它接收两个参数，第一个参数就是需要绑定的this变量，第二个参数是Array，表示函数本身的参数.apply方法可以指定函数的this指向哪个对象。例：
![出错了](./method6.PNG)
***

另一个与apply()类似的方法是call()，唯一区别是：

- apply()把参数打包成Array再传入；
- call()把参数按顺序传入。
例：
![出错了](./method7.PNG)
*** 

对普通函数调用，我们通常把this绑定为null。例：
``` 
//查找最大数
Math.max.apply(null, [3, 5, 4]);
Math.max.call(null, 3, 5, 4); 
```

## 装饰器
不是很懂，照搬

利用apply()，我们还可以动态改变函数的行为。

JavaScript的所有对象都是动态的，即使内置的函数，我们也可以重新指向新的函数。

现在假定我们想统计一下代码一共调用了多少次parseInt()，可以把所有的调用都找出来，然后手动加上count += 1，不过这样做太傻了。最佳方案是用我们自己的函数替换掉默认的parseInt()：
```
'use strict';

var count = 0;
var oldParseInt = parseInt; // 保存原函数

window.parseInt = function () {
    count += 1;
    return oldParseInt.apply(null, arguments); // 调用原函数
};
// 测试:
parseInt('10');
parseInt('20');
parseInt('30');
console.log('count = ' + count); // 3
```
