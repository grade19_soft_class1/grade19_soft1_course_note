# iterable
遍历Array可以采用下标循环，遍历Map和Set就无法使用下标。为了统一集合类型，ES6标准引入了新的iterable类型，Array、Map和Set都属于iterable类型。

具有iterable类型的集合可以通过新的for ... of循环来遍历。

## for of循环
此循环可以对Map和Set进行遍历。
### 例：
![出错了](./iterable.PNG)
***

## foreach方法
foreach是iterable内置的一种遍历数组的方法。
### 例：
![出错了](./iterable2.PNG)
*** 
一般来说此方法最多三个参数，但也可以通过重载来改变方法体。