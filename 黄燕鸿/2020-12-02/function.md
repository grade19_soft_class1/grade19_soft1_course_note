# 函数
就……函数嘛，dddd。  

## 定义方式 
在JavaScript中，定义函数的方式如下：
```
function abs(x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
}
```
还有一种定义方式就是用匿名函数来定义：
```
var abs = function (x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
};
```
要知道的是，由于JavaScript允许传入任意个参数而不影响调用，因此传入的参数比定义的参数多或少它都不会报错。

# 抽象
其实就是一种数学符号，例如：  
$$
\sum_{n=1}^{100}
$$  
这里的话就可以参考一下资料了：
[常用数学符号语法](https://blog.csdn.net/LB_yifeng/article/details/83302697)