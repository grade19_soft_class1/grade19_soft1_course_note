# 数组
## 今天我们(其实是老胡)来讲讲数组的玩法
JS里的数组不同于C#里的数组，JS里的数组可以包含任意的数据类型，且可以自由变换长度

## 获取数组的长度
和C#一样，用.length访问

## 对数组赋值
在JS中，可直接对数组赋值。
### 例：
![出错了](./img/Array.PNG)
***
也可以直接改变数组的长度。例如：a.length=10;

## indexOf()
搜索一个指定的元素的位置。
### 例：
![出错了](./img/Array2.PNG)
***

## slice
截取数组的部分元素，然后返回一个新的数组。例如：arr.slice(0,3)  
意为截取arr里索引从0到3的元素，但不包括3.若是不传参数，则复制整个数组。

## push和pop
push()向Array的末尾添加若干元素，pop()则把Array的最后一个元素删除掉。

## unshift和shift
unshift()向Array的头部添加若干元素，shift()则把Array的第一个元素删除掉。

## sort
sort()可以对当前Array进行排序，它会直接修改当前Array的元素位置。  
直接调用时，按照默认顺序排序。
### 例：
![出错了](./img/Array3.PNG)
***

## reverse
将数组元素反转
### 例：
![出错了](./img/Array4.PNG)
***

## splice
它可以从指定的索引开始删除若干元素，然后再从该位置添加若干元素,参数非常多。
### 例：
![出错了](./img/Array5.PNG)
***

## concat
连接两个数组

## join
把当前Array的每个元素都用指定的字符串连接起来，返回一个字符串。
### 例：
![出错了](./img/Array6.PNG)
***

## 多维数组
即多维数组(**[\[],[\]]**)

# 写完笔记该去写题了