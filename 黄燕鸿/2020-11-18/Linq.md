# LINQ表达式
## 定义
LINQ全称是语言集成查询。它允许以查询数据库的方式操作内存数据。

例：
```
sql语法:select * from Users where name='老胡';
Linq语法:from find in Users where find.name='老胡'select find
Lambda表达式:User.where(find=>find.name=='老胡')
```