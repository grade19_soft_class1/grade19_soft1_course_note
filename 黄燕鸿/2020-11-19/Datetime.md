# 时间
在C#中，关于时间的用法很多，下面列举一些常用的用法和语法：

1.把用户输入的日期字符串转换为日期格式类型  
`DateTime dateTime = DateTime.Parse(Console.ReadLine());`  
2.获取当前时间
![出错了](./img/Datetime.PNG)
***
还有很多方法这里就不一一列举了，要不然写几天都写不完，用的时候再查就行了