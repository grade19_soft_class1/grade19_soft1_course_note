# 如何解决git合并所形成的冲突
一般的冲突来自于git pull 和 git push的时候。

## 当pull出现冲突时
   1.先用 git stash 将本地文件备份入Git栈中
   2.然后git pull
   3.最后git stash pop 将Git栈中的备份恢复
   ### 
## 当push出现冲突时
 1. 可以用 git reset --hard **** 来放弃本次代码的修改，  
 这个****是你提交记录的编码的前六位，
 可以用 git log 查看。
 2. 可以用git reset --soft head^ 来回到上一次提交 ^^是回到上两次提交
 # 再不行就关机重启！重新写！