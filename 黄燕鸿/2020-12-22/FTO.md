# 面向对象
JS不同于java和C#，JS不区分类和实例的概念，而是通过原型（prototype）来实现面向对象编程

## 原型和继承
当我们想创建一个对象，但却没有类型可以引用的时候，就可以把一个现有的其它的对象当作"原型"，而后对这个原型进行继承
例：
![出错了](./FTO.PNG)
***