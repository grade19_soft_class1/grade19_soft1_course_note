# 浏览器

主流浏览器：

- IE 6~11：国内用得最多的IE浏览器，历来对W3C标准支持差。从IE10开始支持ES6标准；

- Chrome：Google出品的基于Webkit内核浏览器，内置了非常强悍的JavaScript引擎——V8。由于Chrome一经安装就时刻保持自升级，所以不用管它的版本，最新版早就支持ES6了；

- Safari：Apple的Mac系统自带的基于Webkit内核的浏览器，从OS X 10.7 Lion自带的6.1版本开始支持ES6，目前最新的OS X 10.11 El Capitan自带的Safari版本是9.x，早已支持ES6；

- Firefox：Mozilla自己研制的Gecko内核和JavaScript引擎OdinMonkey。早期的Firefox按版本发布，后来终于聪明地学习Chrome的做法进行自升级，时刻保持最新；

# 浏览器对象

## 1. window

window对象不但充当全局作用域，而且表示浏览器窗口。

window对象有innerWidth和innerHeight属性，可以获取浏览器窗口的内部宽度和高度。内部宽高是指除去菜单栏、工具栏、边框等占位元素后，用于显示网页的净宽高。

![出错了](./browser.PNG)
***
对应的，还有一个outerWidth和outerHeight属性，可以获取浏览器窗口的整个宽高。

## 2.navigator

navigator对象表示浏览器的信息，最常用的属性包括：

![出错了](./browser2.PNG)
***

## 3. screen

screen对象表示屏幕的信息，常用的属性有：

![出错了](./browser3.PNG)
***

## 4. location

location对象表示当前页面的URL信息。  
一个完整的URL可以用location.href获取。要获得URL各个部分的值，可以这么写：
![出错了](./browser4.PNG)
***

要加载一个新页面，可以调用location.assign()。如果要重新加载当前页面，调用location.reload()方法非常方便:
![出错了](./browser5.PNG)
***

## 5. document

document对象表示当前页面。由于HTML在浏览器中以DOM形式表示为树形结构，document对象就是整个DOM树的根节点。

document的title属性是从HTML文档中的\<title\>xxx</title\>读取的，但是可以动态改变：
![出错了](./browser6.PNG)
***

要查找DOM树的某个节点，需要从document对象开始查找。最常用的查找是根据ID和Tag Name：
![出错了](./browser7.PNG)
![出错了](./browser8.PNG)
***
