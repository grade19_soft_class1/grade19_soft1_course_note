# Map
## Map是一组键值对的结构，具有极快的查找速度
初始化Map需要一个二维数组，或者直接初始化一个空Map。
## 例：
![出错了](./img/Map.PNG)
***

# Set
## Set和Map类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在Set中，没有重复的key。
要创建一个Set，需要提供一个Array作为输入，或者直接创建一个空Set。
## 例：
![出错了](./img/Set.PNG)