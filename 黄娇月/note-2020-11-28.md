## JavaScript 方法
 :fa-certificate: JavaScript 方法是能够在对象上执行的动作。  
 :fa-certificate: JavaScript 方法是包含函数定义的属性。  
 :fa-certificate: 方法是存储为对象属性的函数。
#### - Object构造函数
```
var person1=new Object();
person1.name='Alice';
person1.age=18;
person1.sayName=function(){
    console.log(this.name);
}
person1.sayName();
```
#### -  indexOf() 方法
 :fa-certificate: indexOf() 方法可返回某个指定的字符串值在字符串中首次出现的位置。  
 :tw-26a0:  indexOf() 方法区分大小写。
#### 参数值
1. searchvalue：必需。规定需检索的字符串值。    
2. start： 可选的整数参数。规定在字符串中开始检索的位置。它的合法取值是 0 到 string Object.length - 1。如省略该参数，则将从字符串的首字符开始检索。 |
#### 返回值
Number：查找指定字符串第一次出现的位置，如果没找到匹配的字符串则返回 -1。
