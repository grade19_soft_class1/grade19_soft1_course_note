## 一、方法
1、对象定义：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/103721_e87e1800_7447647.png "2020-12-10_103703.png")


2、如果我们给ll绑定一个函数，就可以做更多的事情。比如，写个age()方法，返回ll的年龄：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/103911_4a58c9ae_7447647.png "2020-12-10_103852.png")
## 二、绑定到对象上的函数称为方法，和普通函数也没啥区别，但是它在内部使用了一个this关键字，这个东东是什么？

在一个方法内部，this是一个特殊变量，它始终指向当前对象，也就是xiaoming这个变量。所以，this.birth可以拿到xiaoming的birth属性。
1、拆开写：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/104503_3df6393c_7447647.png "2020-12-10_104447.png")

*单独调用函数getAge()[发生报错]：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/104620_dad3a520_7447647.png "2020-12-10_104407.png")

*如果调用了this，那么这个this到底指向谁？

答案是，视情况而定！

2、如果这么写

var fn = ll.age; // 先拿到ll的age函数

fn(); // NaN

也是不行的！要保证this指向正确，必须用 obj.xxx()的形式调用！


由于这是一个巨大的设计错误，要想纠正可没那么简单。ECMA决定，在strict模式下让函数的this指向undefined

![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/110759_c0b9d8e4_7447647.png "2020-12-10_110747.png")

这个只是让错误及时暴露出来，并没有解决this应该指向的正确位置。

所以重构一下

![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/111000_348ad829_7447647.png "2020-12-10_110942.png")

结果又报错了！原因是this指针只在age方法的函数内指向ll，在函数内部定义的函数，this又指向undefined了！（在非strict模式下，它重新指向全局对象window！）

3、修复办法，用一个that变量首先捕获this：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/111337_18da1a63_7447647.png "2020-12-10_111324.png")
## 三、Apply

虽然在一个独立的函数调用中，根据是否是strict模式，this指向undefined或window，不过，我们还是可以控制this的指向的！

要指定函数的this指向哪个对象，可以用函数本身的apply方法，它接收两个参数，第一个参数就是需要绑定的this变量，第二个参数是Array，表示函数本身的参数。

用apply修复getAge()调用：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/111813_4509d9fe_7447647.png "2020-12-10_111757.png")

## 四、装饰器

利用apply()，我们还可以动态改变函数的行为

JavaScript的所有对象都是动态的，即使内置的函数，我们也可以重新指向新的函数。

现在假定我们想统计一下代码一共调用了多少次parseInt()，可以把所有的调用都找出来，然后手动加上count += 1，不过这样做太傻了。最佳方案是用我们自己的函数替换掉默认的parseInt()：