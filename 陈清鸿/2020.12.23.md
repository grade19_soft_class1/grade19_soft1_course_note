## 一、面向对象编程

程序中面向对象的基本体现

在 JavaScript 中，所有数据类型都可以视为对象，当然也可以自定义对象。 自定义的对象数据类型就是面向对象中的类（ Class ）的概念。


假设我们要处理学生的成绩表，为了表示一个学生的成绩，面向过程的程序可以用一个对象表示：


 var std1 = { name: 'Michael', score: 98 }

 var std2 = { name: 'Bob', score: 81 }

而处理学生成绩可以通过函数实现，比如打印学生的成绩：


 function printScore (student) {

   console.log('姓名：' + student.name + '  ' + '成绩：' + student.score)
    
 }

如果采用面向对象的程序设计思想，我们首选思考的不是程序的执行流程， 而是 Student 这种数据类型应该被视为一个对象，这个对象拥有 name 和 score 这两个属性（Property）。
 如果要打印一个学生的成绩，首先必须创建出这个学生对应的对象，然后，给对象发一个 printScore 消息，让对象自己把自己的数据打印出来。


抽象数据行为模板（Class）：


 function Student(name, score) {

   this.name = name;

   this.score = score;

   this.printScore = function() {

     console.log('姓名：' + this.name + '  ' + '成绩：' + this.score);
    
   }

 }


 var std1 = new Student('Michael', 98)

 var std2 = new Student('Bob', 81)

实例对象具有自己的具体行为（给对象发消息）：


 std1.printScore() // => 姓名：Michael  成绩：98

 std2.printScore() // => 姓名：Bob  成绩 81



所以，面向对象的设计思想是：


抽象出 Class(构造函数)


根据 Class(构造函数) 创建 Instance


指挥 Instance 得结果


面向对象的抽象程度又比函数要高，因为一个 Class 既包含数据，又包含操作数据的方法。


## 二、构造函数




1.构造函数语法


2.分析构造函数


3.构造函数和实例对象的关系


4.实例的 constructor 属性


5.instanceof 操作符


6.普通函数调用和构造函数调用的区别


7.构造函数的返回值


8.构造函数的问题

## 构造函数：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1223/111846_6ab9a3b0_7447647.png "2020-12-23_111630.png")