## 小日记 
今天是还是想吃清汤粉的一天 （我的宝贝带我去吃了 :kissing_smiling_eyes: ，开心 :dancer:  :dancer: ）

收到一个快递的信息  :scream:  有点小惊讶 因为我没有买东西 （剁手中。。。。）

吃完饭之后去领了这个不知名包裹。。。 是根口红嘿嘿嘿 问了一下，是我的美姿姿给我的生日礼物。。。她果然做什么都挣第一哈哈哈哈。说巧不巧，上课走神的时候我在想要给她个什么惊喜当圣诞礼物 :evergreen_tree:  :evergreen_tree:  :evergreen_tree: 。。。今天就收到了她给我的惊喜嘻嘻.....



## 学习笔记（今日正题）
# 一、字符串
1、字符串可以存储一系列字符，如 "John Doe"。

字符串可以是插入到引号中的任何字符。


2、实例

var answer = "It's alright";

var answer = "He is called 'Johnny'";

var answer = 'He is called "Johnny"';

3、字符串添加转义字符来使用引号：

![操作](https://images.gitee.com/uploads/images/2020/1126/105830_c956bf24_7447647.png "2020-11-26_105145.png")

4、特殊字符：

| 代码 | 输出       |
|----|----------|
| \' | 单引号      |
| \" | 双引号      |
| \\ | 反斜杠      |
| \n | 换行       |
| \r | 回车       |
| \t | tab(制表符) |
| \b | 退格符      |
| \f | 换页符      |

# 二、字符串可以是对象

var x = "John";              // x是一个字符串

var y = new String("John");  // y是一个对象

（ :exclamation: 不要创建 String 对象。它会拖慢执行速度，并可能产生其他副作用：）

var x = "John";  
           
var y = new String("John");

(x === y) // 结果为 false，因为 x 是字符串，y 是对象
# 三、全局变量
声明方式一：
使用var（关键字）+变量名(标识符)的方式在function外部声明，即为全局变量，否则在function声明的是局部变量。该方式即为显式声明详细如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/111352_4cedab68_7447647.png "2020-11-26_111316.png")



